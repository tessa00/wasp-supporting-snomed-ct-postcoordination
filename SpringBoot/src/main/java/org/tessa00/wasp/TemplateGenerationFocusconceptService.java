package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;

@Service
public class TemplateGenerationFocusconceptService {

    private final HttpClientService httpClient;

    public TemplateGenerationFocusconceptService(HttpClientService httpClientService) {
        this.httpClient = httpClientService;
    }

    private boolean lookUpDefinitionStatus(String url, String code) {
        String urlNew = url + "/CodeSystem/$lookup?system=http://snomed.info/sct&code=" + code + "&property=sufficientlyDefined";
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                JSONArray parameter = (JSONArray) json.get("parameter");
                for (int j = 0; j < parameter.length(); j++) {
                    json = (JSONObject) parameter.get(j);
                    if(json.has("part")) {
                        JSONArray part = (JSONArray) json.get("part");
                        json = (JSONObject) part.get(1);
                        Boolean result = json.getBoolean("valueBoolean");
                        return result;
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return true;
    }

    private ArrayList<String> eclRequest(String url, String expression) {
        String urlNew = url + "/ValueSet/$expand?url=" +
                "http://snomed.info/sct?fhir_vs=ecl/" + URLEncoder.encode(expression, StandardCharsets.UTF_8);

        ArrayList<String> arrayList = new ArrayList<>();

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");

            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);
                JSONObject expansion = (JSONObject) json.get("expansion");
                JSONArray contains = (JSONArray) expansion.get("contains");

                for (int k = 0; k < contains.length(); k++) {
                    JSONObject jsonContains = (JSONObject) contains.get(k);
                    String code = jsonContains.getString("code");
                    String display = jsonContains.getString("display");
                    arrayList.add(code);
                }
            }
        } catch (Exception e) {
        }

        return arrayList;
    }
}
