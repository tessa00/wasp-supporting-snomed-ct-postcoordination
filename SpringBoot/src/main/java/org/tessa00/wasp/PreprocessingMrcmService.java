package org.tessa00.wasp;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;


@Service
public class PreprocessingMrcmService {

    private final TerminologyServerRequestsService terminologyServerRequestsService;

    public PreprocessingMrcmService(TerminologyServerRequestsService terminologyServerRequestsService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
    }

    public String start(String url, String version, String mrcm) throws JSONException, IOException {
        ArrayList<ArrayList<String>> arrayList = processingInput(mrcm);

        // check if file is correct
        if (!arrayList.get(0).toString().contains("domainTemplateForPrecoordination")) {
            return "{\"result\": \"Wrong file!\"}";
        }

        String json = "{\"name\":\"MRCMDomainSnapshot\",\"conceptmodel\":[";
        ArrayList<String> headliner = arrayList.get(0);
        for(int i = 1; i < arrayList.size(); i++) {
            ArrayList<String> line = arrayList.get(i);
            assert headliner.size() == line.size();
            json = json + "{";
            for(int j = 0; j < headliner.size(); j++) {
                    String value = arrayList.get(i).get(j);
                    if(!Objects.equals(headliner.get(j), "domainTemplateForPrecoordination")) {
                        json = json + "\"" + headliner.get(j).replace("\r", "") + "\":\"" + value + "\",";
                    } else {
                        json = json + "\"" + headliner.get(j) + "\":[{\"init\":\"" + value + "\"," +
                                "\"focusconcept\":\"" + value.substring(0, value.lastIndexOf(":")).replace("[[+scg(", "").replace(" ", "").split("\\|")[0] + "\"";
                        json = preprocessingRefinement(url, version, value.substring(value.lastIndexOf(":") + 1).replace("\n", "").replace("\t", ""), json);
                        json = json + "}],";
                    }
            }
            json = json.substring(0, json.length()-1);
            json = json + "},";
        }
        json = json.substring(0, json.length()-1);
        json = json + "]}";
        try {
            try {
                JSONObject jsonObject = new JSONObject(json.replace("\r", ""));
                return jsonObject.toString();
            } catch (Exception e) {
                return "{\"result\": \"Something went wrong!\"}";
            }

        } catch (Exception ignored) {
            return "{\"result\": \"Something went wrong!\"}";
        }
    }

    private ArrayList<ArrayList<String>> processingInput(String mrcm) {
        ArrayList<ArrayList<String>> arrayListOutput = new ArrayList<>();
        for (int i = 0; i < mrcm.split("\n").length; i++) {
            String[] arr = mrcm.split("\n")[i].split("\t");
            ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(arr));
            arrayListOutput.add(arrayList);
        }
        return arrayListOutput;
    }

    private String preprocessingRefinement(String url, String version, String refinement, String json) {
        refinement = refinement.replace(" ", "").replace("\n", "");

        System.out.println("-----");
        System.out.println(refinement);

        // check if without rg exists
        if(!refinement.isEmpty()) {
            if (refinement.charAt(8) != '{') {
                if (refinement.contains("{")) {
                    String withoutRg = refinement.substring(0, refinement.indexOf("{") - 9);
                    String withRg = refinement.substring(refinement.indexOf("{") - 8);
                    json = preprocessingRefinementWithoutRg(url, version, withoutRg, json);
                    json = json.substring(0, json.length() - 1);
                    json = json + "]";
                    json = preprocessingRefinementWithRg(url, version, withRg, json);
                } else {
                    json = preprocessingRefinementWithoutRg(url, version, refinement, json);
                    json = json.substring(0, json.length() - 1);
                    json = json + "]";
                }
            } else {
                json = preprocessingRefinementWithRg(url, version, refinement, json);
            }
        }
        return json;
    }

    private String preprocessingRefinementWithoutRg(String url, String version, String withoutRg, String json) {
        json = json + ",\"withoutRoleGroups\":[" ;

        String[] attribute = withoutRg.split(",\\[\\[");

        for (int k = 0; k < attribute.length; k++) {
            if(attribute[k].length() > 0) {
                String carAttr = attribute[k].substring(0, attribute[k].indexOf("]]"));
                String minCarAttr = carAttr.split("\\.\\.")[0].replace("[[", "");
                String maxCarAttr = carAttr.split("\\.\\.")[1];
                String attr = attribute[k].substring(attribute[k].indexOf("]]")).replace("]]", "");
                String attributeNameCode = attr.split("=")[0].split("\\|")[0];
                String attributeNameName = terminologyServerRequestsService.lookUpName(url, version, attributeNameCode);
                String attributeValue = attr.split("=")[1].replace("[[+scg(", "").replace("[[+", "").replace(")", "").replace("..", "..)");
                String[] attributeValueParts = attributeValue.split("\\|");
                String constraint = "";

                for(int l = 0; l < attributeValueParts.length; l = l+2) {
                    constraint = constraint + attributeValueParts[l].replace(",", "");
                    if (constraint.contains("id")) {
                        constraint = constraint + ")";
                    }
                }


                json = json + "{" + "\"attributeCardinalityMin\":\"" + minCarAttr + "\"," +
                        "\"attributeCardinalityMax\":\"" + maxCarAttr + "\"," +
                        "\"attributeNameCode\":\"" + attributeNameCode + "\"," +
                        "\"attributeNameDisplay\":\"" + attributeNameName + "\"," +
                        "\"constraint\":\"" + constraint.replace("[[+id(", "") + "\"";

                json = json + "},";
            }
        }
        json = json.substring(0, json.length()-1);
        json = json + "]";
        return json;
    }

    private String preprocessingRefinementWithRg(String url, String version, String refinement, String json) {
        // split in rolegroups
        String [] roleGroups = refinement.split("}");
        json = json + ",\"withRoleGroups\":[" ;

        for(int i = 0; i < roleGroups.length; i++) {
            String carRg = roleGroups[i].split("\\{")[0].replace("[[", "").replace("]]", "").replace(",", "");
            String minCar = carRg.split("\\.\\.")[0];
            String maxCar = carRg.split("\\.\\.")[1];

            json = json + "{" +
                    "\"roleGroupCardinalityMin\":\"" + minCar + "\"," +
                    "\"roleGroupCardinalityMax\":\"" + maxCar + "\"," +
                    "\"attribute\":[";

            String[] attributeOfRg = roleGroups[i].substring(roleGroups[i].indexOf('{')).split("\\{");
            for(int j = 0; j < attributeOfRg.length; j++) {
                String[] attribute = attributeOfRg[j].split(",\\[\\[");

                for (int k = 0; k < attribute.length; k++) {
                    if(attribute[k].length() > 0) {
                        String carAttr = attribute[k].substring(0, attribute[k].indexOf("]]"));
                        String minCarAttr = carAttr.split("\\.\\.")[0].replace("[[", "");
                        String maxCarAttr = carAttr.split("\\.\\.")[1];
                        String attr = attribute[k].substring(attribute[k].indexOf("]]")).replace("]]", "");
                        String attributeNameCode = attr.split("=")[0].split("\\|")[0];
                        String attributeNameName = terminologyServerRequestsService.lookUpName(url, version, attributeNameCode);
                        String attributeValue = attr.split("=")[1].replace("[[+scg(", "").replace(")", "");
                        String[] attributeValueParts = attributeValue.split("\\|");
                        String constraint = "";

                        for(int l = 0; l < attributeValueParts.length; l = l+2) {
                            constraint = constraint + attributeValueParts[l];
                        }

                        json = json + "{" + "\"attributeCardinalityMin\":\"" + minCarAttr + "\"," +
                                "\"attributeCardinalityMax\":\"" + maxCarAttr + "\"," +
                                "\"attributeNameCode\":\"" + attributeNameCode + "\"," +
                                "\"attributeNameDisplay\":\"" + attributeNameName + "\"," +
                                "\"constraint\":\"" + constraint.replace("[[+id(", "") + "\"";

                        json = json + "},";
                    }
                }
            }
            json = json.substring(0, json.length()-1);
            json = json + "]},";
        }
        json = json.substring(0, json.length()-1);
        json = json + "]";
        return json;
    }



}

