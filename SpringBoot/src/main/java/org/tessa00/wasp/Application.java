package org.tessa00.wasp;

import org.json.JSONException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.IOException;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableConfigurationProperties(MTlsConfiguration.class)
public class Application {
    public static void main(String[] args) throws JSONException, IOException {
        SpringApplication.run(Application.class, args);
    }


}



