package org.tessa00.wasp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class TemplateNames {

    public TemplateNames(){

    }

    String pathUserHome = System.getProperty("user.home").replaceAll("\\\\", "/") + "/";
    String folderPath = pathUserHome + "/desktop/wasp/templates/";

    public JSONObject getAllTemplatesNames(String s) throws JSONException {
        System.out.println("!!!!!! ALL TEMPLATES");
        System.out.println(s);


        JSONObject jsonObject = new JSONObject(s);
        ArrayList<String> namesList = new ArrayList<String>();
        File[] files = new File(folderPath).listFiles();
        for (File file : files) {
            if (file.isFile()) {
                namesList.add(file.getName().replace(".json", ""));
            }
        }
        String json = "{\"names\": [";
        for(String name: namesList) {
            json = json + "{\"name\":\"" + name + "\"," + "\"semantictag\":\"" + getSemanticTag(name)+ "\"},";
        }
        json = json.substring(0, json.length()-1);
        json = json + "]}";
        try {
            JSONObject jsonObj = new JSONObject(json);
            return jsonObj;
        } catch (Exception e){
            System.out.println(e.getMessage().substring(0, 1000));
        }
        return null;
    }

    public static ArrayList<String> semanticTag = new ArrayList<>();

    static {
        semanticTag.add("body structure");
        semanticTag.add("cell");
        semanticTag.add("cell structure");
        semanticTag.add("morphologic abnormality");
        semanticTag.add("finding");
        semanticTag.add("disorder");
        semanticTag.add("environment/location");
        semanticTag.add("environment");
        semanticTag.add("geographic location");
        semanticTag.add("event");
        semanticTag.add("observable entity");
        semanticTag.add("organism");
        semanticTag.add("clinical drug");
        semanticTag.add("medicinal product");
        semanticTag.add("medicinal product form");
        semanticTag.add("physical object");
        semanticTag.add("product");
        semanticTag.add("physical force");
        semanticTag.add("physical object");
        semanticTag.add("product");
        semanticTag.add("procedure");
        semanticTag.add("regime/therapy");
        semanticTag.add("qualifier value");
        semanticTag.add("administration method");
        semanticTag.add("basic dose form");
        semanticTag.add("disposition");
        semanticTag.add("dose form");
        semanticTag.add("intended site");
        semanticTag.add("number");
        semanticTag.add("product name");
        semanticTag.add("release characteristic");
        semanticTag.add("role");
        semanticTag.add("state of matter");
        semanticTag.add("transformation");
        semanticTag.add("supplier");
        semanticTag.add("unit of presentation");
        semanticTag.add("record artifact");
        semanticTag.add("situation");
        semanticTag.add("attribute");
        semanticTag.add("core metadata concept");
        semanticTag.add("foundation metadata concept");
        semanticTag.add("link assertion");
        semanticTag.add("linkage concept");
        semanticTag.add("namespace concept");
        semanticTag.add("social concept");
        semanticTag.add("social concept");
        semanticTag.add("ethnic group");
        semanticTag.add("life style");
        semanticTag.add("occupation");
        semanticTag.add("person");
        semanticTag.add("racial group");
        semanticTag.add("religion/philosophy");
        semanticTag.add("special concept");
        semanticTag.add("inactive concept");
        semanticTag.add("navigational concept");
        semanticTag.add("specimen");
        semanticTag.add("staging scale");
        semanticTag.add("assessment scale");
        semanticTag.add("tumor staging");
        semanticTag.add("substance");
    }
    private String getSemanticTag(String name) {
        for(int i = 0; i < semanticTag.size(); i++) {
            name = name.replace(":", "/");
            if(name.contains(semanticTag.get(i))) {
                return semanticTag.get(i);
            }
        }
        return "";
    }

}
