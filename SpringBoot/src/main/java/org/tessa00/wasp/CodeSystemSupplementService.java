package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class CodeSystemSupplementService {

    private final TerminologyServerRequestsService terminologyServerRequestsService;
    private final HttpClientService httpClient;

    public CodeSystemSupplementService(TerminologyServerRequestsService terminologyServerRequestsService, HttpClientService httpClientService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
        this.httpClient = httpClientService;
    }

    public JSONObject updateCodeSystemSupplement(String url, String sctVersion, String pce, String namePce, String templateName, String id, String nameCss) throws JSONException {
        ArrayList<String> arrayList = getElements(url);

        String json = buildJSON(arrayList, pce, true, namePce, templateName, url, sctVersion, id, nameCss);
        url = url + "/CodeSystem/" + id;

        System.out.println("**********+");
        System.out.println(json);
        System.out.println(url);
        String result = terminologyServerRequestsService.updateCodeSystemSupplement(json, url);

        return new JSONObject("{\"result\":\"" + result + "\"}");
    }

    public ArrayList<String> getElements(String url) throws JSONException {
        System.out.println("-----> " + url);

        JSONArray jsonArray = terminologyServerRequestsService.getCodeSystemSupplement(url);
        ArrayList<String> arrayList = new ArrayList<>();
        String display;
        String code;
        String definition;

        if(!jsonArray.toString().contains("CodeSystem not found")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                if(json.has("concept")) {
                    JSONArray jsonArrayConcept = (JSONArray) json.get("concept");
                    for (int j = 0; j < jsonArrayConcept.length(); j++) {
                        JSONObject jsonConcept = (JSONObject) jsonArrayConcept.get(j);
                        code = jsonConcept.getString("code");
                        try {
                            display = jsonConcept.getString("display");
                        } catch (Exception e) {
                            display = " ";
                        }
                        try {
                            definition = jsonConcept.getString("definition");
                        } catch (Exception e) {
                            definition = " ";
                        }
                        String sum = code + "@" + display + "@" + definition;
                        arrayList.add(sum);
                    }
                } else {
                    return new ArrayList<>();
                }
            }
        }
        return arrayList;
    }

    public String getTitle(String url) throws JSONException {
        JSONArray jsonArray = terminologyServerRequestsService.getCodeSystemSupplement(url);

        System.out.println(jsonArray);
        if(jsonArray.length() != 0) {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String name = jsonObject.getString("title");
            return name;
        } else {
            return "";
        }
    }

    public String buildJSON(ArrayList<String> arrayList, String pce, Boolean b, String namePce, String templateName, String url, String sctVersion, String id, String nameCss) {
        String version = LocalDateTime.now().getYear() + "" + LocalDateTime.now().getMonthValue() + "" + LocalDateTime.now().getDayOfMonth();
        String concept = "";

        for(int i = 0; i < arrayList.size(); i++) {
            concept = concept + "{\"code\":\"" + arrayList.get(i).split("@")[0] + "\",\"display\":\"" + arrayList.get(i).split("@")[1] +
                    "\",\"definition\":\"" + arrayList.get(i).split("@")[2] + "\"},";
        }

        if(b) {
            concept = concept + "{\"code\":\"" + pce + "\",\"display\":\"" + namePce + "\",\"definition\":\"" + templateName + "\"}";
        } else {
            concept = concept.substring(0, concept.lastIndexOf(","));
        }

        System.out.println("'##########");
        System.out.println(url);

        String json = "{" +
                "    \"resourceType\": \"CodeSystem\"," +
                "    \"id\": \"" +  id + "\"," +
                "    \"language\": \"en\"," +
                "    \"url\": \""+ url + "/CodeSystem/" + id + "\"," +
                "    \"version\": \"" + version + "\"," +
                "    \"name\": \"SctPceSupplementPceBuilder\"," +
                "    \"title\": \"" +  nameCss + "\"," +
                "    \"status\": \"draft\"," +
                "    \"experimental\": true," +
                "    \"date\": \"" + java.time.LocalDate.now() + "\"," +
                "    \"content\": \"supplement\"," +
                "    \"supplements\": \"http://snomed.info/sct|" + sctVersion + "\"," +
                "    \"concept\": [" + concept + "]" +
                "}";

        return json;
    }

    public JSONObject updateCodeSystemSupplementWithStoredPce(String url, String id, String pce, String name, String templateName, String idcss, String sctversion, String sameCs, String title) throws JSONException {
        ArrayList<String> arrayList;
        String newUrl;
        if(!url.contains("/CodeSystem/")) {
            System.out.println("ID " + id + " " + idcss);
            arrayList = getElements(url + "/CodeSystem/" + idcss);
            newUrl = url + "/CodeSystem/" + idcss;

        } else {
            idcss = url.split("/CodeSystem/")[1];
            arrayList = getElements(url);
            newUrl = url;

        }

        String updatedPce = pce + "@" + name  + "@" + templateName;
        System.out.println("++++++++++++++++++++");
        System.out.println(url);
        System.out.println(id + " -- " + updatedPce );
//        System.out.println("ID : " + );

        if(sameCs.equalsIgnoreCase("true")) {
            arrayList.set(Integer.parseInt(id), updatedPce);
            System.out.println("----> sameCS");
        } else {
            arrayList.add(updatedPce);
            System.out.println("----> otherCS");
        }

        String json = buildJSON(arrayList, pce, false, name, templateName, url, sctversion, idcss, title);
        System.out.println("---------------------------");
        System.out.println(json);
        System.out.println("---------------------------");
        String result = terminologyServerRequestsService.updateCodeSystemSupplement(json, newUrl);

        return new JSONObject("{\"result\":\"" + result + "\"}");
    }


    public JSONObject getCsSupplements(String url) throws JSONException {
        System.out.println("--> " + url);
        String urlNew = url + "/CodeSystem?name=SctPceSupplementPceBuilder";
        String jsonResult = "{\"value\":[";

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());

            if(response.body().contains("This service uses the Deutsches Forschungsnetz Public Key Infrastructure")) {
                return new JSONObject("{\"message\":\"A certificate of the Deutsches Forschungsnetz is required!\"}");

            }

            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);
                JSONArray entry = json.getJSONArray("entry");
                for(int i = 0; i < entry.length(); i++) {
                    json = (JSONObject) entry.get(i);
                    String fullUrl = json.getString("fullUrl");
                    json = (JSONObject) json.get("resource");
                    String title = json.getString("title");
                    jsonResult = jsonResult + "{\"url\":\"" + fullUrl + "\", \"title\":\"" + title + "\"},";
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        if(jsonResult.contains(",")) {
            jsonResult = jsonResult.substring(0, jsonResult.lastIndexOf(",")) + "]}";
            return new JSONObject(jsonResult);
        } else {
            return new JSONObject();
        }
    }
}
