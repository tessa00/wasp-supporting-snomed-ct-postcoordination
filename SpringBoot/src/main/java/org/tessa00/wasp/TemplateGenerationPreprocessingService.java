package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

@Service
public class TemplateGenerationPreprocessingService {

    private final TerminologyServerRequestsService terminologyServerRequestsService;
    private final TemplateGenerationService templateGenerationService;

    public TemplateGenerationPreprocessingService(TerminologyServerRequestsService terminologyServerRequestsService,
                                                  TemplateGenerationService templateGenerationService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
        this.templateGenerationService = templateGenerationService;
    }

    public String conceptModelReferencedComponentId = "";
    public String conceptModelDomainConstraint = "";
    public String conceptModelParentDomain = "";
    public String conceptModelProximalPrimitiveConstraint = "";
    public String conceptModelProximalPrimitiveRefinement = "";
    public String conceptModelURL = "";
    public String conceptModelTemplate = "";
    public String conceptModelFocusConceptTemplate = "";

    public ArrayList<String> listWithoutMinCar = new ArrayList<>();
    public ArrayList<String> listWithoutMaxCar = new ArrayList<>();
    public ArrayList<String> listWithoutAttributeCode = new ArrayList<>();
    public ArrayList<String> listWithoutAttributeName = new ArrayList<>();
    public ArrayList<String> listWithoutConstraint = new ArrayList<>();
    public ArrayList<String> listChangedConstraintWithout = new ArrayList<>();
    public ArrayList<String> listChangedConstraintFlagWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listAllNamesRangeWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listAllCodesRangeWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listSummaryNameRangeWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listSummaryCodeRangeWithout = new ArrayList<>();
    public ArrayList<String> listSummaryAttributeCodesWithout = new ArrayList<>();
    public ArrayList<String> listSummaryAttributeNamesWithout = new ArrayList<>();
    public ArrayList<String> listSummaryMinCardinalityWithout = new ArrayList<>();
    public ArrayList<String> listSummaryMaxCardinalityWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listSummaryOperatorRangeWithout = new ArrayList<>();
    public ArrayList<ArrayList<String>> listSummaryConstraintRangeWithout = new ArrayList<>();
    public ArrayList<String> listSummaryValueRangeWithout = new ArrayList<>();
    public ArrayList<String> listIntValueWithout = new ArrayList<>();
    public ArrayList<String> listIntFixedWithout = new ArrayList<>();
    public ArrayList<String> listDecValueWithout = new ArrayList<>();
    public ArrayList<String> listDecFixedWithout = new ArrayList<>();
    public ArrayList<String> listChangeCarWithout = new ArrayList<>();
    public ArrayList<String> listOperatorRangeWithout = new ArrayList<>();
    public ArrayList<String> listConstraintRangeWithout = new ArrayList<>();
    public ArrayList<String> listNameRangeWithout = new ArrayList<>();
    public ArrayList<String> listNumberAttributesWithout = new ArrayList<>();

    public ArrayList<String> listInitWithRoleGroupMinCar = new ArrayList<>();
    public ArrayList<String> listInitWithRoleGroupMaxCar = new ArrayList<>();
    public ArrayList<ArrayList<String>> listInitWithMinCar = new ArrayList<>();
    public ArrayList<ArrayList<String>> listInitWithMaxCar = new ArrayList<>();
    public ArrayList<ArrayList<String>> listInitWithAttributeCode = new ArrayList<>();
    public ArrayList<ArrayList<String>> listInitWithAttributeName = new ArrayList<>();
    public ArrayList<ArrayList<String>> listInitWithConstraint = new ArrayList<>();
    public ArrayList<ArrayList<String>> listChangedConstraintWith = new ArrayList<>();
    public ArrayList<ArrayList<String>> listChangedAttributeNameWith = new ArrayList<ArrayList<String>>();
    public ArrayList<ArrayList<String>> listChangedAttributeCodeWith = new ArrayList<ArrayList<String>>();
    public ArrayList<ArrayList<String>> listChangedFlagWith = new ArrayList<>();
    private ArrayList<ArrayList<String>> listCodeWith = new ArrayList();

    public int controlNumber = -1;



    public String init(String url, String version, String focusConceptCode, ArrayList<String> listnumber, String conceptModel) throws JSONException {

        resetData();
        JSONObject jsonObject = templateGenerationService.getMatchingConceptModel(url, version, focusConceptCode, conceptModel);     // get CM as json

        preprocessingGeneral(jsonObject);                                                       // general parts of CM
        preprocessingWithout(jsonObject, url, focusConceptCode);                                // general of without
        preprocessingWith(jsonObject, url, focusConceptCode, listnumber);                                   // general of without
        preprocessingAttributerelation(url, version, focusConceptCode, listnumber);

        String jsonGeneral = buildJsonGeneral();
        String jsonWithout = buildJsonWithout();
        String jsonWith = buildJsonWith();

        String json = "{\"values\":[" + jsonGeneral + "," + jsonWithout +  "," + jsonWith + "]}";
        System.out.println(json);
        return json;

    }

    private void preprocessingGeneral(JSONObject jsonObject) throws JSONException {
        conceptModelReferencedComponentId = (String) jsonObject.get("referencedComponentId");
        conceptModelDomainConstraint = (String) jsonObject.get("domainConstraint");
        conceptModelParentDomain = (String) jsonObject.get("parentDomain");
        conceptModelProximalPrimitiveConstraint = (String) jsonObject.get("proximalPrimitiveConstraint");
        conceptModelProximalPrimitiveRefinement = (String) jsonObject.get("proximalPrimitiveRefinement");
        conceptModelURL = (String) jsonObject.get("guideURL");

        JSONArray domainTemplateForPrecoordination = (JSONArray) jsonObject.get("domainTemplateForPrecoordination");
        JSONObject jsonObjectPre = (JSONObject) domainTemplateForPrecoordination.get(0);

        conceptModelTemplate = (String) jsonObjectPre.get("init");
        conceptModelFocusConceptTemplate = (String) jsonObjectPre.get("focusconcept");

    }


    private String buildJsonGeneral() {
        String json = "" +
                buildJsonPartString("conceptModelReferencedComponentId", conceptModelReferencedComponentId) + "," +
                buildJsonPartString("conceptModelDomainConstraint", conceptModelDomainConstraint) + "," +
                buildJsonPartString("conceptModelParentDomain", conceptModelParentDomain) + "," +
                buildJsonPartString("conceptModelProximalPrimitiveConstraint", conceptModelProximalPrimitiveConstraint) + "," +
                buildJsonPartString("conceptModelProximalPrimitiveRefinement", conceptModelProximalPrimitiveRefinement) + "," +
                buildJsonPartString("conceptModelURL", conceptModelURL) + "," +
                buildJsonPartString("conceptModelTemplate", conceptModelTemplate) + "," +
                buildJsonPartString("conceptModelFocusConceptTemplate", conceptModelFocusConceptTemplate);

        return json;
    }

    private void preprocessingWithout(JSONObject jsonObject, String url, String focusConceptCode) throws JSONException {
        JSONArray domainTemplateForPrecoordination = (JSONArray) jsonObject.get("domainTemplateForPrecoordination");
        JSONObject jsonObjectPre = (JSONObject) domainTemplateForPrecoordination.get(0);
        if(jsonObjectPre.has("withoutRoleGroups")) {
            preprocessingWithoutAttribute(jsonObjectPre,url, focusConceptCode);
        }
    }

    private void preprocessingWith(JSONObject jsonObject, String url, String focusConceptCode, ArrayList<String> listnumber) throws JSONException {
        JSONArray domainTemplateForPrecoordination = (JSONArray) jsonObject.get("domainTemplateForPrecoordination");
        JSONObject jsonObjectPre = (JSONObject) domainTemplateForPrecoordination.get(0);
        if(jsonObjectPre.has("withRoleGroups")) {
            preprocessingWithAttribute(jsonObjectPre,url, focusConceptCode, listnumber);
        }
    }

    private void preprocessingWithAttribute(JSONObject jsonObjectPre, String url, String focusConceptCode, ArrayList<String> listnumber) throws JSONException {
        JSONArray withRg = (JSONArray) jsonObjectPre.get("withRoleGroups");
        controlNumber = withRg.length();
        for(int i = 0; i < withRg.length(); i++) {
            JSONObject withRoleGroup = (JSONObject) withRg.get(i);
            listInitWithRoleGroupMinCar.add("\"" + withRoleGroup.get("roleGroupCardinalityMin") + "\"");
            if (withRoleGroup.get("roleGroupCardinalityMax").toString().equals("*")) {
                listInitWithRoleGroupMaxCar.add("\"" + "Infinity" + "\"");
            } else {
                listInitWithRoleGroupMaxCar.add("\"" + withRoleGroup.get("roleGroupCardinalityMax") + "\"");
            }

            listCodeWith.add(new ArrayList<>());

            JSONArray attribute = (JSONArray) withRoleGroup.get("attribute");
            ArrayList<String> listConstraint = new ArrayList<>();
            ArrayList<String> listMinCar = new ArrayList<>();
            ArrayList<String> listMaxCar = new ArrayList<>();
            ArrayList<String> listCode = new ArrayList<>();
            ArrayList<String> listName = new ArrayList<>();
            ArrayList<String> listFlag = new ArrayList<>();
            ArrayList<String> listChangedC = new ArrayList<>();

            for (int j = 0; j < attribute.length(); j++) {
                JSONObject att = attribute.getJSONObject(j);

                listMinCar.add("\"" + att.get("attributeCardinalityMin").toString().replace("[[", "") + "\"");
                if (att.get("attributeCardinalityMax").equals("*")) {
                    listMaxCar.add("\"" + "Infinity" + "\"");
                } else {
                    listMaxCar.add("\"" + att.get("attributeCardinalityMax") + "\"");
                }
                listCode.add("\"" + att.get("attributeNameCode") + "\"");
                listName.add("\"" + att.get("attributeNameDisplay") + "\"");
                listConstraint.add("\"" + att.get("constraint").toString().replace("id(", "").replace("[[+", "") + "\"");
                listCodeWith.get(i).add((String) att.get("attributeNameCode"));
                listFlag.add("\"" + "0" + "\"");
                listChangedC.add(null);
            }
            listInitWithConstraint.add(listConstraint);
            listInitWithMinCar.add(listMinCar);
            listInitWithMaxCar.add(listMaxCar);
            listInitWithAttributeCode.add(listCode);
            listInitWithAttributeName.add(listName);
            listChangedFlagWith.add(listFlag);
            listChangedConstraintWith.add(listChangedC);
        }
    }

    private void preprocessingWithoutAttribute(JSONObject jsonObjectPre, String url, String focusConceptCode) throws JSONException {
        JSONArray withoutRg = (JSONArray) jsonObjectPre.get("withoutRoleGroups");
        for(int i = 0; i < withoutRg.length(); i++) {
            JSONObject attribute = (JSONObject) withoutRg.get(i);
            listWithoutMinCar.add("\"" + attribute.get("attributeCardinalityMin") + "\"");
            if (attribute.get("attributeCardinalityMax").equals("*")) {
                listWithoutMaxCar.add("\"" + "Infinity" + "\"");
            } else {
                listWithoutMaxCar.add("\"" + attribute.get("attributeCardinalityMax") + "\"");
            }
            listWithoutAttributeCode.add("\"" + attribute.get("attributeNameCode") + "\"");
            listWithoutAttributeName.add("\"" + attribute.get("attributeNameDisplay") + "\"");
            listWithoutConstraint.add("\"" + attribute.get("constraint").toString().replace("id(", "").replace("[[+", "") + "\"");
            listChangedConstraintWithout.add(null);
            listChangedConstraintFlagWithout.add("\"0\"");
            listAllNamesRangeWithout.add(new ArrayList<>());
            listAllCodesRangeWithout.add(new ArrayList<>());
            listSummaryNameRangeWithout.add(new ArrayList<>());
            listSummaryCodeRangeWithout.add(new ArrayList<>());
            listSummaryAttributeCodesWithout.add(null);
            listSummaryAttributeNamesWithout.add(null);
            listSummaryMinCardinalityWithout.add(null);
            listSummaryMaxCardinalityWithout.add(null);
            listSummaryOperatorRangeWithout.add(new ArrayList<>());
            listSummaryConstraintRangeWithout.add(new ArrayList<>());
            listSummaryValueRangeWithout.add(null);
            listIntValueWithout.add(null);
            listIntFixedWithout.add(null);
            listDecValueWithout.add(null);
            listDecFixedWithout.add(null);
            listChangeCarWithout.add("false");
        }
        listOperatorRangeWithout.add(null);
        listConstraintRangeWithout.add(null);
        listNameRangeWithout.add(null);
        listNumberAttributesWithout.add(null);
    }



    private  void preprocessingAttributerelation(String url, String version, String focusConceptCode, ArrayList<String> listnumber) throws JSONException {
        ArrayList<ArrayList<String>> attributeRelations = terminologyServerRequestsService.lookUpNameNormalForm(url, version, focusConceptCode);
        assert attributeRelations != null;
        setAttributerelation(url, version, focusConceptCode,    attributeRelations, listnumber);
    }

    public ArrayList<String> listNumberAttributeRelations = new ArrayList<>();
    public ArrayList<ArrayList<ArrayList<String>>> listIndexWith = new ArrayList<>();
    int subRg = 0;

    private ArrayList<String> setAttributerelation(String url, String version, String focusConceptCode, ArrayList<ArrayList<String>> attributeRelations, ArrayList<String> listnumber) throws JSONException {
        ArrayList<ArrayList<String>> resultCode = new ArrayList<>();
        ArrayList<ArrayList<String>> resultName = new ArrayList<>();
        ArrayList<ArrayList<String>> resultConstraint = new ArrayList<>();
        ArrayList<ArrayList<String>> resultFlag = new ArrayList<>();
        ArrayList<ArrayList<String>> resultChangedConstraint = new ArrayList<>();

        for(int i = 0; i < attributeRelations.size(); i++) {
            resultCode.add(new ArrayList<>());
            resultName.add(new ArrayList<>());
            resultConstraint.add(new ArrayList<>());
            resultFlag.add(new ArrayList<>());
            resultChangedConstraint.add(new ArrayList<>());
            for(int j = 0; j < attributeRelations.get(i).size(); j++) {
                String attributeCode = attributeRelations.get(i).get(j).split("@")[0];
                String attributeValue = attributeRelations.get(i).get(j).split("@")[1];
                String index = attributeRelations.get(i).get(j).split("@")[2];
                String constraint = "";
                if(listCodeWith.toString().contains(attributeCode)) {
                    listNumberAttributeRelations.add(attributeCode + "@" + subRg);
                    if(Objects.equals(index, "code")) {
                        constraint = "<<" + attributeValue;
                    } else if(attributeValue.contains(".")){
                        constraint = "+dec(#" + attributeValue + ")";
                    } else {
                        constraint = "+int(#" + attributeValue + ")";
                    }
                    resultCode.get(resultCode.size() - 1).add("\"" + attributeCode + "\"");
                    System.out.println("--> " + Objects.requireNonNull(terminologyServerRequestsService.lookUpNameFsn(url, version, attributeCode)));
                    resultName.get(resultCode.size() - 1).add("\"" + Objects.requireNonNull(terminologyServerRequestsService.lookUpNameFsn(url, version, attributeCode)).split("\\(")[0] + "\"");
                    resultConstraint.get(resultCode.size() - 1).add("\"" + constraint + "\"");
                    if (terminologyServerRequestsService.lookUpNameFsn(url, version, attributeValue) != null) {
                        resultChangedConstraint.get(resultChangedConstraint.size() - 1).add("\"" + constraint + " |" + terminologyServerRequestsService.lookUpNameFsn(url,version, attributeValue) + "|\"");
                    } else {
                        resultChangedConstraint.get(resultChangedConstraint.size() - 1).add("\"" + constraint  + "\"");
                    }

                    int x = Integer.parseInt(String.valueOf(i))+1;
                    resultFlag.get(resultCode.size() - 1).add("\"" +  x + "\"");
                } else if(listWithoutAttributeCode.toString().contains(attributeCode)){
                    listNumberAttributeRelations.add(attributeCode + "@" + "without");
                    int occurrences = Collections.frequency(listWithoutAttributeCode, attributeCode);
                    if(occurrences < 1) {
                        if (Objects.equals(index, "code")) {
                            constraint = "<<" + attributeValue;
                        } else if (attributeValue.contains(".")) {
                            constraint = "+dec(#" + attributeValue + ")";
                        } else {
                            constraint = "+int(#" + attributeValue + ")";
                        }

                        listWithoutAttributeCode.add("\"" + attributeCode + "\"");
                        listWithoutAttributeName.add("\"" + Objects.requireNonNull(terminologyServerRequestsService.lookUpNameFsn(url, version, attributeCode)).split("\\(")[0] + "\"");
                        listWithoutConstraint.add("\"" + constraint + "\"");
                        if (terminologyServerRequestsService.lookUpNameFsn(url, version, attributeValue) != null) {
                            listChangedConstraintWithout.add("\"" + constraint + " |" + terminologyServerRequestsService.lookUpNameFsn(url, version, attributeValue) + "|\"");
                        } else {
                            listChangedConstraintWithout.add("\"" + constraint + "\"");
                        }
                        listChangedConstraintFlagWithout.add("\"1\"");
                        listWithoutMinCar.add("\"1\"");
                        listWithoutMaxCar.add("\"1\"");
                        listAllNamesRangeWithout.add(new ArrayList<>());
                        listAllCodesRangeWithout.add(new ArrayList<>());
                        listSummaryNameRangeWithout.add(new ArrayList<>());
                        listSummaryCodeRangeWithout.add(new ArrayList<>());
                        listSummaryAttributeCodesWithout.add(null);
                        listSummaryAttributeNamesWithout.add(null);
                        listSummaryMinCardinalityWithout.add(null);
                        listSummaryMaxCardinalityWithout.add(null);
                        listSummaryOperatorRangeWithout.add(new ArrayList<>());
                        listSummaryConstraintRangeWithout.add(new ArrayList<>());
                        listSummaryValueRangeWithout.add(null);
                        listIntValueWithout.add(null);
                        listIntFixedWithout.add(null);
                        listDecValueWithout.add(null);
                        listDecFixedWithout.add(null);
                        listChangeCarWithout.add("false");
                    }
                }
            }
            subRg = subRg + 1;
        }


        for(int i = 0; i < resultCode.size(); i++) {
            if(resultCode.get(i).size() > 0) {
                for(int j = 0; j < resultCode.get(i).size(); j++) {

                    listInitWithAttributeCode.get(0).add(resultCode.get(i).get(j));
                    listInitWithAttributeName.get(0).add(resultName.get(i).get(j));
                    listInitWithConstraint.get(0).add(resultConstraint.get(i).get(j));
                    listChangedFlagWith.get(0).add(resultFlag.get(i).get(j));
                    listChangedConstraintWith.get(0).add(resultChangedConstraint.get(i).get(j));
                    for(int k = 0; k < listInitWithMinCar.get(0).size(); k++) {
                        if(Objects.equals(listInitWithAttributeCode.get(0).get(k), resultCode.get(i).get(j))) {
                            listInitWithMinCar.get(0).add(listInitWithMinCar.get(0).get(k));
                            listInitWithMaxCar.get(0).add(listInitWithMaxCar.get(0).get(k));
                            break;
                        }
                    }
                }
            }
        }

        int id = 0;
        float idOld = 0;
        if(listChangedFlagWith.size() > 0) {
            for(int i = 0; i < listChangedFlagWith.get(0).size(); i++) {
                if(Float.parseFloat(listChangedFlagWith.get(0).get(i).replace("\"", "")) > 0) {
                    float num = Float.parseFloat(listChangedFlagWith.get(0).get(i).replace("\"", ""));
                    if(num != idOld) {
                        id = id + 1;
                        idOld = num;
                    }
                    listChangedFlagWith.get(0).set(i, String.valueOf("\"" + id + "\""));

                }

            }


            // testen, welche Attribute keine AR sind -- kommen die in der subrg nach der anzahl der AR bei den zahlen
            // zählen, wie viele AR pro subrg

            HashSet<String> hashSet = new HashSet<String>(listChangedFlagWith.get(0));
            ArrayList<String> arrayList = new ArrayList<>(hashSet);
            arrayList.sort(null);
            for(int i = 0; i < arrayList.size(); i++) {
                if(arrayList.get(i).equals("\"0\"")) {
                    arrayList.remove(i);
                    break;
                }
            }


            // wissen alle attributrelationen
            int idx = -1;
            ArrayList<ArrayList<ArrayList<String>>> listAr = new ArrayList<>();
            listAr.add(new ArrayList<>());

            for(int i = 0; i < arrayList.size(); i++) {
                listAr.get(0).add(new ArrayList<>());
            }

            for(int i = 0; i < listChangedFlagWith.get(0).size(); i++) {
                if(!listChangedFlagWith.get(0).get(i).equals("\"0\"")) {
                    int pos = Integer.parseInt(listChangedFlagWith.get(0).get(i).replace("\"", ""));
                    listAr.get(0).get(pos-1).add("\"" + String.valueOf(i) + "\"");
                }
            }

            // wissen alle attributrelationen
            // todo -- get(0) muss vermutlich für weitere rg erweitert werden

            ArrayList<ArrayList<String>> arrayListsChanged = new ArrayList<>();
            arrayListsChanged.add(new ArrayList<>());
            for(int i = 0; i < listChangedFlagWith.get(0).size(); i++) {
                if(!listChangedFlagWith.get(0).get(i).equals("\"0\"")) {
                    arrayListsChanged.get(0).add(listChangedFlagWith.get(0).get(i));
                }
            }

            if(!listnumber.isEmpty()) {
                for(int i = 0; i < arrayList.size(); i++) {
                    if(!arrayList.get(i).equals("\"0\"")) {
                        int occurrences = Collections.frequency(listChangedFlagWith.get(0), arrayList.get(i));
                        // testen, ob in einer subrg weitere elemente sind -- diese sind dann nach der letzten AR
                        if(Integer.parseInt(listnumber.get(i).replace("\"", "")) > occurrences) {
                            // hier wird ein neues element, was kein AR ist
                            // muss an die stelle, wo das letzte mal die Zahl für subrg ist
                            int pos = arrayListsChanged.get(0).lastIndexOf(arrayList.get(i));

                            // gucken, wer an dieser stelle ist und dann die nummer zu bestimmen
//                        int posX = Integer.parseInt(listChangedFlagWith.get(0).get(i).replace("\"", ""));
                            int posX = Integer.parseInt(arrayList.get(i).replace("\"", ""));
                            int diff = Integer.parseInt(listnumber.get(i)) - occurrences;
                            for(int j = 0; j < diff; j++) {
                                listAr.get(0).get(posX-1).add("\"@\"");
                            }
                        }

                    }
                }
                listIndexWith = listAr;
            }
        }







        return new ArrayList<>();
    }


    private String buildJsonWithout() {
        String json = "" +
                buildJsonPartListString("listWithoutMinCar", listWithoutMinCar) + "," +
                buildJsonPartListString("listWithoutMaxCar", listWithoutMaxCar) + "," +
                buildJsonPartListString("listWithoutAttributeCode", listWithoutAttributeCode) + "," +
                buildJsonPartListString("listWithoutAttributeName", listWithoutAttributeName) + "," +
                buildJsonPartListString("listWithoutConstraint", listWithoutConstraint) + "," +
                buildJsonPartListString("listChangedConstraintWithout", listChangedConstraintWithout) + "," +
                buildJsonPartListString("listChangedConstraintFlagWithout", listChangedConstraintFlagWithout) + "," +
                buildJsonPartListList("listAllNamesRangeWithout", listAllNamesRangeWithout) + "," +
                buildJsonPartListList("listAllCodesRangeWithout", listAllCodesRangeWithout) + "," +
                buildJsonPartListList("listSummaryNameRangeWithout", listSummaryNameRangeWithout) + "," +
                buildJsonPartListList("listSummaryCodeRangeWithout", listSummaryCodeRangeWithout) + "," +
                buildJsonPartListString("listSummaryAttributeCodesWithout", listSummaryAttributeCodesWithout) + "," +
                buildJsonPartListString("listSummaryAttributeNamesWithout", listSummaryAttributeNamesWithout) + "," +
                buildJsonPartListString("listSummaryMinCardinalityWithout", listSummaryMinCardinalityWithout) + "," +
                buildJsonPartListString("listSummaryMaxCardinalityWithout", listSummaryMaxCardinalityWithout) + "," +
                buildJsonPartListList("listSummaryOperatorRangeWithout", listSummaryOperatorRangeWithout) + "," +
                buildJsonPartListList("listSummaryConstraintRangeWithout", listSummaryConstraintRangeWithout) + "," +
                buildJsonPartListString("listSummaryValueRangeWithout", listSummaryValueRangeWithout) + "," +
                buildJsonPartListString("listIntValueWithout", listIntValueWithout) + "," +
                buildJsonPartListString("listIntFixedWithout", listIntFixedWithout) + "," +
                buildJsonPartListString("listDecValueWithout", listDecValueWithout) + "," +
                buildJsonPartListString("listDecFixedWithout", listDecFixedWithout) + "," +
                buildJsonPartListString("listOperatorRangeWithout", listOperatorRangeWithout) + "," +
                buildJsonPartListString("listConstraintRangeWithout", listConstraintRangeWithout) + "," +
                buildJsonPartListString("listNameRangeWithout", listNameRangeWithout) + "," +
                buildJsonPartListString("listNumberAttributesWithout", listNumberAttributesWithout) + "," +
                buildJsonPartListString("listChangeCarWithout", listChangeCarWithout) ;

        return json;
    }

    private String buildJsonWith() {
        String json = "";
        if(controlNumber > -1) {
            json = buildJsonPartListString("listInitWithRoleGroupMinCar", new ArrayList(listInitWithRoleGroupMinCar.subList(0, controlNumber))) + "," +
            buildJsonPartListString("listInitWithRoleGroupMaxCar", new ArrayList(listInitWithRoleGroupMaxCar.subList(0, controlNumber))) + ",";
        } else {
            json = buildJsonPartListString("listInitWithRoleGroupMinCar", new ArrayList()) + "," +
                    buildJsonPartListString("listInitWithRoleGroupMaxCar", new ArrayList()) + ",";
        }
        json = json +
                buildJsonPartListList("listInitWithMinCar", listInitWithMinCar) + "," +
                buildJsonPartListList("listInitWithMaxCar", listInitWithMaxCar) + "," +
                buildJsonPartListList("listInitWithAttributeCode", listInitWithAttributeCode) + "," +
                buildJsonPartListList("listInitWithAttributeName", listInitWithAttributeName) + "," +
                buildJsonPartListList("listInitWithConstraint", listInitWithConstraint)  + "," +
                buildJsonPartListList("listChangedConstraintWith", listChangedConstraintWith)  + "," +
                buildJsonPartListList("listChangedAttributeCodeWith", listChangedAttributeCodeWith)  + "," +
                buildJsonPartListList("listChangedAttributeNameWith", listChangedAttributeNameWith)  + "," +
                buildJsonPartListList("listChangedFlagWith", listChangedFlagWith)   + "," +
                buildJsonPartListListList("listIndexWith", listIndexWith)  ;

        return json;

    }

    private String buildJsonPartListString(String name, ArrayList<String> list) {
        return "{\"" + name + "\":" + list + "}";
    }

    private String buildJsonPartListList(String name, ArrayList<ArrayList<String>> list) {
        return "{\"" + name + "\":" + list + "}";
    }

    private String buildJsonPartListListList(String name, ArrayList<ArrayList<ArrayList<String>>> list) {
        return "{\"" + name + "\":" + list + "}";
    }

    private String buildJsonPartString(String name, String s) {
        return "{\"" + name + "\":\"" + s + "\"}";
    }

    private void resetData() {
        conceptModelReferencedComponentId = "";
        conceptModelDomainConstraint = "";
        conceptModelParentDomain = "";
        conceptModelProximalPrimitiveConstraint = "";
        conceptModelProximalPrimitiveRefinement = "";
        conceptModelURL = "";
        conceptModelTemplate = "";
        conceptModelFocusConceptTemplate = "";

        listWithoutMinCar = new ArrayList<>();
        listWithoutMaxCar = new ArrayList<>();
        listWithoutAttributeCode = new ArrayList<>();
        listWithoutAttributeName = new ArrayList<>();
        listWithoutConstraint = new ArrayList<>();
        listChangedConstraintWithout = new ArrayList<>();
        listChangedConstraintFlagWithout = new ArrayList<>();
        listAllNamesRangeWithout = new ArrayList<>();
        listAllCodesRangeWithout = new ArrayList<>();
        listSummaryNameRangeWithout = new ArrayList<>();
        listSummaryCodeRangeWithout = new ArrayList<>();
        listSummaryAttributeCodesWithout = new ArrayList<>();
        listSummaryAttributeNamesWithout = new ArrayList<>();
        listSummaryMinCardinalityWithout = new ArrayList<>();
        listSummaryMaxCardinalityWithout = new ArrayList<>();
        listSummaryOperatorRangeWithout = new ArrayList<>();
        listSummaryConstraintRangeWithout = new ArrayList<>();
        listSummaryValueRangeWithout = new ArrayList<>();
        listIntValueWithout = new ArrayList<>();
        listIntFixedWithout = new ArrayList<>();
        listDecValueWithout = new ArrayList<>();
        listDecFixedWithout = new ArrayList<>();
        listChangeCarWithout = new ArrayList<>();
        listOperatorRangeWithout = new ArrayList<>();
        listConstraintRangeWithout = new ArrayList<>();
        listNameRangeWithout = new ArrayList<>();
        listNumberAttributesWithout = new ArrayList<>();

        listInitWithRoleGroupMinCar = new ArrayList<>();
        listInitWithRoleGroupMaxCar = new ArrayList<>();
        listInitWithMinCar = new ArrayList<>();
        listInitWithMaxCar = new ArrayList<>();
        listInitWithAttributeCode = new ArrayList<>();
        listInitWithAttributeName = new ArrayList<>();
        listInitWithConstraint = new ArrayList<>();
        listChangedConstraintWith = new ArrayList<>();
        listChangedAttributeNameWith = new ArrayList<ArrayList<String>>();
        listChangedAttributeCodeWith = new ArrayList<ArrayList<String>>();
        listChangedFlagWith = new ArrayList<>();
        listCodeWith = new ArrayList();

        controlNumber = -1;
    }


}
