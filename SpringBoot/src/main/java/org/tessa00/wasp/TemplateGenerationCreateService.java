package org.tessa00.wasp;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

@Service
public class TemplateGenerationCreateService {

    private final TemplateGenerationFocusconceptService templateGenerationFocusconceptService;


    public TemplateGenerationCreateService(TemplateGenerationFocusconceptService templateGenerationFocusconceptService) {
        this.templateGenerationFocusconceptService = templateGenerationFocusconceptService;

    }

    public String createTemplate(String name, String pce, String term) throws JSONException, IOException {
        pce = pce.replace("\n", "\\n").replace("\t", "\\t");
        String fc = pce.split(":")[0];

        String json = "{\n";

        json = json + "\t\"name\": \"" + name + "\",\n";
        json = json + "\t\"domain\": \"" + "<< " + fc +  "\",\n";
        json = json + "\t\"version\": 1,\n";
        json = json + "\t\"logicalTemplate\": \"" + pce + "\",\n";
        json = json + "\t\"conceptOutline\": {\n";
        json = json + "\t\t\"definitionStatus\": \"FULLY_DEFINED\",\n";
        json = json + "\t\t\"descriptions\": [\n" + descriptionTerm(term, pce);
        json = json + "\t\t]\n";
        json = json + "\t},\n";
        json = json + "\t\"lexicalTemplates\": [\n" + lexicalTerm(term, pce);

        json = json + "\n\t]\n";
        json = json + "}\n";

        JSONObject jsObject = new JSONObject(json);
        return jsObject.toString();
    }

    private String descriptionTerm(String term, String pce) {
        String fsn = "";
        String synonym = "";

        // get semantic tag of focusconcept
        String focusConcept = pce.split(":")[0];
        String semanticTag = focusConcept.substring(focusConcept.lastIndexOf('('), focusConcept.lastIndexOf(')') + 1);

        // check if term containts semantic tag
        if(term.contains("(") && term.contains(")")) {
            if(!term.substring(term.lastIndexOf('('), term.lastIndexOf(')') + 1).equals(semanticTag)) {
                synonym = term;
                fsn = term + " " + semanticTag;
            } else {
                fsn = term;
                synonym = term.substring(0, term.lastIndexOf('('));
            }
        } else {
            synonym = term;
            fsn = term + " " + semanticTag;
        }

        String json = "\t\t\t{\n" +
                "        \"type\": \"FSN\",\n" +
                "        \"lang\": \"en\",\n" +
                "        \"termTemplate\": \"" + fsn + "\",\n" +
                "        \"caseSignificance\": \"CASE_INSENSITIVE\",\n" +
                "        \"acceptabilityMap\": {\n" +
                "          \"900000000000508004\": \"PREFERRED\",\n" +
                "          \"900000000000509007\": \"PREFERRED\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\": \"SYNONYM\",\n" +
                "        \"lang\": \"en\",\n" +
                "        \"termTemplate\": \"" + synonym + "\",\n" +
                "        \"caseSignificance\": \"CASE_INSENSITIVE\",\n" +
                "        \"acceptabilityMap\": {\n" +
                "          \"900000000000508004\": \"PREFERRED\",\n" +
                "          \"900000000000509007\": \"PREFERRED\"\n" +
                "        }\n" +
                "      }\n";
        return json;
    }

    private String lexicalTerm(String term, String pce) throws JSONException {
        String s = pce.split(":")[1];
        String [] a = s.split(",");
        ArrayList<String> arrayList = new ArrayList<>();
        for(int i = 0; i < a.length; i++) {
            if(a[i].contains("{")) {
                arrayList.add(a[i].split("\\{")[1]);
            } else {
                arrayList.add(a[i]);
            }
        }

        String json = "";
        String [] arr = term.split(" ");
        for(int i = 0; i < arr.length; i++) {
            if(arr[i].contains("$")) {
                String code = arr[i].replace("$", "");
                json = json + "\t\t{\n" +
                        "\t\t\t\"name\": \"" + code + "\",\n" +
                        "\t\t\t\"displayName\": \"[" + code + "]\",\n" +
                        "\t\t\t\"takeFSNFromSlot\": \"" + code + "\",\n";

                if(code.contains("finding")) {
                    json = json + "\t\t\t\"removeParts\": [\n" +
                          "\t\t\t\t\"Structure of\",\n" +
                          "\t\t\t\t\"structure of\",\n" +
                          "\t\t\t\t\"structure\"\n" +
                          "\t\t\t],\n";
                }

                for(int j = 0; j < arrayList.size(); j++) {
                    if(arrayList.get(j).contains(code)) {
                        String min = arrayList.get(j).substring(arrayList.get(j).indexOf("~")+1, arrayList.get(j).indexOf("~")+2);
                        if(min.equals("0")) {
                            json = json + "\t\t\"termReplacements\": [\n" +
                            "\t\t{" +
                            "\t\t\t\"existingTerm\": \"$" + code + "$\",\n" +
                            "\t\t\t\"replacement\": \"\",\n" +
                            "\t\t\t\"slotAbsent\": \"true\"\n" +
                            "\t\t\t}\n" +
                            "\t\t\t],\n";
                        }
                    }
               }
                json = json.substring(0, json.lastIndexOf(","));
                json = json + "\n\t\t},\n";
            }
        }
        if(json.contains(",")) {
            json = json.substring(0, json.lastIndexOf(","));
        }
        return json;
    }

    public String downloadTemplate(String name, String pce, String term) throws JSONException, IOException {
        String json = createTemplate(name, pce, term);
        String pathUserHome = System.getProperty("user.home").replaceAll("\\\\", "/") + "/";
        String folderPath = pathUserHome + "/download/" + name + ".json";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(folderPath));
            writer.write(json);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}
