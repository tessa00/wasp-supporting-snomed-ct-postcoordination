package org.tessa00.wasp;

import dev.harrel.jsonschema.ValidatorFactory;

public class ValidationSchema {

    public static void main(String[] args) {
        validateJson();
    }

    public static void validateJson() {
        String schema = "";
        String instance = "";
        boolean valid = new ValidatorFactory().withDisabledSchemaValidation(true).validate(schema, instance).isValid();
        System.out.println(valid);
    }
}
