package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TemplateGenerationService {

    private final TerminologyServerRequestsService terminologyServerRequestsService;

    public TemplateGenerationService(TerminologyServerRequestsService terminologyServerRequestsService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
    }

    public String json = "";
    public String constraintConceptModal = "";

    public String getConstrainFocusConcept(String conceptmodel) throws JSONException {
        JSONObject jsonObject = new JSONObject(conceptmodel);
        JSONArray cm = (JSONArray) jsonObject.get("conceptmodel");
        String constraint = "";
        for(int i = 0; i < cm.length(); i++) {
            JSONObject j = (JSONObject) cm.get(i);
            String dc = (String) j.get("domainConstraint");
            if(dc.contains(":")) {
                String [] array = dc.split(":");
                String minus = "";
                for(int k= 0; k < array.length; k++) {
                    if(array[k].contains(",")) {
                        String [] s = array[k].split(",");
                        for(int l = 0; l < s.length; l++) {
                            if(s[l].contains("[0..0]")) {
                                minus = minus + s[l].split("]")[1].split("\\|")[0].replace(" ", "") + "=*AND";
                            }
                        }
                    }
                }
                minus = minus.substring(0, minus.lastIndexOf("AND"));
                constraint = constraint + "(" + array[0].split("\\|")[0].replace(" ", "") + "MINUS(" + array[0].split("\\|")[0].replace(" ", "") + ":" + minus + "))" + "OR";
            } else if(!dc.contains("^")){
                constraint = constraint + dc.split("\\|")[0].replace(" ", "") + "OR";
            }
        }
        constraint = constraint.substring(0, constraint.lastIndexOf("OR"));
        constraintConceptModal = constraint;
        return constraint;
    }

    public JSONObject getMatchingConceptModel(String url, String version, String code, String conceptmodel) throws JSONException {
        JSONObject jsonObject = new JSONObject(conceptmodel);
        JSONArray cm = (JSONArray) jsonObject.get("conceptmodel");
        ArrayList<String> listDomainConstraint = new ArrayList<>();
        for(int i = 0; i < cm.length(); i++) {
            JSONObject j = (JSONObject) cm.get(i);
            String dc = (String) j.get("domainConstraint");
            if(dc.contains(":")) {
                listDomainConstraint.add(dc.split(":")[0]);
            } else {
                listDomainConstraint.add(dc);
            }

        }
        ArrayList<String> listMatchingCM = checkDomainConstraintCode(url, version, listDomainConstraint, code);
        String conceptModelName = "";

        if(listMatchingCM.size() > 1) {
            for(int i = 0; i < listMatchingCM.size(); i++) {
                for(int j = 0; j < listMatchingCM.size(); j++) {
                    String codeA = listMatchingCM.get(i).split("@")[1].split("\\|")[0].replace(" ", "").replace("<<", "");
                    String codeB = listMatchingCM.get(j).split("@")[1].split("\\|")[0].replace(" ", "").replace("<<", "");
                    String result = terminologyServerRequestsService.getSubsumesRelation(url, version, codeA, codeB);
                    if(result.equals("subsumes")) {
                        conceptModelName = listMatchingCM.get(j);
                    }
                }
            }
        } else {
            conceptModelName = listMatchingCM.get(0);
        }

        return (JSONObject) cm.get(Integer.parseInt(conceptModelName.split("@")[0]));

    }

    private ArrayList<String> checkDomainConstraintCode(String url, String version, ArrayList<String> arrayList, String code) {
        ArrayList<String> listResult = new ArrayList<>();

        for(int i = 0; i < arrayList.size(); i++) {
            String codeA = arrayList.get(i).split("\\|")[0].replace(" ", "");
            if(codeA.contains("<<")) {
                codeA = codeA.replace("<<", "");
                String result = terminologyServerRequestsService.getSubsumesRelation(url, version, codeA, code);
                if((result.equals("equivalent")) || (result.equals("subsumes"))) {
                    listResult.add(i + "@" + arrayList.get(i));
                }
            } else if (codeA.contains("^")) {
                String result = String.valueOf(terminologyServerRequestsService.findDescendantsWithoutFilter(url, codeA, version));
                if(result.contains(code)) {
                    listResult.add(i + "@" + arrayList.get(i));
                }
            }
        }
        return listResult;
    }
}
