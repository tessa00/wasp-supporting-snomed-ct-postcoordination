package org.tessa00.wasp.api;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tessa00.wasp.validation.PreprocessingResource;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.errorTypes.ErrorMessagePceOutputService;
import org.tessa00.wasp.validation.errorTypes.ErrorMessagePceService;

@RestController
public class VersioningController {

    private Logger logger = LoggerFactory.getLogger(PceController.class);

    private final ErrorMessagePceService errorMessagePce;
    private final ErrorMessagePceOutputService errorMessagePceOutputService = new ErrorMessagePceOutputService();
    private final TerminologyServerRequest terminologyServerRequest;

    public VersioningController(ErrorMessagePceService errorMessagePce, TerminologyServerRequest terminologyServerRequest) {
        this.errorMessagePce = errorMessagePce;
        this.terminologyServerRequest = terminologyServerRequest;
    }

    @CrossOrigin
    @PostMapping("/validate-single-concept")
    public String getFocusConceptAttributeValueRequestLimitFSN(@RequestBody String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        String s = jsonObject.getString("mrcm");
        JSONObject jsonObjectMrcm = new JSONObject(s);
        String pce = jsonObject.getString("concept");
        String content = "";
        JSONObject jsonObjectResult = errorMessagePce.initSingleConcept(jsonObject.getString("concept"), jsonObject.getString("url"), jsonObject.getString("version"), jsonObject.getJSONObject("conceptModelAttributes"), jsonObjectMrcm);
        logger.info(String.valueOf(jsonObjectResult));
        content = errorMessagePceOutputService.createOutput(jsonObjectResult);

        JSONObject jsonObjectOutput = new JSONObject();
        jsonObjectOutput.put("content", content);
        return jsonObjectOutput.toString();
    }

    @CrossOrigin
    @PostMapping("/get-pce-json")
    public String getPcesJson(@RequestBody String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        PreprocessingResource preprocessingResource = new PreprocessingResource(terminologyServerRequest);
        System.out.println(preprocessingResource.extractPces(jsonObject.getString("content"), false).toString());
        return preprocessingResource.extractPces(jsonObject.getString("content"), false).toString();
    }


    @CrossOrigin
    @PostMapping("/get-pce-csv")
    public String getPcesCsv(@RequestBody String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        PreprocessingResource preprocessingResource = new PreprocessingResource(terminologyServerRequest);
        return preprocessingResource.extractPces(jsonObject.getString("content"), true).toString();
    }

}
