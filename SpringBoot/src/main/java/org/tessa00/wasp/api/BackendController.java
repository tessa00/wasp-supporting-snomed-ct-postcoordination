package org.tessa00.wasp.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BackendController {

    @CrossOrigin
    @GetMapping("/api/version")
    public String apiVersion() {
        return "{ \"version\": \"WASP 0.8.0\" }";
    }
}
