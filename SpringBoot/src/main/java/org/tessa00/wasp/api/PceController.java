package org.tessa00.wasp.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tessa00.wasp.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Base64;

@RestController
public class PceController {

    private final TerminologyServerRequestsService terminologyServerRequestsService;
    private final TemplateGenerationPreprocessingService templateGenerationPreprocessingService;
    private final CodeSystemSupplementService codeSystemSupplementService;
    private final TemplateGenerationService templateGenerationService;
    private final SettingsService settingsService;
    private final TemplateGenerationCreateService templateGenerationCreateService;
    private final PreprocessingMrcmService preprocessingMrcmService;

    private Logger logger = LoggerFactory.getLogger(PceController.class);

    public PceController(TerminologyServerRequestsService terminologyServerRequestsService,
                         TemplateGenerationPreprocessingService templateGenerationPreprocessingService,
                         CodeSystemSupplementService codeSystemSupplementService,
                         TemplateGenerationService templateGenerationService,
                         SettingsService settingsService,
                         TemplateGenerationCreateService templateGenerationCreateService,
                         PreprocessingMrcmService preprocessingMrcmService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
        this.templateGenerationPreprocessingService = templateGenerationPreprocessingService;
        this.codeSystemSupplementService = codeSystemSupplementService;
        this.templateGenerationService = templateGenerationService;
        this.settingsService = settingsService;
        this.templateGenerationCreateService = templateGenerationCreateService;
        this.preprocessingMrcmService = preprocessingMrcmService;
    }

    @CrossOrigin
    @PostMapping("/upload")
    public String uploadingPost(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (MultipartFile file : uploadingFiles) {
                String fileName = file.getOriginalFilename();

                InputStream inputStream = file.getInputStream();
                String newLine = System.getProperty("line.separator");
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    if (!result.isEmpty()) {
                        result.append(newLine);
                    }
                    result.append(line);
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("fileName", fileName);
                jsonObject.put("content", result.toString());
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        } catch (Exception ignored) {
        }
        return null;
    }

    @CrossOrigin
    @GetMapping(value = "/templatecontent")
    public String getTemplateName(@RequestParam String url, @RequestParam String version, @RequestParam String template) throws JSONException, IOException {
        TemplateProcessingService templateProcessingService = new TemplateProcessingService(terminologyServerRequestsService);
        JSONObject jsonObject = templateProcessingService.init(url, version, template);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/attributevaluesfilterlimitfsn")
    public String getAttributeValueRequestLimitFSN(@RequestParam String url, @RequestParam String expression, @RequestParam String filter, @RequestParam String count, @RequestParam String version) throws JSONException, IOException {
        JSONObject jsonObject = terminologyServerRequestsService.findDescendantsWithFilterLimitFSN(url, expression, filter, count, version);
        return jsonObject.toString();
    }


    @CrossOrigin
    @PostMapping("/focusconceptattributevaluesfilterlimitfsn")
    public String getFocusConceptAttributeValueRequestLimitFSN(@RequestBody String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        String expression = templateGenerationService.getConstrainFocusConcept(jsonObject.getString("conceptmodel"));
        JSONObject jsonObjectResult = terminologyServerRequestsService.findDescendantsWithFilterLimitFSN(jsonObject.getString("url"), expression, jsonObject.getString("filter"), jsonObject.getString("count"), jsonObject.getString("version"));
        return jsonObjectResult.toString();
    }


    @CrossOrigin
    @GetMapping(value = "/attributevalueswithoutfilter")
    public String getAttributeValueRequestX(@RequestParam String url,@RequestParam String expression, @RequestParam String version) throws JSONException, IOException {
        JSONObject jsonObject = terminologyServerRequestsService.findDescendantsWithoutFilter(url,expression, version);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/templatelanguage")
    public String getTemplateLanguage(@RequestParam String template) throws JSONException, IOException {
        TemplateProcessingService templateProcessingService = new TemplateProcessingService(terminologyServerRequestsService);
        JSONObject jsonObject = templateProcessingService.getTemplateLanguage(template);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/lookupname")
    public String getConceptName(@RequestParam String url, @RequestParam String version, @RequestParam String code) throws JSONException, IOException {
        String name = terminologyServerRequestsService.lookUpName(url, version, code);
        JSONObject jsonObject = new JSONObject("{\"name\":\"" + name +  "\"}");
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/lookupnamefsn")
    public String getConceptNameFsn(@RequestParam String url, @RequestParam String version, @RequestParam String code) throws JSONException, IOException {
        String name = terminologyServerRequestsService.lookUpNameFsn(url, version, code);
        JSONObject jsonObject = new JSONObject("{\"name\":\"" + name +  "\"}");
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/holeconstraint")
    public String getHoleConstraint(@RequestParam String template) throws JSONException, IOException {
        TemplateProcessingService templateProcessingService = new TemplateProcessingService(terminologyServerRequestsService);
        JSONObject jsonObject = templateProcessingService.getHoleConstraint(template);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/updatenewcodesystemsupplement")
    public String updateCodeSystemSupplementX(@RequestParam String url, @RequestParam String sctversion, @RequestParam String pce, @RequestParam String pcename ,  @RequestParam String templatename, @RequestParam String id, @RequestParam String namecss) throws JSONException, IOException {
        JSONObject jsonObject = codeSystemSupplementService.updateCodeSystemSupplement(url, sctversion, pce, pcename, templatename, id, namecss);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/updateexistingcodesystemsupplement")
    public String updateExistingCodeSystemSupplement(@RequestParam String url, @RequestParam String sctversion, @RequestParam String pce, @RequestParam String pcename ,  @RequestParam String templatename, @RequestParam String idcss, @RequestParam String title) throws JSONException, IOException {
        JSONObject jsonObject = codeSystemSupplementService.updateCodeSystemSupplementWithStoredPce(url, "-1", pce, pcename, templatename, idcss, sctversion, "false", title);
        return jsonObject.toString();
    }



    @CrossOrigin
    @GetMapping(value = "/getallcodesystemsupplement")
    public String getAllCodeSystemSupplement(@RequestParam String url) throws JSONException, IOException {
         JSONObject jsonObject = codeSystemSupplementService.getCsSupplements(url);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/getcodesystemsupplement")
    public String getCodeSystemSupplement(@RequestParam String url) throws JSONException, IOException {
        JSONArray jsonArray = terminologyServerRequestsService.getCodeSystemSupplement(url);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", jsonArray);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/updatecodesystemsupplementwithstoredpce")
    public String updateCodeSystemSupplement(@RequestParam String url, @RequestParam String id, @RequestParam String pce, @RequestParam String name, @RequestParam String templatename, @RequestParam String namecssupplement, @RequestParam String sctversion, @RequestParam String samecs, @RequestParam String title) throws JSONException, IOException {
        JSONObject jsonObject = codeSystemSupplementService.updateCodeSystemSupplementWithStoredPce(url, id, pce, name, templatename, namecssupplement, sctversion, samecs, title);
        return jsonObject.toString();
    }

    // TEMPLATE GENERATION

    @CrossOrigin
    @GetMapping(value = "/getmatchingconceptmodel")
    public String getConceptModels(@RequestParam String url, @RequestParam String version, @RequestParam String code, @RequestParam String conceptmodel) throws JSONException, IOException {
        JSONObject jsonObject = templateGenerationService.getMatchingConceptModel(url, version, code,conceptmodel);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/getsubsumption")
    public String getSubsumption(@RequestParam String url, @RequestParam String version, @RequestParam String codea, @RequestParam String codeb) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(terminologyServerRequestsService.getSubsumesRelation(url, version, codea, codeb));
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/createnewtemplate")
    public String buildTemplate(@RequestParam String name, @RequestParam String pce, @RequestParam String term) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(templateGenerationCreateService.createTemplate(name, pce, term));
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/downloadtemplate")
    public String downloadTemplate(@RequestParam String name, @RequestParam String pce, @RequestParam String term) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(templateGenerationCreateService.downloadTemplate(name, pce, term));
        return jsonObject.toString();
    }



    // ----- SETTINGS

    @CrossOrigin
    @GetMapping(value = "/settingsurl")
    public String testUrl(@RequestParam String url) throws JSONException, IOException {
        JSONObject jsonObject = settingsService.init(url);
        return jsonObject.toString();
    }

    @CrossOrigin
    @GetMapping(value = "/settingssnomedversions")
    public String getSnomedVersions(@RequestParam String url) throws JSONException, IOException {
        JSONObject jsonObject = settingsService.getSnomedVersions(url);
        if (jsonObject.has("message") && !jsonObject.getString("message").isBlank()) {
            logger.warn("Message while requesting SNOMED CT versions from {}: {}", url, jsonObject.getString("message"));
        }
        return jsonObject.toString();
    }

    @CrossOrigin
    @PostMapping("/preprocessingtemplategeneration")
    public String getPreprocessedCm(@RequestBody String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        System.out.println("> " + jsonObject.getString("version"));
        return templateGenerationPreprocessingService.init(jsonObject.getString("url"), jsonObject.getString("version"), jsonObject.getString("code"), new ArrayList<>(), jsonObject.getString("conceptmodel"));
    }

    @CrossOrigin
    @PostMapping(value = "/preprocessingtemplategenerationupdate")
    public String update (@RequestBody String json) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(json);
        JSONArray arr = jsonObject.getJSONArray("listnumber");
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            arrayList.add(arr.getString(i));
        }
        return templateGenerationPreprocessingService.init(jsonObject.getString("url"), jsonObject.getString("version"), jsonObject.getString("code"), arrayList, jsonObject.getString("conceptmodel"));
    }

    @CrossOrigin
    @GetMapping(value = "/subsumptionproduct")
    public String getSubsumptionProduct(@RequestParam String url, @RequestParam String version, @RequestParam String code) throws JSONException, IOException {
        return terminologyServerRequestsService.testSubsumptionProduct(url, version, code);
    }

    @CrossOrigin
    @GetMapping(value = "/validatepce")
    public String validatePce(@RequestParam String url, @RequestParam String pce, @RequestParam String version) throws JSONException, IOException {
        return terminologyServerRequestsService.validatePce(pce, url, version);
    }

    // ----- PROCESSING MRCM
    @CrossOrigin
    @PostMapping(value = "/processingmrcm")
    public String processingMrcm(@RequestBody String json) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(json);
        return preprocessingMrcmService.start(jsonObject.getString("url"), jsonObject.getString("version"), jsonObject.getString("mrcm"));
    }




    @CrossOrigin
    @PostMapping("/postbody")
    public String postBody(@RequestBody String json) {
        return json;
    }



}
