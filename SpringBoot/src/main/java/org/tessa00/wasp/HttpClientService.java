package org.tessa00.wasp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.http.HttpClient;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Duration;


@Service
public class HttpClientService {

    private static final Logger logger = LoggerFactory.getLogger(TerminologyServerRequestsService.class);
    private final HttpClient client;

    public HttpClientService(MTlsConfiguration mTlsConfiguration) throws UnrecoverableKeyException, CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        this.client = configureHttpClient(mTlsConfiguration);
    }

    private HttpClient configureHttpClient(MTlsConfiguration mtlsConfiguration) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        SSLContext sslContext = null;
        if (mtlsConfiguration.isConfigured()) {
            logger.info("Configuring mTLS access using the certificate at {}", mtlsConfiguration.getCertificatePath());
            KeyStore keyStore = KeyStore.getInstance(mtlsConfiguration.getKeystoreType());
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            try (FileInputStream fis = new FileInputStream(mtlsConfiguration.getCertificatePath().toFile())) {
                keyStore.load(fis, mtlsConfiguration.getCertificatePassword().toCharArray());
            }
            keyManagerFactory.init(keyStore, mtlsConfiguration.getCertificatePassword().toCharArray());
            sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
        } else {
            logger.info("mTLS for terminology server connections is not configured");
        }

        HttpClient.Builder builder = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .connectTimeout(Duration.ofSeconds(10));
        if (sslContext != null) {
            builder.sslContext(sslContext);
        }
        return builder.build();
    }

    public HttpClient getClient() {
        return client;
    }
}
