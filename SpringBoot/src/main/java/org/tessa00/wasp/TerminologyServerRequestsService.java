package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;


@Service
public class TerminologyServerRequestsService {

    private static final Logger logger = LoggerFactory.getLogger(TerminologyServerRequestsService.class);
    private final HttpClientService httpClient;

    public TerminologyServerRequestsService(HttpClientService httpClientService)  {
        this.httpClient = httpClientService;
    }



    public String lookUpName(String url, String version, String code) {
        String urlNew = url + "/CodeSystem/$lookup?system=http://snomed.info/sct&code=" + code.replace("[[+id(<<", "").replace("[[", "").replace("]]", "") + "&version=" + version;
        if(!code.isEmpty()) {
            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .GET()
                        .uri(URI.create(urlNew))
                        .build();
                HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
                JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
                for (int j = 0; j < jsonArray.length(); j++) {
                    JSONObject json = (JSONObject) jsonArray.get(j);
                    JSONArray parameter = (JSONArray) json.get("parameter");
                    JSONObject jsonParameter = (JSONObject) parameter.get(1);
                    return jsonParameter.getString("valueString");
                }
            } catch (Exception e) {
                return "";
            }
        }
        return "";
    }

    public ArrayList<ArrayList<String>> lookUpNameNormalForm(String url, String version, String code) {
        String urlNew = url + "/CodeSystem/$lookup?system=http://snomed.info/sct&code=" + code + "&property=*&version=" + version;

        ArrayList<ArrayList<String>> arrayList = new ArrayList<>();


        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);
                JSONArray parameter = (JSONArray) json.get("parameter");
                for(int i = 0; i < parameter.length(); i++) {
                    json = (JSONObject) parameter.get(i);
                    if(json.toString().contains("property")) {
                        JSONArray part = (JSONArray) json.get("part");
                        json = (JSONObject) part.get(0);
                        if(json.get("valueCode").toString().matches("[0-9]+")) {
                            arrayList.add(new ArrayList<>());

                            for(int k = 0; k < part.length(); k++) {
                                if(part.get(k).toString().contains("part")) {
                                    json = (JSONObject) part.get(k);
                                    JSONArray part2 = (JSONArray) json.get("part");

                                    json = (JSONObject) part2.get(0);
                                    String attributeName = (String) json.get("valueCode");
                                    json = (JSONObject) part2.get(1);

                                    String attributeValue = "";

                                    if (json.toString().contains("valueCode")) {
                                        attributeValue = (String) json.get("valueCode") + "@code";
                                    } else if (json.toString().contains("valueDecimal")) {
                                        attributeValue = json.get("valueDecimal").toString() + "@dec";
                                    } else if (json.toString().contains("valueInteger")) {
                                        attributeValue = json.get("valueInteger").toString() + "@int";
                                    }
                                    arrayList.get(arrayList.size() - 1).add(attributeName + "@" + attributeValue);
                                } else if(!part.get(k).toString().contains("609096000") && k < part.length()-2){
                                    json = (JSONObject) part.get(0);
                                    String attributeName = (String) json.get("valueCode");
                                    String s = getSubsumesRelation(url, version, "762705008", json.getString("valueCode"));
                                    json = (JSONObject) part.get(1);
                                    if(s.equals("subsumes") || s.equals("equivalent")) {
                                        String attributeValue = "";
                                        if (json.toString().contains("valueCode")) {
                                            attributeValue = (String) json.get("valueCode") + "@code";
                                        } else if (json.toString().contains("valueDecimal")) {
                                            attributeValue = json.get("valueDecimal").toString() + "@dec";
                                        } else if (json.toString().contains("valueInteger")) {
                                            attributeValue = json.get("valueInteger").toString() + "@int";
                                        }
                                        logger.info("----> {} {}", attributeName, attributeValue);
                                        arrayList.get(arrayList.size() - 1).add(attributeName + "@" + attributeValue);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ArrayList<ArrayList<String>> result = new ArrayList<>();
            for(int i = 0; i < arrayList.size(); i++) {
                if(!arrayList.get(i).isEmpty()) {
                    result.add(arrayList.get(i));
                }
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public String lookUpNameFsn(String url, String version, String code) {
        String valueString = "";
        String urlNew = url + "/CodeSystem/$lookup?system=http://snomed.info/sct&code=" + code + "&property=designation&version=" + version;
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);
                if(json.has("parameter")) {
                    JSONArray parameter = (JSONArray) json.get("parameter");
                    for(int i = 0; i < parameter.length(); i++) {
                        json = (JSONObject) parameter.get(i);
                        if (json.get("name").toString().equals("designation")) {
                            JSONArray part = (JSONArray) json.get("part");
                            for (int k = 0; k < part.length(); k++) {
                                json = (JSONObject) part.get(k);

                                if (json.toString().contains("valueCoding")) {
                                    json = (JSONObject) json.get("valueCoding");
                                    if (json.toString().contains("code")) {
                                        if (json.get("code").toString().equals("900000000000003001")) {
                                            json = (JSONObject) part.get(k + 1);
                                            valueString = (String) json.get("valueString");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return valueString;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public JSONObject findDescendantsWithFilterLimitFSN(String url, String expression, String filter, String count, String version) {
        String urlNew = url + "/ValueSet/$expand?filter=" + URLEncoder.encode(filter, StandardCharsets.UTF_8) + "&url=" +
                "http://snomed.info/sct?fhir_vs=ecl/" + URLEncoder.encode(expression, StandardCharsets.UTF_8) + "&count=" + URLEncoder.encode(count, StandardCharsets.UTF_8) +
                "&version=" + version +
                "&includeDesignations=true";

        JSONObject jsonObject = new JSONObject();

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            jsonObject = new JSONObject(response.body());
            jsonObject =preprocessing(jsonObject);
        } catch (Exception ignored) {
        }

        return jsonObject;

    }

    private JSONObject preprocessing(JSONObject jsonObject) throws JSONException {
        String json = "{\"values\":[";
        JSONObject expansion = (JSONObject) jsonObject.get("expansion");
        JSONArray contains = (JSONArray) expansion.get("contains");

        for(int i = 0; i < contains.length(); i++) {
            json = json + "{";
            JSONObject jo = contains.getJSONObject(i);
            String code = (String) jo.get("code");
            json = json + "\"code\":\"" + code + "\",";
            JSONArray designation = (JSONArray) jo.get("designation");
            for(int j = 0; j < designation.length(); j++) {
                String s = designation.get(j).toString();
                if(s.contains("900000000000003001")) {
                    jo = (JSONObject) designation.get(j);
                    String fsn = (String) jo.get("value");
                    json = json + "\"fsn\":\"" + fsn + "\"";
                }
            }
            json = json + "},";
        }
        json = json.substring(0, json.length()-1);
        json = json + "]}";
        return new JSONObject(json);
    }

    public JSONObject findDescendantsWithoutFilter(String url, String expression, String version) {
        String urlNew = url + "/ValueSet/$expand?url=" +
                "http://snomed.info/sct?fhir_vs=ecl/" + URLEncoder.encode(expression, StandardCharsets.UTF_8) +
                "&version=" + version;

        JSONObject jsonObject = new JSONObject();

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            jsonObject = new JSONObject(response.body());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    public JSONArray getCodeSystemSupplement(String url) {
        JSONArray jsonArray = new JSONArray();

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(url))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            jsonArray = new JSONArray("[" + response.body() + "]");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public String updateCodeSystemSupplement(String json, String url) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .PUT(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/fhir+json")
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.toString().contains("200") || response.toString().contains("201") || response.body().contains("Duplicate code")) {
                return "success";
            } else if(response.toString().contains("403")) {
                return "error@ 403 Forbidden!";
            } else if(response.toString().contains("404")) {
                return "error@ 403 Not Found!";
            }
            if(response.body().contains("This service uses the Deutsches Forschungsnetz Public Key Infrastructure")) {
                return "error@A certificate of the Deutsches Forschungsnetz is required!";
            }
            JSONObject jsonResponse = new JSONObject(response.body());
            JSONArray issue = jsonResponse.getJSONArray("issue");
            String diagnostics = issue.getJSONObject(0).getString("diagnostics");
            return "error" + "@" + diagnostics;


        } catch (Exception ignored) {
        }
        return "error";
    }


    public String getSubsumesRelation(String url, String version, String codeDomain, String codeFocusConcept) {
        String value = "";
        String urlNew = url + "/CodeSystem/$subsumes?" +
                "system=http://snomed.info/sct&codeA=" + codeDomain + "&codeB=" + codeFocusConcept + "&version=" + version;
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);

                JSONArray parameter = (JSONArray) json.get("parameter");
                json = (JSONObject) parameter.get(0);

                value = (String) json.get("valueCode");
                break;

            }
        } catch (Exception e) {

        }
        return value;
    }


    public String testSubsumptionProduct(String url, String version, String code) {
        String json = "{\"result\":";
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("781405001");
        arrayList.add("373873005");
        for(int i = 0; i < arrayList.size(); i++) {
            String result = getSubsumesRelation(url, version, arrayList.get(i), code);
            if(result.equals("subsumes") || result.equals("equivalent")) {
                return json + "\"" + result + "\"}";
            }
        }
        return json + "\"not-subsumed\"}";
    }


    public String validatePce(String pce, String url, String version) {
        String urlNew = url + "/CodeSystem/$validate-code";

        String jsonResult = "{\"result\":";

        String json = "{\n" +
                "    \"resourceType\": \"Parameters\",\n" +
                "    \"parameter\": [\n" +
                "        {\n" +
                "            \"name\": \"url\",\n" +
                "            \"valueUri\": \"http://snomed.info/sct\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"coding\",\n" +
                "            \"valueCoding\": {\n" +
                "                \"system\": \"http://snomed.info/sct\",\n" +
                "                \"version\": \"" + version + "\",\n" +
                "                \"code\": \"" + pce + "\"\n" +
                "            }\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(urlNew))
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .header("Content-Type", "application/fhir+json")
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONArray parameter = jsonObject.getJSONArray("parameter");
            jsonObject = parameter.getJSONObject(0);
            if(jsonObject.toString().contains("\"valueBoolean\":true")) {
                return  jsonResult + "\"" + "True" + "\"}";
            } else {
                jsonObject = parameter.getJSONObject(3);
                String x = (String) jsonObject.get("valueString");
                return  jsonResult + "\"" + x + "\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }


}
