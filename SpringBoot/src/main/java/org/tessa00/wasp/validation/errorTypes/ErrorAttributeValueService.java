package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

@Service
public class ErrorAttributeValueService {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;

    public ErrorAttributeValueService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
    }

    public JSONObject errorAttributeValue(String error, String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String focusConcept) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        error = error.replace("MRCM: ", "");
        String attributeValue = error.split("is not in range for attribute")[0];
        String attributeName = error.split("is not in range for attribute")[1];
        String newAttributeValue;
        String newAttributeName;
        String codeValue;
        String codeName;

        if(attributeValue.contains("|")) {
            codeValue = attributeValue.split("\\|")[0].replace(" ", "");
        } else {
            codeValue = attributeValue.replace(" ", "");
        }
        if(attributeName.contains("|")) {
            codeName = attributeName.split("\\|")[0].replace(" ", "");
        } else {
            codeName = attributeName.replace(" ", "");
        }
        newAttributeValue = codeValue + " |" + terminologyServerRequest.lookUpNameFSN(url, version, codeValue) + "|";
        error = error.replace(attributeValue, newAttributeValue  + " ");
        newAttributeName = codeName + " |" + terminologyServerRequest.lookUpNameFSN(url, version, codeName) + "|";


        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        if(!newAttributeName.contains("(attribute)")) {
            error = newAttributeName + " is not a valid attribute.";
            jsonObjectInfo.put("answer", "In SNOMED CT over 120 different attributes exist that can be used to represent medical facts. The concept " + newAttributeName + " in the PCE is not a valid attribute.");
        } else {
            error = error.replace(attributeName, " " + newAttributeName);
            jsonObjectInfo.put("answer", "A value range is a subset of SNOMED CT concepts that are valid for an attribute. " +
                    "The attribute value must be selected from this value range. " +
                    "This guarantees only meaningful attribute relations in terms of content or medicine. " +
                    "In the PCE shown, the attribute value " + newAttributeValue + " is not contained in the value range of attribute " + newAttributeName + ".");
        }


        error = "Not MRCM compatible: " + error;
        jsonObject.put("result", error);

        JSONObject jsonObjectRange = new JSONObject();
        jsonObjectRange.put("question", "Which value range has the SNOMED CT attribute?");
        jsonObjectRange.put("answer", getValueRange(attributeName, newAttributeName, jsonObjectConceptModelAttributes));


        JSONObject jsonObjectMatchingAttribute = new JSONObject();
        jsonObjectMatchingAttribute.put("question", "Which SNOMED CT attribute matches the selected attribute value?");
        jsonObjectMatchingAttribute.put("answer", workerService.getMatchingAttribute(newAttributeName, attributeValue, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm, url, version, focusConcept));

        jsonObject.put("info-general", jsonObjectInfo);
        jsonObject.put("info-attribute", jsonObjectRange);
        jsonObject.put("info-matching-attribute", jsonObjectMatchingAttribute);

        return jsonObject;
    }

    private String getAttributeValueRange(String attributeName, JSONObject jsonObjectConceptModelAttributes) throws JSONException {
        String code;
        if(attributeName.contains("|")) {
            code = attributeName.split("\\|")[0].replace(" ", "");
        } else {
            code = attributeName.replace(" ", "");
        }
        JSONArray jsonArrayCodes = jsonObjectConceptModelAttributes.getJSONArray("conceptModelCodes");
        JSONArray jsonArrayConstraint = jsonObjectConceptModelAttributes.getJSONArray("conceptModelConstraint");
        for (int i = 0; i < jsonArrayCodes.length(); i++) {
            if(jsonArrayCodes.getString(i).equals(code) && jsonArrayCodes.getString(i) != null) {
                return jsonArrayConstraint.getString(i).replace("\r", "").replace("\n", "");
            }
        }
        return "";
    }

    private String getAttributeDefinition(String attributeName, JSONObject jsonObjectConceptModelAttributes) throws JSONException {
        String code;
        if(attributeName.contains("|")) {
            code = attributeName.split("\\|")[0].replace(" ", "");
        } else {
            code = attributeName.replace(" ", "");
        }
        JSONArray jsonArrayCodes = jsonObjectConceptModelAttributes.getJSONArray("conceptModelCodes");
        JSONArray jsonArrayDefinition = jsonObjectConceptModelAttributes.getJSONArray("conceptModelDefinition");
        for (int i = 0; i < jsonArrayCodes.length(); i++) {
            if(jsonArrayCodes.getString(i).equals(code) && jsonArrayCodes.getString(i) != null) {
                return jsonArrayDefinition.getString(i);
            }
        }
        return "";
    }

    private JSONArray getValueRange(String attributeName, String newAttributeName, JSONObject jsonObjectConceptModelAttributes) throws JSONException {
        JSONArray jsonArrayValues = new JSONArray();
        JSONObject jsonObjectValue = new JSONObject();
        jsonObjectValue.put("range", getAttributeValueRange(attributeName, jsonObjectConceptModelAttributes));
        jsonObjectValue.put("definition", getAttributeDefinition(attributeName, jsonObjectConceptModelAttributes));
        jsonObjectValue.put("attribute", newAttributeName);
        jsonObjectValue.put("is_attribute", !getAttributeValueRange(attributeName, jsonObjectConceptModelAttributes).isEmpty());
        jsonArrayValues.put(jsonObjectValue);
        return jsonArrayValues;
    }




}
