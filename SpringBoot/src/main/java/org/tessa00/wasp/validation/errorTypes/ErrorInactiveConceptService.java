package org.tessa00.wasp.validation.errorTypes;

import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.r4.model.ValueSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class ErrorInactiveConceptService {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;
    private final ArrayList<String> histroyRefSetList = new ArrayList<>();

    public ErrorInactiveConceptService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;

        histroyRefSetList.add("900000000000527005 |SAME AS association reference set|");
        histroyRefSetList.add("900000000000526001 | REPLACED BY association reference set|");
        histroyRefSetList.add("900000000000528000 | WAS A association reference set|");
    }

    public JSONObject errorInactiveConcepts(Parameters parameters, String url, String version, String pce) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        ArrayList<String> arrayListInactiveConcepts = getInactiveConcepts(parameters);
        HashMap<String, ArrayList<String>> hashMapActiveConcepts = getCorrespondingActiveConcepts(arrayListInactiveConcepts, url, version);
        JSONArray jsonArrayAlternatives = workerService.checkPCE(hashMapActiveConcepts, pce, url, version);

        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        if(arrayListInactiveConcepts.size() == 1) {
            jsonObjectInfo.put("answer", "One of the precoordinated concepts is inactivated in the SNOMED CT version and edition used (" + version + ").");
        } else {
            jsonObjectInfo.put("answer", arrayListInactiveConcepts.size() + " precoordinated concepts of the PCE are inactivated in the SNOMED CT version and edition used (" + version + ").");
        }

        JSONObject jsonObjectAlternative = new JSONObject();
        jsonObjectAlternative.put("question", "Can the inactivated concepts be replaced?");
        if(jsonArrayAlternatives.length() > 0) {
            jsonObjectAlternative.put("answer", jsonArrayAlternatives);
            jsonObjectAlternative.put("message", "Replacement possible to create a syntactically and semantically correct PCE.");
            jsonObjectAlternative.put("boolean_replace", true);
        }else {
            jsonObjectAlternative.put("message", "Replacement not possible because of other errors! Suggestion: Create a new PCE?");
            jsonObjectAlternative.put("boolean_replace", false);
            for (String key: hashMapActiveConcepts.keySet()) {
                for (String value: hashMapActiveConcepts.get(key)) {
                    jsonArrayAlternatives.put(key + "->" + value);
                }
            }
            jsonObjectAlternative.put("answer", jsonArrayAlternatives);
        }
        ArrayList<String> arrayListWrongCodes = new ArrayList<>();
        for (String codes: arrayListInactiveConcepts) {
            arrayListWrongCodes.add(codes.split("@")[0]);
        }
        String replacedList = arrayListWrongCodes.toString().replace("[", "").replace("]", "");
        if(arrayListWrongCodes.size() == 1) {
            jsonObject.put("result", "The following SNOMED CT concept isn't included in the SNOMED edition and version being used: " + replacedList + ".");
        } else {
            jsonObject.put("result", "The following SNOMED CT concepts aren't included in the SNOMED edition and version being used: " + replacedList + ".");

        }

        jsonObject.put("info-general", jsonObjectInfo);
        jsonObject.put("info-alternatives", jsonObjectAlternative);

        System.out.println(jsonObject);
//        String s = jsonObject.getString("result") + "\n";
//        try {
//            Files.write(Paths.get("C:\\Users\\Tessa\\Downloads\\result.txt"), s.getBytes(), StandardOpenOption.APPEND);
//        }catch (IOException e) {
//            //exception handling left as an exercise for the reader
//            e.printStackTrace();
//        }
        return jsonObject;
    }


    private ArrayList<String> getInactiveConcepts(Parameters parameters) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (Parameters.ParametersParameterComponent param : parameters.getParameter()) {
            if ("component".equals(param.getName())) {
                boolean isInactive = false;
                String code = null;
                for (Parameters.ParametersParameterComponent part : param.getPart()) {
                    if ("inactive".equals(part.getName()) && part.getValue() instanceof BooleanType) {
                        isInactive = ((BooleanType) part.getValue()).booleanValue();
                    }
                    if ("code".equals(part.getName()) && part.getValue() instanceof StringType) {
                        code = ((StringType) part.getValue()).getValue();
                    }
                }
                if (isInactive && code != null) {
                    arrayList.add(code);
                }
            }
        }
        return arrayList;
    }

    public HashMap<String, ArrayList<String>> getCorrespondingActiveConcepts(ArrayList<String> arrayListInactiveConcepts, String url, String version) {
        HashMap<String, ArrayList<String>> hashMap = new HashMap<>();
        for (String inactiveCode: arrayListInactiveConcepts) {
            System.out.println("--> " + histroyRefSetList);
            for (String refSet: histroyRefSetList) {
                ValueSet valueSet = terminologyServerRequest.getHistoryRelations(url, version, refSet, inactiveCode);
                if(valueSet.getExpansion().getTotal() == 1) {
                    for (ValueSet.ValueSetExpansionContainsComponent element: valueSet.getExpansion().getContains()) {
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add(element.getCode() + " |" + element.getDisplay() + "|");
                        hashMap.put(inactiveCode, arrayList);
                    }
                }
            }
        }
        return hashMap;
    }
}
