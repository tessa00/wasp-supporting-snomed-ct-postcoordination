package org.tessa00.wasp.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.ValueSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class PreprocessingValueSet {

    FhirContext fhirContext = FhirContext.forR4();
    IParser parser = fhirContext.newJsonParser();
    private final ProcessingQuestionnaire processingQuestionnaire = new ProcessingQuestionnaire();

    public PreprocessingValueSet() {
    }

    public JSONObject extractPcesValueSet(String content) throws JSONException {
        ValueSet valueSet = parser.parseResource(ValueSet.class, content);
        JSONObject jsonObject = getMetaDataValueSet(valueSet);
        JSONArray jsonArray = extractCodingItems(valueSet);
        jsonObject.put("codes", jsonArray);
        return jsonObject;
    }

    private JSONObject getMetaDataValueSet(ValueSet valueSet) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", valueSet.getId());
        jsonObject.put("title", valueSet.getTitle());
        jsonObject.put("name", valueSet.getName());
        jsonObject.put("version", valueSet.getVersion());
        jsonObject.put("description", valueSet.getDescription());
        return jsonObject;
    }

    private JSONArray extractCodingItems(ValueSet valueSet) throws JSONException {
        JSONArray jsonArray = new JSONArray();

        for (ValueSet.ConceptSetComponent include : valueSet.getCompose().getInclude()) {
            String system = include.getSystem();
            String versionValueSet = include.getVersion();
            if (include.getSystem().equals("http://snomed.info/sct")) {
                for (ValueSet.ConceptReferenceComponent concept : include.getConcept()) {
                    JSONObject jsonObjectCode = new JSONObject();
                    if(concept.getCode() != null) {
                        Boolean isPrecoordination = processingQuestionnaire.isPrecoordination(concept.getCode());
                        if(isPrecoordination) {
                            jsonObjectCode.put("code", concept.getCode());
                        } else {
                            jsonObjectCode.put("code", ProcessingQuestionnaire.processingCode(concept.getCode()));
                        }
                        jsonObjectCode.put("display", concept.getDisplay());
                        jsonObjectCode.put("system", system);
                        jsonObjectCode.put("version", versionValueSet);

                        jsonObjectCode.put("precoordination", isPrecoordination);

                        if(processingQuestionnaire.isUnique(jsonArray, jsonObjectCode)) {
                            jsonArray.put(jsonObjectCode);
                        }
                    }
                }
                include.getFilter().forEach(filter -> {
                    JSONObject jsonObjectCode = new JSONObject();
                    Boolean isPrecoordination = processingQuestionnaire.isPrecoordination(filter.getValue());
                    if(isPrecoordination) {
                        try {
                            jsonObjectCode.put("code", filter.getValue());
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    } else {
                        try {
                            jsonObjectCode.put("code", ProcessingQuestionnaire.processingCode(filter.getValue()));
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    try {
                        jsonObjectCode.put("display", "");
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    try {
                        jsonObjectCode.put("system", system);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    try {
                        jsonObjectCode.put("version", versionValueSet);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        }
        return jsonArray;
    }



}
