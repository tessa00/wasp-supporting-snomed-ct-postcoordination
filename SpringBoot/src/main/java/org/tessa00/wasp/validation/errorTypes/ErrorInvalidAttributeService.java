package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.PreprocessingPce;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.ArrayList;

@Service
public class ErrorInvalidAttributeService {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;
    PreprocessingPce preprocessingPce = new PreprocessingPce();

    public ErrorInvalidAttributeService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
    }


    public JSONObject errorInvalidAttribute(String error, String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String focusConcept, String pce) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        error = error.replace("MRCM: ", "");
        String attributeName = error.split("IS NOT a valid attribute for")[0];
        String focusConceptError = error.split("IS NOT a valid attribute for")[1];
        String newAttributeName;
        String newFocusconcept;

        String codeAttribute;
        String codeFocusConcept;

        if(attributeName.contains("|")) {
            codeAttribute = attributeName.split("\\|")[0].replace(" ", "");
        } else {
            codeAttribute = attributeName.replace(" ", "");
        }
        newAttributeName = codeAttribute + " |" + terminologyServerRequest.lookUpNameFSN(url, version, codeAttribute) + "|";
        error = error.replace(attributeName,  newAttributeName  + " ");
        if(attributeName.contains("|")) {
            codeFocusConcept = focusConceptError.split("\\|")[0].replace(" ", "");
        } else {
            codeFocusConcept = focusConceptError.replace(" ", "");
        }
        newFocusconcept = codeFocusConcept + " |" + terminologyServerRequest.lookUpNameFSN(url, version, codeFocusConcept) + "|";

        JSONArray jsonArrayPossibleAttributes = checkPossibleDomainAttributes(pce, codeAttribute, newAttributeName, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm, url, version, newFocusconcept);
        if(jsonArrayPossibleAttributes.length() > 0) {
            JSONObject jsonObjectMatchingAttribute = new JSONObject();
            jsonObjectMatchingAttribute.put("question", "Which attribute does the attribute value match of the focus concept domain?");
            jsonObjectMatchingAttribute.put("answer", jsonArrayPossibleAttributes);
            jsonObject.put("info-matching-attribute-domain", jsonObjectMatchingAttribute);
        } else {
            JSONObject jsonObjectMatchingAttribute = new JSONObject();
            jsonObjectMatchingAttribute.put("question", "Which domain does the attribute match?");
            jsonObjectMatchingAttribute.put("answer", getAttributeDomainResult(jsonObjectConceptModelAttributeMrcm, newAttributeName));
            jsonObject.put("info-matching-attribute-domain", jsonObjectMatchingAttribute);
        }
        error = error.replace(focusConceptError, " focus concept " + newFocusconcept);
        error = "Not MRCM compatible: " + error;
        jsonObject.put("result", error + ".");
        if(focusConcept.contains("+")) {
            focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
        }
        int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
        JSONArray jsonArrayMatchingDomain = workerService.getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectConceptModelAttributeMrcm, count);
        String domainNames = workerService.getDomainNames(jsonArrayMatchingDomain).toString().replace("\"", "");
        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        jsonObjectInfo.put("answer", "Each PCE or pre-coordinated concept can be assigned to a ConceptModel domain. These are usually sub-hierarchies of SNOMED CT. " +
                "To guarantee medically meaningful expressions, SNOMED CT attributes are defined for a domain, which can be used to create SNOMED CT expressions." +
                "The SNOMED CT attribute " + newAttributeName + " was used in the PCE shown, which is not allowed in the determined domain " + domainNames + ".");
        jsonObject.put("info-general", jsonObjectInfo);

        return jsonObject;
    }

    private JSONArray getMatchingConceptModelDomain(JSONObject jsonObjectConceptModelAttributeMrcm, String attributeCode) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < jsonObjectConceptModelAttributeMrcm.getJSONArray("conceptmodel").length(); i++) {
            JSONObject jsonObjectDomain = jsonObjectConceptModelAttributeMrcm.getJSONArray("conceptmodel").getJSONObject(i);
            String domainTemplatePrecoordination = jsonObjectDomain.getString("domainTemplateForPrecoordination");
            if(domainTemplatePrecoordination.contains(attributeCode)) {
                jsonArray.put(jsonObjectDomain);
            }
        }
        return jsonArray;
    }

    public JSONArray getAttributeDomainResult(JSONObject jsonObjectConceptModelAttributeMrcm, String newAttributeName) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArrayMatchingDomainAttribute = getMatchingConceptModelDomain(jsonObjectConceptModelAttributeMrcm, newAttributeName.split("\\|")[0].replace(" ", ""));
        JSONArray domainNamesAttribute = workerService.getDomainNames(jsonArrayMatchingDomainAttribute);

        JSONObject jsonObjectMatchingAttribute = new JSONObject();
        jsonObjectMatchingAttribute.put("domains", domainNamesAttribute);
        jsonObjectMatchingAttribute.put("suggestion", "Change focus concept.");

        jsonArray.put(jsonObjectMatchingAttribute);
        return jsonArray;
    }

    private ArrayList<String> getAttributeValue(String pce, String attribute) throws JSONException {
        ArrayList<String> arrayList = new ArrayList<>();
        JSONObject jsonObjectPce = new JSONObject(preprocessingPce.createJson(pce));
        if(jsonObjectPce.getJSONArray("withoutRoleGroup").length() > 0) {
            JSONArray jsonArrayWithoutRoleGroup = jsonObjectPce.getJSONArray("withoutRoleGroup");
            getAttributeValueHelper(attribute, arrayList, jsonArrayWithoutRoleGroup);
        }
        if(jsonObjectPce.getJSONArray("withRoleGroup").length() > 0) {
            JSONArray jsonArrayWithRoleGroup = jsonObjectPce.getJSONArray("withRoleGroup");
            for (int i = 0; i < jsonArrayWithRoleGroup.length(); i++) {
                JSONArray jsonArrayRoleGroup = jsonArrayWithRoleGroup.getJSONObject(i).getJSONArray("roleGroup");
                getAttributeValueHelper(attribute, arrayList, jsonArrayRoleGroup);
            }
        }
        return arrayList;
    }

    private void getAttributeValueHelper(String attribute, ArrayList<String> arrayList, JSONArray jsonArrayRoleGroup) throws JSONException {
        for (int j = 0; j < jsonArrayRoleGroup.length(); j++) {
            String attributeCode = jsonArrayRoleGroup.getJSONObject(j).getString("attributecode");
            String valueCode = jsonArrayRoleGroup.getJSONObject(j).getString("valuecode");
            if(attribute.equals(attributeCode)) {
                arrayList.add(valueCode);
            }
        }
    }

    private JSONArray checkPossibleDomainAttributes(String pce, String codeAttribute, String attributeName, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String url, String version, String focusConcept) throws JSONException {
        ArrayList<String> listCodeValue = getAttributeValue(pce, codeAttribute);
        JSONArray jsonArrayResult = new JSONArray();
        for (String string : listCodeValue) {
            JSONArray jsonArray = workerService.getMatchingAttribute(attributeName, string, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm, url, version, focusConcept);
            System.out.println(jsonArray);
            for (int j = 0; j < jsonArray.length(); j++) {
                if (jsonArray.getJSONObject(j).getBoolean("is-in-domain") && !jsonArrayResult.toString().contains(jsonArray.getJSONObject(j).toString())) {
                    jsonArrayResult.put(jsonArray.getJSONObject(j));
                }
            }
        }
       return jsonArrayResult;
    }

}
