package org.tessa00.wasp.validation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

@Service
public class WorkerService {

    private final TerminologyServerRequest terminologyServerRequest;

    public WorkerService(TerminologyServerRequest terminologyServerRequest) {
        this.terminologyServerRequest = terminologyServerRequest;
    }

    public JSONArray getMatchingAttribute(String attributeName, String attributeValue, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String url, String version, String focusConcept) throws JSONException {
        if(focusConcept.contains("+")) {
            focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
        }
        int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
        JSONArray jsonArrayMatchingDomain = getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectConceptModelAttributeMrcm, count);
        JSONArray jsonArrayResults = new JSONArray();
        JSONArray jsonArrayCodes = jsonObjectConceptModelAttributes.getJSONArray("conceptModelCodes");
        JSONArray jsonArrayNames = jsonObjectConceptModelAttributes.getJSONArray("conceptModelNames");
        JSONArray jsonArrayDefinition = jsonObjectConceptModelAttributes.getJSONArray("conceptModelDefinition");
        JSONArray jsonArrayConstraint = jsonObjectConceptModelAttributes.getJSONArray("conceptModelConstraint");
        for (int i = 0; i < jsonArrayConstraint.length(); i++) {
            String constraint = jsonArrayConstraint.getString(i).replace("\r", "").replace("\n", "").replace("\t", "");
            JSONObject jsonObject = terminologyServerRequest.eclRequest(url, "(" + constraint + ") AND " + attributeValue, version, false);
            if(jsonObject.has("total")) {
                int total = jsonObject.getInt("total");
                if(total != 0) {
                    boolean isAttributeInDomain = isAttributeInDomain(jsonArrayMatchingDomain, jsonArrayCodes.getString(i));
                    JSONObject jsonObjectAttribute = new JSONObject();
                    jsonObjectAttribute.put("code", jsonArrayCodes.getString(i));
                    jsonObjectAttribute.put("name", jsonArrayNames.getString(i));
                    jsonObjectAttribute.put("definition", jsonArrayDefinition.getString(i));
                    jsonObjectAttribute.put("constraint", constraint);
                    jsonObjectAttribute.put("is-in-domain", isAttributeInDomain);

                    if(isAttributeInDomain) {
                        String newAttribute = jsonArrayCodes.getString(i) + " |" + jsonArrayNames.getString(i) + "|";
                        jsonObjectAttribute.put("suggestion", "Replace the attribute " + attributeName + " used with attribute " + newAttribute + " to ensure a syntactically and semantically correct PCE.");
                    } else {
                        String attributeValueComplete = attributeValue.replace(" ", "") + " |" + terminologyServerRequest.lookUpNameFSN(url, version, attributeValue) + "|";
                        String domains = getDomainNames(jsonArrayMatchingDomain).toString().replace("\"", "");
                        jsonObjectAttribute.put("suggestion", "Selected attribute value " + attributeValueComplete + " does not match with any value range of an attribute of the ConceptModel domain of the focus concept " + domains + ". Change the attribute value or create a new PCE that can contain the selected attribute value.");
                    }
                    jsonArrayResults.put(jsonObjectAttribute);
                }
            }
        }
        for (int i = 0; i < jsonArrayResults.length(); i++) {
            if(jsonArrayResults.getJSONObject(i).getBoolean("is-in-domain")) {
                JSONArray jsonArray = new JSONArray();
                for (int j = 0; j < jsonArrayResults.length(); j++) {
                    if(jsonArrayResults.getJSONObject(j).getBoolean("is-in-domain")) {
                        jsonArray.put(jsonArrayResults.getJSONObject(j));
                    }
                }
                return jsonArray;
            }
        }

        return jsonArrayResults;
    }

    public JSONArray getDomainNames(JSONArray jsonArrayMatchingDomain) throws JSONException {
        JSONArray domainNames = new JSONArray();
        for (int i = 0; i < jsonArrayMatchingDomain.length(); i++) {
            String domainConstraint = jsonArrayMatchingDomain.getJSONObject(i).getString("domainConstraint");
            String domain = domainConstraint.replace("<", "").replace("^", "").split("\\|")[1];
            domainNames.put(domain);
        }
        return domainNames;
    }

    public JSONArray getMatchingConceptModelDomain(String url, String version, String code, JSONObject jsonObjectMrcm, int idx) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArrayMRCM = jsonObjectMrcm.getJSONArray("conceptmodel");
        for (int i = 0; i < jsonArrayMRCM.length(); i++) {
            JSONObject jsonObjectDomainConstraint = jsonArrayMRCM.getJSONObject(i);
            String domainConstraint = jsonObjectDomainConstraint.getString("domainConstraint");
            JSONObject jsonObject = terminologyServerRequest.eclRequest(url, "(" + domainConstraint + ")AND" + code, version, false);
            if(jsonObject.has("total")) {
                int total = jsonObject.getInt("total");
                if(total == idx) {
                    jsonArray.put(jsonObjectDomainConstraint);
                }
            }
        }
        return jsonArray;
    }

    public boolean isAttributeInDomain(JSONArray jsonArrayMatchingDomain, String attributeCode) throws JSONException {
        for (int i = 0; i < jsonArrayMatchingDomain.length(); i++) {
            JSONArray jsonArray = jsonArrayMatchingDomain.getJSONObject(i).getJSONArray("domainTemplateForPrecoordination");
            JSONObject jsonObjectDomain = jsonArray.getJSONObject(0);
            String init = jsonObjectDomain.getString("init");
            return init.contains(attributeCode);
        }
        return false;
    }

    public JSONArray checkPCE(HashMap<String, ArrayList<String>> replaceConcepts, String pce, String url, String version) throws JSONException {
        ArrayList<String> keys = new ArrayList<>(replaceConcepts.keySet());
        return combineValues(replaceConcepts, keys, 0, new ArrayList<>(), pce, url, version, new JSONArray());
    }

    public JSONArray combineValues(HashMap<String, ArrayList<String>> map, ArrayList<String> keys, int index, List<String> currentCombination, String pce, String url, String version, JSONArray jsonArrayAlternatives) throws JSONException {
        if (index == keys.size()) {
            String newPce = pce;
            JSONArray newCombination = new JSONArray();
            boolean validation = false;
            for (int i = 0; i < keys.size(); i++) {
                String key = keys.get(i);
                String value;
                if(pce.contains("|")) {
                    value = currentCombination.get(i);
                } else {
                    value = currentCombination.get(i).split("\\|")[0];
                }
                String regex = "(?<!\\d)" + key + "(?!\\d)";
                newPce = newPce.replaceAll(regex, Matcher.quoteReplacement(value));
                JSONObject jsonObjectValidationResult = terminologyServerRequest.validateCode(newPce, url, version);
                if(jsonObjectValidationResult.has("result")) {
                    validation = jsonObjectValidationResult.getBoolean("result");
                }
                newCombination.put(key + "->" + currentCombination.get(i));

                if(validation) {
                    jsonArrayAlternatives.put(newCombination);
                }
            }
            return jsonArrayAlternatives;
        }
        String key = keys.get(index);
        List<String> values = map.get(key);
        for (String value : values) {
            currentCombination.add(value);
            combineValues(map, keys, index + 1, currentCombination, pce, url, version, jsonArrayAlternatives);
            currentCombination.remove(currentCombination.size() - 1);
        }
        return jsonArrayAlternatives;
    }


}
