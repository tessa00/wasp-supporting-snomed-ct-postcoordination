package org.tessa00.wasp.validation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class PreprocessingResource {

    private final TerminologyServerRequest terminologyServerRequest;

    public PreprocessingResource(TerminologyServerRequest terminologyServerRequest) {
        this.terminologyServerRequest = terminologyServerRequest;
    }


    public JSONObject extractPces(String content, boolean isCsv) throws JSONException {
        if(isQuestionnaire(content)) {
            ProcessingQuestionnaire processingQuestionnaire = new ProcessingQuestionnaire();
            return processingQuestionnaire.extractPcesQuestionnaire(content);
        } else if(isValueSet(content)) {
            PreprocessingValueSet preprocessingValueSet = new PreprocessingValueSet();
            return preprocessingValueSet.extractPcesValueSet(content);
        } else if(isCsv) {
            PreprocessingCsvFile preprocessingCsvFile = new PreprocessingCsvFile();
            return preprocessingCsvFile.extractPcesCsv(content);
        }
        return new JSONObject();
    }


    private Boolean isQuestionnaire(String content) {
        String type = "\"resourceType\": \"Questionnaire\"";
        return content.replace(" ", "").contains(type.replace(" ", ""));
    }

    private Boolean isValueSet(String content) {
        String type = "\"resourceType\": \"ValueSet\"";
        return content.replace(" ", "").contains(type.replace(" ", ""));
    }

    public Boolean isUnique(JSONArray jsonArray, JSONObject jsonObject) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            if (Objects.equals(jsonArray.getJSONObject(i).toString(), jsonObject.toString())) {
                return false;
            }
        }
        return true;
    }

    private Boolean isPrecoordination(String code) {
        return !(code.contains(":") || code.contains("=") || code.contains("{") || code.contains("}"));
    }

    private String processingCode(String pce) {
        PreprocessingPce preprocessingPce = new PreprocessingPce();
        return preprocessingPce.getPceCodes(pce);
    }
}
