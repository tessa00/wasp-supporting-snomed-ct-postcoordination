package org.tessa00.wasp.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Questionnaire;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProcessingQuestionnaire {

    FhirContext fhirContext = FhirContext.forR4();
    IParser parser = fhirContext.newJsonParser();

    public ProcessingQuestionnaire() {

    }

    public JSONObject extractPcesQuestionnaire(String content) throws JSONException {
        Questionnaire questionnaire = parser.parseResource(Questionnaire.class, content);
        JSONObject jsonObject = getMetaDataQuestionnaire(questionnaire);
        List<Coding> allCodings = new ArrayList<>();
        extractCodingItems(questionnaire.getItem(), allCodings);
        JSONArray jsonArrayCodes = processingCodes(allCodings);
        jsonObject.put("codes", jsonArrayCodes);
        return  jsonObject;
    }

    private JSONObject getMetaDataQuestionnaire(Questionnaire questionnaire) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", questionnaire.getId());
        jsonObject.put("title", questionnaire.getTitle());
        jsonObject.put("name", questionnaire.getName());
        jsonObject.put("version", questionnaire.getVersion());
        jsonObject.put("description", questionnaire.getDescription());
        return jsonObject;
    }


    private void extractCodingItems(List<Questionnaire.QuestionnaireItemComponent> items, List<Coding> allCodes) {
        for (Questionnaire.QuestionnaireItemComponent item : items) {
            allCodes.addAll(item.getCode());
            for (Questionnaire.QuestionnaireItemAnswerOptionComponent answerOption : item.getAnswerOption()) {
                if (answerOption.getValueCoding() != null) {
                    allCodes.add(answerOption.getValueCoding());
                }
            }
            if (item.hasItem()) {
                extractCodingItems(item.getItem(), allCodes);
            }
        }
    }

    private JSONArray processingCodes(List<Coding> allCodes) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Coding coding : allCodes) {
            JSONObject jsonObjectCode = new JSONObject();
            if(coding.getCode() != null) {
                if(coding.getSystem().contains("snomed.info")) {
                    Boolean isPrecoordination = isPrecoordination(coding.getCode());
                    if(isPrecoordination) {
                        jsonObjectCode.put("code", coding.getCode());
                    } else {
                        jsonObjectCode.put("code", processingCode(coding.getCode()));
                    }
                    jsonObjectCode.put("display", coding.getDisplay());
                    jsonObjectCode.put("system", coding.getSystem());
                    jsonObjectCode.put("version", coding.getVersion());

                    jsonObjectCode.put("precoordination", isPrecoordination);

                    if(isUnique(jsonArray, jsonObjectCode)) {
                        jsonArray.put(jsonObjectCode);
                    }
                }
            }
        }
        return jsonArray;
    }

    public Boolean isUnique(JSONArray jsonArray, JSONObject jsonObject) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            if (Objects.equals(jsonArray.getJSONObject(i).toString(), jsonObject.toString())) {
                return false;
            }
        }
        return true;
    }

    public Boolean isPrecoordination(String code) {
        return !(code.contains(":") || code.contains("=") || code.contains("{") || code.contains("}"));
    }

    public static String processingCode(String pce) {
        PreprocessingPce preprocessingPce = new PreprocessingPce();
        return preprocessingPce.getPceCodes(pce);
    }

}
