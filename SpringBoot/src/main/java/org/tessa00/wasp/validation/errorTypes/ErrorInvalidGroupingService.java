package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.PreprocessingPce;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.ArrayList;

@Service
public class ErrorInvalidGroupingService {

    PreprocessingPce preprocessingPce = new PreprocessingPce();
    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;
    ArrayList<String> selfGroupedAttributeCode = new ArrayList<>();

    public ErrorInvalidGroupingService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
    }


    public JSONObject errorInvalidGrouping(String url, String version, JSONObject jsonObjectConceptModelAttributeMrcm, String pce) throws JSONException {
        setSelfGroupedAttributes();
        return wrongBrackets(url, version, pce, jsonObjectConceptModelAttributeMrcm);
    }

    private JSONObject wrongBrackets(String url, String version, String pce, JSONObject jsonObjectConceptModelAttributeMrcm) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        jsonObjectInfo.put("answer", "The Concept Model defines whether the individual attributes of the hierarchies " +
                "must be ungrouped or grouped (Role Groups). By these, the meaning of the concept is concept is clearly defined. " +
                "The error message states that some or all of the previously ungrouped attributes must be grouped."
                );

        int countStart = (int) pce.chars().filter(ch -> ch == '{').count();
        int countEnd = (int) pce.chars().filter(ch -> ch == '}').count();

        if(countStart == countEnd) {
            JSONObject jsonObjectPce = new JSONObject(preprocessingPce.createJson(pce));

            JSONObject jsonObjectGroupedAttributes = new JSONObject();
            JSONArray jsonArrayGroupedAttributesGrouped = checkUngroupedAttributes(url, version, jsonObjectPce, pce, jsonObjectConceptModelAttributeMrcm);
            jsonObjectGroupedAttributes.put("question", "Grouped or ungrouped elements?");
            jsonObjectGroupedAttributes.put("answer", jsonArrayGroupedAttributesGrouped);

            JSONObject jsonObjectUngroupedAttributes = new JSONObject();
            JSONArray jsonArrayGroupedAttributesUngrouped = checkGroupedAttributes(url, version, jsonObjectPce, pce, jsonObjectConceptModelAttributeMrcm);
            jsonObjectUngroupedAttributes.put("question", "Grouped or ungrouped elements?");
            jsonObjectUngroupedAttributes.put("answer", jsonArrayGroupedAttributesUngrouped);

            String newError;
            if (jsonArrayGroupedAttributesUngrouped.length() == 0 && jsonArrayGroupedAttributesGrouped.length() > 0) {
                newError = "There are missing brackets for the representation of a RoleGroup. " + jsonArrayGroupedAttributesGrouped.length() + " additional attribute relation(s) of the PCE must be grouped and displayed in the form of RoleGroups.";
            } else if (jsonArrayGroupedAttributesUngrouped.length() > 0 && jsonArrayGroupedAttributesGrouped.length() == 0) {
                newError = jsonArrayGroupedAttributesUngrouped.length() + " additional attribute relation(s) of the PCE must be represented ungrouped without Role Groups.";
            } else {
                newError = "Incorrect syntactic representation of attribute relations. " + jsonArrayGroupedAttributesGrouped.length() + " additional attribute relations must be displayed grouped, while " + jsonArrayGroupedAttributesUngrouped.length() + " additional attribute relations have to be presented ungrouped.";
            }
            jsonObject.put("result", newError);
            jsonObject.put("info-general", jsonObjectInfo);
            jsonObject.put("grouped-attributes", jsonObjectGroupedAttributes);
            jsonObject.put("ungrouped-attributes", jsonObjectUngroupedAttributes);
        } else {
            String newError = "Not MRCM compatible: ";
            JSONObject jsonObjectSuggestion = new JSONObject();
            if(countStart > countEnd) {
                newError = newError + "More open brackets than closed brackets! (" + countStart + " { versus " + countEnd + " })";
                pce = pce + "}";
                JSONObject jsonObjectValidate = terminologyServerRequest.validateCode(pce, url, version);
                boolean result = jsonObjectValidate.getBoolean("result");
                if(result) {;
                    jsonObjectSuggestion.put("suggestion", "Add closed bracket at the end of the pce.");
                }
            } else  {
                newError = newError + "More closed brackets than open brackets! (" + countStart + " { versus " + countEnd + " })";
                pce = pce.replace(":", ":{");
                JSONObject jsonObjectValidate = terminologyServerRequest.validateCode(pce, url, version);
                boolean result = jsonObjectValidate.getBoolean("result");
                if(result) {
                    jsonObjectSuggestion.put("suggestion", "Add open bracket at the beginning of the refinement.");
                }
            }
            jsonObject.put("result", newError);
            jsonObject.put("info-general", jsonObjectInfo);
            jsonObject.put("suggestion", jsonObjectSuggestion);
        }
        System.out.println("---");
        System.out.println(jsonObject);
        return jsonObject;
    }

    private JSONArray checkUngroupedAttributes(String url, String version, JSONObject jsonObjectPce, String pce, JSONObject jsonObjectMrcm) throws JSONException {
        JSONArray jsonArrayWithout = jsonObjectPce.getJSONArray("withoutRoleGroup");
        String focusConcept = pce.split(":")[0];

        JSONArray jsonArrayResult = new JSONArray();
        if(focusConcept.contains("+")) {
            focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
        }
        int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
        JSONArray jsonArrayDomain = workerService.getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectMrcm, count);

        for (int i = 0; i < jsonArrayWithout.length(); i++) {
            JSONObject jsonObjectWithout = jsonArrayWithout.getJSONObject(i);
            String attributePce = jsonObjectWithout.getString("attributecode");
            String valuePce = jsonObjectWithout.getString("valuecode");
            for (int j = 0; j < jsonArrayDomain.length(); j++) {
                boolean grouped = false;
                JSONArray jsonArrayTemplate = jsonArrayDomain.getJSONObject(j).getJSONArray("domainTemplateForPrecoordination");
                if(jsonArrayTemplate.getJSONObject(0).has("withRoleGroups")) {
                    JSONArray jsonArrayRoleGroup = jsonArrayTemplate.getJSONObject(0).getJSONArray("withRoleGroups");
                    for(int k = 0; k < jsonArrayRoleGroup.length(); k++) {
                        JSONArray jsonArrayAttribute = jsonArrayRoleGroup.getJSONObject(k).getJSONArray("attribute");
                        for (int l = 0; l < jsonArrayAttribute.length(); l++) {
                            String attributeDomain = jsonArrayAttribute.getJSONObject(l).getString("attributeNameCode");
                            if (attributePce.equals(attributeDomain)) {
                                grouped = true;
                                break;
                            }
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("attributeCode", attributePce);
                        jsonObject.put("attributeName", terminologyServerRequest.lookUpNameFSN(url, version, attributePce));
                        jsonObject.put("attributeValueCode", valuePce);
                        jsonObject.put("attributeValueName", terminologyServerRequest.lookUpNameFSN(url, version, valuePce));
                        jsonObject.put("mustGrouped", grouped);
                        jsonObject.put("selfGrouped", selfGroupedAttributeCode.contains(attributePce));
                        if(!jsonArrayResult.toString().contains(jsonObject.toString())) {
                            jsonArrayResult.put(jsonObject);
                        }
                    }
                }
            }
        }
        return jsonArrayResult;
    }

    private JSONArray checkGroupedAttributes(String url, String version, JSONObject jsonObjectPce, String pce, JSONObject jsonObjectMrcm) throws JSONException {
        JSONArray jsonArrayWith = jsonObjectPce.getJSONArray("withRoleGroup");

        if(jsonArrayWith.length() > 0) {
            JSONArray jsonRoleGroup = jsonArrayWith.getJSONObject(0).getJSONArray("roleGroup");
            String focusConcept = pce.split(":")[0];

            JSONArray jsonArrayResult = new JSONArray();
            if(focusConcept.contains("+")) {
                focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
            }
            int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
            JSONArray jsonArrayDomain = workerService.getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectMrcm, count);

            for (int i = 0; i < jsonRoleGroup.length(); i++) {
                JSONObject jsonObjectRg = jsonRoleGroup.getJSONObject(i);
                String attributePce = jsonObjectRg.getString("attributecode");
                String valuePce = jsonObjectRg.getString("valuecode");
                for (int j = 0; j < jsonArrayDomain.length(); j++) {
                    boolean ungrouped = false;
                    JSONArray jsonArrayTemplate = jsonArrayDomain.getJSONObject(j).getJSONArray("domainTemplateForPrecoordination");
                    if (jsonArrayTemplate.getJSONObject(0).has("withoutRoleGroups")) {
                        JSONArray jsonArrayWithout = jsonArrayTemplate.getJSONObject(0).getJSONArray("withoutRoleGroups");
                        System.out.println(jsonArrayWithout);
                        for (int k = 0; k < jsonArrayWithout.length(); k++) {
                            String attributeDomain = jsonArrayWithout.getJSONObject(k).getString("attributeNameCode");
                            if (attributePce.equals(attributeDomain)) {
                                ungrouped = true;
                                break;
                            }
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("attributeCode", attributePce);
                        jsonObject.put("attributeName", terminologyServerRequest.lookUpNameFSN(url, version, attributePce));
                        jsonObject.put("attributeValueCode", valuePce);
                        jsonObject.put("attributeValueName", terminologyServerRequest.lookUpNameFSN(url, version, valuePce));
                        jsonObject.put("mustUngrouped", ungrouped);
                        if (!jsonArrayResult.toString().contains(jsonObject.toString())) {
                            jsonArrayResult.put(jsonObject);
                        }
                    }
                }
            }
            return jsonArrayResult;
        }
        return new JSONArray();
    }


    private void setSelfGroupedAttributes() {
        selfGroupedAttributeCode.add("288556008");    // before
        selfGroupedAttributeCode.add("371881003");    // during
        selfGroupedAttributeCode.add("255234002");    // after
        selfGroupedAttributeCode.add("42752001");     // due to
        selfGroupedAttributeCode.add("263502005");    // clinical course
        selfGroupedAttributeCode.add("726633004");    // temporally related
        selfGroupedAttributeCode.add("47429007");     // associated with
    }
}
