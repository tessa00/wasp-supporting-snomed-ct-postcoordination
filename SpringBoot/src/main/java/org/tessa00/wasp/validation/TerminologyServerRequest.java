package org.tessa00.wasp.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.*;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.HttpClientService;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

@Service
public class TerminologyServerRequest {

    FhirContext ctx = FhirContext.forR4();
    IParser parser = ctx.newJsonParser();

    private final HttpClientService httpClient;

    public TerminologyServerRequest(HttpClientService httpClientService) {
        this.httpClient = httpClientService;
    }

    public JSONObject validateCode(String code, String url, String version) {
        String urlNew = url + "/CodeSystem/$validate-code";
        String system = "http://snomed.info/sct";
        JSONObject jsonObjectCode = new JSONObject();
        String body = createRequestParametersValidate(system, code.replace("\n", ""), version);

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(urlNew))
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .header("Content-Type", "application/fhir+json")
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());

            Parameters parametersResult = parser.parseResource(Parameters.class, response.body());

            Type booleanType = parametersResult.getParameter("result").getValue();
            Boolean booleanValue = ((BooleanType) booleanType).getValue();
            jsonObjectCode.put("result", booleanValue);
            jsonObjectCode.put("parameters", response.body());
            return jsonObjectCode;
        } catch (Exception ignored) {
        }
        return jsonObjectCode;
    }

    public String lookUpNameFSN(String url, String version, String code) {
        String urlNew = url + "/CodeSystem/$lookup?" +
                "system=http://snomed.info/sct&" +
                "code=" + code.replace("[[+id(<<", "").replace("[[", "").replace("]]", "").replace(" ", "")  + "&" +
                "property=designation&" +
                "version=" + version;

        if(!code.isEmpty()) {
            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .GET()
                        .uri(URI.create(urlNew))
                        .build();
                HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
                Parameters parametersResult = parser.parseResource(Parameters.class, response.body());

                String fullySpecifiedName;
                for (Parameters.ParametersParameterComponent param : parametersResult.getParameter()) {
                    if (param.getName().equals("designation")) {
                        for (Parameters.ParametersParameterComponent part : param.getPart()) {
                            if (part.getName().equals("use") && part.getValue() instanceof Coding coding) {
                                if ("http://snomed.info/sct".equals(coding.getSystem()) && "900000000000003001".equals(coding.getCode())) {     // 900000000000003001 = FSN
                                    for (Parameters.ParametersParameterComponent valuePart : param.getPart()) {
                                        if (valuePart.getName().equals("value") && valuePart.getValue() instanceof StringType) {
                                            fullySpecifiedName = ((StringType) valuePart.getValue()).getValue();
                                            return fullySpecifiedName;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                return "";
            }
        }
        return "";
    }

    public JSONObject eclRequest(String url, String expression, String version, boolean getConcepts) {
        String urlNew = url + "/ValueSet/$expand?" +
                "url=http://snomed.info/sct?fhir_vs=ecl/" + URLEncoder.encode(expression, StandardCharsets.UTF_8) + "&" +
                "version=" + version;
        JSONObject jsonObject = new JSONObject();
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();
            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());

            if(!getConcepts) {
                ValueSet valueSet = parser.parseResource(ValueSet.class, response.body());
                if (valueSet.hasExpansion() && valueSet.getExpansion().hasTotal()) {
                    int total = valueSet.getExpansion().getTotal();
                    jsonObject.put("total", total);
                }
            } else {
                jsonObject.put("valueSet", response.body());
            }
        } catch (Exception ignored) {
        }
        return jsonObject;
    }

    public ValueSet getHistoryRelations(String url, String version, String refSet, String code) {
        String urlNew = url + "/ValueSet/$expand";
        String body = createRequestParametersValidateExpand(version, refSet, code);

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(urlNew))
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .header("Content-Type", "application/fhir+json")
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            return parser.parseResource(ValueSet.class, response.body());
        } catch (Exception ignored) {
        }
        return new ValueSet();
    }

    private String createRequestParametersValidate(String system, String code, String version) {
        Parameters parameters  = new Parameters();

        Parameters.ParametersParameterComponent parameterUrl = new Parameters.ParametersParameterComponent();
        parameterUrl.setName("url");
        parameterUrl.setValue(new UriType(system));
        parameters.addParameter(parameterUrl);

        Parameters.ParametersParameterComponent parameterCoding = new Parameters.ParametersParameterComponent();
        Coding coding = new Coding();
        coding.setSystem(system);
        coding.setCode(code);
        coding.setVersion(version);
        parameterCoding.setName("coding");
        parameterCoding.setValue(coding);
        parameters.addParameter(parameterCoding);
        return parser.encodeResourceToString(parameters);
    }

    private String createRequestParametersValidateExpand(String version, String refSet, String code) {
        ValueSet.ConceptSetFilterComponent filter = new ValueSet.ConceptSetFilterComponent();
        filter.setProperty("constraint");
        filter.setOp(ValueSet.FilterOperator.EQUAL);
        filter.setValue("^ [targetComponentId] " + refSet + " {{ M referencedComponentId = " + code + " }}");

        ValueSet.ConceptSetComponent include = new ValueSet.ConceptSetComponent();
        include.setSystem("http://snomed.info/sct");
        include.setVersion(version);
        include.addFilter(filter);

        ValueSet.ValueSetComposeComponent compose = new ValueSet.ValueSetComposeComponent();
        compose.addInclude(include);

        ValueSet valueSet = new ValueSet();
        valueSet.setCompose(compose);

        Parameters parameters = new Parameters();
        parameters.addParameter().setName("valueSet").setResource(valueSet);

        return parser.encodeResourceToString(parameters);
    }

}
