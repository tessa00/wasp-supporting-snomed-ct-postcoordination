package org.tessa00.wasp.validation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class PreprocessingCsvFile {

    private final ProcessingQuestionnaire processingQuestionnaire = new ProcessingQuestionnaire();

    public PreprocessingCsvFile() {}

    public JSONObject extractPcesCsv(String content) throws JSONException {
        JSONObject jsonObjectFinal = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < content.split("\n").length; i++) {
            String code = "";
            String display = "";
            if(content.split("\n")[i].contains(";")) {
                code = content.split("\n")[i].split(";")[0];
                if(content.split("\n")[i].split(";").length > 1) {
                    display = content.split("\n")[i].split(";")[1];
                }
            }
            if(!Objects.equals(code, "")) {
                JSONObject jsonObjectCode = new JSONObject();
                Boolean isPrecoordination = processingQuestionnaire.isPrecoordination(code);
                if(isPrecoordination) {
                    jsonObjectCode.put("code", code);
                } else {
                    jsonObjectCode.put("code", ProcessingQuestionnaire.processingCode(code));
                }
                jsonObjectCode.put("display", display);
                jsonObjectCode.put("precoordination", isPrecoordination);
                if(processingQuestionnaire.isUnique(jsonArray, jsonObjectCode)) {
                    jsonArray.put(jsonObjectCode);
                }
            }
        }
        jsonObjectFinal.put("codes", jsonArray);
        return jsonObjectFinal;
    }
}
