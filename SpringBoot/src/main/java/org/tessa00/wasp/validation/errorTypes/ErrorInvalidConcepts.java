package org.tessa00.wasp.validation.errorTypes;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.ValueSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.*;

@Service
public class ErrorInvalidConcepts {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;
    FhirContext ctx = FhirContext.forR4();
    IParser parser = ctx.newJsonParser();

    public ErrorInvalidConcepts(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
    }

    public JSONObject errorConcept(Parameters parameters, String url, String version, String pce) throws JSONException {
        ArrayList<String> arrayListErrors = getIncorrectConcepts(parameters);
        return getAlternativeConcepts(arrayListErrors, pce, url, version);
    }


    public HashMap<String, ArrayList<String>> getPossibleConcepts(ArrayList<String> arrayListErrors, String url, String version) throws JSONException {
        JSONObject jsonObjectEcl = terminologyServerRequest.eclRequest(url, "<!138875005", version, true);

        ArrayList<String> arrayListTopLevelConcepts = new ArrayList<>();
        ValueSet valueSet = parser.parseResource(ValueSet.class, jsonObjectEcl.getString("valueSet"));
        if (valueSet.hasExpansion() && valueSet.getExpansion().hasContains()) {
            for (ValueSet.ValueSetExpansionContainsComponent contains : valueSet.getExpansion().getContains()) {
                String code = contains.getCode();
                arrayListTopLevelConcepts.add(code);
            }
        }

        HashMap<String, ArrayList<String>> hashMapResult = new HashMap<>();

        for (String topLevelConcept : arrayListTopLevelConcepts) {
            JSONObject jsonObjectTopLevelConcept = terminologyServerRequest.eclRequest(url, "<<" + topLevelConcept, version, true);
            valueSet = parser.parseResource(ValueSet.class, jsonObjectTopLevelConcept.getString("valueSet"));
            if (valueSet.hasExpansion() && valueSet.getExpansion().hasContains()) {
                for (ValueSet.ValueSetExpansionContainsComponent contains : valueSet.getExpansion().getContains()) {
                    String code = contains.getCode();
                    for (String arrayListError : arrayListErrors) {
                        String codeError = arrayListError.split("@")[0];
                        if (code.contains(codeError)) {
                            ArrayList<String> arrayListResult = new ArrayList<>();
                            if (hashMapResult.containsKey(codeError)) {
                                arrayListResult = hashMapResult.get(codeError);
                            }
                            arrayListResult.add(code + "| " + terminologyServerRequest.lookUpNameFSN(url, version, code) + "|");
                            hashMapResult.put(codeError, arrayListResult);
                        }
                    }
                }
            }
        }
        return hashMapResult;
    }


    public ArrayList<String> getIncorrectConcepts(Parameters parameters) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (Parameters.ParametersParameterComponent parameter : parameters.getParameter()) {
            if (parameter.getName().equals("component")) {
                for (Parameters.ParametersParameterComponent part : parameter.getPart()) {
                    if (part.hasResource() && part.getResource() instanceof OperationOutcome outcome) {
                        List<OperationOutcome.OperationOutcomeIssueComponent> issues = outcome.getIssue();
                        for (OperationOutcome.OperationOutcomeIssueComponent issue : issues) {
                            if (issue.hasDetails() && issue.getDetails().hasText()) {
                                String text = issue.getDetails().getText();
                                String code = text.split("\"")[1];
                                arrayList.add(code + "@" + text);
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public JSONObject getAlternativeConcepts(ArrayList<String> arrayListErrors, String pce, String url, String version) throws JSONException {
        HashMap<String, ArrayList<String>> replaceConcepts = getPossibleConcepts(arrayListErrors, url, version);
        JSONArray jsonArrayAlternatives = workerService.checkPCE(replaceConcepts, pce, url, version);
        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        if(arrayListErrors.size() == 1) {
            jsonObjectInfo.put("answer", "One precoordinated concept of the PCE doesn't exist in the selected version of SNOMED CT (" + version + "), " +
                    "because of an incorrectly entered Identifier.");
        } else {
            jsonObjectInfo.put("answer", arrayListErrors.size() + " precoordinated concepts of the PCE don't exist in the selected version of SNOMED CT, " +
                    "because of incorrectly entered Identifiers.");
        }

        JSONObject jsonObjectAlternative = new JSONObject();
        jsonObjectAlternative.put("question", "Can the incorrect concepts be replaced?");
        if(jsonArrayAlternatives.length() > 0) {
            jsonObjectAlternative.put("answer", jsonArrayAlternatives);
            jsonObjectAlternative.put("message", "Replacement possible to create a syntactically and semantically correct PCE.");
            jsonObjectAlternative.put("boolean_replace", true);
        } else {
            jsonObjectAlternative.put("message", "Replacement not possible because of other errors! Suggestion: Create a new PCE?");
            jsonObjectAlternative.put("boolean_replace", false);
            for (String key: replaceConcepts.keySet()) {
                jsonArrayAlternatives.put(new JSONArray());
                for (String value: replaceConcepts.get(key)) {
                    jsonArrayAlternatives.getJSONArray(jsonArrayAlternatives.length()-1).put(key + "->" + value);
                }
            }
            jsonObjectAlternative.put("answer", jsonArrayAlternatives);
        }

        ArrayList<String> arrayListWrongCodes = new ArrayList<>();
        for (String codes: arrayListErrors) {
            arrayListWrongCodes.add(codes.split("@")[0]);
        }
        String replacedList = arrayListWrongCodes.toString().replace("[", "").replace("]", "");
        JSONObject jsonObject = new JSONObject();
        if(arrayListWrongCodes.size() == 1) {
            jsonObject.put("result", "The following SNOMED CT concept isn't included in the SNOMED edition and version being used: " + replacedList + ".");
        } else {
            jsonObject.put("result", "The following SNOMED CT concepts aren't included in the SNOMED edition and version being used: " + replacedList + ".");

        }

        jsonObject.put("info-general", jsonObjectInfo);
        jsonObject.put("info-alternatives", jsonObjectAlternative);
        return jsonObject;
    }

}
