package org.tessa00.wasp.validation;


import java.util.ArrayList;

public class PreprocessingPce {

    public PreprocessingPce() {

    }

    public String getPceCodes(String pce) {
        int intDoubleColon = (int) pce.chars().filter(ch -> ch == ':').count();
        ArrayList<String> ungroupedElements = setUngroupedAttributes();
        if (pce.contains("|")) {
            String pceCode = pce.replaceAll("[a-zA-Z]", "").replace(" ", "").replace("\n", "").replace("()", "");
            ArrayList<Integer> arrayListPosition = new ArrayList<>();
            if(pceCode.contains(":")) {
                for(int i = 0; i < pceCode.length(); i++) {
                    if(pceCode.charAt(i) == '|') {
                        arrayListPosition.add(i);
                    }
                }
                StringBuilder newPce = getPce(arrayListPosition, pceCode);
                pceCode = newPce.toString().replace("|", "");

                int countStart = (int) pceCode.chars().filter(ch -> ch == '{').count();
                int countEnd = (int) pceCode.chars().filter(ch -> ch == '}').count();
                int countStartInit = (int) pce.chars().filter(ch -> ch == '{').count();
                int countEndInit = (int) pce.chars().filter(ch -> ch == '}').count();

                if((countStart != countEnd) && (countStartInit == countEndInit)) {
                    pceCode = pceCode + "}";
                }
            }
            if(!pceCode.contains("{") && !pceCode.contains("}") && intDoubleColon == 1) {
                pceCode = setRoleGroupBrackets(ungroupedElements, pceCode);
            }
            return pceCode;
        }
        if(!pce.contains("{") && !pce.contains("}") && intDoubleColon == 1) {
            pce = setRoleGroupBrackets(ungroupedElements, pce);
        }
        return pce;
    }

    private String setRoleGroupBrackets(ArrayList<String> ungroupedElements, String pceCode) {
        for(String codeAttribute: ungroupedElements) {
            if(pceCode.contains(codeAttribute)) {
                return pceCode;
            }
        }
        return pceCode.replace(":", ":{").replace(")", "})") + "}";
    }

    private static StringBuilder getPce(ArrayList<Integer> arrayListPosition, String pceCode) {
        StringBuilder newPce = new StringBuilder();
        for(int i = 0; i < arrayListPosition.size()-1; i++) {
            if(i % 2 != 0) { // ungerade Zahl sind immer Codes (siehe Pos 0 = Fc)
                newPce.append(pceCode, arrayListPosition.get(i), arrayListPosition.get(i + 1));
            }
            if(i == 0) {
                newPce.append(pceCode, 0, arrayListPosition.get(i));
            }
        }
        if(pceCode.substring(arrayListPosition.get(arrayListPosition.size()-1)).contains("=")) {
            newPce.append(pceCode.substring(arrayListPosition.get(arrayListPosition.size()-1)));
        }
        return newPce;
    }

    public String createJson(String pce) {
        String focusConcept = pce.split(":")[0];
        String[] listFc = focusConcept.split("[+]");
        String json = "{";

        json = json + createJsonFocusConcept(listFc);

        if(pce.contains(":")) {
            String refinement = pce.split(":")[1];
            // get position of start and end of rg
            ArrayList<Integer> arrayListPositionStart = new ArrayList<>();
            ArrayList<Integer> arrayListPositionEnd = new ArrayList<>();
            for (int i = 0; i < refinement.length(); i++) {
                if (refinement.charAt(i) == '{') {
                    arrayListPositionStart.add(i);
                } else if (refinement.charAt(i) == '}') {
                    arrayListPositionEnd.add(i);
                }
            }
            json = json + createJsonWithoutRoleGroup(refinement, arrayListPositionStart, arrayListPositionEnd);
            json = json + createJsonWithRoleGroup(refinement, arrayListPositionStart, arrayListPositionEnd);
        }

        json = json + "}";
        return json;
    }

    private String createJsonFocusConcept(String[] array) {
        StringBuilder json = new StringBuilder("\"focusconcept\" : [");
        for (String s : array) {
            json.append("{\"code\" :\"").append(s).append("\"},");
        }
        json = new StringBuilder(json.substring(0, json.length() - 1));
        json.append("]");
        return json.toString();
    }

    private String createJsonWithoutRoleGroup(String refinement, ArrayList<Integer> arrayListPositionStart, ArrayList<Integer> arrayListPositionEnd) {
        StringBuilder json = new StringBuilder(",\"withoutRoleGroup\" : [");
        if(arrayListPositionStart.isEmpty() && arrayListPositionEnd.isEmpty()) {
            String[] attributeRelation = refinement.split(",");
            json.append(helperAttributeRelation(attributeRelation));
        } else {
            if(arrayListPositionStart.get(0) != 0) {    // ungruppierte Attribute am Anfang
                String[] attributeRelation = refinement.substring(0, arrayListPositionStart.get(0)).split(",");
                json.append(helperAttributeRelation(attributeRelation));
            }
            if(arrayListPositionEnd.size()-1 > -1) {
                if (arrayListPositionEnd.get(arrayListPositionEnd.size()-1) != refinement.length()) { // ungruppierte Attribute am Ende
                    String[] attributeRelation = refinement.substring(arrayListPositionEnd.get(arrayListPositionEnd.size()-1)).split(",");
                    json.append(helperAttributeRelation(attributeRelation));
                }
            }

            for (int i = 0; i < arrayListPositionEnd.size()-1; i++) { // dazwischen sind ungruppierte Attribute
                int c = arrayListPositionStart.get(i + 1) - arrayListPositionEnd.get(i);
                if(c != 2) {    // Attribute sind dazwischen und nicht nur ein Komma
                    String[] attributeRelation = refinement.substring(arrayListPositionEnd.get(i) + 2, arrayListPositionStart.get(i + 1)).split(",");
                    json.append(helperAttributeRelation(attributeRelation));
                }
            }
        }
        if(!json.toString().equals(",\"withoutRoleGroup\" : [")) {
            json = new StringBuilder(json.substring(0, json.length() - 1));
        }
        json.append("]");
        return json.toString();
    }

    private String createJsonWithRoleGroup(String refinement, ArrayList<Integer> arrayListPositionStart, ArrayList<Integer> arrayListPositionEnd) {
        StringBuilder json = new StringBuilder(",\"withRoleGroup\" : [");
        for(int i = 0; i < arrayListPositionStart.size(); i++) {
            if(arrayListPositionStart.get(i) < arrayListPositionEnd.get(i)) {
                String roleGroup = refinement.substring(arrayListPositionStart.get(i) + 1, arrayListPositionEnd.get(i));
                String[] attributeRelation = roleGroup.split(",");
                json.append("{\"roleGroup\" : [");
                json.append(helperAttributeRelation(attributeRelation));
                json = new StringBuilder(json.substring(0, json.length() - 1));
                json.append("]},");
            }
        }
        if(!arrayListPositionStart.isEmpty()) {
            json = new StringBuilder(json.substring(0, json.length() - 1));
        }
        json.append("]");
//        System.out.println(json);
        return json.toString();
    }

    private String helperAttributeRelation(String[] array) {
        StringBuilder json = new StringBuilder();
        for (String s : array) {
            if(s.contains("=")) {
                String code = s.split("=")[0];
                String value = s.split("=")[1];
                json.append("{\"attributecode\" : \"").append(code).append("\",\"valuecode\" : \"").append(value).append("\"},");
            }
        }
        return json.toString();
    }

    private ArrayList<String> setUngroupedAttributes() {
        ArrayList<String> ungroupedAttributes = new ArrayList<>();
        ungroupedAttributes.add("272741003");
        ungroupedAttributes.add("726542003");
        ungroupedAttributes.add("733928003");
        ungroupedAttributes.add("733930001");
        ungroupedAttributes.add("733931002");
        ungroupedAttributes.add("733932009");
        ungroupedAttributes.add("733933004");
        ungroupedAttributes.add("736472000");
        ungroupedAttributes.add("736473005");
        ungroupedAttributes.add("736474004");
        ungroupedAttributes.add("736475003");
        ungroupedAttributes.add("736476002");
        ungroupedAttributes.add("736518005");
        ungroupedAttributes.add("738774007");
        ungroupedAttributes.add("763032000");
        ungroupedAttributes.add("766939001");
        ungroupedAttributes.add("774081006");
        ungroupedAttributes.add("774158006");
        ungroupedAttributes.add("774159003");
        ungroupedAttributes.add("827081001");
        ungroupedAttributes.add("836358009");
        ungroupedAttributes.add("840560000");
        ungroupedAttributes.add("860781008");
        ungroupedAttributes.add("1142139005");
        ungroupedAttributes.add("1142140007");
        ungroupedAttributes.add("1142141006");
        ungroupedAttributes.add("1142143009");
        ungroupedAttributes.add("1148793005");
        ungroupedAttributes.add("1148965004");
        ungroupedAttributes.add("1148967007");
        ungroupedAttributes.add("1148968002");
        ungroupedAttributes.add("1148969005");
        ungroupedAttributes.add("1149367008");
        ungroupedAttributes.add("1230370004");
        ungroupedAttributes.add("320091000221107");
        return ungroupedAttributes;
    }
}
