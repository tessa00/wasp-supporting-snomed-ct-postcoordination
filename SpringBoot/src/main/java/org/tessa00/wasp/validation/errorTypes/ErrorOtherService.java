package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.PreprocessingPce;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ErrorOtherService {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;
    private final ErrorInvalidConcepts errorInvalidConcepts;
    private final ErrorInvalidGroupingService errorInvalidGroupingService;
    private final ErrorInvalidAttributeService errorInvalidAttributeService;
    private final PreprocessingPce preprocessingPce = new PreprocessingPce();

    public ErrorOtherService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService, ErrorInvalidConcepts errorInvalidConcepts, ErrorInvalidGroupingService errorInvalidGroupingService, ErrorInvalidAttributeService errorInvalidAttributeService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
        this.errorInvalidConcepts = errorInvalidConcepts;
        this.errorInvalidGroupingService = errorInvalidGroupingService;
        this.errorInvalidAttributeService = errorInvalidAttributeService;
    }

    public JSONObject errorOtherSyntax(String error, String url, String version, String pce) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if(error.contains("mismatched input")) {
            jsonObject = getMismatchedInput(pce, error);
        } else if(error.contains("recognition error")) {
            jsonObject = getRecognitionError(pce, error, url, version);
        } else if (error.contains("SCTID") || error.contains("parse error (inline)")) {
            jsonObject = getMissingElementError(error);
        }

        return jsonObject;
    }

    public String fixPce(String pce, int line, int position, char replaceChar) {
        StringBuilder newPce = new StringBuilder();
        for (int i = 0; i < pce.split("\n").length; i++) {
            if(i == line - 1) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(pce.split("\n")[i]);
                stringBuilder.setCharAt(position, replaceChar);
                newPce.append(stringBuilder.toString());
            } else {
                newPce.append(pce.split("\n")[i]);
            }
        }
        if(!pce.contains("{")) {
            return preprocessingPce.getPceCodes(newPce.toString());
        }
        return newPce.toString();
    }

    private JSONObject getMismatchedInput(String pce, String error) throws JSONException {
        StringBuilder newError = new StringBuilder("Invalid post-coordinated expression ");
        String[] errorArray = error.split(" ");
        int line = -1;
        int position = -1;
        char wrongChar = 0;
        char replaceChar = 0;
        for (int i = 0; i < errorArray.length; i++) {
            if(errorArray[i].contains(":")) {
                String regex = "\\d+:\\d+"; // number : number - show line and position
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(errorArray[i]);
                if(matcher.matches()) {
                    newError.append("at position ").append(errorArray[i].split(":")[1]).append(": ");
                    line = Integer.parseInt(errorArray[i].split(":")[0]);
                    position = Integer.parseInt(errorArray[i].split(":")[1]);
                }
            }
            if(errorArray[i].contains("'") && errorArray[i-1].contains("input")) {
                newError.append("wrong character is ").append(errorArray[i]);
                wrongChar = errorArray[i].replace("'", "").charAt(0);
            } else if(errorArray[i].contains("'") && errorArray[i-1].contains("expecting")) {
                newError.append(", but expecting character ").append(errorArray[i]).append(".");
                replaceChar = errorArray[i].replace("'", "").charAt(0);
            }
        }
        String fixedPce = "";
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        jsonObjectInfo.put("answer", "For syntactic correctness, the PCEs must comply with the rules of Compositional Grammar. " +
                "The Compositional Grammar is based on the Augmented Backus-Naur Form. It contains a series of rules regarding how SNOMED CT expressions may be structured. " +
                "However, the PCE shown contains one or more characters that are not provided for in the Compositional Grammar at a certain position.");

        JSONObject jsonObjectSuggestion = new JSONObject();
        jsonObjectSuggestion.put("question", "How can the error be fixed?");
        if(replaceChar == 0) {
            jsonObjectSuggestion.put("answer", "Replace the wrong character ('" + wrongChar + "')" + ". Attention, this PCE may contain further bugs.");
            jsonObjectSuggestion.put("pce", fixedPce);
            jsonObject.put("second-check", true);
        } else {
            fixedPce = fixPce(pce, line, position, replaceChar);
            jsonObjectSuggestion.put("answer", "Replace the wrong character ('" + wrongChar + "' -> '" + replaceChar + "'): " + ". Attention, this PCE may contain further bugs (if so, further error messages are displayed).");
            jsonObjectSuggestion.put("pce", fixedPce);
            jsonObject.put("second-check", false);
        }

        jsonObject.put("result", newError);
        jsonObject.put("info-general", jsonObjectInfo);
        jsonObject.put("info-suggestion", jsonObjectSuggestion);
        jsonObject.put("fixed-pce", fixedPce);

        return jsonObject;
    }

    private JSONObject getRecognitionError(String pce, String error, String url, String version) throws JSONException {
        String character = error.split("token recognition error at: ")[1];

        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(character);

        StringBuilder result = new StringBuilder();
        while (matcher.find()) {
            result.append(matcher.group());
        }
        character = result.toString();
        if(!character.isEmpty()) {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(character);

            return errorInvalidConcepts.getAlternativeConcepts(arrayList, pce, url, version);
        } else {
            character = error.split("token recognition error at: ")[1].replace("'", "");

            StringBuilder newError = new StringBuilder();
            String[] errorArray = error.split(" ");
            int line = -1;
            int position = -1;
            for (int i = 0; i < errorArray.length; i++) {
                if(errorArray[i].contains(":")) {
                    String regex = "\\d+:\\d+"; // number : number - show line and position
                    pattern = Pattern.compile(regex);
                    matcher = pattern.matcher(errorArray[i]);
                    if(matcher.matches()) {
                        line = Integer.parseInt(errorArray[i].split(":")[0]);
                        position = Integer.parseInt(errorArray[i].split(":")[1]);
                    }
                }
            }
            newError.append("Invalid post-coordinated expression at position ").append(position).append(": '").append(character).append("'.");

            JSONObject jsonObject = new JSONObject();
            JSONObject jsonObjectInfo = new JSONObject();
            jsonObjectInfo.put("question", "What does this error mean?");
            jsonObjectInfo.put("answer", "For syntactic correctness, the PCEs must comply with the rules of Compositional Grammar. " +
                    "The Compositional Grammar is based on the Augmented Backus-Naur Form. It contains a series of rules regarding how SNOMED CT expressions may be structured. " +
                    "However, the PCE shown contains one or more characters that are not provided for in the Compositional Grammar at a certain position.");

            JSONObject jsonObjectSuggestion = new JSONObject();
            jsonObjectSuggestion.put("question", "How can the error be fixed?");
            String fixedPce = fixPce(pce, line, position, '\0');
            fixedPce = fixedPce.replace("\0", "");
            jsonObjectSuggestion.put("answer", "Delete the wrong character ('" + character + "')" + ". Attention, this PCE may contain further bugs (if so, further error messages are displayed).");
            jsonObjectSuggestion.put("pce", fixedPce);
            jsonObject.put("second-check", false);

            jsonObject.put("result", newError);
            jsonObject.put("info-general", jsonObjectInfo);
            jsonObject.put("info-suggestion", jsonObjectSuggestion);
            jsonObject.put("fixed-pce", fixedPce);

            return jsonObject;
        }
    }

    private JSONObject getMissingElementError(String error) throws JSONException {
        StringBuilder newError = new StringBuilder();
        String[] errorArray = error.split(" ");
        int position = 0;
        for (String version : errorArray) {
            if (version.contains(":")) {
                String regex = "\\d+:\\d+"; // number : number - show line and position
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(version);
                if (matcher.matches()) {
                    position = Integer.parseInt(version.split(":")[1]);
                }
            }
        }
        newError.append("Invalid post-coordinated expression: Missing SNOMED CT Identifier at position ").append(position).append(".");

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        jsonObjectInfo.put("answer", "For syntactic correctness, the PCEs must comply with the rules of Compositional Grammar. " +
                "The Compositional Grammar is based on the Augmented Backus-Naur Form. It contains a series of rules regarding how SNOMED CT expressions may be structured. " +
                "However, a SNOMED CT concept is missing in the PCE shown at a certain position.");

        jsonObject.put("result", newError);
        jsonObject.put("info-general", jsonObjectInfo);

        return jsonObject;
    }



}
