package org.tessa00.wasp.validation.errorTypes;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.StringType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.PreprocessingPce;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class ErrorMessagePceService {

    private final WorkerService workerService;
    FhirContext ctx = FhirContext.forR4();
    IParser parser = ctx.newJsonParser();

    private final TerminologyServerRequest terminologyServerRequest;
    private final ErrorAttributeValueService errorAttributeValueService;
    private final ErrorInvalidAttributeService errorInvalidAttributeService;
    private final ErrorInvalidGroupingService errorInvalidGrouping;
    private final ErrorCardinalityService errorCardinalityService;
    private final ErrorInvalidConcepts errorInvalidConcepts;
    private final ErrorInactiveConceptService errorInactiveConcept;
    private final ErrorOtherService errorOtherService;
    private final ErrorDomainFocusConcept errorDomainFocusConcept;

    public ErrorMessagePceService(TerminologyServerRequest terminologyServerRequest, ErrorAttributeValueService errorAttributeValueService, ErrorInvalidAttributeService errorInValidAttributeService, ErrorInvalidGroupingService errorInvalidGrouping, ErrorCardinalityService errorCardinalityService, ErrorInvalidConcepts errorInvalidConcepts, ErrorInactiveConceptService errorInactiveConcept, ErrorOtherService errorOtherService, WorkerService workerService, ErrorDomainFocusConcept errorDomainFocusConcept) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.errorAttributeValueService = errorAttributeValueService;
        this.errorInvalidAttributeService = errorInValidAttributeService;
        this.errorInvalidGrouping = errorInvalidGrouping;
        this.errorCardinalityService = errorCardinalityService;
        this.errorInvalidConcepts = errorInvalidConcepts;
        this.errorInactiveConcept = errorInactiveConcept;
        this.errorOtherService = errorOtherService;
        this.workerService = workerService;
        this.errorDomainFocusConcept = errorDomainFocusConcept;
    }

    public JSONObject initSingleConcept(String code, String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm) throws JSONException {
        if(code.contains(":")) {
            PreprocessingPce preprocessingPce = new PreprocessingPce();
            code = preprocessingPce.getPceCodes(code);
        }
        JSONObject jsonObject = terminologyServerRequest.validateCode(code, url, version);
        if(jsonObject.has("result")) {
            boolean result = jsonObject.getBoolean("result");
            Parameters parameters = parser.parseResource(Parameters.class, jsonObject.getString("parameters"));
            if (!result) {
                jsonObject.put("message", getMessage(parameters, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm,  code));
            }
        }
        return jsonObject;
    }


    public JSONObject getMessage(Parameters parameters,  String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String concept) throws JSONException {
        String serialized = parser.encodeResourceToString(parameters);
        System.out.println(serialized);

        JSONObject jsonObject = new JSONObject();

        ArrayList<String> arrayListReason = getErrorReasons(parameters);

//        if(concept.contains(":") || concept.contains("=")) {
            jsonObject = processingErrors(arrayListReason, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm,  concept.split(":")[0], concept, parameters);
//        }
        return jsonObject;

    }

    private ArrayList<String> getErrorReasons(Parameters parameters) {
        ArrayList<String> arrayListReason = new ArrayList<>();

        for (Parameters.ParametersParameterComponent param : parameters.getParameter()) {
            if ("reason".equals(param.getName()) || "message".equals(param.getName())) {
                StringType valueString = (StringType) param.getValue();
                if(!valueString.getValue().contains("Expression violates the MRCM") && !arrayListReason.toString().contains("MUST be a GROUPED attribute")) {
                    arrayListReason.add(valueString.getValue());
                }
            }
        }
        return arrayListReason;
    }

    private JSONObject processingErrors(ArrayList<String> arrayList, String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm, String focusConcept, String pce, Parameters parameters) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObjectResult = new JSONObject();

        pce = pce.replace("\n", "");

        // multiple focus concepts
        if(pce.contains("+")) {
            int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
            if(focusConcept.contains("+")) {
                focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
            }
            JSONArray jsonArrayDomain = workerService.getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectConceptModelAttributeMrcm, count);
            if(jsonArrayDomain.length() == 0) {
                jsonObjectResult = errorDomainFocusConcept.getErrorDomainFocusConcept(focusConcept, count, url, version, jsonObjectConceptModelAttributeMrcm);
                if(jsonObjectResult.length() > 0) {
                    jsonObjectResult.put("identifier", "ERROR_DOMAIN");
                    jsonArray.put(jsonObjectResult);
                    if (jsonArray.length() > 0) {
                        jsonObject.put("errors", processingJsonArray(jsonArray));
                    }
                }
            }
        }

        System.out.println(arrayList);

        String serializedParameters = parser.encodeResourceToString(parameters);
        if(serializedParameters.contains("inactive")) {
            jsonObjectResult = errorInactiveConcept.errorInactiveConcepts(parameters, url, version, pce);
            jsonObjectResult.put("identifier", "ERROR_INACTIVE_CONCEPT");
            if(jsonObjectResult.length() > 0) {
                jsonArray.put(jsonObjectResult);
            }
        } else {
            for (String error : arrayList) {
                boolean idx = true;
                if (error.toLowerCase().contains("not in range")) {
                    jsonObjectResult = errorAttributeValueService.errorAttributeValue(error, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm, focusConcept);
                    jsonObjectResult.put("identifier", "ERROR_ATTRIBUTE_VALUE");
                } else if (error.toLowerCase().contains("is not a valid attribute")) {
                    jsonObjectResult = errorInvalidAttributeService.errorInvalidAttribute(error, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm, focusConcept, pce);
                    jsonObjectResult.put("identifier", "ERROR_ATTRIBUTE");
                } else if (error.toLowerCase().contains("missing '}'") || error.toLowerCase().contains("missing '{'") || error.toLowerCase().contains("must be a grouped") || error.toLowerCase().contains("must be an ungrouped")) {
                    jsonObjectResult = errorInvalidGrouping.errorInvalidGrouping(url, version, jsonObjectConceptModelAttributeMrcm, pce);
                    jsonObjectResult.put("identifier", "ERROR_GROUPING");
                } else if (error.toLowerCase().contains("occurrences of") && error.toLowerCase().contains("; expected") && !error.toLowerCase().contains("not in range")) {
                    jsonObjectResult = errorCardinalityService.errorCardinality(error, url, version, jsonObjectConceptModelAttributeMrcm, pce);
                    jsonObjectResult.put("identifier", "ERROR_CARDINALITY");
                } else if (error.toLowerCase().contains("one or more codes in the expression is invalid") || error.toLowerCase().contains("code-invalid")) {
                    jsonObjectResult = errorInvalidConcepts.errorConcept(parameters, url, version, pce);
                    jsonObjectResult.put("identifier", "ERROR_CONCEPT");
                } else if (error.toLowerCase().contains("invalid post-coordinated expression")) {
                    jsonObjectResult = errorOtherService.errorOtherSyntax(error, url, version, pce);
                    jsonObjectResult.put("pce", pce);
                    if(!jsonArray.toString().contains("info-alternatives")) {
                        jsonObjectResult.put("identifier", "ERROR_OTHER");
                        JSONObject jsonObjectSecondCheck = getSecondCheck(jsonObjectResult, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm);
                        if(jsonObjectSecondCheck.length() > 0) {
                            JSONArray jsonArraySecondCheck = jsonObjectSecondCheck.getJSONObject("message").getJSONArray("errors");
                            for (int i = 0; i < jsonArraySecondCheck.length(); i++) {
                                jsonArray.put(jsonArraySecondCheck.getJSONObject(i));
                            }
                        }
                    } else {
                        jsonObjectResult.put("identifier", "ERROR_CONCEPT");
                    }
                    jsonArray.put(jsonObjectResult);
                    idx = false;
                } else if(error.contains("is not known to belong to the provided code system")) {
                    jsonObjectResult.put("identifier", "ERROR_OTHER");
                    jsonObjectResult.put("result", "Syntactic error! Ask expert for help!");
                }
                if (jsonObjectResult.length() > 0 && idx) {
                    jsonArray.put(jsonObjectResult);
                }
            }
        }
        if (jsonArray.length() > 0) {
            jsonObject.put("errors", processingJsonArray(jsonArray));
        }
        jsonObject.put("pce", pce.replace("\n", ""));
        return jsonObject;
    }

    private JSONArray processingJsonArray(JSONArray jsonArray) throws JSONException {
        JSONArray jsonArrayResult = new JSONArray();
        if(jsonArray.toString().contains("not a valid attribute.")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                boolean b = jsonArray.get(i).toString().contains("not a valid attribute.");
                if (b) {
                    String result = jsonArray.getJSONObject(i).getString("result");
                    String concept = result.replace("\"Not MRCM compatible: ", "").replace(" is not a valid attribute.", "");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        result = jsonArray.getJSONObject(j).getString("result");
                        if (!result.contains(concept + " IS NOT a valid attribute for focus concept")) {
                            jsonArrayResult.put(jsonArray.getJSONObject(j));
                        }
                    }
                    return jsonArrayResult;
                }
            }
        }
        return jsonArray;
    }

    private JSONObject getSecondCheck(JSONObject jsonObjectResult, String url, String version, JSONObject jsonObjectConceptModelAttributes, JSONObject jsonObjectConceptModelAttributeMrcm) throws JSONException {
        String fixedPce = "";
        boolean secondCheck = false;
        if (jsonObjectResult.has("fixed-pce")) {
            fixedPce = jsonObjectResult.getString("fixed-pce");
        }
        if (jsonObjectResult.has("second-check")) {
            secondCheck = jsonObjectResult.getBoolean("second-check");
        }
        if(!secondCheck && !Objects.equals(fixedPce, "")) {
            return initSingleConcept(fixedPce, url, version, jsonObjectConceptModelAttributes, jsonObjectConceptModelAttributeMrcm);
        }
        return new JSONObject();
    }




}

