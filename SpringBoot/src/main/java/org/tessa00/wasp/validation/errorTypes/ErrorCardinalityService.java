package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

@Service
public class ErrorCardinalityService {

    private final TerminologyServerRequest terminologyServerRequest;
    private final WorkerService workerService;

    public ErrorCardinalityService(TerminologyServerRequest terminologyServerRequest, WorkerService workerService) {
        this.terminologyServerRequest = terminologyServerRequest;
        this.workerService = workerService;
    }

    public JSONObject errorCardinality(String error, String url, String version, JSONObject jsonObjectConceptModelAttributeMrcm, String pce) throws JSONException {
        return wrongCardinality(error, url, version, pce, jsonObjectConceptModelAttributeMrcm);
    }

    private JSONObject wrongCardinality(String error, String url, String version, String pce, JSONObject jsonObjectConceptModelAttributeMrcm) throws JSONException, NumberFormatException {
        JSONObject jsonObject = new JSONObject();

        String newError = "Not MRCM compatible: ";

        if (!error.contains("[]")) {
            int occurrence = Integer.parseInt(error.split("occurrences")[0].replace(" ", "").replace("MRCM:", ""));
            String attribute =  error.split(" attribute")[1].split("is not")[0];
            attribute = attribute.split("\\|")[0].replace(" ", "") + " |" + terminologyServerRequest.lookUpNameFSN(url, version, attribute.split("\\|")[0].replace(" ", "")) + "|";
            String cardinality = error.split("expected ")[1];
            int minCar = Integer.parseInt(cardinality.split("\\.\\.")[0].replace("[", ""));
            int maxCar = Integer.parseInt(cardinality.split("\\.\\.")[1].replace("]", ""));

            boolean isMaxCar = false;

            if(occurrence > maxCar && occurrence >= minCar) {
                newError = "Not MRCM compatible: The attribute " + attribute + " can only be used to a maximum of " + maxCar + " per Role Group.";
                isMaxCar = true;
            } else if(occurrence < minCar && occurrence <= maxCar) {
                newError = "Not MRCM compatible: The attribute " + attribute + " must be used with a minimum of " + maxCar + " per Role Group.";
            }

            JSONObject jsonObjectInfo = new JSONObject();
            jsonObjectInfo.put("question", "What does this error mean?");
            jsonObjectInfo.put("answer", "A cardinality is defined for each attribute relation as well as RoleGroup. " +
                    "This specifies the minimum and maximum number of times a particular attribute relation or RoleGroup " +
                    "and maximum number of times it may be used."
            );

            JSONObject jsonObjectAttributeCarInfo = new JSONObject();
            jsonObjectAttributeCarInfo.put("question", "What are the cardinalities of the attribute?");
            jsonObjectAttributeCarInfo.put("answer", getCardinality(jsonObjectConceptModelAttributeMrcm, attribute, pce, url, version, isMaxCar));


            jsonObject.put("result", newError);
            jsonObject.put("info-general", jsonObjectInfo);
            jsonObject.put("info-cardinality", jsonObjectAttributeCarInfo);
        }
        return jsonObject;
    }

    private JSONArray getCardinality(JSONObject jsonObjectConceptModelAttributeMrcm, String attribute, String pce, String url, String version, boolean isMaxCar) throws JSONException {
        String focusConcept = pce.split(":")[0];
        JSONArray jsonArrayResult = new JSONArray();
        if(focusConcept.contains("+")) {
            focusConcept = "(" + focusConcept.replace("+", " OR ") + ")";
        }
        int count = (int) focusConcept.chars().filter(ch -> ch == '+').count() + 1;
        JSONArray jsonArray = workerService.getMatchingConceptModelDomain(url, version, focusConcept, jsonObjectConceptModelAttributeMrcm, count);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObjectTemplate = jsonArray.getJSONObject(i).getJSONArray("domainTemplateForPrecoordination").getJSONObject(0);
            String domainConstraint = jsonArray.getJSONObject(i).getString("domainConstraint");
            if(jsonObjectTemplate.has("withRoleGroups")) {
                JSONArray jsonArrayWithRg = jsonObjectTemplate.getJSONArray("withRoleGroups").getJSONObject(0).getJSONArray("attribute");
                JSONArray jsonArrayWithRgResult = extractInfo(jsonArrayWithRg, attribute, domainConstraint, isMaxCar);
                if(jsonArrayWithRgResult.length() > 0 && !jsonArrayResult.toString().contains(jsonArrayWithRgResult.toString())) {
                    jsonArrayResult.put(jsonArrayWithRgResult);
                }
            }
            if(jsonObjectTemplate.has("withoutRoleGroups")) {
                JSONArray jsonArrayWithoutRg = jsonObjectTemplate.getJSONArray("withoutRoleGroups");
                JSONArray jsonArrayWithoutRgResult = extractInfo(jsonArrayWithoutRg, attribute, domainConstraint, isMaxCar);
                if(jsonArrayWithoutRgResult.length() > 0 && !jsonArrayResult.toString().contains(jsonArrayWithoutRgResult.toString())) {
                    jsonArrayResult.put(jsonArrayWithoutRgResult);
                }
            }

        }
        return jsonArrayResult;
    }
    
    private JSONArray extractInfo(JSONArray jsonArrayAttributes, String attribute, String domainConstraint, boolean maxCarError) throws JSONException {
        String attributeCode = attribute.split("\\|")[0].replace(" ", "");
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < jsonArrayAttributes.length(); i++) {
            if(jsonArrayAttributes.getJSONObject(i).getString("attributeNameCode").equals(attributeCode)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("domainConstraint", domainConstraint.split("\\|")[1]);
                jsonObject.put("attributeCode", jsonArrayAttributes.getJSONObject(i).getString("attributeNameCode"));
                jsonObject.put("attributeName", jsonArrayAttributes.getJSONObject(i).getString("attributeNameDisplay"));
                jsonObject.put("minCar", jsonArrayAttributes.getJSONObject(i).getString("attributeCardinalityMin").replace("]]", ""));
                jsonObject.put("maxCar", jsonArrayAttributes.getJSONObject(i).getString("attributeCardinalityMax").replace("[[", ""));
                jsonObject.put("is-max-car-error", maxCarError);
                if(maxCarError) {
                    jsonObject.put("suggestion", "The attribute should be represented in a separate RoleGroup.");
                } else {
                    jsonObject.put("suggestion", "Insert attribute relation with " + attribute + " in the RoleGroup.");
                }
                jsonArray.put(jsonObject);
            }
        }
        return jsonArray;
    }
}
