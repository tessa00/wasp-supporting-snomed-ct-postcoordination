package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class ErrorMessagePceOutputService {


    public ErrorMessagePceOutputService() {}

    public String createOutput(JSONObject jsonObject) throws JSONException {
        if(!jsonObject.getBoolean("result")) {
            JSONObject message = jsonObject.getJSONObject("message");

            StringBuilder content = new StringBuilder();
            content.append("Error: ").append(message.getString("pce").replace("\n", "")).append("\n");

            JSONArray errors = message.getJSONArray("errors");
            if(errors.length() == 2) {
                if(errors.getString(0).equals(errors.getString(1))) {
                    errors.remove(0);
                }
//                System.out.println("--> " + errors.getString(0));
//                System.out.println("--> " + errors.getString(1));
            }
            for (int i = 0; i < errors.length(); i++) {
                JSONObject error = errors.getJSONObject(i);
                if(error.has("result")) {
                    if (i == 0) {
                        content.append("### error no ").append(i + 1).append(" ###\n");
                    } else {
                        content.append("\n\n### error ").append(i + 1).append(" ###\n");
                    }

                    content.append(error.getString("result")).append("\n\n");
                    String identifierInactiveConcept = "ERROR_INACTIVE_CONCEPT";
                    String identifierAttributeValue = "ERROR_ATTRIBUTE_VALUE";
                    String identifierAttribute = "ERROR_ATTRIBUTE";
                    String identifierCardinality = "ERROR_CARDINALITY";
                    String identifierOther = "ERROR_OTHER";
                    String identifierConcept = "ERROR_CONCEPT";
                    String identifierGrouping = "ERROR_GROUPING";
                    String identifierDomain = "ERROR_DOMAIN";
                    if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierConcept) || Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierInactiveConcept)) {
                        getMessageConcept(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierAttributeValue)) {
                        getMessageAttributeValue(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierAttribute)) {
                        getMessageAttribute(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierGrouping)) {
                        getMessageGrouping(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierCardinality)) {
                        getMessageCardinality(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierDomain)) {
                        getMessageDomain(error, content);
                    } else if (Objects.equals(errors.getJSONObject(i).getString("identifier"), identifierOther)) {
                        getMessageOther(error, content);
                    }
                }
            }
            content.append("\n###################################################");
            return content.toString();
        }
        return "-";
    }

    private void getMessageAttributeValue(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");
        content.append(infoGeneral.getString("answer")).append("\n\n");

        JSONObject infoAttribute = error.getJSONObject("info-attribute");
        content.append(infoAttribute.getString("question")).append("\n");
        JSONArray attributeAnswers = infoAttribute.getJSONArray("answer");
        for (int j = 0; j < attributeAnswers.length(); j++) {
            if(j != 0) {
                content.append("---------\n");
            }
            JSONObject attributeAnswer = attributeAnswers.getJSONObject(j);
            String attribute = !attributeAnswer.getString("attribute").isEmpty() ? attributeAnswer.getString("attribute") : "-";
            String definition = !attributeAnswer.getString("definition").isEmpty() ? attributeAnswer.getString("definition") : "-";
            String range = !attributeAnswer.getString("range").isEmpty() ? attributeAnswer.getString("range") : "-";
            content.append("Attribute: ").append(attribute).append("\n");
            content.append("Definition: ").append(definition).append("\n");
            content.append("Range: ").append(range).append("\n");
        }
        JSONObject infoMatchingAttribute = error.getJSONObject("info-matching-attribute");
        content.append("\n").append(infoMatchingAttribute.getString("question")).append("\n");
        JSONArray matchingAnswers = infoMatchingAttribute.getJSONArray("answer");
        for (int j = 0; j < matchingAnswers.length(); j++) {
            if(j != 0) {
                content.append("---------\n");
            }
            JSONObject matchingAnswer = matchingAnswers.getJSONObject(j);
            String attribute = !matchingAnswer.getString("code").isEmpty() ? matchingAnswer.getString("code") : "-";
            String name = !matchingAnswer.getString("name").isEmpty() ? matchingAnswer.getString("name") : "-";
            if(attribute.length() > 1 && name.length() > 1) {
                content.append("Attribute: ").append(attribute).append(" |").append(name).append("|").append("\n");
            } else {
                content.append("Attribute: ").append("-").append("\n");
            }
            String definition = !matchingAnswer.getString("definition").isEmpty() ? matchingAnswer.getString("definition") : "-";
            String range = !matchingAnswer.getString("constraint").isEmpty() ? matchingAnswer.getString("constraint") : "-";
            content.append("Definition: ").append(definition).append("\n");
            content.append("Range: ").append(range).append("\n");
            content.append("In domain of focus concept: ").append(matchingAnswer.getBoolean("is-in-domain")).append("\n");
            content.append("Suggestion: ").append(matchingAnswer.getString("suggestion")).append("\n");
        }
    }

    private void getMessageAttribute(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");
        content.append(infoGeneral.getString("answer")).append("\n\n");

        JSONObject infoMatchingAttributeDomain = error.getJSONObject("info-matching-attribute-domain");
        content.append(infoMatchingAttributeDomain.getString("question")).append("\n");
        JSONArray matchingDomainAnswers = infoMatchingAttributeDomain.getJSONArray("answer");
        for (int j = 0; j < matchingDomainAnswers.length(); j++) {
            if(j != 0) {
                content.append("---------\n");
            }
            JSONObject matchingAnswer = matchingDomainAnswers.getJSONObject(j);
            if(matchingAnswer.has("code")) {
                String attribute = !matchingAnswer.getString("code").isEmpty() ? matchingAnswer.getString("code") : "-";
                String definition = !matchingAnswer.getString("definition").isEmpty() ? matchingAnswer.getString("definition") : "-";
                String range = !matchingAnswer.getString("constraint").isEmpty() ? matchingAnswer.getString("constraint") : "-";
                String name = !matchingAnswer.getString("name").isEmpty() ? matchingAnswer.getString("name") : "-";
                if(attribute.length() > 1 && name.length() > 1) {
                    content.append("Attribute: ").append(attribute).append(" |").append(name).append("|").append("\n");
                } else {
                    content.append("Attribute: ").append("-").append("\n");
                }
                content.append("Definition: ").append(definition).append("\n");
                content.append("Range: ").append(range).append("\n");
                content.append("In domain of focus concept: ").append(matchingAnswer.getBoolean("is-in-domain")).append("\n");
            }

            if(matchingAnswer.has("domains")) {
                for (int i = 0; i < matchingAnswer.getJSONArray("domains").length(); i++) {
                    if(i == 0) {
                        content.append("Domain: ");
                        content.append(matchingAnswer.getJSONArray("domains").getString(i)).append("\n");
                    } else {
                        content.append("      ").append(matchingAnswer.getJSONArray("domains").getString(i)).append("\n");
                    }
                }
            }
            content.append("Suggestion: ").append(matchingAnswer.getString("suggestion")).append("\n");
        }
    }

    private void getMessageGrouping(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");
        content.append(infoGeneral.getString("answer")).append("\n\n");

        if(error.has("ungrouped-attributes")) {
            JSONObject ungroupedAttributes = error.getJSONObject("ungrouped-attributes");
            content.append(ungroupedAttributes.getString("question")).append("\n");

            if (error.getJSONObject("ungrouped-attributes").getJSONArray("answer").length() > 0) {
                ungroupedAttributes = error.getJSONObject("ungrouped-attributes");
                JSONArray ungroupedAnswers = ungroupedAttributes.getJSONArray("answer");
                for (int j = 0; j < ungroupedAnswers.length(); j++) {
                    if(j != 0) {
                        content.append("---------\n");
                    }
                    JSONObject ungroupedAnswer = ungroupedAnswers.getJSONObject(j);
                    content.append("Attribute: ").append(ungroupedAnswer.getString("attributeCode")).append(" |").append(ungroupedAnswer.getString("attributeName")).append("|\n");
                    if (!ungroupedAnswer.getString("attributeValueName").isEmpty()) {
                        content.append("Attribute value:").append(ungroupedAnswer.getString("attributeValueCode")).append(" |").append(ungroupedAnswer.getString("attributeValueName")).append("|\n");
                    } else {
                        content.append("Attribute value:").append(ungroupedAnswer.getString("attributeValueCode")).append("\n");
                    }
                    content.append("Must ungrouped: ").append(ungroupedAnswer.getBoolean("mustUngrouped")).append("\n");

                }
            }
        }

        if(error.has("ungrouped-attributes")) {
            if (error.getJSONObject("grouped-attributes").getJSONArray("answer").length() > 0) {
                JSONObject groupedAttributes = error.getJSONObject("grouped-attributes");
                JSONArray groupedAnswers = groupedAttributes.getJSONArray("answer");
                for (int j = 0; j < groupedAnswers.length(); j++) {
                    if(j != 0) {
                        content.append("---------\n");
                    }
                    JSONObject groupedAnswer = groupedAnswers.getJSONObject(j);
                    content.append("Attribute: ").append(groupedAnswer.getString("attributeCode")).append(" |").append(groupedAnswer.getString("attributeName")).append("|\n");
                    if (!groupedAnswer.getString("attributeValueName").isEmpty()) {
                        content.append("Attribute value:").append(groupedAnswer.getString("attributeValueCode")).append(" |").append(groupedAnswer.getString("attributeValueName")).append("|\n");
                    } else {
                        content.append("Attribute value:").append(groupedAnswer.getString("attributeValueCode")).append("\n");
                    }
                    content.append("Must grouped: ").append(groupedAnswer.getBoolean("mustGrouped")).append("\n");
                    content.append("Self grouped: ").append(groupedAnswer.getBoolean("selfGrouped")).append("\n");
                }
            }
        }

        if(error.has("suggestion")) {
            JSONObject jsonObjectSuggestion = error.getJSONObject("suggestion");
            content.append("Suggestion:").append("\n");
            content.append(jsonObjectSuggestion.getString("suggestion")).append("\n");
        }
    }

    private void getMessageCardinality(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");
        content.append(infoGeneral.getString("answer")).append("\n\n");

        JSONObject infoCardinality = error.getJSONObject("info-cardinality");
        if (infoCardinality != null) {
            content.append(infoCardinality.getString("question")).append("\n");
            JSONArray cardinalityAnswers = infoCardinality.getJSONArray("answer");
            for (int j = 0; j < cardinalityAnswers.length(); j++) {
                JSONArray attributeDetails = cardinalityAnswers.getJSONArray(j);
                for (int k = 0; k < attributeDetails.length(); k++) {
                    if(j != 0) {
                        content.append("---------\n");
                    }
                    JSONObject attributeDetail = attributeDetails.getJSONObject(k);
                    content.append("Attribute value: ").append(attributeDetail.getString("attributeCode")).append(" |").append(attributeDetail.getString("attributeName")).append("|\n");
                    content.append("Domain Constraint: ").append(attributeDetail.getString("domainConstraint")).append("\n");
                    content.append("Minimum Car: ").append(attributeDetail.getString("minCar").replace("[[", "")).append("\n");
                    content.append("Maximum Car: ").append(attributeDetail.getString("maxCar").replace("[[", "")).append("\n");
                    content.append("Suggestion:  ").append(attributeDetail.getString("suggestion")).append("\n");
                }
            }
        }
    }


    private void getMessageConcept(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");
        content.append(infoGeneral.getString("answer")).append("\n\n");

        JSONObject infoAlternatives = error.getJSONObject("info-alternatives");
        content.append(infoAlternatives.getString("question")).append("\n");
        content.append("Message: ").append(infoAlternatives.getString("message")).append("\n");
        for (int i = 0; i < infoAlternatives.getJSONArray("answer").length(); i++) {
            for (int j = 0; j < infoAlternatives.getJSONArray("answer").getJSONArray(i).length(); j++) {
//                if(j != 0) {
//                    content.append("---------\n");
//                }
                content.append(infoAlternatives.getJSONArray("answer").getJSONArray(i).get(j));
            }
        }
    }

    private void getMessageOther(JSONObject error, StringBuilder content) throws JSONException {
        if(error.has("info-general")) {
            JSONObject infoGeneral = error.getJSONObject("info-general");
            content.append(infoGeneral.getString("question")).append("\n");
            content.append(infoGeneral.getString("answer")).append("\n\n");
        }
        if(error.has("info-suggestion")) {
            JSONObject infoSuggestion = error.getJSONObject("info-suggestion");
            content.append(infoSuggestion.getString("question")).append("\n");
            content.append(infoSuggestion.getString("answer")).append("\n");
            content.append(infoSuggestion.getString("pce")).append("\n\n");
        }
    }

    private void getMessageDomain(JSONObject error, StringBuilder content) throws JSONException {
        JSONObject infoGeneral = error.getJSONObject("info-general");
        content.append(infoGeneral.getString("question")).append("\n");

        content.append(infoGeneral.getString("answer")).append("\n\n");
        JSONObject infoMatchingDomain = error.getJSONObject("info-matching-domain");
        content.append(infoMatchingDomain.getString("question")).append("\n");
        for (int i = 0; i < infoMatchingDomain.getJSONArray("answer").length(); i++) {
            content.append(infoMatchingDomain.getJSONArray("answer").getString(i)).append("\n");
        }
        content.append("Suggestion: ").append(infoMatchingDomain.getString("suggestion")).append("\n");
    }

}
