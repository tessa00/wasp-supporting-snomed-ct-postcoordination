package org.tessa00.wasp.validation.errorTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.tessa00.wasp.validation.TerminologyServerRequest;
import org.tessa00.wasp.validation.WorkerService;

@Service
public class ErrorDomainFocusConcept {

    private final WorkerService workerService;
    private final TerminologyServerRequest terminologyServerRequest;

    public ErrorDomainFocusConcept(WorkerService workerService, TerminologyServerRequest terminologyServerRequest) {
        this.workerService = workerService;
        this.terminologyServerRequest = terminologyServerRequest;
    }

    public JSONObject getErrorDomainFocusConcept(String focusConcept, int count, String url, String version, JSONObject jsonObjectMrcm) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        JSONObject jsonObjectInfo = new JSONObject();
        jsonObjectInfo.put("question", "What does this error mean?");
        jsonObjectInfo.put("answer", "Each PCE or pre-coordinated concept can be assigned to a ConceptModel domain. These are usually sub-hierarchies of SNOMED CT. " +
                "To guarantee medically meaningful expressions, SNOMED CT attributes are defined for a domain, which can be used to create SNOMED CT expressions. " +
                "The domains for the focus concepts are different in the PCE shown, so that no syntactically and semantically correct PCE can be formed.");

        String[] focusConcepts = focusConcept.split("OR");
        JSONArray jsonArrayDomains = new JSONArray();
        for (String concept : focusConcepts) {
            String code = concept.replace(" ", "").replace("(", "").replace(")", "");
            JSONArray jsonObjectMatchingDomain = workerService.getMatchingConceptModelDomain(url, version, code, jsonObjectMrcm, 1);
            JSONArray jsonArray = workerService.getDomainNames(jsonObjectMatchingDomain);
            String fsn = code + " |" + terminologyServerRequest.lookUpNameFSN(url, version, code) + "|";
            for (int j = 0; j < jsonArray.length(); j++) {
                jsonArrayDomains.put(fsn + " -> " + jsonArray.getString(j));
            }
        }

        JSONObject jsonObjectMatchingDomains = new JSONObject();
        jsonObjectMatchingDomains.put("question",  "Which domains do the focus concepts belong to?");
        jsonObjectMatchingDomains.put("answer",  jsonArrayDomains);
        jsonObjectMatchingDomains.put("suggestion",  "Change the focus concepts so that it only belongs to one domain.");

        jsonObject.put("result", "The " + count + " focus concepts do not belong to the same domain.");
        jsonObject.put("info-general", jsonObjectInfo);
        jsonObject.put("info-matching-domain", jsonObjectMatchingDomains);

        if(jsonArrayDomains.length() == 2) {
            String domainA  = jsonArrayDomains.getString(0);
            String domainB = jsonArrayDomains.getString(1);
            if(domainA.contains("Clinical finding (finding)") && domainB.contains("Disease (disorder)") || domainB.contains("Clinical finding (finding)") && domainA.contains("Disease (disorder)")) {
                return new JSONObject();
            }
        }

        return jsonObject;
    }
}
