package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class TemplateProcessingService {

    private final TerminologyServerRequestsService terminologyServerRequestsService;

    public TemplateProcessingService(TerminologyServerRequestsService terminologyServerRequestsService) {
        this.terminologyServerRequestsService = terminologyServerRequestsService;
    }

    public JSONObject init(String url, String version, String template) throws JSONException, IOException {
//
//        String input = readJsonFile(file);
        JSONObject jsonObjInput = new JSONObject(template);
        String logicalTemplate = getLogicalTemplate(jsonObjInput);

//        System.out.println("######");
//        System.out.println(logicalTemplate);

        String templateName = jsonObjInput.getString("name").replace(".json", "").replace("v2", "").replace("v1", "");

//        logicalTemplate = "71388002|Procedure(procedure)|:" +
//                "[[~1..1]]{" +
//                "   [[~0..*]]260686004|Method(attribute)|=312251004|Computedtomographyimaging-action(qualifiervalue)|," +
//                "   [[~0..*]]405813007|Proceduresite-Direct(attribute)|=[[+id(<<11527006|Arterialsystemstructure(bodystructure)|)@procSite]]}";



//        logicalTemplate = "
//        64572001|Disease(disorder)|:" +
//                "[[0..1]]42752001|Dueto(attribute)|=[[+id(<<272379006|Event(event)|)@event]]" ;
//                "[[0..*]]{" +
//                "   [[0..*]]116676008|Associatedmorphology(attribute)|=[[+id(<<13924000|Wound(morphologicabnormality)|)@woundMorphology]]" +
//                "   [[0..1]]363698007|Findingsite(attribute)|=[[+id(<<123037004|Bodystructure(bodystructure)|)@bodyStructure]]," +
//                "   [[0..1]]246075003|Causativeagent(attribute)|=[[+id(<<260787004|Physicalobject(physicalobject)|OR<<78621006|Physicalforce(physicalforce)|OR<<105590001|Substance(substance)|)@physicalObject]]," +
//                "   [[0..1]]370135005|Pathologicalprocess(attribute)|=[[+id(<<441862004|Infectiousprocess(qualifiervalue)|)@infectiousProcess]]" +
//                "}";

//        logicalTemplate = "410652009 |Blood product (product)|:\\n\\t[[~0..1]] 1142139005 |Count of base of active ingredient (attribute)| = [[+int(>#0..) @countOfBaseOfActiveIngredient]]";

//        logicalTemplate = "64572001:{116676008=49755003,370135005=472964009,363698007=442083009,246075003=105590001}";

//        System.out.println(logicalTemplate);


        String termTemplate = getTermTemplate(jsonObjInput); // TODO -- erst nach auswahl benötigt

        long t0 = System.nanoTime();

        String preprocessedLogicalTemplate = preprocessingLogicalTemplate(logicalTemplate);

        // todo
         String json =  createJson(url, version, preprocessedLogicalTemplate, templateName, termTemplate);

         try {
             System.out.println("----");
             System.out.println(json);
             JSONObject jsonObj = new JSONObject(json);

             long t1 = System.nanoTime();
             long seconds = TimeUnit.NANOSECONDS.toSeconds( t1-t0);
             System.out.println("Time: " + seconds);

             return jsonObj;
         } catch (Exception e){
            System.out.println(e.getMessage().substring(0, 100));
         }

        return null;


    }

    public JSONObject getTemplateLanguage(String template) throws IOException, JSONException {
        JSONObject jsonObjInput = new JSONObject(template);

        return jsonObjInput;
    }


    public String createJson(String url, String version, String preprocessedLogicalTemplate, String templateName, String termTemplate) throws JSONException {
        String json;

        // split in focus concept and refinement -- TODO: check if focus concept is a concept slot replacement
        if (preprocessedLogicalTemplate.contains(":")) {
            // Focus concept
            String focusConcept = preprocessedLogicalTemplate.split(":")[0];
            String focusConceptCode = "";
            String focusConceptName = "";
            if(focusConcept.contains("+")) {
                String [] s = focusConcept.split("\\+");
                for(int i = 0; i < s.length; i++) {
                    focusConceptCode = focusConceptCode + s[i].split("\\|")[0] + "+";
                    focusConceptName = focusConceptName + terminologyServerRequestsService.lookUpNameFsn(url, version, s[i].split("\\|")[0]) + "+";
                }
                focusConceptCode = focusConceptCode.substring(0, focusConceptCode.length()-1);
                focusConceptName = focusConceptName.substring(0, focusConceptName.length()-1);
            } else {
                focusConceptCode = focusConcept.split("\\|")[0];
                focusConceptName = terminologyServerRequestsService.lookUpNameFsn(url, version, focusConceptCode);
            }
            String fc = preprocessedLogicalTemplate.split(":")[0];

            json = "{\"nameTemplate\":\"" + templateName + "\",\"termTemplate\":\"" + termTemplate + "\",\"focusConcept\":{" +
                    "\"code\":\"" + focusConceptCode + "\",\"name\":\"" + focusConceptName + "\",\"fc\":\"" + fc + "\"}";

            // Refinement with and without role groups
            String refinement = preprocessedLogicalTemplate.split(":")[1];
            ArrayList<String> attributesWithoutRoleGroupsList = preprocessingRefinement(refinement).get(0);
            ArrayList<String> attributesWithRoleGroupsList = preprocessingRefinement(refinement).get(1);

            if (attributesWithoutRoleGroupsList.size() > 0 && !Objects.equals(attributesWithoutRoleGroupsList.get(0), "")) {
                // processing of attributes without role groups
                ArrayList<ArrayList<String>> preprocessedAttributesWithoutRoleGroupList = preprocessingAttributeValuePairs(attributesWithoutRoleGroupsList);
                json = json + ",\"withoutRoleGroups\":{\"attribute\":[";
                json = createJSONAttributes(url, version, preprocessedAttributesWithoutRoleGroupList, json);
                json =  json + "]},";
                json = json.substring(0, json.length()-1);
            }

            if (attributesWithRoleGroupsList.size() > 0) {

                json = json + ",\"withRoleGroups\":[" ;
                for (int i = 0; i < attributesWithRoleGroupsList.size(); i++) {
                    String s = attributesWithRoleGroupsList.get(i);
                    String minCardinality = s.split("%")[0].substring(3,4);
                    String maxCardinality = s.split("%")[0].substring(6,7);
                    String attribute = s.split("%")[1];

                    for (int j = 1; j < attribute.length()-1; j++) {
                        if (!((attribute.charAt(j-1) == ']') & (attribute.charAt(j+1) == '[')) & (attribute.charAt(j) == ',') & !((attribute.charAt(j-1) == '|') || (attribute.charAt(j+1) == '|')) & (attribute.charAt(j) == ',')) {
//                            System.out.println("   " + attribute.charAt(j-1) + attribute.charAt(j) + attribute.charAt(j+1));
                            attribute = attribute.substring(0, j-1) + "-" + attribute.substring(j+1);
                        }
                    }
                    ArrayList<String> helperList = new ArrayList<>();

                    Collections.addAll(helperList, attribute.split(","));
                    // processing of attributes with role groups
                    ArrayList<ArrayList<String>> preprocessedAttributesWithRoleGroupList = preprocessingAttributeValuePairs(helperList);

                    json = json + "{" +
                            "\"roleGroupCardinalityMin\":\"" + minCardinality + "\"," +
                            "\"roleGroupCardinalityMax\":\"" + maxCardinality + "\"," +
                            "\"attribute\":[";
                    json = createJSONAttributes(url, version, preprocessedAttributesWithRoleGroupList, json);
                    json = json + "]},";
                }
                json = json.substring(0, json.length()-1);
                json = json + "]";
//                System.out.println(json.length());
            }

            // last char
            json = json + "}";

            System.out.println(json);
            return json;
        }
        return "";

    }


    private String createJSONAttributes(String url, String version, ArrayList<ArrayList<String>> preprocessedAttributesList, String json) {
        for (int i = 0; i < preprocessedAttributesList.size(); i++) {
            ArrayList<String> attributeList = preprocessedAttributesList.get(i);
            String attributeValueDescendant = "";

            if (!Objects.equals(attributeList.toString(), "[]")) {
                String minCardinality = attributeList.get(0).substring(2, 3);
                String maxCardinality = attributeList.get(0).substring(5, 6);
                String attributeNameCode = attributeList.get(1).split("\\|")[0];
                String attributeNameName = terminologyServerRequestsService.lookUpName(url, version, attributeNameCode);
                if(attributeList.size() > 2) {
                    if (attributeList.get(2).contains("OR")) {
                        String[] s = attributeList.get(2).split("\\|OR");
                        for (int j = 0; j < s.length; j++) {
                            attributeValueDescendant = attributeValueDescendant + s[j].split("\\|")[0].replace("[[", "").replace("+id(", "").replace("(", "").replace(")", "");
                            if (j < s.length - 1) {
                                attributeValueDescendant = attributeValueDescendant + "OR";
                            }
                        }
                    } else if (attributeList.get(2).contains("AND")) {
                        String[] s = attributeList.get(2).split("\\|AND");
                        for (int j = 0; j < s.length; j++) {
                            attributeValueDescendant = attributeValueDescendant + s[j].split("\\|")[0].replace("[[", "").replace("+id(", "").replace("(", "").replace(")", "");
                            if (j < s.length - 1) {
                                attributeValueDescendant = attributeValueDescendant + "AND";
                            }
                        }
                    } else if (attributeList.get(2).contains("|MINUS")) {
                        String[] s = attributeList.get(2).split("\\|MINUS");
                        for (int j = 0; j < s.length; j++) {
                            attributeValueDescendant = attributeValueDescendant + s[j].split("\\|")[0].replace("[[", "").replace("+id(", "").replace("(", "").replace(")", "");
                            if (j < s.length - 1) {
                                attributeValueDescendant = attributeValueDescendant + "MINUS";
                            }
                        }

                    } else {
                        attributeValueDescendant = attributeList.get(2).split("\\|")[0].replace("[[", "").replace("+id(", "").replace("(", "").replace(")", "");
                        if (attributeValueDescendant.equals(attributeNameCode)) {
                            attributeValueDescendant = "";
                        }
                    }
                } else {
                    attributeValueDescendant = "";
                }
                json = json + "{" + "\"attributeCardinalityMin\":\"" + minCardinality + "\"," +
                        "\"attributeCardinalityMax\":\"" + maxCardinality + "\"," +
                        "\"attributeNameCode\":\"" + attributeNameCode.replace("[[", "") + "\"," +
                        "\"attributeNameDisplay\":\"" + attributeNameName + "\"," +
                        "\"constraint\":\"" + attributeValueDescendant + "\"";

                json = json + "},";
            }
        }
        json = json.substring(0, json.lastIndexOf(","));
        return json;
    }

    private ArrayList<ArrayList<String>> preprocessingAttributeValuePairs(ArrayList<String> attributeList) {


        ArrayList<ArrayList<String>> attributeSplitList = new ArrayList<>();
        for(int i = 0; i < attributeList.size(); i++) {
            ArrayList<String> helperList = new ArrayList<>();
            String attribute = attributeList.get(i).replace("~", "");

            if (!attribute.contains("+id")) {
                String s = attribute.replace("=", "=[[");
                attribute = s + "]]";
            }
            //        System.out.println(attribute);

            if (attribute.length() > 0) {
                int lastClosedBracket = 0;
                int lastOpenedBracket = 0;
                for (int j = 0; j < attribute.length() - 1; j++) {
                    if (attribute.charAt(j) == '[' && attribute.charAt(j + 1) == '[') {
                        lastOpenedBracket = j;
                        String s = attribute.substring(lastClosedBracket, lastOpenedBracket).replace("]]", "").replace("=", "");
                        if (!Objects.equals(s, "")) {
                            helperList.add(s);
                        }
                    } else if (attribute.charAt(j) == ']' && attribute.charAt(j + 1) == ']') {
                        lastClosedBracket = j;
                        String s = attribute.substring(lastOpenedBracket, lastClosedBracket + 2);
                        if (!Objects.equals(s, "") && !Objects.equals(s, "]]")) {
                            helperList.add(s);
                        }
                    }
                }
                attributeSplitList.add(helperList);
            }
        }
        return attributeSplitList;
    }

    private ArrayList<ArrayList<String>> preprocessingRefinement(String refinement) {
        ArrayList<String> attributesWithoutRoleGroupsList = new ArrayList<>();
        ArrayList<String> attributesWithRoleGroupsList = new ArrayList<>();
        int lastClosedCurlyBracket = 0;
        int lastOpenedCurlyBracket = 0;

        //        System.out.println("--> " + refinement);


        for (int i = 0; i < refinement.length(); i++) {
            if (refinement.charAt(i) == '{') {
                lastOpenedCurlyBracket = i;
                if (lastOpenedCurlyBracket - 10 >= 0) {
                    String attributes = refinement.substring(lastClosedCurlyBracket, lastOpenedCurlyBracket - 10).replace("}", "");
                    //        System.out.println("A: " + attributes);
                    if(!attributes.equals("")) {
                        if (attributes.charAt(attributes.length() - 2) != ']') {
                            attributes = attributes + "]";
                        }
                    }

                    if (attributes.contains(",")) {
                        String[] array = attributes.split(",");
                        Collections.addAll(attributesWithoutRoleGroupsList, array);
                    } else {
                        attributesWithoutRoleGroupsList.add(attributes);
                    }
                }

            } else if (refinement.charAt(i) == '}') {
                lastClosedCurlyBracket = i;
                if (lastOpenedCurlyBracket - 9 >= 0) {
                    attributesWithRoleGroupsList.add(refinement.substring(lastOpenedCurlyBracket - 9, lastClosedCurlyBracket + 1).replace("{", "%").replace("}", ""));
                }
            }
        }
        if (refinement.lastIndexOf("}") < refinement.length()) {
            String attributes = refinement.substring(refinement.lastIndexOf("}") + 1);
            if (attributes.contains(",")) {
                String[] array = attributes.split(",");
                Collections.addAll(attributesWithoutRoleGroupsList, array);
            }  else {
                attributesWithoutRoleGroupsList.add(attributes);
            }
        }

        // nur role group
//        if (attributesWithRoleGroupsList.size() == 0) {
//            System.out.println("ref : " + refinement);
//            String car = "";
//            for (int i = 0; i < refinement.length(); i++) {
//                if (refinement.charAt(i) == '{') {
//                    lastOpenedCurlyBracket = i;
//                    car = refinement.substring(lastOpenedCurlyBracket - 8, lastOpenedCurlyBracket - 1);
//                } else if (refinement.charAt(i) == '}') {
//                    lastClosedCurlyBracket = i;
//                }
//                attributesWithRoleGroupsList.add(car + refinement.substring(lastOpenedCurlyBracket, lastClosedCurlyBracket).replace("{", "%").replace("}", ""));
//            }
////        System.out.println("result : " + lastOpenedCurlyBracket + " : " + lastClosedCurlyBracket);
//        }
//        System.out.println("HIER " + attributesWithRoleGroupsList);


        //        System.out.println(attributesWithoutRoleGroupsList);
        //        System.out.println(attributesWithRoleGroupsList);


        ArrayList<ArrayList<String>> result = new ArrayList<>();
        result.add(attributesWithoutRoleGroupsList);
        result.add(attributesWithRoleGroupsList);

        return result;
    }


    public String preprocessingLogicalTemplate(String logicalTemplate) {
        // remove all \t, \n and blanks
        String preprocessedLogicalTemplate = logicalTemplate.replace("\t", "").replace("\n", "").replace("\\t", "").replace("\\n", "").replace(" ", "").replace("\u000b", "");
        String s = "";
        // check if cardinalities are missing
        for (int j = 1; j < preprocessedLogicalTemplate.length(); j++) {
            if ((preprocessedLogicalTemplate.charAt(j) == '{' ) && !(preprocessedLogicalTemplate.charAt(j - 1) == ']' )) {
//                System.out.println("-- " + preprocessedLogicalTemplate.charAt(j) + " : " + j);
                preprocessedLogicalTemplate = preprocessedLogicalTemplate.substring(0, j) + "[[~1..*]]" + preprocessedLogicalTemplate.substring(j);
            }
        }

        for (int j = 1; j < preprocessedLogicalTemplate.length(); j++) {
            if ((preprocessedLogicalTemplate.charAt(j) == '{' ) && !(preprocessedLogicalTemplate.charAt(j + 1) == '[' )) {
                s = String.valueOf(preprocessedLogicalTemplate.charAt(j+1));
//                System.out.println(":: " + preprocessedLogicalTemplate.charAt(j) + preprocessedLogicalTemplate.charAt(j+1) + " " + isStringInt(s));
                if (isStringInt(s)) {
                    String car = "[[~1..*]]";
                    preprocessedLogicalTemplate = preprocessedLogicalTemplate.substring(0, j + 1) + car + preprocessedLogicalTemplate.substring(j + 1);
                    j += car.length();
                }
            }
        }

        for (int j = 1; j < preprocessedLogicalTemplate.length(); j++) {
            if (preprocessedLogicalTemplate.charAt(j) == ',') {
                s = String.valueOf(preprocessedLogicalTemplate.charAt(j+1));
//                System.out.println(":: " + preprocessedLogicalTemplate.charAt(j) + preprocessedLogicalTemplate.charAt(j+1) + " " + isStringInt(s));
                if (isStringInt(s)) {
                    String car = "[[~1..*]]";
                    preprocessedLogicalTemplate = preprocessedLogicalTemplate.substring(0, j + 1) + car + preprocessedLogicalTemplate.substring(j + 1);
                    j += car.length();
                }
            }
        }

        for (int j = 2; j < preprocessedLogicalTemplate.length(); j++) {
            if (preprocessedLogicalTemplate.charAt(j-2) == '[' && preprocessedLogicalTemplate.charAt(j-1) == '[' && preprocessedLogicalTemplate.charAt(j+1) == '.' && preprocessedLogicalTemplate.charAt(j+2) == '.') {
                preprocessedLogicalTemplate = preprocessedLogicalTemplate.substring(0, j) + "~" + preprocessedLogicalTemplate.substring(j);
            }
        }

        //        System.out.println(preprocessedLogicalTemplate);
        return preprocessedLogicalTemplate;
    }


    public boolean isStringInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private String getLogicalTemplate(JSONObject jsonObject)  {
        try {
            String logicalTemplate = (String) jsonObject.get("logicalTemplate");
            return logicalTemplate;
        } catch (Exception e) {
            return "";
        }
    }

    private String getTermTemplate(JSONObject jsonObject)  {
        try {
            JSONObject conceptOutline = (JSONObject) jsonObject.get("conceptOutline");
            JSONArray descriptions = (JSONArray) conceptOutline.get("descriptions");
            JSONObject j = (JSONObject) descriptions.get(0);
            String termTemplate = (String) j.get("termTemplate");
            return termTemplate;
        } catch (Exception e) {
            return "";
        }
    }

    public JSONObject getHoleConstraint(String template) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(template);
        String temp = (String) jsonObject.get("logicalTemplate");
        //        System.out.println("TEMP : " + temp);

        String output = "{";

        if(!temp.contains("{")) {
            //        System.out.println("H>IER0");
            output = output + "\"withoutRoleGroup\":[";

            String attributes = temp.split(":")[1].replace("{", "").replace("}", "");

            String[] separateAttributes = attributes.split(",\n");
            for(int i = 0; i < separateAttributes.length; i++) {
                //        System.out.println("-------------");
                //        System.out.println(separateAttributes[i]);
            }
            // only for without attributes
            for(int i = 0; i < separateAttributes.length; i++) {
                String constraint = separateAttributes[i].split("=")[1].replace("[[ +id (", "").replace("]]", "").replace("[[+id(", "");
                for (int j = 0; j < constraint.length(); j++) {
//                    if(Character.toString(constraint.charAt(j)).equals("@")) {
//                        //        System.out.println(".... " + constraint);
//                        constraint = constraint.substring(0, j);
//                    }
                }
//                if(constraint.startsWith(" )", constraint.length()-3)) {
//                    constraint = constraint.substring(0, constraint.length()-3);
//                }

//                if(constraint.contains("|")) {
//                    constraint = constraint.substring(0, constraint.lastIndexOf("|")+1);
//                }


                output = output + "{\"constraint\":\"" + constraint + "\"},";
            }
            output = output.substring(0, output.length()-1);
            output = output + "]}";
            return new JSONObject(output);
        }

        if(temp.contains("{")) {
            //        System.out.println("H>IER1");
            String attributes = temp.split(":")[1];

            String without = "";
            String with = "";

            String [] part = attributes.split("\\{");
            //        System.out.println("PART : " + part);
            for(int i = 0; i < part.length; i++) {
                if (!part[i].contains("}")) {
                    part[i] = part[i].replace("[[~1..1]]", "");
                    part[i] = part[i].replace("[[~0..1]]", "");
                    part[i] = part[i].replace("[[~0..*]]", "");
                    part[i] = part[i].replace("[[~1..*]]", "");
                    String[] attribute = part[i].split("\n");
                    for (int k = 0; k < attribute.length; k++) {
                        if (attribute[k].contains("=")) {
                            String constraint = attribute[k].split("=")[1].replace("[[ +id (", "").replace("]]", "");
                            for (int j = 0; j < constraint.length(); j++) {
                                if (Character.toString(constraint.charAt(j)).equals("@")) {
                                    constraint = constraint.substring(0, j);
                                }
                            }
                            if (constraint.startsWith(" )", constraint.length() - 3)) {
                                constraint = constraint.substring(0, constraint.length() - 3);
                            }
                            if(constraint.contains(",")) {
                                without = without + "{\"constraint\":\"" + constraint.substring(0, constraint.indexOf(",")) + "\"},";
                            } else {
                                without = without + "{\"constraint\":\"" + constraint.replace("[[+id(", "").replace("|)", "") + "\"},";
                            }
                        }
                    }
                } else {
                    part[i] = part[i].replace("[[~1..1]]", "");
                    part[i] = part[i].replace("[[~0..1]]", "");
                    part[i] = part[i].replace("[[~0..*]]", "");
                    part[i] = part[i].replace("[[~1..*]]", "");
                    String[] rg = part[i].split("},");
                    for (int j = 0; j < rg.length; j++) {
                        if (rg[j].contains("=")) {
                            String[] attribute = rg[j].split("\n");

                            for (int k = 0; k < attribute.length; k++) {
                                if (attribute[k].length() > 2) {
                                    //        System.out.println("-------------------");
                                    //        System.out.println(attribute[k]);
                                    if (attribute[k].contains("=")) {
                                        String constraint = attribute[k].split("=")[1].replace("[[ +id (", "").replace("[[ +id(", "").replace("]]", "").replace("}", "").replace("{", "").replace("\n", "").replace("[[+id(", "").replace("[[+id (", "");
                                        //        System.out.println(constraint);
                                        for (int l = 0; l < constraint.length(); l++) {
//                                            if (Character.toString(constraint.charAt(l)).equals("@")) {
//                                                //        System.out.println(".... " + constraint);       // attributeCode +
//                                                constraint = constraint.substring(0, l);
//                                            }
                                        }
//                                        if (constraint.startsWith(" )", constraint.length() - 3)) {
//                                            constraint = constraint.substring(0, constraint.length() - 3);
//                                        }
//                                        if(constraint.contains("|")) {
//                                            constraint = constraint.substring(0, constraint.lastIndexOf("|") + 1);
//                                        }
                                        String code = attribute[k].split("=")[0].split("\\|")[0].replace(" ", "");
                                        //        System.out.println(code + " :: " + constraint);
                                        System.out.println(constraint);
                                        with = with + "{\"code\":" + code + ",\"constraint\":\"" + constraint.replace(",", "") + "\"},";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(without.length() > 0) {
                without = without.substring(0, without.length()-1);
                output = output + "\"withoutRoleGroup\":[" + without + "],";
            }
            with = with.substring(0, with.length()-1);
            output = output + "\"withRoleGroup\":[" + with + "]}";

            return new JSONObject(output);
        }
        return new JSONObject("{}");
    }




}
