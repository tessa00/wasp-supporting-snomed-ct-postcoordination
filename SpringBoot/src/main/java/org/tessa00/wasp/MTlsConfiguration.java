package org.tessa00.wasp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.nio.file.Path;

@ConfigurationProperties(prefix = "mtls-configuration")
@ConstructorBinding
public class MTlsConfiguration {

    private String keystoreType = null;
    private String certificatePassword = null;
    private Path certificatePath = null;

    @ConstructorBinding
    public MTlsConfiguration(String certificatePath, String certificatePassword, String keystoreType) {
        if (certificatePath != null) {
            this.certificatePath = Path.of(certificatePath);
            if (!this.certificatePath.toFile().exists()) {
                throw new IllegalStateException("A certificate was configured, but the file at %s is not readable".formatted(certificatePath));
            }
            if (certificatePassword == null) {
                throw new IllegalStateException("A certificate for mTLS was configured, but no password was provided!");
            }
            if (keystoreType == null) {
                throw new IllegalStateException("Missing keystore type");
            }
        }
        if (certificatePassword != null) {
            this.certificatePassword = certificatePassword;
        }
        if (keystoreType != null && !keystoreType.isBlank()) {
            this.keystoreType = keystoreType;
        }
    }

    public Path getCertificatePath() {
        return certificatePath;
    }

    public String getCertificatePassword() {
        return certificatePassword;
    }

    public String getKeystoreType() {
        return keystoreType;
    }

    public boolean isConfigured() {
        return getCertificatePath() != null && certificatePassword != null && keystoreType != null;
    }
}