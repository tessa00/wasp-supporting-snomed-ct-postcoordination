package org.tessa00.wasp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class SettingsService {

    private final HttpClientService httpClient;

    public SettingsService(HttpClientService httpClientService) {
        this.httpClient = httpClientService;
    }

    public JSONObject init(String url) throws JSONException {
        String cs = testUrl(url, "CodeSystem");
        String vs = testUrl(url, "ValueSet");
        String json;

        if(cs.contains("true") && vs.contains("true")) {
            json = "{\"result\":\"true\"}";
        } else if(cs.contains("@")) {
            json = "{\"result\":\"false" + "@" + cs.split("@")[1] + "\"}";
        } else {
            json = "{\"result\":\"false\"}";
        }
        return new JSONObject(json);
    }

    public String testUrl(String url, String value) {
        String urlNew = url + "/" + value;

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());
            if(response.toString().contains("200")) {
                return "true";
            } else if(response.toString().contains("403")) {
                return "error@ 403 Forbidden!";
            } else if(response.toString().contains("404")) {
                return "error@ 403 Not Found!";
            } else if(response.body().contains("This service uses the Deutsches Forschungsnetz Public Key Infrastructure")) {
                return "false@A certificate of the Deutsches Forschungsnetz is required!";
            }
            return "false";
        } catch (Exception e) {
            return "false";
        }
    }

    public JSONObject getSnomedVersions(String url) throws JSONException {
        String urlNew = url + "/CodeSystem";
        StringBuilder result = new StringBuilder("{\"value\": [");

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(urlNew))
                    .build();

            HttpResponse<String> response = httpClient.getClient().send(request, HttpResponse.BodyHandlers.ofString());

            if(response.body().contains("This service uses the Deutsches Forschungsnetz Public Key Infrastructure")) {
                return new JSONObject("{\"result\":\"" +  "\", \"message\":\"A certificate of the Deutsches Forschungsnetz is required!\"}");

            }

            JSONArray jsonArray = new JSONArray("[" + response.body() + "]");

            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject json = (JSONObject) jsonArray.get(j);
                JSONArray entry = (JSONArray) json.get("entry");

                for (int k = 0; k < entry.length(); k++) {
                    json = (JSONObject) entry.get(k);
                    json = (JSONObject) json.get("resource");
                    if(json.toString().contains("\"version\"")) {
                        String version = json.getString("version");
                        if(!version.contains("/xsct/") && version.contains("snomed.info/sct")) {
                            result.append("{\"result\":\"").append(version).append("\"},");
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        }
        if(result.toString().contains(",")) {
            result = new StringBuilder(result.substring(0, result.lastIndexOf(",")));
        } else {
            return new JSONObject("{\"result\":\"" +  "\", \"message\":\"No SNOMED CT version on the server!\"}");
        }
        return new JSONObject(result + "]}");
    }

}
