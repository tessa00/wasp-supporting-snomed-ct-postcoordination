Error: 64572001:{116676008=72704001,116676008=72704001}
### error no 1 ###
Result:		Not MRCM compatible: The attribute 116676008 |Associated morphology (attribute)| can only be used to a maximum of 1 per Role Group

What does this error mean?
Answer:		A cardinality is defined for each attribute relation as well as RoleGroup. This specifies the minimum and maximum number of times a particular attribute relation or RoleGroup and maximum number of times it may be used.

What are the cardinalities of the attribute?
Answer:		Attribute value:	116676008 |Associated morphology|
			Domain Constraint:	Clinical finding (finding)
			Minimum Car:		0
			Maximum Car:		1
			Suggestion:			The attribute should be represented in a separate RoleGroup.
			---------
			Attribute value:	116676008 |Associated morphology|
			Domain Constraint:	Disease (disorder)
			Minimum Car:		0
			Maximum Car:		1
			Suggestion:			The attribute should be represented in a separate RoleGroup.

###################################################