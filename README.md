# WASP - Supporting SNOMED CT Postcoordination

## Journal:
WASP—A Web Application to Support Syntactically and Semantically Correct SNOMED CT Postcoordination;
Drenkhahn, Cora, Ohlsen, Tessa, Wiedekopf, Joshua, and Ingenerf, Josef;
Applied Sciences May 2023

## 1. Preparations:
- Folder "wasp" must be on the desktop (Download: https://gitlab.com/tessa00/wasp-data/-/tree/main)
- Specification of the own terminology server is defined in the file server.json

    `{
        "name": "name of terminology server",
        "url": "url of terminology server"
    }` 

## 2. Running project:
- Run package.json in the console with the command "npm install"
- Start SpringBoot and Angular application
- WASP can be started at the following URL: http://localhost:4200/

## 3. Functionalities:

First of all, a SNOMED CT Version and Edition must be selected, which is located on the selected terminology server.
All entries follow the same principle: The descriptions of a SNOMED CT concept (variant1) or a SNOMED CT Identifier (variant2) can be entered. In variant1: based on the entered character combination, possible concepts are displayed in a list. The focus concept is selected from this list.
In addition, information about the attribute, such as a definition or the value range, is displayed in an information field.


### 3.1 Create PCE based on the Concept Model:
- **in WASP:** Create new PCE -> Using Concept Model

Here, first the focus concept is entered in the input field. 
The SNOMED CT attributes that are based on the Concept Model Domain and are relevant for a use case can be selected. Valid attributes are displayed in a list. This is shown in the table "Attribute summary".
The attribute value is selected at the bottom of the right-hand side in the "Attribute value" section. In addition, information about the attribute is displayed in an information field.
Finally, the PCE is created based on the Compositional Grammar. This can be saved (see extra section).

<img src="SpringBoot/src/main/resources/images/image-3.1.png" style="display: inline-block; margin: 0 auto; max-width: 300px" alt="">


### 3.2 Create PCE based on SNOMED CT Expression Templates:
- **in WASP:** Create new PCE -> Using Templates

First, a template (SNOMED CT Expression Template or your own generated template) is selected. The attribute values are then selected. Additional information is also displayed for each attribute by clicking on the "i" of the associated attribute. Finally, the PCE is created. This can be saved (see extra section).

<img src="SpringBoot/src/main/resources/images/image-3.2.png" style="display: inline-block; margin: 0 auto; max-width: 300px" alt="">


### 3.3 Generate own template:
- **in WASP:** Generate template

Here, the form is structured in the same way as the PCE creation based on the Concept Model. 
In addition, cardinalities are selected here. 
Furthermore, logical operators and constraint operators can be used to define the value range. 
This is shown below right in the section "Attribute value range".
The template is created based on the SNOMED CT Expression Template Language and stored in JSON format 
in the folder "templates", which is located in the folder "wasp".

<img src="SpringBoot/src/main/resources/images/image-3.3.png" style="display: inline-block; margin: 0 auto; max-width: 300px" alt="">


### 3.4 Additional features:

After a PCE is created, the following options are possible:
- Copy to clip board
- Download as text file
- Storage in a CodeSystem Supplement.

When saving in WASP for the first time, a new CodeSystem Supplement should be created 
(WASP only recognizes CodeSystem Supplements with the name "SctPceSupplementPceBuilder") 
and the PCE should be created in it. If a CodeSystem Supplement with the name "SctPceSupplementPceBuilder" 
exists, the PCEs can be stored in it in the further process.
In addition, PCEs stored in a CodeSystem Supplement created in WASP can be reloaded into WASP and modified
(Update existing PCE).
A new storage or an update is possible.
