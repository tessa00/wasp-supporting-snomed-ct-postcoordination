import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom, Observable} from 'rxjs';
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class TemplatesService {

  constructor(public backendService: BackendService, private settingsService: SettingsService, private settingsCsSupplementService: SettingsCsSupplementService, private settingsTemplateService: SettingsTemplatesService, private  appService: AppService, private templateGenerationService: TemplatesGenerationService, private pceWithoutTemplateService:PceWithoutTemplateService) {
  }

  public testChangeBoolean = false;


  click(id: string) {
    this.appService.selectedApp = id;
    this.appService.selectedItem = '';
    this.templateGenerationService.start = '';
  }


  getTemplateName(value: string): Observable<any> {
    let template = this.encodeURIComponent(value);
    const url = this.backendService.getBackendUrl() + 'templatecontent/?url=' + this.settingsService.serverInit + '&version=' + this.settingsService.sctVersion + '&template=' + template;
    return this.backendService.httpClient.get(url);
  }

  getTemplateLanguage(value: string): Observable<any> {
    let template = this.encodeURIComponent(value);
    const url = this.backendService.getBackendUrl() + 'templatelanguage/?template=' + template;
    return this.backendService.httpClient.get(url);
  }

  getHoleConstraint(value: string) {
    let template = this.encodeURIComponent(value);
    const url = this.backendService.getBackendUrl() + 'holeconstraint?template=' + template;
    return this.backendService.httpClient.get(url);
  }

  lookUpName(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'lookupname/?url=' + this.settingsService.serverInit + '&code=' + c + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  getAttributeValueRequest(expression: string)  {
    let ex = this.encodeURIComponent(expression);
    const url = this.backendService.getBackendUrl() + 'attributevalueswithoutfilter/?url=' + this.settingsService.serverInit + '&expression=' + ex + '&version' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);

    // return this.backendService.httpClient.get(url);
  }

  updateNewCodeSystem(pce: string, name: string, templatename: string) {
    let pce1 = this.encodeURIComponent(pce);
    let name1 = this.encodeURIComponent(name);
    let templatename1 = this.encodeURIComponent(templatename);

    const url = this.backendService.getBackendUrl() + 'updatenewcodesystemsupplement/?url=' + this.settingsTemplateService.serverChange + '&sctversion=' + this.settingsService.sctVersion + '&pce=' + pce1 + '&pcename=' + name1 + '&templatename=' + templatename1 + '&id=' + this.settingsTemplateService.newIdCsSupplement  + '&namecss=' + this.settingsTemplateService.newNameCsSupplement;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  updateExistingCodeSystem(pce: string, name: string, templatename: string) {
    let pce1 = this.encodeURIComponent(pce);
    let name1 = this.encodeURIComponent(name);
    let templatename1 = this.encodeURIComponent(templatename);


    const url = this.backendService.getBackendUrl() + 'updateexistingcodesystemsupplement/?url=' + this.settingsTemplateService.serverChange + '&sctversion=' + this.settingsService.sctVersion + '&pce=' + pce1 + '&pcename=' + name1 + '&templatename=' + templatename1 + '&idcss=' + this.settingsTemplateService.newIdCsSupplement + '&title=' + this.settingsTemplateService.newNameCsSupplement;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  getCodeSystem(urlCs: string) {
    let u = this.encodeURIComponent(urlCs);
    const url = this.backendService.getBackendUrl() + 'getcodesystemsupplement/?url=' + u;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  updateCodeSystemUpdatedPce(id:string, pce: string, name: string, templatename: string, samecs: string) {
    let id1 = this.encodeURIComponent(id);
    let pce1 = this.encodeURIComponent(pce);
    let name1 = this.encodeURIComponent(name);
    let templatename1 = this.encodeURIComponent(templatename);
    const url = this.backendService.getBackendUrl() + 'updatecodesystemsupplementwithstoredpce/?url=' + this.settingsTemplateService.serverChange + '&id=' + id1 + '&pce=' + pce1 + '&name=' + name1 + '&templatename=' + templatename1 + '&namecssupplement=' + this.settingsTemplateService.newIdCsSupplement + '&sctversion=' + this.settingsService.sctVersion + '&samecs=' + samecs + '&title=' + this.settingsTemplateService.newNameCsSupplement;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  validatePce(pce: string)  {
    let p = this.encodeURIComponent(pce);
    const url = this.backendService.getBackendUrl() + 'validatepce/?url=' + this.settingsService.serverInit + '&pce=' + p + '&version' + this.settingsService.snomedBrowserVersion.split("-").join("");
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

  // validatepce

  async test(body: any) {
    const url = this.backendService.getBackendUrl() + 'postbody';
    const response = this.backendService.httpClient.post(url, body);
    return await lastValueFrom(response);
  }






  async getHttpAutoFillItem(body: any) {
    const url = this.backendService.getBackendUrl() + 'repair/autofill_item';
    const response = this.backendService.httpClient.post(url, body);
    return await lastValueFrom(response);
  }

  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }




}
