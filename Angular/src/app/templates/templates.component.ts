import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {TemplatesService} from "./templates.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";
import {HighlightingPceService} from "../highlightingPce/highlightingPce.service";
import {HighlightingTemplateService} from "../highlightingTemplate/highlightingTemplate.service";
import {DiagramComponent} from "../ui/diagram.component";
import {arrow} from "@popperjs/core";
import {Title} from "@angular/platform-browser";
import {ProcessingMrcmService} from "../processingMrcm/processingMrcm.service";
import {ValidationToolService} from "../validationTool/validationTool.service";


@Component({
  selector: 'app-template',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})

export class TemplatesComponent {

  constructor(public templateService: TemplatesService, private titleService:Title, private http: HttpClient, public appService: AppService, public dia: DiagramComponent, public highlightingPceService: HighlightingPceService, public highlightingTemplateService: HighlightingTemplateService, public templateGenerationService: TemplatesGenerationService, public settingsService: SettingsService, public settingsCsService: SettingsCsSupplementService, public settingsTemplateService: SettingsTemplatesService, public pceWithoutTemplateService: PceWithoutTemplateService, public processingMrcmService: ProcessingMrcmService, public validationToolService: ValidationToolService) {
    this.titleService.setTitle("WASP");
  }



  public url = 'http://localhost:8080/';

  public title = 'WASP';
  public code = '';


  public templateNamesList = new Array();
  public templateName = '';

  public showTemplateContent = false;
  public changeTemplateChoice = false;

  // Focusconcept
  public focusConceptName = '';
  public focusConceptCode = '';
  public focusConcept = '';

  // attributes without role group
  public listAttributeWithoutMinCar = new Array();
  public listAttributeWithoutMaxCar = new Array();
  public listAttributeWithoutName = new Array();
  public listAttributeWithoutCode = new Array();
  public listAttributeWithoutConstraint = new Array();
  public listAttributeWithoutConstraintComplete = new Array();
  public attributesWithoutGroup = new Array();
  public attributeValuesNoConstraintWithout = new Array();
  public listAttributeValueCodesFilter = new Array();
  public listAttributeValueNameFilter = new Array();
  public attributesFilterBoolean = false;
  public fixedValuesCodeWithout = new Array();
  public fixedValuesNameWithout = new Array();
  public attributeNoConstraintWithoutBoolean = new Array();

  // attribute values without groups
  public attributeValueWithout = new Array();
  public listAttributeValueCodesWithout = new Array();
  public listAttributeValueNameWithout = new Array();
  public idAttributeWithout = -1;

  public oldId: number = -1;
  // table of attributes without groups
  public showTablesWithout = false;
  public listSelectedValueName = new Array();
  public listSelectedValueCode = new Array();
  public listSelectedAttributeValueCodesSummaryWithout = new Array();
  public listSelectedAttributeValueNameSummaryWithout = new Array();
  public idTableRow = '';
  public showAttributeWithout = false;
  public fixedBooleanWithout = new Array();

  // attributes with role group
  public listRoleGroupsMinCar = new Array();
  public listRoleGroupsMaxCar = new Array();
  public attributesWithGroup = new Array();
  public attributeValueWith = new Array();
  public listAttributeWithMinCar = new Array();
  public listAttributeWithMaxCar = new Array();
  public listAttributeWithName = new Array();
  public listAttributeWithCode = new Array();
  public listAttributeWithConstraint = new Array();
  public attributeValuesNoConstraintWith = new Array();
  public listAttributeWithConstraintComplete = new Array();
  public listAttributeWithConstraintCompleteCodes = new Array();
  public fixedBooleanWith = new Array();
  public fixedBooleanRGWith = new Array();
  public attributeNoConstraintWithBoolean = new Array();

  public listSelectedAttributeValueCodesSummaryWith = new Array();
  public listSelectedAttributeValueNameSummaryWith = new Array();

  public listAttributeValueCodesWith = new Array();
  public listAttributeValueNameWith = new Array();
  public fixedValuesCodeWith = new Array();
  public fixedValuesNameWith = new Array();
  public keypressWith = true;

  public idWith = '';

  public showTableWith = false;
  public oldIDRow = -1;

  public oldIDCol = -1;

  public showAttributeWith = false;
  // summary role group
  public showSummaryRoleGroup = false;
  public listSummaryGroupsName = new Array();
  public listSummaryGroupsCodes = new Array();
  public listNumberRoleGroups = new Array();

  public isDeleteSummaryDisabled = true;
  // create pce
  public resultPCE = '';
  public namePCE = '';

  public showResult = false;
  public templateLanguage = '';

  public savePCETextarea = '';
  // attribute definition, when clicking on the attribute names
  public attributeDefinitionCMCode = new Array();
  public attributeDefinitionCMName = new Array();
  public attributeDefinitionCMDefinition = new Array();
  public attributeDefinitionCMRangeConstraint = new Array();

  // update stores pce
  public listStoredPCEs = new Array();
  public isBtnUpdateActive = false;
  public listStoredPCEsName = new Array();
  public id = -1;
  public update = false;
  public idRowStoredPCE = '';
  public semanticTagList = new Array();

  // -------------------------------------------------------------------------------------------------------------------

  click(id: string) {
    this.appService.selectedApp = id;
    this.appService.selectedItem = '';
    this.templateGenerationService.start = '';
    this.pceWithoutTemplateService.start = '';
  }

  clickItem(id: string) {
    this.appService.selectedItem = id;
  }

  deleteItemId() {
    this.appService.selectedItem = '';
    this.templateGenerationService.start = '';
  }

  goToLink(url: string){
    window.open(url, "_blank");
  }

  // identical for both

  public templateNamesListInit = new Array();
  public templateNamesListSemanticTagInit = new Array();
  public templateNamesListSemanticTag = new Array();
  public templateNamesListSemanticTag01 = new Array();
  public templateNamesListSemanticTag02 = new Array();



  ngOnInit(): void {
    if(this.settingsService.sctVersion.length === 0) {
      this.settingsService.clickSettings('ID_SETTINGS_START');
      this.appService.openPopup('settingsModal');
    }

    this.resetAllVariables();
    this.setSemanticTag();

    const waspData = localStorage.getItem("wasp_data");
    if (waspData) {
      let element = document.getElementById("useCacheData");
      // @ts-ignore
      element.disabled = false;
      console.log("Cache data")
    } else {
      console.log("No cache data")
    }
  }

  closeStartModel() {
    let element = document.getElementById("start");
    // @ts-ignore
    element.setAttribute("style", "pointer-events: auto;");
  }


  getTemplates() {
    this.loadAttributeDefinitionConceptModel();
    this.templateNamesListInit = new Array();
    this.templateNamesListSemanticTagInit = new Array();
    this.templateNamesListSemanticTag01 = new Array();
    this.templateNamesListSemanticTag02 = new Array();

    for (let i = 0; i < this.appService.allTemplates.templates.length; i++) {
      if(this.appService.allTemplates.templates[i].fileName !== undefined) {
        const name = this.appService.allTemplates.templates[i].fileName.split(".json").join("").split(":").join("/");
        const semanticTag = this.getSemanticTag(name);
        this.templateNamesList.push(name);
        this.templateNamesListInit.push(name);
        this.templateNamesListSemanticTagInit.push(semanticTag);
        if(!this.templateNamesListSemanticTag.includes(semanticTag)) {
          this.templateNamesListSemanticTag.push(semanticTag);
        }
      }
    }

    this.templateNamesListSemanticTag01 = new Array();
    this.templateNamesListSemanticTag02 = new Array();
    for(let i = 0; i < this.templateNamesListSemanticTag.length; i++) {
      if(i < this.templateNamesListSemanticTag.length/2) {
        this.templateNamesListSemanticTag01.push(this.templateNamesListSemanticTag[i]);
      } else {
        this.templateNamesListSemanticTag02.push(this.templateNamesListSemanticTag[i]);
      }
    }
    this.templateNamesList.sort();

  }


  getSemanticTag(name: string) {
    for (let i = 0; i < this.semanticTagList.length; i++) {
      if (name.includes("(" + this.semanticTagList[i] + ")")) {
        return this.semanticTagList[i];
      }
    }
    return "";
  }


  setSemanticTag() {
    this.semanticTagList.push("body structure");
    this.semanticTagList.push("cell");
    this.semanticTagList.push("cell structure");
    this.semanticTagList.push("morphologic abnormality");
    this.semanticTagList.push("finding");
    this.semanticTagList.push("disorder");
    this.semanticTagList.push("environment/location");
    this.semanticTagList.push("environment");
    this.semanticTagList.push("geographic location");
    this.semanticTagList.push("event");
    this.semanticTagList.push("observable entity");
    this.semanticTagList.push("organism");
    this.semanticTagList.push("clinical drug");
    this.semanticTagList.push("medicinal product");
    this.semanticTagList.push("medicinal product form");
    this.semanticTagList.push("physical object");
    this.semanticTagList.push("product");
    this.semanticTagList.push("physical force");
    this.semanticTagList.push("physical object");
    this.semanticTagList.push("product");
    this.semanticTagList.push("procedure");
    this.semanticTagList.push("regime/therapy");
    this.semanticTagList.push("regime-therapy");
    this.semanticTagList.push("qualifier value");
    this.semanticTagList.push("administration method");
    this.semanticTagList.push("basic dose form");
    this.semanticTagList.push("disposition");
    this.semanticTagList.push("dose form");
    this.semanticTagList.push("intended site");
    this.semanticTagList.push("number");
    this.semanticTagList.push("product name");
    this.semanticTagList.push("release characteristic");
    this.semanticTagList.push("role");
    this.semanticTagList.push("state of matter");
    this.semanticTagList.push("transformation");
    this.semanticTagList.push("supplier");
    this.semanticTagList.push("unit of presentation");
    this.semanticTagList.push("record artifact");
    this.semanticTagList.push("situation");
    this.semanticTagList.push("attribute");
    this.semanticTagList.push("core metadata concept");
    this.semanticTagList.push("foundation metadata concept");
    this.semanticTagList.push("link assertion");
    this.semanticTagList.push("linkage concept");
    this.semanticTagList.push("namespace concept");
    this.semanticTagList.push("social concept");
    this.semanticTagList.push("social concept");
    this.semanticTagList.push("ethnic group");
    this.semanticTagList.push("life style");
    this.semanticTagList.push("occupation");
    this.semanticTagList.push("person");
    this.semanticTagList.push("racial group");
    this.semanticTagList.push("religion/philosophy");
    this.semanticTagList.push("special concept");
    this.semanticTagList.push("inactive concept");
    this.semanticTagList.push("navigational concept");
    this.semanticTagList.push("specimen");
    this.semanticTagList.push("staging scale");
    this.semanticTagList.push("assessment scale");
    this.semanticTagList.push("tumor staging");
    this.semanticTagList.push("substance");
    this.semanticTagList.push("substance");
  }

  getTemplatesTag(i: number, id: string) {
    let elementInput = document.getElementById('tag' + id + i);

    // @ts-ignore
    let isChecked = elementInput.checked;
    let semanticTag = '';
    if(id === '01') {
      semanticTag = this.templateNamesListSemanticTag01[i];
    } else if(id === '02') {
      semanticTag = this.templateNamesListSemanticTag02[i];
    }

    if(isChecked) { // wieder hinzufügen
      for(let i = 0; i < this.templateNamesListSemanticTagInit.length; i++) {
        if(this.templateNamesListSemanticTagInit[i] === semanticTag) {
          this.templateNamesList.push(this.templateNamesListInit[i]);
        }
      }

    } else {    // wieder löschen
      for(let i = 0; i < this.templateNamesList.length; i++) {
        this.templateNamesList[i] = this.templateNamesList[i].split(":").join("/");
        if(this.templateNamesList[i].includes(semanticTag)) {
          delete this.templateNamesList[i];
        }
      }
    }
    // @ts-ignore
    this.templateNamesList = this.templateNamesList.filter(x => x !== undefined);
    this.templateNamesList.sort();
  }

  loadAttributeDefinitionConceptModel() {
    const file = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "conceptModelObjectAttributes.csv");
    const fileContent = file[0].fileContent;
    const arr = fileContent.split("\n");
    for (let i = 0; i < arr.length; i++) {
      let value = arr[i].split(";")
      this.attributeDefinitionCMCode.push(value[0]);
      this.attributeDefinitionCMName.push(value[1]);
      this.attributeDefinitionCMDefinition.push(value[2]);
      this.attributeDefinitionCMRangeConstraint.push(value[3]);
    }
  }

  getConstraintWithout(responseStart: any) {
    this.templateService.getHoleConstraint(this.appService.selectedTemplate).subscribe((response: any) => {
      if (typeof response.withoutRoleGroup != "undefined") {
        for (let i = 0; i < response.withoutRoleGroup.length; i++) {
          let x = response.withoutRoleGroup[i].constraint;
          if(x.toString().includes("+int") || x.toString().includes("+dec")) {
            this.listAttributeWithoutConstraintComplete.push(x.split(")").join(""));
          } else {
            let x = response.withoutRoleGroup[i].constraint.toString();
            let count = x.toString().split("|").length - 1
            if(count === 1) {
              x = x.split(") ").join(")") + "|";
            }
            this.listAttributeWithoutConstraintComplete.push(x.split(" <").join("<").split("| ").join("|"));
          }

        }
      }
      this.highlightingTemplateService.listAttributeWithoutConstraintComplete = this.listAttributeWithoutConstraintComplete;
      this.processingAttributesWithout(responseStart);
    })
  }

  processingAttributesWithout(response: any) {
    this.attributesWithoutGroup = response.withoutRoleGroups.attribute;

    for (let i = 0; i <= this.attributesWithoutGroup.length; i++) {
      if (typeof this.attributesWithoutGroup[i] != "undefined") {
        this.listAttributeWithoutMinCar.push(this.attributesWithoutGroup[i].attributeCardinalityMin);

        if (this.attributesWithoutGroup[i].attributeCardinalityMax === '*') {
          this.listAttributeWithoutMaxCar.push(Number.POSITIVE_INFINITY);
        } else {
          this.listAttributeWithoutMaxCar.push(this.attributesWithoutGroup[i].attributeCardinalityMax);
        }
        this.listAttributeWithoutCode.push(this.attributesWithoutGroup[i].attributeNameCode);
        this.listAttributeWithoutName.push(this.attributesWithoutGroup[i].attributeNameDisplay);
        this.listAttributeWithoutConstraint.push(this.attributesWithoutGroup[i].constraint);
      }

      for (let i = 0; i < this.listAttributeWithoutConstraint.length; i++) {
        this.listSelectedAttributeValueNameSummaryWithout.push(new Array());
        this.listSelectedAttributeValueCodesSummaryWithout.push(new Array());
      }

      this.highlightingTemplateService.listAttributeWithoutMinCar = this.listAttributeWithoutMinCar;
      this.highlightingTemplateService.listAttributeWithoutMaxCar = this.listAttributeWithoutMaxCar;
      this.highlightingTemplateService.listAttributeWithoutCode = this.listAttributeWithoutCode;
      this.highlightingTemplateService.listAttributeWithoutName = this.listAttributeWithoutName;
      this.highlightingTemplateService.listAttributeWithoutConstraint = this.listAttributeWithoutConstraint;

      this.fixedValuesNameWithout = new Array(this.listAttributeWithoutConstraint.length);
      this.fixedValuesCodeWithout = new Array(this.listAttributeWithoutConstraint.length);


      for (let i = 0; i < this.listAttributeWithoutConstraint.length; i++) {
        if (this.fixedValueWithout(i)) {
          this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithoutConstraint[i], '', '100000').then((response: any) => {
            const value = response.values;
            if(typeof value !== 'undefined') {
              for (let j = 0; j < value.length; j++) {
                this.fixedValuesCodeWithout[i] = value[j].code;
                this.fixedValuesNameWithout[i] = value[j].fsn;
                this.attributeValueWithout[i] = value[j].fsn;

                this.listSelectedValueName = value[j].fsn;
                this.listSelectedValueCode = value[j].code;

                if(!this.listSelectedAttributeValueCodesSummaryWithout[i].includes(value[j].code)) {

                  this.listSelectedAttributeValueNameSummaryWithout[i].push(this.listSelectedValueName);
                  this.listSelectedAttributeValueCodesSummaryWithout[i].push(this.listSelectedValueCode);

                  this.listAttributeValueCodesWithout = new Array();
                  this.listAttributeValueNameWithout = new Array();

                  this.fixedBooleanWithout[i] = true;
                }
              }
            } else if(!this.listAttributeWithoutConstraintComplete[i].includes("..")) {
              this.fixedBooleanWithout[i] = true;
              let value = this.listAttributeWithoutConstraintComplete[i].replace("[[+dec(", "").replace("[[+int(", "").split(" ").join("").replace("#", "").replace(")", "");
              if(value.includes("@")) {
                value = value.split("@")[0];
              }
              this.attributeValueWithout[i] = value;
              this.listSelectedAttributeValueNameSummaryWithout[i] = ["#" + value];
              this.listSelectedAttributeValueCodesSummaryWithout[i] = ["#" + value];
            }
          })
          this.templateService.getAttributeValueRequest(this.listAttributeWithoutConstraint[i]).then((response: any) => {
            const expansion = response.expansion;
            if (typeof expansion != "undefined") {
              const contains = expansion.contains;
              if (typeof contains != "undefined") {
                for (let j = 0; j <= contains.length; j++) {
                  if (typeof contains[j] != "undefined") {
                    if (typeof contains[j].display != "undefined") {
                      this.fixedValuesCodeWithout[i] = contains[j].code;
                      this.fixedValuesNameWithout[i] = contains[j].display;
                      this.attributeValueWithout[i] = contains[j].display;

                      this.listSelectedValueName = contains[j].display;
                      this.listSelectedValueCode = contains[j].code;

                      if(!this.listSelectedAttributeValueCodesSummaryWithout[i].includes(contains[j].code)) {

                        this.listSelectedAttributeValueNameSummaryWithout[i].push(this.listSelectedValueName);
                        this.listSelectedAttributeValueCodesSummaryWithout[i].push(this.listSelectedValueCode);

                        this.listAttributeValueCodesWithout = new Array();
                        this.listAttributeValueNameWithout = new Array();

                        this.fixedBooleanWithout[i] = true;
                      }
                    }
                  }
                }
              }
            }
          })

        } else {
          this.fixedBooleanWithout[i] = false;
        }
      }
      this.showAttributeWithout = true;
      if(!this.update) {
        this.showTemplateContent = true;
      }
    }

    for (let i = 0; i < this.listAttributeWithoutConstraint.length; i++) {
      this.attributeValuesNoConstraintWithout.push(new Array());
    }

    for (let i = 0; i < this.listAttributeWithoutConstraint.length; i++) {
      let constraint = this.listAttributeWithoutConstraint[i];
      if(!constraint.includes('<')) {
        if (constraint.toUpperCase().includes('OR')) {
          let or = constraint.toUpperCase().split('OR');
          let arr = new Array();
          for (let t = 0; t < or.length; t++) {

            this.templateService.lookUpName(or[t]).then((response: any) => {
              arr.push(response.name);
            })

          }
          this.attributeValuesNoConstraintWithout[i] = arr;
          this.attributeNoConstraintWithoutBoolean[i] = true;
        } else {
          this.templateService.lookUpName(constraint).then((response: any) => {
            let arr = new Array();
            arr.push(response.name);
            this.attributeValuesNoConstraintWithout[i] = arr;
            this.attributeNoConstraintWithoutBoolean[i] = true;
          })
        }
      } else {
        this.attributeValuesNoConstraintWithout[i] = new Array();
        this.attributeNoConstraintWithoutBoolean[i] = false;
      }
      setTimeout(() => {
        if(this.listAttributeWithoutMaxCar[i].toString() === '1') {
          let element = document.getElementById('add' + i);
          if (element != null) {
            // @ts-ignore
            element.disabled = true;
          }
        }
      }, 1000);
    }
  }

  popupInformationConstraintWithout(i: number) {
    let s = this.listAttributeWithoutConstraintComplete[i];
    if(s.includes("+int")) {
      s = s.split(" ").join("").split("[[+int").join("Integer: ").split(")").join("").split("(").join("");
    } else if(s.includes("+dec")) {
      s = s.split(" ").join("").split("[[+dec").join("Decimal: ").split(")").join("").split("(").join("");
    }

    if(s.includes("@")) {
      s = s.substring(0, s.indexOf("@")).split("|)").join("|");
    }
    s = s.replace("[[+id(", "");
    return s.split("|)").join("|");
  }


  popupInformationDefinition(code: string) {
    let info = '';
    for (let i = 0; i < this.attributeDefinitionCMCode.length; i++) {
      if (this.attributeDefinitionCMCode[i].toString() === code) {
        info = this.attributeDefinitionCMDefinition[i];
      }
    }
    return info;
  }

  popupInformationConstraint(code: string, row: number, col: number) {
    let number = 0;
    for(let i = 0; i < row; i++) {
      number = number + this.attributeValueWith[i].length;
    }
    number = number + col;
    let s = this.listAttributeWithConstraintComplete[row][number];
    if(s !== undefined) {
      if(s.includes("+int")) {
        s = s.split(" ").join("").split("[[+int").join("Integer: ").split(")").join("").split("(").join("");
      } else if(s.includes("+dec")) {
        s = s.split(" ").join("").split("[[+dec").join("Decimal: ").split(")").join("").split("(").join("");
      }
      if(s.includes("@")) {
        s = s.substring(0, s.indexOf("@")).split("|)").join("|");
      }
      return s.split("|)").join("|").split("| )").join("|");
    }
  }


  getConstraintWith(id: number) {
    this.templateService.getHoleConstraint(this.appService.selectedTemplate).subscribe((response: any) => {
      let fixedCounter = 0;
      if (typeof response.withRoleGroup != "undefined") {
        for (let i = 0; i < response.withRoleGroup.length; i++) {
          if(this.listAttributeWithConstraint[id].includes())
          this.listAttributeWithConstraintCompleteCodes[id].push(response.withRoleGroup[i].code);
          this.listAttributeWithConstraintComplete[id].push(response.withRoleGroup[i].constraint);
        }

        for (let j = 0; j < this.attributesWithGroup[id].length; j++) {
          if (this.fixedValueWith(id, j)) {
            this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithConstraint[id][j], '', '100000').then((response: any) => {
              const value = response.values;
              if (typeof value !== 'undefined') {
                for (let k = 0; k < value.length; k++) {
                  this.fixedValuesCodeWith[id][j] = value[k].code;
                  this.fixedValuesNameWith[id][j] = value[k].fsn;

                  this.attributeValueWith[id][j] = value[k].fsn;
                  this.fixedBooleanWith[id][j] = true;

                  let helperListName = new Array();
                  let helperListCode = new Array();
                  if (typeof this.listSelectedAttributeValueNameSummaryWith[id][j] !== 'undefined') {
                    helperListName = this.listSelectedAttributeValueNameSummaryWith[id][j];
                    helperListCode = this.listSelectedAttributeValueCodesSummaryWith[id][j];
                  }
                  helperListName.push(value[k].fsn);
                  this.listSelectedAttributeValueNameSummaryWith[id][j] = helperListName;
                  helperListCode.push(value[k].code);
                  this.listSelectedAttributeValueCodesSummaryWith[id][j] = helperListCode;
                }
              } else if(!this.listAttributeWithConstraintComplete[id][j].includes("..")) {
                this.fixedBooleanWith[id][j] = true;
                let value = this.listAttributeWithConstraintComplete[id][j].replace("[[+dec(", "").replace("[[+int(", "").split(" ").join("").replace("#", "").replace(")", "");
                if(value.includes("@")) {
                  value = value.split("@")[0];
                }
                this.attributeValueWith[id][j] = value;
                let helperListName = new Array();
                let helperListCode = new Array();
                if (typeof this.listSelectedAttributeValueNameSummaryWith[id][j] !== 'undefined') {
                  helperListName = this.listSelectedAttributeValueNameSummaryWith[id][j];
                  helperListCode = this.listSelectedAttributeValueCodesSummaryWith[id][j];
                }
                helperListName.push("#" + value);
                this.listSelectedAttributeValueNameSummaryWith[id][j] = helperListName;
                helperListCode.push("#" + value);
                this.listSelectedAttributeValueCodesSummaryWith[id][j] = helperListCode;
              }
              fixedCounter = fixedCounter + 1;
              if(fixedCounter === this.listSelectedAttributeValueNameSummaryWith[id].length) {
                this.addValueToRoleGroup(id);

                setTimeout(() => {
                  let element = document.getElementById('div-summary-button' + id);
                  if(element !== null) {
                    // @ts-ignore
                    element.style.display = 'none';
                  }
                  element = document.getElementById('div-summary' + id);
                  if(element !== null) {
                    // @ts-ignore
                    element.style.display = 'none';
                  }
                }, 10);
              }
            })
          } else {
            this.fixedBooleanWith[id][j] = false;
          }
        }
      }
      this.highlightingTemplateService.listAttributeWithConstraintComplete = this.listAttributeWithConstraintComplete;
    })
  }


  disabledOkButton() {
    return !this.templateNamesList.includes(this.templateName);

  }

  changeBackgroundColor() {
    setTimeout(() => {
      let element = document.getElementById('buttonStart');
      if (element != null) {
        // @ts-ignore
        element.disabled = true;
      }
    }, 10);
  }

  clickChooseTemplate() {

    setTimeout(() => {
      let element = document.getElementById('buttonStart');
      if (element != null) {
        // @ts-ignore
        element.disabled = true;
      }
    }, 10);


    this.highlightingTemplateService.reset();

    let template = '';
    for (let i = 0; i < this.appService.allTemplates.templates.length; i++) {
      const name = this.appService.allTemplates.templates[i].fileName.split(".json").join("").split("/").join(":");
      if(this.templateName === name) {
        template = this.appService.allTemplates.templates[i].fileContent;
        this.appService.selectedTemplate = template;
        this.changeTemplateChoice = true;
        // this.getInfoLanguageTemplate(template);

        this.templateService.getTemplateName(template).subscribe((response: any) => {
            // get template elements
            this.namePCE = response.termTemplate;
            this.focusConceptCode = response.focusConcept.code;
            this.focusConceptName = response.focusConcept.name;
            this.focusConcept = response.focusConcept.fc;

            this.highlightingTemplateService.focusConceptCode = this.focusConceptCode;
            this.highlightingTemplateService.focusConceptName = this.focusConceptName;

            // without role groups
            this.preprocessingWithoutRoleGroup(response);

            // with role groups
            this.preprocessingWithRoleGroup(response);

            if(this.update) {
              this.updatePCE();
            }

        })
        return;
      }
    }

    this.pceWithoutTemplateService.start = 'ID_PCE_WITHOUT_TEMPLATE';
    this.pceWithoutTemplateService.clickTemplateGeneration('ID_PCE_WITHOUT_TEMPLATE');
    this.templateGenerationService.clickTemplateGeneration('');
    this.clickItem('');

    this.resultPCE = '';
    let idx = Number.parseInt(this.idRowStoredPCE);
    let pce = this.listStoredPCEs[idx].split("@@")[1];


    this.pceWithoutTemplateService.getDataPceUpdate(pce,idx);
    this.pceWithoutTemplateService.isUpdate = true;



  }

  preprocessingWithoutRoleGroup(response: any) {
    if (typeof response.withoutRoleGroups != "undefined") {
      this.getConstraintWithout(response);
    }

    this.showAttributeWithout = true;
  }


  preprocessingWithRoleGroup(response: any) {
    if (typeof response.withRoleGroups != "undefined") {
      for (let i = 0; i <= response.withRoleGroups.length; i++) {
        if (typeof response.withRoleGroups[i] != "undefined") {
          this.listRoleGroupsMaxCar.push(response.withRoleGroups[i].roleGroupCardinalityMax);
          this.listRoleGroupsMinCar.push(response.withRoleGroups[i].roleGroupCardinalityMin);
          this.attributesWithGroup.push(response.withRoleGroups[i].attribute);
        }
      }

      console.log(this.attributesWithGroup)

      for (let i = 0; i <= this.attributesWithGroup.length; i++) {
        const helperListMinCar = new Array();
        const helperListMaxCar = new Array();
        const helperListName = new Array();
        const helperListCode = new Array();
        const helperListConstraint = new Array();
        const dummy1 = new Array();
        const dummy2 = new Array();
        const dummy3 = new Array();
        const dummy4 = new Array();
        const dummy5 = new Array();
        const dummy6 = new Array();
        const dummy7 = new Array();
        const dummy8 = new Array();

        this.listAttributeWithConstraintComplete.push(new Array())
        this.listAttributeWithConstraintCompleteCodes.push(new Array())

        if (typeof this.attributesWithGroup[i] != "undefined") {
          for (let j = 0; j <= this.attributesWithGroup[i].length; j++) {
            if (typeof this.attributesWithGroup[i][j] != "undefined") {

              helperListMinCar.push(this.attributesWithGroup[i][j].attributeCardinalityMin);
              if(this.attributesWithGroup[i][j].attributeCardinalityMax === '*') {
                helperListMaxCar.push(Number.POSITIVE_INFINITY);
              } else {
                helperListMaxCar.push(this.attributesWithGroup[i][j].attributeCardinalityMax);
              }
              helperListCode.push(this.attributesWithGroup[i][j].attributeNameCode);
              helperListName.push(this.attributesWithGroup[i][j].attributeNameDisplay);
              helperListConstraint.push(this.attributesWithGroup[i][j].constraint);


              dummy1.push(new Array());
              dummy2.push(new Array());
              dummy3.push(new Array())
              dummy4.push(new Array());
              dummy5.push(new Array());
              dummy6.push(new Array());
              dummy7.push(new Array());
              dummy8.push(new Array());
            }
          }
          this.listAttributeWithMinCar.push(helperListMinCar);
          this.listAttributeWithMaxCar.push(helperListMaxCar);
          this.listAttributeWithCode.push(helperListCode);
          this.listAttributeWithName.push(helperListName);
          this.listAttributeWithConstraint.push(helperListConstraint);
          this.attributeValueWith.push(dummy1);
          this.listSelectedAttributeValueNameSummaryWith.push(dummy2);
          this.listSelectedAttributeValueCodesSummaryWith.push(dummy3);
          this.fixedValuesCodeWith.push(dummy4);
          this.fixedValuesNameWith.push(dummy5);
          this.fixedBooleanWith.push(dummy6);
          this.attributeNoConstraintWithBoolean.push(dummy7);
          this.attributeValuesNoConstraintWith.push(dummy8);

          this.getConstraintWith(i);
        }
      }

      this.highlightingTemplateService.listAttributeWithMinCar = this.listAttributeWithMinCar;
      this.highlightingTemplateService.listAttributeWithMaxCar = this.listAttributeWithMaxCar;
      this.highlightingTemplateService.listAttributeWithCode = this.listAttributeWithCode;
      this.highlightingTemplateService.listAttributeWithName = this.listAttributeWithName;
      this.highlightingTemplateService.listAttributeWithConstraint = this.listAttributeWithConstraint;
      this.highlightingTemplateService.listRoleGroupsMinCar = this.listRoleGroupsMinCar;
      this.highlightingTemplateService.listRoleGroupsMaxCar = this.listRoleGroupsMaxCar;
      this.highlightingTemplateService.attributeValueWith = this.attributeValueWith;

      for(let i = 0; i < this.fixedBooleanWith.length; i++) {
        let flag = true;
        for(let j = 0; j < this.fixedBooleanWith[i].length; j++) {
          if (this.fixedBooleanWith[i][j].length === 0) {
            this.fixedBooleanWith[i][j] = true;
          }
          if (typeof this.fixedBooleanWith[i][j] !== 'undefined' && typeof this.listRoleGroupsMinCar[i][j] !== 'undefined' && typeof this.listRoleGroupsMaxCar[i][j] !== 'undefined') {
            if ((this.fixedBooleanWith[i][j] === true && this.listRoleGroupsMinCar[i][j] === '1' && this.listRoleGroupsMaxCar[i][j] === '1')) {
              continue;
            } else {
              flag = false;
              break;
            }
          }
        }
        this.fixedBooleanRGWith[i] = flag;
      }
      for(let i = 0; i < this.fixedBooleanRGWith.length; i++) {
        if(this.fixedBooleanRGWith[i]) {
          this.listSummaryGroupsName.push(this.listSelectedAttributeValueNameSummaryWith[i]);
          this.listSummaryGroupsCodes.push(this.listSelectedAttributeValueCodesSummaryWith[i]);
          this.listNumberRoleGroups.push(i);
        }
      }

      for (let i = 0; i < this.listAttributeWithConstraint.length; i++) {
        for (let j = 0; j < this.listAttributeWithConstraint[i].length; j++) {
          let constraint = this.listAttributeWithConstraint[i][j];
          if (typeof constraint !== 'undefined') {
            if (!constraint.includes('<')) {
              if (constraint.toUpperCase().includes('OR')) {

                let or = constraint.toUpperCase().split('OR');
                let arr = new Array();
                for (let t = 0; t < or.length; t++) {
                  this.templateService.lookUpName(or[t]).then((response: any) => {
                    arr.push(response.name);
                  })
                }
                this.attributeValuesNoConstraintWith[i][j] = arr;
                this.attributeNoConstraintWithBoolean[i][j] = true;
              } else {
                let arr = new Array();
                this.templateService.lookUpName(constraint).then((response: any) => {
                  arr.push(response.name);
                  this.attributeValuesNoConstraintWith[i][j] = arr;
                  this.attributeNoConstraintWithBoolean[i][j] = true;
                })
              }
            } else {
              this.attributeValuesNoConstraintWith[i][j] = new Array();
              this.attributeNoConstraintWithBoolean[i][j] = false;
            }

            setTimeout(() => {
              if(i !== undefined && j !== undefined && this.listAttributeWithMaxCar[i][j] != null && this.listAttributeWithMaxCar[i][j] !== undefined) {
                if(this.listAttributeWithMaxCar[i][j].toString() === '1') {
                  let element = document.getElementById('add' + i + j);
                  if (element != null) {
                    // @ts-ignore
                    element.disabled = true;
                  }
                }
              }
            }, 1000);
          }
        }
        // this.getConstraintWith(i);
      }
      this.showAttributeWith = true;

      if(!this.update) {
        this.showTemplateContent = true;
      }
    }
  }


  lookupNameCode(input: string) {
    this.templateService.lookUpName(input).then((response: any) => {
      return response.name;
    })

  }


  // -------------------------------------------------------------------------------------------------------------------

  // only for attributes WITHOUT ROLE GROUP

  keypressGetAttributeValueWithoutRoleGroup(i: number) {
    const arr = this.attributeValueWithout[i] || [];

    if(Number.parseInt(arr)) {
      this.keypressEnterWithout(i);
    }

    if (arr.length > 2) {
      let s = '';
      this.oldId = i;


      // if (this.oldInput !== this.attributeValueWithout[i]) {
      if (true) {

        let elementInput = document.getElementById('spinnerWithout' + i);
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'inline';
        }

        this.listAttributeValueCodesWithout = new Array();
        this.listAttributeValueNameWithout = new Array();
        this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithoutConstraint[i], this.attributeValueWithout[i], '1000').then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let i = 0; i < value.length; i++) {
              if(!this.listAttributeValueCodesWithout.includes(value[i].code)) {
                this.listAttributeValueCodesWithout.push(value[i].code);
                this.listAttributeValueNameWithout.push(value[i].fsn);
              }
            }
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          } else {
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          }
          // const expansion = response.expansion;
          // if (typeof expansion != "undefined") {
          //   const contains = expansion.contains;
          //   if (typeof contains != "undefined") {
          //     for (let j = 0; j <= contains.length; j++) {
          //       if (typeof contains[j] != "undefined") {
          //         if (!this.listAttributeValueCodesWithout.includes(contains[j].code)) {
          //           this.listAttributeValueCodesWithout.push(contains[j].code);
          //           this.listAttributeValueNameWithout.push(contains[j].display);
          //         }
          //       }
          //     }
          //   }
          //   this.oldInput = this.attributeValueWithout[i];
          // }
        })
      }
    } else {
      this.listAttributeValueCodesWithout = new Array();
      this.listAttributeValueNameWithout = new Array();
    }
  }


  changeWithout(row: number) {

    if(!Number(this.attributeValueWithout[row])) {
      let elementInput = document.getElementById(this.getIndex(row));
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
      }

      let elementDummy = document.getElementById(this.getIndex(row, 'dummy'));
      if (elementDummy != null) {
        // @ts-ignore
        elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';
        if(elementDummy.style.display === 'none') {
          // this.attributeValueWithout[row] = '';
        }
      }

      let elementDeleteBtn = document.getElementById('add' + row);
      if (elementDeleteBtn != null && elementDummy != null) {
        // @ts-ignore
        elementDeleteBtn.disabled = elementDummy.style.display === 'none';
      } else {
        // @ts-ignore
        elementDeleteBtn.disabled = true;
      }
    }

    let elementDeleteBtn = document.getElementById('add' + row);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = true;
    }
    for(let i = 0; i < this.listAttributeValueNameWithout.length; i++) {
      if(this.listAttributeValueNameWithout[i] === this.attributeValueWithout[row]) {
        let elementDeleteBtn = document.getElementById('add' + row);
        if (elementDeleteBtn != null) {
          // @ts-ignore
          elementDeleteBtn.disabled = false;
        }
        break;
      }
    }
    if(this.listAttributeWithoutMaxCar[row].toString() === '1') {
      if (this.listSelectedAttributeValueNameSummaryWithout[row].length.toString() === this.listAttributeWithoutMaxCar[row].toString()) {
        let elementDeleteBtn = document.getElementById('add' + row);
        if (elementDeleteBtn != null) {
          // @ts-ignore
          elementDeleteBtn.disabled = true;
        }
      }
    }
  }

  getRangeWithout(id: number, key: string) {
    if(this.listAttributeWithoutConstraintComplete[id] !== undefined) {
      let c = this.listAttributeWithoutConstraintComplete[id].split(" ").join("").split("[[+int").join("").split("[[+dec").join("").split("(").join("").split(")").join("").split("#").join("");
      if(c.includes(">")) {
        c = c.split('<').join('').split('>').join('');
        if(key === 'min') {
          return Number.parseFloat(c.split("..")[0]) + 1;
        } else if(key === 'max') {
          if(c.split("..")[1] === "" || c.split("..")[1].toString().includes("@")) {
            return Number.POSITIVE_INFINITY;
          } else {
            return Number.parseFloat(c.split("..")[1]) - 1;
          }
        }
      } else {
        if(key === 'min') {
          return c;
        } else if(key === 'max') {
          if(c.split("..")[1] === "") {
            return Number.POSITIVE_INFINITY;
          } else {
            return c;
          }
        }
      }
    }
    return -1;
  }

  getNumberTypeWithout(i: number, id: string) {
    if(this.listAttributeWithoutConstraintComplete[i] === undefined) {
      return false;
    } else if(this.listAttributeWithoutConstraint[i] === '' && this.listAttributeWithoutConstraintComplete[i].includes(id)) {
      return true;
    } else {
      return false;
    }
  }

  addNumberWithout(i: number) {
    let len = this.listSelectedAttributeValueCodesSummaryWithout[i].length;
    let max = Number.parseInt(this.listAttributeWithoutMaxCar[i]);

    if(this.listAttributeWithoutConstraintComplete[i].includes('+int')) {
      if((Number.isInteger(this.attributeValueWithout[i])) && (len < max) && (this.attributeValueWithout[i] >= this.getRangeWithout(i, 'min')) && (this.attributeValueWithout[i] <= this.getRangeWithout(i, 'max'))) {
        // if((len < max) && (this.attributeValueWithout[i] >= this.getRangeWithout(i, 'min')) && (this.attributeValueWithout[i] <= this.getRangeWithout(i, 'max'))) {
        let element = document.getElementById('add' + i);
        if (element != null) {
          // @ts-ignore
          element.disabled = false;
        }
      } else {
        let element = document.getElementById('add' + i);
        if (element != null) {
          // @ts-ignore
          element.disabled = true;
        }
      }
    } else if(this.listAttributeWithoutConstraintComplete[i].includes('+dec')) {
      if((len < max) && (this.attributeValueWithout[i] >= this.getRangeWithout(i, 'min')) && (this.attributeValueWithout[i] <= this.getRangeWithout(i, 'max'))) {
        let element = document.getElementById('add' + i);
        if (element != null) {
          // @ts-ignore
          element.disabled = false;
        }
      } else {
        let element = document.getElementById('add' + i);
        if (element != null) {
          // @ts-ignore
          element.disabled = true;
        }
      }
    }
  }


  showInfoButton(i: number) {
    if(this.attributeValueWithout[i] !== undefined) {
      if(this.attributeValueWithout[i].length > 0) {
        return false;
      }
    }
    return true;
  }


  keypressEnterWithout(i: number) {
    let s = '';
    this.listAttributeValueNameWithout = new Array();
    this.listAttributeValueCodesWithout = new Array();
    let array1 = new Array();
    let array2 = new Array();

    if (s != this.attributeValueWithout[i]) {
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithoutConstraint[i], this.attributeValueWithout[i], '100000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let j = 0; j < value.length; j++) {
           array1.push(value[j].code);
            array2.push(value[j].fsn);
            this.attributeValueWithout[i] = value[j].fsn;
          }
          array1.forEach((item) => {
            if (!this.listAttributeValueCodesWithout.includes(item)) {
              this.listAttributeValueCodesWithout.push(item);
            }
          })
          array2.forEach((item) => {
            if (!this.listAttributeValueNameWithout.includes(item)) {
              this.listAttributeValueNameWithout.push(item);
            }
          })

          let elementInput = document.getElementById('spinnerWithout' + i);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
          elementInput = document.getElementById('add' + i);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = false;
          }
        } else {
          let elementInput = document.getElementById('spinnerWithout' + i);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }
        this.keypressWith = false;
      })
    }
  }

  attributesNoConstraintWithout() {
    return this.attributeValuesNoConstraintWithout[this.idAttributeWithout];
  }

  attributesNoConstraintWithoutBoolean() {
    return this.attributeNoConstraintWithoutBoolean[this.idAttributeWithout];
  }

  getElementNumber(i: number) {
    this.oldId = this.idAttributeWithout;
    this.idAttributeWithout = i
    // this.attributeValueWithout[this.oldId] = '';
  }

  getIdWithout(i: number) {
    if (this.oldId !== i) {
      // this.attributeValueWithout[i] = '';
      this.listAttributeValueCodesWithout = new Array();
      this.listAttributeValueNameWithout = new Array();
    }

  }

  addValueToTableWithout(i: number) {
    let k = -1;
    for(let j = 0; j < this.listAttributeValueNameWithout.length; j++) {
      if(this.listAttributeValueNameWithout[j] === this.attributeValueWithout[i]) {
        k = j;
        break;
      }
    }
    if(this.listAttributeValueCodesWithout[k] !== undefined) {
      this.listSelectedValueCode = this.listAttributeValueCodesWithout[k];
      this.listSelectedValueName = this.attributeValueWithout[i];
      this.listSelectedAttributeValueNameSummaryWithout[i].push(this.listSelectedValueName);
      this.listSelectedAttributeValueCodesSummaryWithout[i].push(this.listSelectedValueCode);
    } else if(this.update)  {
      this.listSelectedValueCode = this.listAttributeValueCodesWithout[i];
      this.listSelectedValueName = this.attributeValueWithout[i];
      this.listSelectedAttributeValueNameSummaryWithout[i].push(this.listSelectedValueName);
      this.listSelectedAttributeValueCodesSummaryWithout[i].push(this.listSelectedValueCode);
    } else {
      if(!this.attributeValueWithout[i].toString().includes("(")) {
        this.listSelectedValueCode = new Array('#');
        if(this.listAttributeWithoutConstraintComplete[i].includes("+int")) {
          this.listSelectedValueName = new Array(Math.round(Number.parseFloat(this.attributeValueWithout[i])).toString());
        } else if (this.listAttributeWithoutConstraintComplete[i].includes("+dec")) {
          this.listSelectedValueName = new Array(Math.round(Number.parseFloat(this.attributeValueWithout[i])).toString());
        }
        else {
          this.listSelectedValueName = this.attributeValueWithout[i];
        }
        this.listSelectedAttributeValueNameSummaryWithout[i].push(this.listSelectedValueName);
        this.listSelectedAttributeValueCodesSummaryWithout[i].push(this.listSelectedValueCode);
        this.attributeValueWithout[i] = '';

      } else {
        this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.attributeValueWithout[i].toString(), '1').then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let j = 0; j < value.length; j++) {
              console.log("---> " + this.attributeValueWithout[i])
              this.listSelectedAttributeValueNameSummaryWithout[i].push(this.attributeValueWithout[i]);
              this.listSelectedAttributeValueCodesSummaryWithout[i].push(value[j].code);
              this.listSelectedValueName = this.attributeValueWithout[i];
              this.listSelectedValueCode = value[j].code;
              this.attributeValueWithout[i] = '';
            }
          }
        })
      }
    }
    this.listAttributeValueCodesWithout = new Array();
    this.listAttributeValueNameWithout = new Array();
    this.showTablesWithout = true;
    this.changeWithout(i);
  }

  clickTableRow(event: any, table: number, row: number) {
    this.idTableRow = '';
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      if (i !== table) {
        continue
      }

      this.idTableRow = table + "|" + row;

      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idTableRow) {
            element.style.background = 'lightgray';
          }
        }
      }
    }
    let elementDeleteBtn = document.getElementById('btn-delete' + table);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = false;
    }
  }

  clickRow(event: any, row: number) {
    this.idTableRow = '';
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      this.idTableRow = row.toString();

      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idTableRow) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
  }

  getValueTableWithout(i: number, j: number) {
    if(this.listSelectedAttributeValueNameSummaryWithout[i][j].toString().includes("(")) {
      return this.listSelectedAttributeValueNameSummaryWithout[i][j].toString().split("(")[0];
    } else {
      return this.listSelectedAttributeValueNameSummaryWithout[i][j];
    }

  }

  getIdRow(row: number) {
    return row;
  }

  addCode() {
    this.listSelectedValueName = this.listAttributeValueNameFilter[parseInt(this.idTableRow)];
    this.listSelectedValueCode = this.listAttributeValueCodesFilter[parseInt(this.idTableRow)];

    this.listSelectedAttributeValueNameSummaryWithout[this.id].push(this.listSelectedValueName);
    this.listSelectedAttributeValueCodesSummaryWithout[this.id].push(this.listSelectedValueCode);

    this.listAttributeValueCodesWithout = new Array();
    this.listAttributeValueNameWithout = new Array();
    this.attributeValueWithout[2] = '';

    this.showTablesWithout = true;
  }

  getIdTableRow(table: number, row: number) {
    return table + "|" + row;
  }

  isButtonAddActiveWithoutModal() {
    if(typeof this.listSelectedAttributeValueNameSummaryWithout[this.idAttributeWithout] !== 'undefined') {
      if (this.idTableRow !== '' && this.listSelectedAttributeValueNameSummaryWithout[this.idAttributeWithout].length < this.listAttributeWithoutMaxCar[this.idAttributeWithout]) {
        return false;
      }
    }
    return true;
  }

  isMaxCardinality(table: number) {
    // delete: && this.attributeValueWithout[table].length > 0
    if (this.listSelectedAttributeValueNameSummaryWithout[table].length >= this.listAttributeWithoutMaxCar[table]) {
      return true;
    } else {
      return false;
    }
  }

  isButtonAddActive(table: number) {
    if ((this.listAttributeValueNameWithout != null && this.listAttributeValueNameWithout.includes(this.attributeValueWithout[table]) && this.listSelectedAttributeValueNameSummaryWithout[table].length < this.listAttributeWithoutMaxCar[table])) {
      return false;
    }
    return true;
  }

  isButtonDeleteActive(table: number) {
    if (this.idTableRow.split("|")[0] != table.toString()) {
      return true;
    } else if (this.listSelectedAttributeValueNameSummaryWithout[table].length === 0) {
      return true;
    } else {
      return false;
    }
  }

  deleteValueWithout() {
    const table = Number(this.idTableRow.split("|")[0]);
    const row = Number(this.idTableRow.split("|")[1]);

    let helperArrayName = new Array();
    let helperArrayCode = new Array();

    for(let i = 0; i < this.listSelectedAttributeValueNameSummaryWithout[table].length; i++) {
      if(i !== row) {
        helperArrayCode.push(this.listSelectedAttributeValueCodesSummaryWithout[table][i]);
        helperArrayName.push(this.listSelectedAttributeValueNameSummaryWithout[table][i]);
      }
    }

    this.listSelectedAttributeValueNameSummaryWithout[table] = helperArrayName;
    this.listSelectedAttributeValueCodesSummaryWithout[table] = helperArrayCode;

    this.idTableRow = '';

    let elementDeleteBtn = document.getElementById('btn-delete' + table);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = true;
    }

    for(let i = 0; i < this.listSelectedAttributeValueCodesSummaryWithout.length; i++) {
      if(this.listSelectedAttributeValueCodesSummaryWithout[i].length === 0) {
        continue;
      } else {
        return "";
      }
    }
    this.showTablesWithout = false;
    return "";
  }

  clickTableRowWith(event: any, row: number, col: number, element: number) {
    this.idTableRow = '';

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      this.idTableRow = row + "|" + col + "|" + element;

      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idTableRow) {
            element.style.background = 'lightgray';
          }
        }
      }
    }
  }

  getIndex(index: number, idx?: string) {
    return "attributeValue" + index + idx;
  }

  getAllPossibleConceptsWithout(i: number) {
    this.id = i;
    // this.templateService.getAttributeValueRequest(this.listAttributeWithoutConstraint[i]).then((response: any) => {
    //   const expansion = response.expansion;
    //   if (typeof expansion != "undefined") {
    //     const contains = expansion.contains;
    //     if (typeof contains != "undefined") {
    //       for (let j = 0; j <= contains.length; j++) {
    //         if (typeof contains[j] != "undefined") {
    //           this.listAttributeValueCodesFilter.push(contains[j].code);
    //           this.listAttributeValueNameFilter.push(contains[j].display);
    //         }
    //       }
    //     }
    //   }

    this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithoutConstraint[i], '', '100000').then((response: any) => {
      const value = response.values;
      if(typeof value !== 'undefined') {
        for (let i = 0; i < value.length; i++) {
          if(!this.listAttributeValueCodesFilter.includes(value[i].code)) {
            this.listAttributeValueCodesFilter.push(value[i].code);
            this.listAttributeValueNameFilter.push(value[i].fsn);
          }
        }
      }
      this.attributesFilterBoolean = true;
    })
  }

  closeModal() {
    this.listAttributeValueCodesFilter = new Array();
    this.listAttributeValueNameFilter = new Array();
    this.id = -1;
    this.idWith = '';
    this.idTableRow = '';
    this.attributesFilterBoolean = false;
  }

  fixedValueWithout(i: number) {
    if(typeof this.listAttributeWithoutConstraint[i] != 'undefined') {
      if (!this.listAttributeWithoutConstraint[i].includes("<") && !this.listAttributeWithoutConstraint[i].includes("OR") && !this.listAttributeWithoutConstraint[i].includes("MINUS") && this.listAttributeWithoutMaxCar[i] === '1' && this.listAttributeWithoutMinCar[i] === '1') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getRelatedConceptsWithout(i: number) {
    const row = Number(this.idTableRow.split("|")[1]);
    let url = "https://browser.ihtsdotools.org/?perspective=full&conceptId1=" + this.listSelectedAttributeValueCodesSummaryWithout[i][row] + "&edition=MAIN/"+ this.settingsService.snomedBrowserVersion + "&release=&languages=en";
    this.goToLink(url);
  }








  // -------------------------------------------------------------------------------------------------------------------


  // only for attributes WITH ROLE GROUP

  public oldInput = '';


  keypressGetAttributeValueWithRoleGroup(row: number, col: number) {
    const arr = this.attributeValueWith[row][col] || [];

    if(Number.parseInt(arr)) {
      this.keypressEnter(row, col);
    }

    if (arr.length > 2) {
      let s = '';
      this.oldIDRow = row;
      this.oldIDCol = col;

      let elementInput = document.getElementById('spinnerWith' + row + col);

      if(this.listAttributeValueCodesWith.length == 0) {
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'inline';
        }
      }

      if (this.oldInput !== this.attributeValueWith[row][col] && !Number(this.attributeValueWith[row][col])) {
        this.listAttributeValueCodesWith = new Array();
        this.listAttributeValueNameWith = new Array();
        this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithConstraint[row][col], this.attributeValueWith[row][col], '100000').then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let i = 0; i < value.length; i++) {
              if(!this.listAttributeValueCodesWith.includes(value[i].code)) {
                this.listAttributeValueCodesWith.push(value[i].code);
                this.listAttributeValueNameWith.push(value[i].fsn);
              }
            }
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          } else {
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          }
          this.oldInput = this.attributeValueWith[row][col];
        })
      }

    }
  }


  changeWith(row: number, col: number) {
    if(!Number(this.attributeValueWith[row][col])) {
      let elementInput = document.getElementById(this.getIndexWith(row, col));
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
      }

      let elementDummy = document.getElementById(this.getIndexWith(row, col, 'dummy'));
      if (elementDummy != null) {
        // @ts-ignore
        elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';
      }

      let elementDeleteBtn = document.getElementById('add' + row + col);
      if (elementDeleteBtn != null && elementDummy != null) {
        // @ts-ignore
        elementDeleteBtn.disabled = elementDummy.style.display === 'none';
        // @ts-ignore
        if(elementDummy.style.display === 'none') {
          this.attributeValueWith[row][col] = '';
        }
      }
    }
    let elementDeleteBtn = document.getElementById('add' + row + col);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = true;
    }

    if(this.attributesNoConstraintWithBoolean()) {
      let elementDeleteBtn = document.getElementById('add' + row + col);
      if (elementDeleteBtn != null) {
        // @ts-ignore
        elementDeleteBtn.disabled = false;
      }
    } else {
      for(let i = 0; i < this.listAttributeValueNameWith.length; i++) {
        if(this.listAttributeValueNameWith[i] === this.attributeValueWith[row][col]) {
          let elementDeleteBtn = document.getElementById('add' + row + col);
          if (elementDeleteBtn != null) {
            // @ts-ignore
            elementDeleteBtn.disabled = false;
          }
          break;
        }
      }
    }

    if(this.listAttributeValueNameWith[row] !== undefined) {
      if(this.listAttributeValueNameWith[row][col] !== undefined) {
        if (this.listAttributeValueNameWith[row][col].length > 0 && this.attributeValueWith[row][col] !== '') {
          let elementInput = document.getElementById('spinnerWith' + row + col);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }
      }
    }

    if(this.listAttributeWithMaxCar[row][col].toString() === '1') {
      if (this.listSelectedAttributeValueNameSummaryWith[row][col].length.toString() === this.listAttributeWithMaxCar[row][col].toString()) {
        let elementDeleteBtn = document.getElementById('add' + row + col);
        if (elementDeleteBtn != null) {
          // @ts-ignore
          elementDeleteBtn.disabled = true;
        }
      }
    }
  }



  keypressEnter(row: number, col: number) {
    this.listAttributeValueCodesWith = new Array();
    this.listAttributeValueNameWith = new Array();

    this.oldIDRow = row;
    this.oldIDCol = col;
    let s = '';
    if (s != this.attributeValueWith[row][col]) {
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithConstraint[row][col], this.attributeValueWith[row][col], '100000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAttributeValueCodesWith.includes(value[i].code)) {
              this.listAttributeValueCodesWith.push(value[i].code);
              this.listAttributeValueNameWith.push(value[i].fsn);
              this.attributeValueWith[row][col] = value[i].fsn;
            }
          }
          let elementInput = document.getElementById('spinnerWith' + row + col);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
          elementInput = document.getElementById('add' + row + col);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled= false;
          }
        } else {
          let elementInput = document.getElementById('spinnerWith' + row + col);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }
        this.keypressWith = false;
      })
    }
  }

  getIdDatalistWith(row: number, col: number) {
    return "datalist" + row + "|" + col;
  }

  getNumberTypeWith(i: number, j: number, id: string) {
    if(this.listAttributeWithConstraintComplete[i][j] === undefined) {
      return false;
    } else if(this.listAttributeWithConstraint[i][j] === '' && this.listAttributeWithConstraintComplete[i][j].includes(id)) {
      return true;
    } else {
      return false;
    }
  }

  getRangeWith(i: number, j: number, key: string) {
    if(this.listAttributeWithConstraintComplete[i][j] !== undefined) {
      let c = this.listAttributeWithConstraintComplete[i][j].split(" ").join("").split("[[+int").join("").split("[[+dec").join("").split("(").join("").split(")").join("").split("#").join("");
      if(c.includes(">")) {
        c = c.split('<').join('').split('>').join('');
        if(key === 'min') {
          return Number.parseFloat(c.split("..")[0]) + 1;
        } else if(key === 'max') {
          if(c.split("..")[1] === "") {
            return Number.POSITIVE_INFINITY;
          } else {
            return Number.parseFloat(c.split("..")[1]) - 1;
          }
        }
      } else {
        if(key === 'min') {
          return c;
        } else if(key === 'max') {
          if(c.split("..")[1] === "") {
            return Number.POSITIVE_INFINITY;
          } else {
            return c;
          }
        }
      }
    }
    return -1;
  }

  numberWith(i: number, j: number) {
    if(!this.listAttributeWithConstraintComplete[i][j].includes(">")) {
      this.attributeValueWith[i][j] = Number.parseInt(this.listAttributeWithConstraintComplete[i][j].split(" ").join("").split("[[+int").join("").split("[[+dec").join("").split("(").join("").split(")").join("").split("#").join("").split("]").join("").split("..").join(""));
      let len = this.listSelectedAttributeValueCodesSummaryWith[i][j].length;
      let max = Number.parseInt(this.listAttributeWithMaxCar[i][j]);
      if(Number.isInteger(this.attributeValueWith[i][j]) && len < max) {
        let element = document.getElementById('add' + i + j);
        if (element != null) {
          // @ts-ignore
          element.disabled = false;
          element.style.display = 'inline';
        }
      }
    }
  }

  addNumberWith(i: number, j: number) {
    let len = this.listSelectedAttributeValueCodesSummaryWith[i][j].length;
    let max = Number.parseInt(this.listAttributeWithMaxCar[i][j]);
    this.numberWith(i, j);

    if(this.listAttributeWithConstraintComplete[i][j].includes('+int')) {
      if((len < max) && (this.attributeValueWith[i][j] >= this.getRangeWith(i, j, 'min')) && (this.attributeValueWith[i][j] <= this.getRangeWith(i, j,'max'))) {
        let element = document.getElementById('add' + i + j);
        if (element != null) {
          // @ts-ignore
          element.disabled = false;
        }
      } else {
        let element = document.getElementById('add' + i + j);
        if (element != null) {
          // @ts-ignore
          element.disabled = true;
        }
      }
    } else if(this.listAttributeWithConstraintComplete[i][j].includes('+dec')) {
      if((len < max) && (this.attributeValueWith[i][j] >= this.getRangeWith(i, j, 'min')) && (this.attributeValueWith[i][j] <= this.getRangeWith(i, j,'max'))) {
        let element = document.getElementById('add' + i + j);
        if (element != null) {
          // @ts-ignore
          element.disabled = false;
        }
      } else {
        let element = document.getElementById('add' + i + j);
        if (element != null) {
          // @ts-ignore
          element.disabled = true;
        }
      }
    }

  }

  showInfoButtonWith(row: number, col: number) {
    if(this.attributeValueWith[row][col] !== undefined) {
      if(this.attributeValueWith[row][col].length > 0) {
        return false;
      }
    }
    return true;
  }

  addValueToTableWith(row: number, col: number) {
    let maxCar = 0;
    if(this.listRoleGroupsMaxCar[row] === '*') {
      maxCar = Number.POSITIVE_INFINITY;
    } else {
      maxCar = Number.parseInt(this.listRoleGroupsMaxCar[row]);
    }

    let element = document.getElementById(this.getIdButton(row));
    if (element != null) {
      // @ts-ignore
      element.disabled = false;
    }

    if(this.listSelectedAttributeValueCodesSummaryWith[row].includes('#')) {
      this.showSummaryRoleGroup = true;
    }


    // easy insert
    if(maxCar > 1) {
      const helperListName = this.listSelectedAttributeValueNameSummaryWith[row][col];
      helperListName.push(this.attributeValueWith[row][col]);
      this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;

      let j = 0;

      for (let i = 0; i < this.listAttributeValueNameWith.length; i++) {
        if (this.listAttributeValueNameWith[i] === this.attributeValueWith[row][col]) {
          j = i;
          break;
        }
      }
      const helperListCode = this.listSelectedAttributeValueCodesSummaryWith[row][col];

      let x = '';
      if(this.listAttributeValueCodesWith[j] !== undefined) {
        x = this.listAttributeValueCodesWith[j];
        helperListCode.push(x);
        this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
      } else {
        if(!this.attributeValueWith[row][col].toString().includes("(")) {
          x = '#';
          helperListCode.push(x);
          this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
          let element = document.getElementById('add' + row + col);
          if (element != null) {
            // @ts-ignore
            element.disabled = true;
          }
          this.attributeValueWith[row][col] = new Array();
          element = document.getElementById(this.getIdButton(row));
          if (element != null) {
            // @ts-ignore
            element.disabled = false;
          }
        } else {
          this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.attributeValueWith[row][col].toString(), '1').then((response: any) => {
            const value = response.values;
            if(typeof value !== 'undefined') {
              for (let i = 0; i < value.length; i++) {
                x = value[i].code;
                helperListCode.push(x);
                this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
              }
            }
          })
        }

      }


      this.listAttributeValueCodesWith = new Array();
      this.listAttributeValueNameWith = new Array();

      this.showAttributeWith = true;
      this.showTableWith = true;


    } else {
      if (this.listNumberRoleGroups.includes(row)) {
        let helperListName = this.listSelectedAttributeValueNameSummaryWith[row][col];
        helperListName.push(this.attributeValueWith[row][col]);
        let helperListCode = this.listSelectedAttributeValueCodesSummaryWith[row][col];

        if (typeof this.listAttributeValueCodesFilter[parseInt(this.idTableRow)] !== 'undefined') {
          helperListCode.push(this.listAttributeValueCodesFilter[parseInt(this.idTableRow)]);
        } else {
          let j = 0;

          for (let i = 0; i < this.listAttributeValueNameWith.length; i++) {
            if (this.listAttributeValueNameWith[i] === this.attributeValueWith[row][col]) {
              j = i;
              break;
            }
          }
          if(this.listAttributeValueCodesWith[j] !== undefined) {
            helperListCode.push(this.listAttributeValueCodesWith[j]);
          } else {
            if(!this.attributeValueWith[row][col].toString().includes("(")) {
              let x = '#';
              helperListCode.push(x);
              this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
              this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;
              let element = document.getElementById('add' + row + col);
              if (element != null) {
                // @ts-ignore
                element.disabled = true;
              }
              this.attributeValueWith[row][col] = new Array();
              element = document.getElementById(this.getIdButton(row));
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
            } else {
              this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.attributeValueWith[row][col].toString(), '1').then((response: any) => {
                const value = response.values;
                if(typeof value !== 'undefined') {
                  for (let i = 0; i < value.length; i++) {
                    let x = value[i].code;
                    helperListCode.push(x);
                    this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
                    this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;
                  }
                }
              })
            }
          }
        }

        this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;
        this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
        this.showTableWith = true;

      } else {
        const helperListName = this.listSelectedAttributeValueNameSummaryWith[row][col];
        helperListName.push(this.attributeValueWith[row][col]);
        this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;
        const helperListCode = this.listSelectedAttributeValueCodesSummaryWith[row][col];

        let j = 0;

        for (let i = 0; i < this.listAttributeValueNameWith.length; i++) {
          if (this.listAttributeValueNameWith[i] === this.attributeValueWith[row][col]) {
            j = i;
            break;
          }
        }
        if(this.listAttributeValueCodesWith[j] !== undefined) {
          helperListCode.push(this.listAttributeValueCodesWith[j]);
        } else {
          if(!this.attributeValueWith[row][col].toString().includes("(")) {
            let x = '#';
            helperListCode.push(x);
            this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
            let element = document.getElementById('add' + row + col);
            if (element != null) {
              // @ts-ignore
              element.disabled = true;
            }
            this.attributeValueWith[row][col] = new Array();
            element = document.getElementById(this.getIdButton(row));
            if (element != null) {
              // @ts-ignore
              element.disabled = false;
            }
          } else {
            this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.attributeValueWith[row][col].toString(), '1').then((response: any) => {
              const value = response.values;
              if(typeof value !== 'undefined') {
                for (let i = 0; i < value.length; i++) {
                  let x = value[i].code;
                  helperListCode.push(x);
                  this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
                }
              }
            })
          }
        }
        this.listAttributeValueCodesWith = new Array();
        this.listAttributeValueNameWith = new Array();
        this.attributeValueWith[row][col] = '';

        this.showAttributeWith = true;
        this.showTableWith = true;

        this.listSummaryGroupsName.push(this.listSelectedAttributeValueNameSummaryWith[row]);
        this.listSummaryGroupsCodes.push(this.listSelectedAttributeValueCodesSummaryWith[row]);
        this.listNumberRoleGroups.push(row);

      }
    }
    this.attributeValueWith[row][col] = '';
    this.showTableWith = true;


    this.changeWith(row, col);
  }

  isAddGroupButtonVisible(row: number) {
    let a = this.fixedBooleanRGWith[row];
    let b = this.listRoleGroupsMaxCar[row] === '1';
    if(this.listAttributeWithConstraintComplete[row].toString().includes('+dec') || this.listAttributeWithConstraintComplete[row].toString().includes('+int')) {
      return true;
    } else {
      return !(a || b);
    }
  }

  isCarCorrect(row: number) {
    if(this.listAttributeWithConstraintComplete[row].toString().includes('+dec') || this.listAttributeWithConstraintComplete[row].toString().includes('+int')) {
      return true;
    }

    let b = false;
    for (let j = 0; j < this.listSummaryGroupsName.length; j++) {
      if (row === this.listNumberRoleGroups[j]) {
        b = true;
        for (let k = 0; k < this.listSummaryGroupsName[j].length; k++) {
          let id = this.listNumberRoleGroups[j];
          if (this.listSummaryGroupsName[j][k].length >= this.listAttributeWithMinCar[id][k]) {
            continue;
          } else {
            return false;
          }
        }
      }
    }
    return true;
  }

  isButtonAddActiveWith(row: number, col: number) {
    if ((this.listAttributeValueNameWith.includes(this.attributeValueWith[row][col]) && this.listSelectedAttributeValueNameSummaryWith[row][col].length < this.listAttributeWithMaxCar[row][col] && !this.fixedValueWith(row, col))) {
      return false;
    }
    return true;
  }

  isButtonAddActiveWithModal() {
    let row = Number.parseInt(this.idWith.split("|")[0]);
    let col = Number.parseInt(this.idWith.split("|")[1]);

    if (this.idWith != '') {
      if (this.idTableRow !== '' && this.listSelectedAttributeValueNameSummaryWith[row][col].length < this.listAttributeWithMaxCar[row][col]) {
        return false;
      }
    }
    return true;
  }

  deleteValueWith() {
    const row = Number(this.idTableRow.split("|")[0]);
    const col = Number(this.idTableRow.split("|")[1]);
    const element = Number(this.idTableRow.split("|")[2]);

    let helperArrayName = new Array();
    let helperArrayCode = new Array();

    for(let i = 0; i < this.listSelectedAttributeValueNameSummaryWith[row].length; i++) {
      for(let j = 0 ; j < this.listSelectedAttributeValueNameSummaryWith[row][i].length; j++) {
        if(j !== element) {
          helperArrayCode.push(this.listSelectedAttributeValueCodesSummaryWith[row][i][j]);
          helperArrayName.push(this.listSelectedAttributeValueNameSummaryWith[row][i][j]);
        }
      }
    }
    this.listSelectedAttributeValueNameSummaryWith[row][col] = helperArrayName;
    this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperArrayCode;

    this.idTableRow = '';

    // delete in summary, if deleted attribute is the only of role group
    let counter = 0;
    for(let i = 0; i < this.listSelectedAttributeValueNameSummaryWith[row].length; i++) {
      if(this.listSelectedAttributeValueNameSummaryWith[row][i].length === 0) {
        counter = counter + 1;
      }
    }
    if(counter === this.listSelectedAttributeValueNameSummaryWith[row].length) {
      // @ts-ignore
      this.listSummaryGroupsName = this.listSummaryGroupsName.filter(x => x !== this.listSelectedAttributeValueNameSummaryWith[row]);
      // @ts-ignore
      this.listSummaryGroupsCodes = this.listSummaryGroupsCodes.filter(x => x !== this.listSelectedAttributeValueCodesSummaryWith[row]);
      // @ts-ignore
      this.listNumberRoleGroups = this.listNumberRoleGroups.filter(x => x !== row);
    }

    for(let i = 0; i < this.listSelectedAttributeValueNameSummaryWith[row].length; i++) {
      for(let j = 0 ; j < this.listSelectedAttributeValueNameSummaryWith[row][i].length; j++) {
        if(this.listSelectedAttributeValueNameSummaryWith[i][j].length === 0) {
          continue;
        } else {
          return "";
        }
      }
    }
    this.showTableWith = false;
    return "";
  }

  getIdTableRowWith(row: number, col: number, element: number) {
    return row + "|" + col + "|" + element;
  }

  isButtonDeleteActiveWith(row: number, col: number) {
    if (!this.idTableRow.includes(row + "|" + col + "|")) {
      return true;
    } else if (this.listSelectedAttributeValueNameSummaryWith[row][col].length === 0) {
      return true;
    } else {
      return false;
    }
  }

  getIdWith(row: number, col: number) {
    this.idWith = row + "|" + col;
    if (this.oldIDRow !== row || this.oldIDCol !== col) {
      if (this.oldIDCol !== -1 && this.oldIDRow !== -1) {
        // this.attributeValueWith[this.oldIDRow][this.oldIDCol] = '';
        this.listAttributeValueCodesWith = new Array();
        this.listAttributeValueNameWith = new Array();
      }
    }
  }

  getElementNumberWith(row: number, col: number) {
    this.oldIDCol = col;
    this.oldIDRow = row;
  }

  getIndexWith(i: number, j: number, idx?: string) {
    return "attributeValue" + i + j + idx;
  }

  getIdAttributes(i: number) {
    return "idAttribute-" + i;
  }

  getAllPossibleConceptsWith(row: number, col: number) {
    this.idWith = row + '|' + col;
    // this.templateService.getAttributeValueRequest(this.listAttributeWithConstraint[row][col]).then((response: any) => {
    //   const expansion = response.expansion;
    //   if (typeof expansion != "undefined") {
    //     const contains = expansion.contains;
    //     if (typeof contains != "undefined") {
    //       for (let j = 0; j <= contains.length; j++) {
    //         if (typeof contains[j] != "undefined") {
    //           this.listAttributeValueCodesFilter.push(contains[j].code);
    //           this.listAttributeValueNameFilter.push(contains[j].display);
    //         }
    //       }
    //     }
    //   }
    this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listAttributeWithConstraint[row][col], '', '100000').then((response: any) => {
      const value = response.values;
      if(typeof value !== 'undefined') {
        for (let i = 0; i < value.length; i++) {
          if(!this.listAttributeValueCodesFilter.includes(value[i].code)) {
            this.listAttributeValueCodesFilter.push(value[i].code);
            this.listAttributeValueNameFilter.push(value[i].fsn);
          }
        }
      }
      this.attributesFilterBoolean = true;
    })
  }

  addCodeWith() {
    let row = Number.parseInt(this.idWith.split("|")[0]);
    let col = Number.parseInt(this.idWith.split("|")[1]);

    if(this.listRoleGroupsMaxCar[row] === '1') {
      this.attributeValueWith[row][col] = this.listAttributeValueNameFilter[parseInt(this.idTableRow)];
      this.listSelectedValueName = this.listAttributeValueNameFilter[parseInt(this.idTableRow)];
      this.listSelectedValueCode = this.listAttributeValueCodesFilter[parseInt(this.idTableRow)];
      const helperListCode = this.listSelectedAttributeValueCodesSummaryWith[row][col];
      helperListCode.push(this.listAttributeValueCodesFilter[parseInt(this.idTableRow)]);
      this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;
      this.showTableWith = true;

      this.addValueToTableWith(row, col);
      this.attributeValueWith[row][col] = '';

    } else {
      this.listSelectedValueName = this.listAttributeValueNameFilter[parseInt(this.idTableRow)];
      this.listSelectedValueCode = this.listAttributeValueCodesFilter[parseInt(this.idTableRow)];

      const helperListName = this.listSelectedAttributeValueNameSummaryWith[row][col];
      helperListName.push(this.listSelectedValueName);
      this.listSelectedAttributeValueNameSummaryWith[row][col] = helperListName;

      const helperListCode = this.listSelectedAttributeValueCodesSummaryWith[row][col];
      helperListCode.push(this.listSelectedValueCode);
      this.listSelectedAttributeValueCodesSummaryWith[row][col] = helperListCode;

      this.showTableWith = true;
    }

    let elementDeleteBtn = document.getElementById('add' + row + col);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = true;
    }

  }

  fixedValueWith(row: number, col: number) {
    if(typeof this.listAttributeWithConstraint[row][col] != 'undefined') {
      if (!this.listAttributeWithConstraint[row][col].includes("<") && !this.listAttributeWithConstraint[row][col].includes("OR") && !this.listAttributeWithConstraint[row][col].includes("MINUS") && this.listAttributeWithMaxCar[row][col] === '1' && this.listAttributeWithMinCar[row][col] === '1') {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  getValueTableWith(name: String) {
    if(name.toString().includes("(")) {
      return name.split("(")[0];
    } else {
      return name;
    }

  }

  getRelatedConceptsWith(i: number, j: number) {
    const element = Number(this.idTableRow.split("|")[2]);
    let url = "https://browser.ihtsdotools.org/?perspective=full&conceptId1=" + this.listSelectedAttributeValueCodesSummaryWith[i][j][element] + "&edition=MAIN/" + this.settingsService.snomedBrowserVersion + "&release=&languages=en";
    this.goToLink(url);
  }

  getRelatedConceptsModal(row: number) {
    let url = "https://browser.ihtsdotools.org/?perspective=full&conceptId1=" + this.listAttributeValueCodesFilter[row] + "&edition=MAIN/" + this.settingsService.snomedBrowserVersion + "&release=&languages=en";
    this.goToLink(url);
  }


  // -------------------------------------------------------------------------------------------------------------------
  // summary


  addValueToRoleGroup(row: number) {
    if(this.listAttributeWithConstraintComplete[row].toString().includes('+dec') || this.listAttributeWithConstraintComplete[row].toString().includes('+int')) {
      this.showSummaryRoleGroup = true;
    }

    if(!this.fixedBooleanWith[row] || this.showSummaryRoleGroup) {
      let helperListName = [...this.listSelectedAttributeValueNameSummaryWith[row]];
      let helperListCode = [...this.listSelectedAttributeValueCodesSummaryWith[row]];

      this.listSummaryGroupsName.push(helperListName);
      this.listSummaryGroupsCodes.push(helperListCode);

      for (let col = 0; col < this.listSelectedAttributeValueNameSummaryWith[row].length; col++) {
        if (!this.fixedBooleanWith[row][col]) {
          this.listSelectedAttributeValueNameSummaryWith[row][col] = new Array();
          this.listSelectedAttributeValueCodesSummaryWith[row][col] = new Array();
        }
      }

      this.listNumberRoleGroups.push(row);

    } else {//    if(this.fixedBooleanWith[row])
      if (!this.showSummaryRoleGroup) {
        for(let i = 0; i < this.listNumberRoleGroups.length; i++) {
          if(row === this.listNumberRoleGroups[i]) {
            let helperListName = [...this.listSelectedAttributeValueNameSummaryWith[row]]
            let helperListCode = [...this.listSelectedAttributeValueCodesSummaryWith[row]]

            this.listSummaryGroupsName[i] = helperListName;
            this.listSummaryGroupsCodes[i] = helperListCode;
            this.listNumberRoleGroups[i] = row;
          } else if (!this.listNumberRoleGroups.toString().includes(row.toString())) {
            let helperListName = [...this.listSelectedAttributeValueNameSummaryWith[row]]
            let helperListCode = [...this.listSelectedAttributeValueCodesSummaryWith[row]]

            this.listSummaryGroupsName.push(helperListName);
            this.listSummaryGroupsCodes.push(helperListCode);
            this.listNumberRoleGroups.push(row);
          }
        }
        if(this.listNumberRoleGroups.length === 0) {
          let helperListName = [...this.listSelectedAttributeValueNameSummaryWith[row]]
          let helperListCode = [...this.listSelectedAttributeValueCodesSummaryWith[row]]

          this.listSummaryGroupsName[row] = helperListName;
          this.listSummaryGroupsCodes[row] = helperListCode;
          this.listNumberRoleGroups[row] = row;
        }

        if(this.listAttributeWithConstraintComplete[row].toString().includes('+int') || this.listAttributeWithConstraintComplete[row].toString().includes('+dec')) {
          this.listSelectedAttributeValueNameSummaryWith[row] = new Array();
          this.listSelectedAttributeValueCodesSummaryWith[row] = new Array();
        }


        for (let col = 0; col < this.listSelectedAttributeValueNameSummaryWith[row].length; col++) {
          if (!this.fixedBooleanWith[row][col]) {
            this.listSelectedAttributeValueNameSummaryWith[row][col] = new Array();
            this.listSelectedAttributeValueCodesSummaryWith[row][col] = new Array();
          }
        }
      }
    }
    this.showSummaryRoleGroup = true;
  }

  isAddGroupPossible(row: number) {
    // max cardinality of role group
    let max = 0;
    if (this.listRoleGroupsMaxCar[row] === '*') {
      max = Number.POSITIVE_INFINITY;
    } else {
      max = this.listRoleGroupsMaxCar[row];
    }
    let c = 0;
    if (this.listNumberRoleGroups != null) {
      for (let i = 0; i < this.listNumberRoleGroups.length; i++) {
        if (this.listNumberRoleGroups[i] === row) {
          c = c + 1;
        }
      }
      if (String(max).includes(String(c))) {
        let elementWarningBtn = document.getElementById(this.getIdWarningButton(row));
        if (elementWarningBtn != null) {
          elementWarningBtn.style.display = 'inline';
        }
        // @ts-ignore
        let elementAddBtn = document.getElementById(this.getIdButton(row));
        if (elementAddBtn != null) {
          return true;
        }
      }
    }

    // cardinality of attributes -- mandatory field
    for (let i = 0; i < this.listSelectedAttributeValueNameSummaryWith[row].length; i++) {
      if (this.listSelectedAttributeValueNameSummaryWith[row][i].length >= this.listAttributeWithMinCar[row][i] || this.fixedBooleanWith[row][i] === true) {
        continue;
      } else {
        return true;
      }
    }

    // at least 1 attribute must be filled out
    let counter = 0;
    for (let i = 0; i < this.listSelectedAttributeValueNameSummaryWith[row].length; i++) {
      if (this.listSelectedAttributeValueNameSummaryWith[row][i].length === 0) {
        counter = counter + 1;
      }
    }

    if((this.listRoleGroupsMaxCar[row] === '*' || this.listRoleGroupsMaxCar[row] === Number.POSITIVE_INFINITY) && this.fixedBooleanWith[row].toString().includes('true')) {
      return false;
    }
    for (let j = 0; j < this.listNumberRoleGroups.length; j++) {
      if(row === this.listNumberRoleGroups[j]) {
        if(typeof this.listSummaryGroupsName[j] !== 'undefined') {
          for (let k = 0; k < this.listSummaryGroupsName[j].length; k++) {
            let id = this.listNumberRoleGroups[j];
            if (this.listSummaryGroupsName[j][k].length >= this.listAttributeWithMinCar[id][k]) {
              continue;
            } else {
              return false;
            }
          }
        }
      }
    }
    if (counter === this.listSelectedAttributeValueNameSummaryWith[row].length) {               // || this.fixedBooleanWith[row].toString().includes('true')
      return true;
    }
    return false;
  }

  getIdWarningButton(row: number) {
    return "warningButton" + row;
  }

  getIdButton(row: number) {
    return "button" + row;
  }

  deleteRoleGroup(row: number) {
    let rgId = this.idTableRow.split("|")[0];
    let tableRow = this.idTableRow.split("|")[1];
    let counter = -1;

    for (let i = 0; i < this.listSummaryGroupsName.length; i++) {
      let number = this.listNumberRoleGroups[i];
      if (number.toString() === rgId.toString()) {
        counter = counter + 1;
        if (counter === Number.parseInt(tableRow)) {
          // @ts-ignore
          this.listSummaryGroupsName = this.listSummaryGroupsName.filter(x => x !== this.listSummaryGroupsName[i]);
          // @ts-ignore
          this.listSummaryGroupsCodes = this.listSummaryGroupsCodes.filter(x => x !== this.listSummaryGroupsCodes[i]);
          this.listNumberRoleGroups.splice(i, 1);

          if (!this.listNumberRoleGroups.toString().includes(String(row))) {
            this.showSummaryRoleGroup = false;
          }
        }
      }
    }

    // if(this.fixedBooleanWith[Number.parseInt(rgId)].toString().includes('true') && !this.listNumberRoleGroups.toString().includes(rgId) && this.isCarCorrect(Number.parseInt(rgId))) {
    //   this.listNumberRoleGroups.push(Number.parseInt(this.idTableRow.split('|')[0]));
    //   for (let i = 0; i <= this.fixedBooleanWith[Number.parseInt(rgId)].length; i++) {
    //       if (this.fixedBooleanWith[Number.parseInt(rgId)][i]) {
    //         // this.listSummaryGroupsName[Number.parseInt(rgId)] = this.listSelectedAttributeValueNameSummaryWith[Number.parseInt(rgId)]
    //         this.listSummaryGroupsName.push(this.listSelectedAttributeValueNameSummaryWith[Number.parseInt(rgId)])
    //         // this.listSummaryGroupsCodes[Number.parseInt(rgId)] = this.listSelectedAttributeValueCodesSummaryWith[Number.parseInt(rgId)]
    //         this.listSummaryGroupsCodes.push(this.listSelectedAttributeValueCodesSummaryWith[Number.parseInt(rgId)])
    //     }
    //   }
    // }

    // event
    let elementDeleteBtn = document.getElementById('delete' + row);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = true;
    }

    this.idTableRow = '';
    this.isDeleteSummaryDisabled = true;

  }

  showSummary(row: number) {
    let l = new Array();
    for(let i = 0; i < this.listSummaryGroupsName.length; i++) {
      let number = this.listNumberRoleGroups[i];
      if(typeof number != 'undefined') {
        if (number.toString() === row.toString()) {
          l.push(this.listSummaryGroupsName[i]);
        }
      }
    }

    return l;
  }

  clickTableRowSummary(event: any, row: number, idx: number) {
    this.isDeleteSummaryDisabled = false;
    this.idTableRow = '';
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      if (i !== row) {
        continue
      }
      this.idTableRow = row + '|' + idx;
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];

        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idTableRow) {
            element.style.background = 'lightgray';
          }
        }
      }
    }

    if(this.fixedBooleanRGWith[this.listNumberRoleGroups[row]]) {
      this.isDeleteSummaryDisabled = true;
    }

    // event
    let elementDeleteBtn = document.getElementById('delete' + row);
    if (elementDeleteBtn != null) {
      // @ts-ignore
      elementDeleteBtn.disabled = false;
    }
  }

  attributesNoConstraintWith() {
    let row = Number.parseInt(this.idWith.split("|")[0]);
    let col = Number.parseInt(this.idWith.split("|")[1]);
    return this.attributeValuesNoConstraintWith[row][col];
  }

  attributesNoConstraintWithBoolean() {
    let row = Number.parseInt(this.idWith.split("|")[0]);
    let col = Number.parseInt(this.idWith.split("|")[1]);
    if(this.idWith === '') {
      return false;
    } else {
      return this.attributeNoConstraintWithBoolean[row][col];
    }
  }



  // -------------------------------------------------------------------------------------------------------------------
  // create PCE

  createPCE() {
    this.resultPCE = this.buildPCE();
    this.showResult = true;
    this.createNamePce();
  }

  public jsonDiagramPce = '';


  checkRoleGroupCar() {
    for (let i = 0; i < this.listRoleGroupsMinCar.length; i++) {
      let count = 0;
      if (this.listRoleGroupsMaxCar[i] !== '1') {
        for (let j = 0; j < this.listNumberRoleGroups.length; j++) {
          if (Number(i) === Number(this.listNumberRoleGroups[j])) {
            count = count + 1;
          }
        }
        if (count < this.listRoleGroupsMinCar[i]) {
          this.mandatoryHighlightingWith();
          return false
        }

      } else {        // maxCar == 1
        for(let k = 0; k < this.listRoleGroupsMaxCar.length; k++) {
          if(this.listRoleGroupsMinCar[k] === '1') {                          // this.listRoleGroupsMaxCar[k] === '1' &&
            for (let j = 0; j < this.listNumberRoleGroups.length; j++) {
              if(typeof this.listSummaryGroupsName[j] != 'undefined') {
                for (let k = 0; k < this.listSummaryGroupsName[j].length; k++) {
                  let id = this.listNumberRoleGroups[j];
                  if (this.listSummaryGroupsName[j][k].length >= this.listAttributeWithMinCar[id][k]) {
                    continue;
                  } else {
                    if (this.update && this.listSummaryGroupsName[j][k].length >= this.listAttributeWithMinCar[id][Number.parseInt(this.listNumberRoleGroups[j])]) {
                      continue;
                    } else {
                      return false;
                    }
                  }
                }
              }
            }
          }
        }
        for(let k = 0; k < this.listRoleGroupsMaxCar.length; k++) {
          if(!this.listNumberRoleGroups.includes(k) && this.listRoleGroupsMinCar[k] > 0) {
            this.mandatoryHighlightingWith();
            return false;
          }
        }
      }
    }
    this.mandatoryHighlightingWith();
    return true;
  }

  mandatoryHighlightingWithout() {
    let checker = false;
    for(let i = 0; i < this.listAttributeWithoutMinCar.length; i++) {
      if(this.listAttributeWithoutMinCar[i].toString() === '1' && this.listSelectedAttributeValueNameSummaryWithout[i].length === 0) {
        checker = true;
        setTimeout(() => {
          let element = document.getElementById(this.getIdAttributes(i));
          if (element != null) {
            // @ts-ignore
            element.style.color = '#e93549';
          }
        }, 10);
      } else {
        setTimeout(() => {
          let element = document.getElementById(this.getIdAttributes(i));
          if (element != null) {
            // @ts-ignore
            element.style.color = 'black';
          }
        }, 10);
      }
    }
    if(checker) {
      return "Syntax error! Mandatory fields at attributes without RoleGroups";
    }
    return "";
  }

  mandatoryHighlightingWith(id?: string) {
    for(let i = 0; i < this.listRoleGroupsMinCar.length; i++) {
      if (this.listRoleGroupsMinCar[i].toString() === '1' && !this.listNumberRoleGroups.includes(i)) {
        setTimeout(() => {
          let element = document.getElementById('id-label' + i);
          if (element != null) {
            // @ts-ignore
            element.style.color = '#e93549';
          }
        }, 10);
      } else {
        setTimeout(() => {
          let element = document.getElementById('id-label' + i);
          if (element != null) {
            // @ts-ignore
            element.style.color = 'black';
          }
        }, 10);
      }

      for(let j = 0; j < this.listAttributeWithMinCar[i].length; j++) {
        if(id !== 'start') {
          if (this.listAttributeWithMinCar[i][j].toString() === '1' && this.listSelectedAttributeValueNameSummaryWith[i][j].length === 0) {
            setTimeout(() => {
              let element = document.getElementById("label-" + this.getIndexWith(i, j));
              if (element != null) {
                // @ts-ignore
                element.style.color = '#e93549';
              }
              element = document.getElementById('id-label' + i);
              if (element != null) {
                // @ts-ignore
                element.style.color = '#e93549';
              }
            }, 10);
          } else {
            setTimeout(() => {
              let element = document.getElementById("label-" + this.getIndexWith(i, j));
              if (element != null) {
                // @ts-ignore
                element.style.color = 'black';
              }
            }, 10);
          }
        } else {
          setTimeout(() => {
            let element = document.getElementById("label-" + this.getIndexWith(i, j));
            if (element != null) {
              // @ts-ignore
              element.style.color = 'black';
            }
          }, 10);




        }
      }
    }


  }

  buildPCE() {
    this.jsonDiagramPce = "";
    this.highlightingPceService.focusConceptCode = "";
    this.highlightingPceService.focusConceptName = "";
    this.highlightingPceService.listSummaryAttributeCodesWithout = new Array();
    this.highlightingPceService.listSummaryAttributeNamesWithout = new Array();
    this.highlightingPceService.listSummaryValueWithout = new Array();
    this.highlightingPceService.listSummaryAttributeCodesWith = new Array();
    this.highlightingPceService.listSummaryAttributeNamesWith = new Array();
    this.highlightingPceService.listSummaryValueWith = new Array();
    this.highlightingPceService.input = 'Template';


    let focusConcept = '';
    let withoutRoleGroup = '';

    this.jsonDiagramPce = this.jsonDiagramPce + "{\"focusconcept\" : [{\"code\" :\"" + this.focusConceptCode.split("+").join(" + ") + "\",\"name\" :\"" + this.focusConceptName.split("+").join(" + ")  + "\"}]";

    if(this.focusConceptCode.includes('+') && this.focusConceptName.includes('+')) {
      let code = this.focusConceptCode.split('+');
      let name = this.focusConceptName.split('+');
      for(let  i = 0; i < code.length; i++) {
        focusConcept = focusConcept + code[i] + "|" + name[i] + "|+";
      }
      focusConcept = focusConcept.substring(0, focusConcept.length-1) + ":" + "\n"
    } else {
      focusConcept = focusConcept + this.focusConceptCode + "|" + this.focusConceptName + "|" + ":" + "\n";
    }

    this.highlightingPceService.focusConceptCode = this.focusConceptCode;
    this.highlightingPceService.focusConceptName = this.focusConceptName;

    // WITHOUT ROLE GROUP
    this.jsonDiagramPce = this.jsonDiagramPce + ",\"withoutRoleGroup\" : [";
    for (let i = 0; i < this.listAttributeWithoutMinCar.length; i++) {
      let max = 0;
      if (this.listAttributeWithoutMaxCar[i] === '*') {
        max = Number.POSITIVE_INFINITY;
      } else {
        max = this.listAttributeWithoutMaxCar[i];
      }

      if(typeof this.listSelectedAttributeValueNameSummaryWithout[i] !== 'undefined') {
        let response = this.mandatoryHighlightingWithout();
        // @ts-ignore
        if(response.length > 0) {
          this.highlightingPceService.input = '';
          return response;
        } else {
          for (let j = 0; j < this.listSelectedAttributeValueNameSummaryWithout[i].length; j++) {
            this.jsonDiagramPce = this.jsonDiagramPce + "{\"attributecode\" : \"" + this.listAttributeWithoutCode[i] + "\",\"attributename\" : \"" + this.listAttributeWithoutName[i]  + " (attribute)\",";
            let x = '';
            if(!this.listSelectedAttributeValueCodesSummaryWithout[i][j].includes('#')) {
              x = this.listSelectedAttributeValueCodesSummaryWithout[i][j] + " |" + this.listSelectedAttributeValueNameSummaryWithout[i][j] + "|" + "," + "";
              this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + this.listSelectedAttributeValueCodesSummaryWithout[i][j] + "\",\"valuename\" : \"" + this.listSelectedAttributeValueNameSummaryWithout[i][j] + "\"},"
            } else {
              x = this.listSelectedAttributeValueNameSummaryWithout[i][j] + "," + "\n";
              this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + "" + "\",\"valuename\" : \"" + x.split(",").join("").split("\n").join("") + "\"},"
            }
            withoutRoleGroup = withoutRoleGroup + (this.listAttributeWithoutCode[i] + " |" + this.listAttributeWithoutName[i] + "  (attribute)|" + "=" + x);
            this.highlightingPceService.listSummaryAttributeCodesWithout.push(this.listAttributeWithoutCode[i]);
            this.highlightingPceService.listSummaryAttributeNamesWithout.push(this.listAttributeWithoutName[i]);
            this.highlightingPceService.listSummaryValueWithout.push(x.split(",").join(""));
          }
          this.mandatoryHighlightingWithout();
        }
      } else {
        let len = 0;
        if (!(len >= this.listAttributeWithoutMinCar[i] && len <= max)) {
          return "Syntax error (eg: Mandatory fields)";
        }
      }
    }
    if(withoutRoleGroup.length > 0) {
      this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.length-1);
    }
    this.jsonDiagramPce = this.jsonDiagramPce + "]";
    withoutRoleGroup = withoutRoleGroup.substring(0, withoutRoleGroup.length - 2);

    // WITH ROLE GROUP
    let withRoleGroup = '';

    if (this.checkRoleGroupCar()) {
      if (withoutRoleGroup.length > 0) {
        withRoleGroup = ',';
      } else {
        withRoleGroup = '';
      }
      // @ts-ignore
      this.listSummaryGroupsName = this.listSummaryGroupsName.filter(x => typeof x !== 'undefined');
      // @ts-ignore
      this.listSummaryGroupsCodes = this.listSummaryGroupsCodes.filter(x => typeof x !== 'undefined');
      // @ts-ignore
      this.listNumberRoleGroups = this.listNumberRoleGroups.filter(x => typeof x !== 'undefined');

      this.jsonDiagramPce = this.jsonDiagramPce + ",\"withRoleGroup\" : [";

      for (let i = 0; i < this.listSummaryGroupsName.length; i++) {

        this.highlightingPceService.listSummaryAttributeCodesWith.push(new Array());
        this.highlightingPceService.listSummaryAttributeNamesWith.push(new Array());
        this.highlightingPceService.listSummaryValueWith.push(new Array());
        this.highlightingPceService.isRefinement = true;
        this.jsonDiagramPce = this.jsonDiagramPce + "{\"roleGroup\" : ["

        if (withRoleGroup.length > 0) {
          withRoleGroup = withRoleGroup + '\n' + '{';
        } else {
          withRoleGroup = withRoleGroup + '{';
        }

        if (this.listSummaryGroupsName[i] !== null) {
          for (let j = 0; j < this.listSummaryGroupsName[i].length; j++) {

            this.highlightingPceService.listSummaryAttributeCodesWith[i].push(new Array());
            this.highlightingPceService.listSummaryAttributeNamesWith[i].push(new Array());
            this.highlightingPceService.listSummaryValueWith[i].push(new Array());

            if(this.listSummaryGroupsName[i][j].length > 0 && typeof this.attributesWithGroup[this.listNumberRoleGroups[i]][j] != 'undefined') {
              let minCar = this.attributesWithGroup[this.listNumberRoleGroups[i]][j].attributeCardinalityMin;
              let maxCar = this.attributesWithGroup[this.listNumberRoleGroups[i]][j].attributeCardinalityMax;
              let code = this.attributesWithGroup[this.listNumberRoleGroups[i]][j].attributeNameCode;
              let name = this.attributesWithGroup[this.listNumberRoleGroups[i]][j].attributeNameDisplay;
              if(maxCar === '*') {
                maxCar = Number.POSITIVE_INFINITY;
              }

              this.highlightingPceService.listSummaryAttributeCodesWith[i][j].push(new Array());
              this.highlightingPceService.listSummaryAttributeNamesWith[i][j].push(new Array());
              this.highlightingPceService.listSummaryValueWith[i][j].push(new Array());

              if (this.listSummaryGroupsName[i][j].length > 0) {
                if (this.listSummaryGroupsName[i][j].length >= minCar && this.listSummaryGroupsName[i][j].length <= maxCar) {
                  for (let k = 0; k < this.listSummaryGroupsName[i][j].length; k++) {
                    this.jsonDiagramPce = this.jsonDiagramPce + "{\"attributecode\" : \"" + code + "\",\"attributename\" : \"" + name  + " (attribute)\",";
                    let x = '';
                    if (!this.listSummaryGroupsCodes[i][j][k].includes('#')) {
                      x = this.listSummaryGroupsCodes[i][j][k] + " |" + this.listSummaryGroupsName[i][j][k] + "|";
                      this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + this.listSummaryGroupsCodes[i][j][k] + "\",\"valuename\" : \"" + this.listSummaryGroupsName[i][j][k] + "\"},"
                    } else {
                      x = this.listSummaryGroupsCodes[i][j][k];
                      this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + "" + "\",\"valuename\" : \"" + x + "\"},"
                    }
                    withRoleGroup = withRoleGroup + code + "|" + name + " (attribute)|" + "=" + x + ",\n";
                    if(code.length > 0) {
                      this.highlightingPceService.listSummaryAttributeCodesWith[i][j].push(code);
                      this.highlightingPceService.listSummaryAttributeNamesWith[i][j].push(name);
                      this.highlightingPceService.listSummaryValueWith[i][j].push(x);
                      this.highlightingPceService.isRoleGroup = true;
                    }
                  }
                } else {
                  this.highlightingPceService.input = '';
                  return "Syntax error (eg: Mandatory fields 0)";
                }
              }
              // @ts-ignore
              this.highlightingPceService.listSummaryAttributeCodesWith[i][j] = this.highlightingPceService.listSummaryAttributeCodesWith[i][j].filter(x => x.length !== 0);
              this.highlightingPceService.listSummaryAttributeNamesWith[i][j] = this.highlightingPceService.listSummaryAttributeNamesWith[i][j].filter((x: string | any[]) => x.length !== 0);
              this.highlightingPceService.listSummaryValueWith[i][j] = this.highlightingPceService.listSummaryValueWith[i][j].filter((x: string | any[]) => x.length !== 0);
            }
          }
          // @ts-ignore
          this.highlightingPceService.listSummaryAttributeCodesWith[i] = this.highlightingPceService.listSummaryAttributeCodesWith[i].filter(x => x.length !== 0);
          this.highlightingPceService.listSummaryAttributeNamesWith[i] = this.highlightingPceService.listSummaryAttributeNamesWith[i].filter((x: string | any[]) => x.length !== 0);
          this.highlightingPceService.listSummaryValueWith[i] = this.highlightingPceService.listSummaryValueWith[i].filter((x: string | any[]) => x.length !== 0);

          withRoleGroup = withRoleGroup.substring(0, withRoleGroup.length - 2);
          withRoleGroup = withRoleGroup + "},";
          this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.length-1);
          this.jsonDiagramPce = this.jsonDiagramPce + "]},";

          let open = withRoleGroup.split("{").length - 1;
          let close = withRoleGroup.split("}").length - 1;
          if(open !== close) {
            this.highlightingPceService.input = '';
            this.jsonDiagramPce = '';
            return "Syntax error (eg: Mandatory fields 2)";
          }
        }
        else {
          return "Syntax error (eg: Mandatory fields 3 )";
        }
      }
      if(this.listSummaryGroupsName.length > 0) {
        this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.lastIndexOf(","));
      }
      withRoleGroup = withRoleGroup.substring(0, withRoleGroup.length - 1);
      this.mandatoryHighlightingWith('start')
    } else {
      this.highlightingPceService.input = '';
      this.mandatoryHighlightingWith();

      return "Syntax error! A RoleGroup or attribute is mandatory!";
    }
    this.jsonDiagramPce = this.jsonDiagramPce + "]}";

    setTimeout(() => {
      this.dia.drawDiagram(this.jsonDiagramPce);
    }, 10);

    if(withoutRoleGroup.length === 0 && withRoleGroup.length === 0 && !this.listRoleGroupsMinCar.includes('1') && !this.listAttributeWithoutMinCar.includes('1')) {
      return focusConcept.replace(":", "");
    } else if(withoutRoleGroup.length > 0 || withRoleGroup.length > 0) {
      let pce = focusConcept + withoutRoleGroup + withRoleGroup;
      if(pce.substring(pce.length-2) === ',}') {
        return pce.substring(0, pce.length-2);
      } else {
        return focusConcept + withoutRoleGroup + withRoleGroup;
      }
    }
    else {
      this.highlightingPceService.input = '';
      return "Syntax error (eg: Mandatory fields 5)";
    }
  }

  isCopyToClickbloard = false;
  isDownload = false;

  copyPceToClipboard(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.resultPCE.split("=").join(" = ");
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.isCopyToClickbloard = true;
    setTimeout(() => {
      this.isCopyToClickbloard = false;
    }, 2000);
  }


  downloadPce() {
    this.isDownload = true;
    setTimeout(() => {
      this.isDownload = false;
    }, 2000);
    // @ts-ignore
    const blob = new Blob([this.resultPCE], { type: 'text/json' });
    const url= window.URL.createObjectURL(blob);
    const anchorElement = document.createElement('a');
    document.body.appendChild(anchorElement);
    anchorElement.style.display = 'none';
    anchorElement.href = url;
    anchorElement.download = "pce-" + this.focusConceptName + ".txt";
    anchorElement.click();
    window.URL.revokeObjectURL(url);
  }

  createNamePce() {
    this.namePCE =  this.resultPCE.split(":")[0].replace(/[0-9]/g, '').split("|").join("");
    if(this.resultPCE.split(":")[1] !== undefined) {
      this.namePCE = this.namePCE + ":"
      let y = this.resultPCE.split(":")[1].split(",");
      for(let i = 0; i < y.length; i++) {
        if(!y[i].includes("#")) {
          this.namePCE = this.namePCE + y[i].replace(/[0-9]/g, '').split(' |').join('').split('|').join('') + ",";
        } else {
          let attr = y[i].toString().split("=")[0];
          let value = y[i].toString().split("=")[1];
          this.namePCE = this.namePCE + attr.replace(/[0-9]/g, '').split(' |').join('').split('|').join('') + "= " + value + ",";
        }
      }
    } else {
      this.namePCE.split(":").join("");
    }
    if(this.namePCE.includes(',')) {
      this.namePCE = this.namePCE.substring(0, this.namePCE.lastIndexOf(','))
    }

  }

  // -------------------------------------------------------------------------------------------------------------------
  // Save pce

  isCreateSuccessful() {
    return this.resultPCE.includes("Syntax error") || this.resultPCE.length === 0;
  }

  savePCE() {
    this.createNamePce();
    this.savePCETextarea = "PCE: \n" + this.resultPCE + "\n \nName: \n" + this.namePCE;

  }


  // -------------------------------------------------------------------------------------------------------------------
  // Update PCE

  public oldUrlCsSupplement = '';

  getStoredPCEs(url: string) {
    this.listStoredPCEs = new Array();
    this.listStoredPCEsName = new Array();

    if(this.settingsCsService.newUrl.length > 0) {
      this.templateService.getCodeSystem(url).then((response: any) => {
        let result = response.result;
        let concept = result[0].concept;
        for (let i = 0; i < concept.length; i++) {
          let id = i;
          let code = concept[i].code;
          let display = concept[i].display;
          let definition = concept[i].definition;

          if (!this.listStoredPCEs.includes(id + "@@" + code + "@@" + display + "@@" + definition)) {
            this.listStoredPCEs.push(id + "@@" + code + "@@" + display + "@@" + definition);
            if(typeof display !== 'undefined') {
              this.listStoredPCEsName.push(display.split(":").join("  :  ").split(",").join("  ,  "));
            } else {
              this.listStoredPCEsName.push("");
            }
          }
        }
        this.oldUrlCsSupplement = url;
      })
    }
  }

  clickRowStore(event: any, row: number) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      this.idRowStoredPCE = row.toString();

      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idRowStoredPCE) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    if(this.appService.selectedItem === 'ID_TEMPLATE_UPDATE') {
      this.isBtnUpdateActive = true;
      this.idRowStoredPCE = row.toString();
    }

  }

  getIdRowStore(row: number) {
    return row;
  }

  updatePCE() {
    this.resultPCE = '';
    let idx = Number.parseInt(this.idRowStoredPCE);
    let pce = this.listStoredPCEs[idx].split("@@")[1];

    let refinement = pce.split(":")[1];
    let posRGStart = new Array();
    let posRGEnd = new Array();

    for(let  i = 0; i < refinement.length; i++) {
      if(refinement[i] === "{") {
        posRGStart.push(i+1);
      } else if(refinement[i] === "}") {
        posRGEnd.push(i+1);
      }
    }

    if(posRGStart.length > 0 && posRGEnd.length > 0 ) {
      if(posRGStart[0] !== 1) {
        this.updateWithoutRoleGroup(posRGStart, posRGEnd, refinement)
      }
      this.updateWithRoleGroups(posRGStart, posRGEnd, refinement);
    } else {
      this.updateWithoutRoleGroup(posRGStart, posRGEnd, refinement);
    }
    this.showTemplateContent = true;
  }

  updateWithoutRoleGroup(posRGStart: any, posRGEnd: any, refinement: any) {
    let withoutRGSummary = '';
    if(posRGStart.length > 0 && posRGEnd.length > 0) {
      if (refinement.substring(0, posRGStart[0] - 1) === ',') {
        withoutRGSummary = refinement.substring(0, posRGStart[0] - 2);
      } else {
        withoutRGSummary = refinement.substring(0, posRGStart[0] - 1);
      }
    } else {
      withoutRGSummary = refinement;
    }

    let listAttributeNameCode = new Array();
    let listAttributeNameName = new Array();
    let listAttributeValueCode = new Array();
    let listAttributeValueName = new Array();
    let attribute = new Array();


    if(withoutRGSummary.includes(",")) {
      attribute = withoutRGSummary.split(",")   // todo -- HIER KOMMA
    } else {
      attribute = withoutRGSummary.split("|")
    }

    for(let j = 0; j < attribute.length; j++) {
      if(attribute[j] !== '' && attribute[j].includes("=")) {


        if(attribute[j].includes("=")) {
          let attributeNameCode = attribute[j].split("=")[0].split("|")[0].split(" ").join("");
          let attributeNameName = attribute[j].split("=")[0].split("|")[1];
          let attributeValueCode = attribute[j].split("=")[1].split("|")[0];
          let attributeValueName = attribute[j].split("=")[1].split("|")[1];
          if(attributeValueName === undefined) {
            attributeValueName = attributeValueCode.split("#").join("").split("}").join("").split("{").join("");
            attributeValueCode = attributeValueCode.split("#").join("").split("}").join("").split("{").join("");
          }


          listAttributeNameCode.push(attributeNameCode);
          listAttributeNameName.push(attributeNameName);
          listAttributeValueCode.push(attributeValueCode);
          listAttributeValueName.push(attributeValueName);
        }
      }
    }
    let helperNumberList = new Array();

    // check with element number
    for (let j = 0; j < this.listAttributeWithoutCode.length; j++) {
      for (let k = 0; k < listAttributeNameCode.length; k++) {
        if (this.listAttributeWithoutCode[j] === listAttributeNameCode[k]) {
          helperNumberList.push(j);
        }
      }
    }

    for (let i = 0; i < helperNumberList.length; i++) {
      if (this.fixedBooleanWithout[helperNumberList[i]] === false) {
        this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(listAttributeValueCode[i], "", "10000").then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let k = 0; k < value.length; k++) {
              this.attributeValueWithout[helperNumberList[i]] = value[k].fsn;
              this.listAttributeValueCodesWithout.push(value[k].code);
              this.listSelectedAttributeValueCodesSummaryWithout[helperNumberList[i]].push(value[k].code);
              this.addValueToTableWithout(helperNumberList[i]);
            }
          } else {
            this.listSelectedAttributeValueNameSummaryWithout[helperNumberList[i]].push(listAttributeValueName[i].split("#").join(""));
            this.listSelectedAttributeValueCodesSummaryWithout[helperNumberList[i]].push(listAttributeValueName[i].split("#").join(""));
          }
          // @ts-ignore
          this.listSelectedAttributeValueCodesSummaryWithout[helperNumberList[i]] = this.listSelectedAttributeValueCodesSummaryWithout[helperNumberList[i]].filter(x => typeof x !== 'undefined');
        })
      }
    }
  }

  updateWithRoleGroups(posRGStart: any, posRGEnd: any, refinement: any) {
    for(let  i = 0; i < posRGStart.length; i++) {
      let listAttributeNameCode = new Array();
      let listAttributeNameName = new Array();
      let listAttributeValueCode = new Array();
      let listAttributeValueName = new Array();
      let numberRg = -1;

      let rg = refinement.substring(posRGStart[i], posRGEnd[i]);
      let attribute = rg.split(",")

      // für jede rg die einzelnen attribut namen (codes)
      for(let j = 0; j < attribute.length; j++) {
        if(attribute[j].includes("=")) {
          let attributeNameCode = attribute[j].split("=")[0].split("|")[0];
          let attributeNameName = attribute[j].split("=")[0].split("|")[1];
          let attributeValueCode = attribute[j].split("=")[1].split("|")[0];
          let attributeValueName = attribute[j].split("=")[1].split("|")[1];

          if(attributeValueName === undefined) {
            attributeValueName = attributeValueCode.split("#").join("").split("}").join("").split("}").join("");
            attributeValueCode = attributeValueCode.split("#").join("").split("}").join("").split("{").join("");
          }

          listAttributeNameCode.push(attributeNameCode);
          listAttributeNameName.push(attributeNameName);
          listAttributeValueCode.push(attributeValueCode);
          listAttributeValueName.push(attributeValueName);
        }
      }

      let helperNumberList = new Array();
      // check which number of rg
      for (let j = 0; j < this.listAttributeWithCode.length; j++) {
        let b = false;
        for (let k = 0; k < listAttributeNameCode.length; k++) {
          if (this.listAttributeWithCode[j].includes(listAttributeNameCode[k])) {
            b = true;
          } else {
            break;
          }
        }
        if (b) {
          helperNumberList.push(j);
          numberRg = j;
          b = false;
        }
      }

      // insert in Summary lists
      if(Number.parseInt(this.listRoleGroupsMaxCar[numberRg]) !== 1) {
        for (let j = 0; j < listAttributeNameCode.length; j++) {
          let pos = this.listAttributeWithCode[numberRg].indexOf(listAttributeNameCode[j]);
          if(!this.fixedBooleanWith[numberRg][pos]) {
            this.listSelectedAttributeValueCodesSummaryWith[numberRg][pos] = [listAttributeValueCode[j]];
            this.listSelectedAttributeValueNameSummaryWith[numberRg][pos] = [listAttributeValueName[j]];
          }
          this.showTableWith = true;
        }
      }

      for (let j = 0; j < listAttributeNameCode.length; j++) {
        let pos = this.listAttributeWithCode[numberRg].indexOf(listAttributeNameCode[j]);
        if (!this.fixedBooleanWith[numberRg][pos] && Number.parseInt(this.listRoleGroupsMaxCar[numberRg]) === 1) {
          this.templateService.getAttributeValueRequest(listAttributeValueCode[j]).then((response: any) => {
            const expansion = response.expansion;
            if (typeof expansion != "undefined") {
              const contains = expansion.contains;
              if (typeof contains != "undefined") {
                for (let k = 0; k <= contains.length; k++) {
                  if (typeof contains[k] != "undefined") {
                    this.attributeValueWith[numberRg][pos] = contains[k].display;
                    this.listAttributeValueCodesWith.push(contains[k].code);
                    this.addValueToTableWith(numberRg, pos);
                  }
                }
              }
            } else {
                this.listSelectedAttributeValueNameSummaryWith[numberRg][pos].push(listAttributeValueName[j].split("#").join(""));
                this.listSelectedAttributeValueCodesSummaryWith[numberRg][pos].push(listAttributeValueName[j].split("#").join(""));
              }
          })
        }
      }

      if(Number.parseInt(this.listRoleGroupsMaxCar[numberRg]) !== 1) {
        this.addValueToRoleGroup(numberRg);
      }

    }
  }

  loadPCE() {

    this.resultPCE = '';
    let idx = Number.parseInt(this.idRowStoredPCE);
    this.templateName = this.listStoredPCEs[idx].split("@@")[3];

    let pce = this.listStoredPCEs[idx].split("@@")[1];

    this.templateService.validatePce(pce).then((response: any) => {
      let value = response.result;

      if(value === 'True') {
        this.appService.selectedItem = 'ID_TEMPLATE_CREATE';
        this.changeTemplateChoice = true;

        if(this.templateName !== 'ConceptModel') {
          this.update = true;
          this.clickChooseTemplate();
        } else {
          this.pceWithoutTemplateService.start = 'ID_PCE_WITHOUT_TEMPLATE';
          this.pceWithoutTemplateService.clickTemplateGeneration('ID_PCE_WITHOUT_TEMPLATE');
          this.templateGenerationService.clickTemplateGeneration('');
          this.clickItem('');

          this.resultPCE = '';
          let idx = Number.parseInt(this.idRowStoredPCE);


          this.pceWithoutTemplateService.getDataPceUpdate(pce,idx);
          this.pceWithoutTemplateService.isUpdate = true;

        }
      } else {
        this.validateCodeMessage = "Selected PCE ID " + idx + " -- " + value;
      }
    })
  }

  public validateCodeMessage = '';





  updateCodeSystem() {
    setTimeout(() => {
      let element = document.getElementById('div-settingstemplates');
      if (element != null) {
        // @ts-ignore
        element.style.pointerEvents = 'none';
      }
    }, 10);

    this.createNamePce();
    let id = this.listStoredPCEs[Number.parseInt(this.idRowStoredPCE)].split("@@")[0];
    let templateName = this.listStoredPCEs[Number.parseInt(this.idRowStoredPCE)].split("@@")[3];
    this.appService.uploadPCE = 'start'

    let sameCS = '';
    if(this.settingsCsService.newUrl === this.settingsTemplateService.mainUrl) {
      sameCS = 'true';
    } else {
      sameCS = 'false';
    }

    this.templateService.updateCodeSystemUpdatedPce(id, this.resultPCE.split("\n").join(""), this.namePCE.split("\n").join(""), templateName, sameCS).then((response: any) => {   // TODO
      const message = response.result;
      this.appService.uploadPCE = '';
      if(!message.includes('error')) {
        this.settingsTemplateService.ready = false;
        this.appService.uploadPCE = 'success';
      } else {
        this.appService.uploadPCE = 'error';
        this.settingsTemplateService.ready = false;
        if(message.includes('@')) {
          this.appService.errorMessage = response.result.split('@')[1];
        } else {
          this.appService.errorMessage = '';
        }
      }
      setTimeout(() => {
        let element = document.getElementById('div-settingstemplates');
        if (element != null) {
          // @ts-ignore
          element.style.pointerEvents = 'auto';
        }
      }, 10);

    })
    this.settingsTemplateService.ready = false;
  }


  storePCE() {
    this.appService.uploadPCE = 'start';

    if(this.settingsTemplateService.newNameCsSupplement.length > 0) {
      this.templateService.updateNewCodeSystem(this.resultPCE.split('\n').join(''), this.namePCE.split('\n').join('').split('|').join(''), this.templateName).then((response: any) => {
        if(response.result.toString().includes("@")) {
          this.appService.uploadPCE = response.result.split("@")[0];
          this.appService.errorMessage = response.result.split("@")[1];
        } else {
          this.appService.uploadPCE = response.result;
          this.appService.errorMessage = '';
        }
      })
    } else {
      this.templateService.updateExistingCodeSystem(this.resultPCE.split('\n').join(''), this.namePCE.split('\n').join('').split('|').join(''), this.templateName).then((response: any) => {
        if(response.result.toString().includes("@")) {
          this.appService.uploadPCE = response.result.split("@")[0];
          this.appService.errorMessage = response.result.split("@")[1];
        } else {
          this.appService.uploadPCE = response.result;
          this.appService.errorMessage = '';
        }
      })
    }

    this.settingsTemplateService.ready = false;
  }

  isBtnSaveActive() {
    let b = !this.settingsTemplateService.ready;
    let c = this.appService.uploadPCE === 'start';

    if(this.appService.uploadPCE === 'start') {
      return true;
    } else if(this.settingsTemplateService.id === 'new') {
      if(this.settingsTemplateService.newIdCsSupplement !== undefined) {
        return !((this.settingsTemplateService.newIdCsSupplement.length > 0) && (this.settingsTemplateService.newNameCsSupplement.length > 0));
      }
      return false;
    } else if(this.settingsTemplateService.id === 'existing') {
      if(this.settingsTemplateService.newIdCsSupplement !== undefined){
        return !(this.settingsTemplateService.newIdCsSupplement.length > 0);
      }
    }
    return !(b || c);
  }


  openFile(){
    // @ts-ignore
    document.querySelector('input').click()
  }












  // -------------------------------------------------------------------------------------------------------------------

  cancel() {
    this.ngOnInit();
    if(this.appService.selectedItem !== '') {
      this.appService.selectedItem = '';
      this.appService.selectedApp = '';
    }
  }

  resetAllVariables() {
    this.id = -1;
    this.idWith = '';
    this.templateNamesList = new Array();
    this.templateName = '';
    this.showTemplateContent = false;
    this.changeTemplateChoice = false;
    this.focusConceptName = '';
    this.focusConceptCode = '';
    this.focusConcept = '';
    this.listAttributeWithoutMinCar = new Array();
    this.listAttributeWithoutMaxCar = new Array();
    this.listAttributeWithoutName = new Array();
    this.listAttributeWithoutCode = new Array();
    this.listAttributeWithoutConstraint = new Array();
    this.attributesWithoutGroup = new Array();
    this.attributeValueWithout = new Array();
    this.listAttributeValueCodesWithout = new Array();
    this.listAttributeValueNameWithout = new Array();
    this.listAttributeWithoutConstraintComplete = new Array();
    this.oldId = -1;
    this.showTablesWithout = false;
    this.listSelectedValueName = new Array();
    this.listSelectedValueCode = new Array();
    this.listSelectedAttributeValueCodesSummaryWithout = new Array();
    this.listSelectedAttributeValueNameSummaryWithout = new Array();
    this.idTableRow = '';
    this.showAttributeWithout = false;
    this.listRoleGroupsMinCar = new Array();
    this.listRoleGroupsMaxCar = new Array();
    this.attributesWithGroup = new Array();
    this.attributeValueWith = new Array();
    this.listAttributeWithMinCar = new Array();
    this.listAttributeWithMaxCar = new Array();
    this.listAttributeWithName = new Array();
    this.listAttributeWithCode = new Array();
    this.listAttributeWithConstraint = new Array();
    this.listSelectedAttributeValueCodesSummaryWith = new Array();
    this.listSelectedAttributeValueNameSummaryWith = new Array();
    this.listAttributeValueCodesWith = new Array();
    this.listAttributeValueNameWith = new Array();
    this.showTableWith = false;
    this.oldIDRow = -1;
    this.oldIDCol = -1;
    this.showAttributeWith = false;
    this.showSummaryRoleGroup = false;
    this.listSummaryGroupsName = new Array();
    this.listSummaryGroupsCodes = new Array();
    this.listNumberRoleGroups = new Array();
    this.isDeleteSummaryDisabled = true;
    this.resultPCE = '';
    this.showResult = false;
    this.attributeNoConstraintWithoutBoolean = new Array();
    this.attributeValuesNoConstraintWithout = new Array();
    this.attributeNoConstraintWithBoolean = new Array();
    this.attributeValuesNoConstraintWith = new Array();
    this.templateLanguage = "";
    this.attributeDefinitionCMCode = new Array();
    this.attributeDefinitionCMName = new Array();
    this.attributeDefinitionCMDefinition = new Array();
    this.attributeDefinitionCMRangeConstraint = new Array();
    this.listAttributeValueCodesFilter = new Array();
    this.listAttributeValueNameFilter = new Array();
    this.attributesFilterBoolean = false;
    this.listAttributeWithConstraintComplete = new Array();
    this.listAttributeWithConstraintCompleteCodes = new Array();
    this.keypressWith = true
    this.fixedValuesCodeWith = new Array();
    this.fixedValuesNameWith = new Array();
    this.fixedValuesCodeWithout = new Array();
    this.fixedValuesNameWithout = new Array();
    this.fixedBooleanWithout = new Array();
    this.fixedBooleanWith = new Array();
    this.fixedBooleanRGWith = new Array();
    this.namePCE = '';
    // this.appService.uploadPCE = '';
    this.savePCETextarea = '';
    this.idAttributeWithout = -1;
    this.isBtnUpdateActive = false;
    this.update = false;
    this.listStoredPCEs = new Array();
    this.idRowStoredPCE = '';
    this.listStoredPCEsName = new Array();
    this.oldUrlCsSupplement = '';
    this.jsonDiagramPce = '';
    this.templateNamesListInit = new Array();
    this.templateNamesListSemanticTagInit = new Array();
    this.templateNamesListSemanticTag01 = new Array();
    this.templateNamesListSemanticTag02 = new Array();
  }


}

