import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {ValidationToolService} from "./validationTool.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";

@Component({
  selector: 'app-validationTool',
  templateUrl: './validationTool.component.html',
  styleUrls: ['./validationTool.css']
})

export class ValidationToolComponent {

  public ID_TEXT_FIELD = 'text-field';
  public ID_UPLOAD = 'upload-file';
  // public idStart = this.ID_TEXT_FIELD;
  public idStart = this.ID_UPLOAD;
  public singleConceptInput = '';
  public content = '';
  public showInfoAlert = false;
  public showValidateAlert = false;
  public showDownloadAlert = false;
  public finishValidation = false;
  public file: File | null = null;
  public fileName = '';
  public fileContent = '';
  public listConcepts: any = [];
  public listResult: any = [];
  public listResultContent: any = [];
  public idx = -1;

  constructor(public validationToolService: ValidationToolService, public appService: AppService, public templateGenerationService: TemplatesGenerationService, public settingsService: SettingsService, public settingsCsService: SettingsCsSupplementService, public settingsTemplateService: SettingsTemplatesService, public pceWithoutTemplateService: PceWithoutTemplateService) {
  }

  ngOnInit() {
    this.loadAttributeDefinitionConceptModel();
   }

  public setInputId(identifier: string) {
    this.idStart = identifier;
    this.content = '';
    this.listResultContent = [];
    this.listResult = [];
    this.singleConceptInput = '';
    this.showInfoAlert = false;
    this.showValidateAlert = false;
    this.showDownloadAlert = false;
  }

  loadAttributeDefinitionConceptModel() {
    const file = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "conceptModelObjectAttributes.csv");
    const fileContent = file[0].fileContent;
    const arr = fileContent.split("\n");
    for (let i = 0; i < arr.length; i++) {
      let value = arr[i].split(";")
      this.validationToolService.attributeDefinitionCMCode.push(value[0]);
      this.validationToolService.attributeDefinitionCMName.push(value[1]);
      this.validationToolService.attributeDefinitionCMDefinition.push(value[2]);
      this.validationToolService.attributeDefinitionCMRangeConstraint.push(value[3]);
    }
  }

  public startValidationSingleConcept() {
    this.showValidateAlert = true;
    if(this.idStart === this.ID_TEXT_FIELD) {
      this.validateSinglePce();
    } else {
      this.validateFile();
    }
  }

  public validateSinglePce() {
    this.content = '';
    this.content = '';
    this.validationToolService.validateSingleCode(this.singleConceptInput).then((response: any) => {
      console.log(response)
      if(JSON.stringify(response.content).length > 0 && !JSON.stringify(response.content).includes("ID-pre-coordinated")) {
        if (response.content !== undefined) {
          this.content = response.content.split("\\n").join("\n").split("\\t").join("\t").split("\"E").join("E").split("#\"").join("#");
        }
      }
      this.finishValidation = true;
      this.showValidateAlert = false;
    })
  }

  public validateFile() {
    if(this.fileName.includes(".json")) {
      this.processingJsonFile(this.fileContent);
    } else if(this.fileName.includes(".csv")) {
      this.processingCsvFile(this.fileContent);
    }
  }

  public downloadFile(content: string, fileName: string, contentType: string) {
    const blob = new Blob([content], { type: contentType });
    const url = window.URL.createObjectURL(blob);

    const anchor = document.createElement('a');
    anchor.href = url;
    anchor.download = fileName;
    document.body.appendChild(anchor);
    anchor.click();

    window.URL.revokeObjectURL(url);
    document.body.removeChild(anchor);
  }

  changeAlert(identifier?: String) {
    if(identifier) {
      this.showInfoAlert = !this.showInfoAlert;
    } else {
      this.showInfoAlert = false;
    }
  }

  public getNumberOfRows(message: string) {
    if(message !== undefined) {
      if (message.split('\n').length - 1 < 15) {
        return message.split('\n').length - 1;
      } else {
        return 15;
      }
    } else {
      return 0;
    }
  }

  public getContent(message: string) {
    if(message !== undefined) {
      return message.split('\n\n###################################################').join('');
    }
    return '';
  }

  public filePicked(files: any): void {
    if (files && files.length > 0) {
      let file = files[0];
      let reader = new FileReader();

      reader.readAsText(file);
      reader.onload = () => {
        this.fileContent = reader.result == null ? '' : reader.result.toString();
        this.fileName = file.name;
        this.singleConceptInput = '';
      };
    }
  }

  public processingJsonFile(fileContent: string) {
    this.validationToolService.checkInputFileJson(fileContent).then((response: any) => {
      this.listConcepts = response.codes;
      for (let i = 0; i < this.listConcepts.length; i++) {
        this.listResult.push("...");
        this.listResultContent.push("...");
      }
      this.validateAllCodes();
    })
  }

  public processingCsvFile(fileContent: string) {
    this.validationToolService.checkInputFileCsv(fileContent).then((response: any) => {
      this.listConcepts = response.codes;
      for (let i = 0; i < this.listConcepts.length; i++) {
        this.listResult.push("...");
        this.listResultContent.push("...");
      }
      this.validateAllCodes();
    })
  }


  async validateAllCodes() {
    this.listResult = new Array();
    this.listResultContent = new Array();
    let promises = this.listConcepts.map((concept: { code: string; }, index: string | number) =>
      this.validationToolService.validateSingleCode(concept.code).then((response: any) => {
        if (JSON.stringify(response.content).length > 0 && !JSON.stringify(response.content).includes("ID-pre-coordinated")) {
          if (response.content !== undefined) {
            let content = response.content.split("\\n").join("\n").split("\\t").join("\t").split("\"E").join("E").split("#\"").join("#");
            if (content === '-') {
              this.listResult[index] = 'correct';
            } else {
              this.listResult[index] = 'error';
            }
            this.listResultContent[index] = content;
          }
        }
      }).catch((error) => {
        console.error(`Error validating code at index ${index}:`, error);
        this.listResult[index] = 'error';
        this.listResultContent[index] = 'Validation failed';
      })
    );
    await Promise.all(promises);
    this.showValidateAlert = false;
    this.finishValidation = true;
  }

  public showError(idx: number) {
    this.idx = idx;
  }

  public isDisabledDownload() {
    return ((this.fileName.length == 0 || this.singleConceptInput.length == 0 || this.listConcepts.length == 0) && !this.finishValidation);
  }

  // 1155942004 |Allergy to metal and/or metal compound|:
  // {719722006 |Has realization| = 472964009 |Allergic process|,
  // 472964009 |Allergic process| = 256526003 |Cobalt-chromium alloy|}

  public downloadErrorMessages() {
    this.showDownloadAlert = true;
    let fileName = 'error-message-wasp.txt';
    let contentType = 'text/plain';
    let content = '';
    if(this.idStart === this.ID_TEXT_FIELD) {
      content = this.content;
    } else {
      for (let i = 0; i < this.listResultContent.length; i++) {
        if(this.listResultContent[i] !== '-') {
          content += this.listResultContent[i] + "\n\n";
        }
      }
    }
    this.downloadFile(content, fileName, contentType);
    setTimeout(() => {
      this.showDownloadAlert = false;
    }, 3000);
  }

  public fileError() {
    let identifierQuestionnaire = "\"resourceType\": \"Questionnaire\"";
    let identifierValueSet = "\"resourceType\": \"ValueSet\"";
    if((!this.content.split(identifierQuestionnaire) && !this.content.split(identifierValueSet)) || !this.fileName.includes(".csv")) {
      return "invalidFile";
    } else {
      return "noPCE";
    }
  }

}

