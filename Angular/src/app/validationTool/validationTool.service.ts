import {Injectable} from '@angular/core';
import {lastValueFrom} from "rxjs";
import {BackendService} from "../backendService/backend.service";
import {SettingsService} from "../settings/settings.service";
import {AppService} from "../app.service";

@Injectable({
  providedIn: 'root'
})
export class ValidationToolService {

  public start = '';
  public attributeDefinitionCMCode: any[] = [];
  public attributeDefinitionCMName: any[] = [];
  public attributeDefinitionCMDefinition: any[] = [];
  public attributeDefinitionCMRangeConstraint: any[] = [];

  constructor(public backendService: BackendService, public settingsService: SettingsService, public appService: AppService) {
  }

  clickValidationTool(id: string) {
    this.start = id;
  }

  validateSingleCode(concept: string) {
    const url = this.backendService.getBackendUrl() + 'validate-single-concept';
    let body = {concept: concept, url: this.settingsService.serverChange, version: this.settingsService.sctVersion, conceptModelAttributes: {conceptModelCodes: this.attributeDefinitionCMCode, conceptModelNames: this.attributeDefinitionCMName, conceptModelDefinition: this.attributeDefinitionCMDefinition, conceptModelConstraint: this.attributeDefinitionCMRangeConstraint}, mrcm: this.appService.selectedConceptModel}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  checkInputFileJson(content: string) {
    const url = this.backendService.getBackendUrl() + 'get-pce-json';
    let body = {content: content, url: this.settingsService.serverChange, version: this.settingsService.sctVersion};
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  checkInputFileCsv(content: string) {
    const url = this.backendService.getBackendUrl() + 'get-pce-csv';
    let body = {content: content, url: this.settingsService.serverChange, version: this.settingsService.sctVersion};
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

}
