import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class HighlightingTemplateService {

  constructor(public backendService: BackendService,
              private settingsService: SettingsService,
              private settingsCsSupplementService: SettingsCsSupplementService,
              private settingsTemplateService: SettingsTemplatesService,
              private appService: AppService,
              private templateGenerationService: TemplatesGenerationService,
              private pceWithoutTemplateService: PceWithoutTemplateService) {
  }

  public focusConceptCode = '';
  public focusConceptName = '';
  public listAttributeWithoutMinCar = new Array();
  public listAttributeWithoutMaxCar = new Array();
  public listAttributeWithoutCode = new Array();
  public listAttributeWithoutName = new Array();
  public listAttributeWithoutConstraintComplete = new Array();
  public listAttributeWithoutConstraint = new Array();

  public listAttributeWithMinCar = new Array();
  public listAttributeWithMaxCar = new Array();
  public listAttributeWithCode = new Array();
  public listAttributeWithName = new Array();
  public listAttributeWithConstraintComplete = new Array();
  public listAttributeWithConstraint = new Array();
  public listRoleGroupsMinCar = new Array();
  public listRoleGroupsMaxCar = new Array();
  public attributeValueWith = new Array();

  maxCar(value: string) {
    if (value.toString() === 'Infinity') {
      return "*";
    } else {
      return value;
    }
  }

  value(value: string) {
    if (value !== undefined) {
      if (value.toString().includes("+int")) {
        value = value.split("[[+int(").join("").split("..)").join("..").split(" ").join("");
      } else if (value.toString().includes("+dec")) {
        value = value.split("[[+dec(").join("").split("..)").join("..").split(" ").join("");
      }
      if (value.includes("@")) {
        value = value.substring(0, value.indexOf("@")).split("|)").join("|");
      }
      return value.split("|)").join("|");
    }
    return '';
  }

  isIntDecNormalWithout(i: number) {
    let s = this.listAttributeWithoutConstraintComplete[i];
    if (s !== undefined) {
      if (s.includes("+int")) {
        return 'int';
      } else if (s.includes("+dec")) {
        return 'dec';
      } else {
        return 'normal';
      }
    }
    return '';
  }

  isIntDecNormal(code: string, row: number, col: number) {
    let number = 0;
    if (this.attributeValueWith.length > 0) {
      for (let i = 0; i < row; i++) {
        number = number + this.attributeValueWith[i].length;
      }
    }
    number = number + col;
    if (this.listAttributeWithConstraintComplete[row] !== undefined) {
      let s = this.listAttributeWithConstraintComplete[row][number];
      if (s !== undefined) {
        if (s.includes("+int")) {
          return 'int';
        } else if (s.includes("+dec")) {
          return 'dec'
        } else {
          return 'normal'
        }
      }
    }
    return '';
  }

  popupInformationConstraintWithout(i: number) {
    let s = this.listAttributeWithoutConstraintComplete[i];
    if (s !== undefined) {
      if (s.includes("+int")) {
        s = s.split(" ").join("").split("[[+int").join("").split(")").join("").split("(").join("");
      } else if (s.includes("+dec")) {
        s = s.split(" ").join("").split("[[+dec").join("").split(")").join("").split("(").join("");
      }
      if (s.includes("@")) {
        s = " " + s.substring(s.indexOf("@") - 1).split(".").join("");
        return s;
      }
      return "";
    }

  }

  popupInformationConstraint(code: string, row: number, col: number, b: boolean) {
    let number = 0;
    if (this.attributeValueWith.length > 0) {
      for (let i = 0; i < row; i++) {
        number = number + this.attributeValueWith[i].length;
      }
    }
    number = number + col;
    if (this.listAttributeWithConstraintComplete[row] !== undefined) {
      let s = this.listAttributeWithConstraintComplete[row][number];
      if (s !== undefined) {
        if (s.includes("+int")) {
          s = s.split(" ").join("").split("[[+int").join("").split(")").join("").split("(").join("");
        } else if (s.includes("+dec")) {
          s = s.split(" ").join("").split("[[+dec").join("").split(")").join("").split("(").join("");
        }

        if (s.substring(0, 1) === ' ') {
          s = s.substring(1);
        }

        if (!b) {
          if (s.includes("@")) {
            return s.substring(s.indexOf("@")).split("[[+int(").join("").split("[[+dec(").join("");
          }
        } else {
          if (s.includes("@")) {
            s = s.substring(0, s.indexOf("@")).split("|)").join("|");
          }
          return s.split("|)").join("|").split("| ").join("|").split(" <").join("<");
        }
      }
    }
    return "";
  }

  reset() {
    this.focusConceptCode = '';
    this.focusConceptName = '';
    this.listAttributeWithoutMinCar = new Array();
    this.listAttributeWithoutMaxCar = new Array();
    this.listAttributeWithoutCode = new Array();
    this.listAttributeWithoutName = new Array();
    this.listAttributeWithoutConstraintComplete = new Array();
    this.listAttributeWithoutConstraint = new Array();
    this.listAttributeWithMinCar = new Array();
    this.listAttributeWithMaxCar = new Array();
    this.listAttributeWithCode = new Array();
    this.listAttributeWithName = new Array();
    this.listAttributeWithConstraintComplete = new Array();
    this.listAttributeWithConstraint = new Array();
    this.listRoleGroupsMinCar = new Array();
    this.listRoleGroupsMaxCar = new Array();
    this.attributeValueWith = new Array();
  }
}
