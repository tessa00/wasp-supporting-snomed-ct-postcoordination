import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {HighlightingTemplateService} from "./highlightingTemplate.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";

@Component({
  selector: 'app-highlightingTemplate',
  templateUrl: './highlightingTemplate.component.html',
  styleUrls: ['./highlightingTemplate.component.css']
})

export class HighlightingTemplateComponent {

  constructor(public highlightingTemplateService: HighlightingTemplateService, private http: HttpClient, public appService: AppService, public templateGenerationService: TemplatesGenerationService, public settingsService: SettingsService, public settingsCsService: SettingsCsSupplementService, public settingsTemplateService: SettingsTemplatesService, public pceWithoutTemplateService: PceWithoutTemplateService) {
  }

}

