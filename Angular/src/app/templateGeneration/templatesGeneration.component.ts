import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {TemplatesGenerationService} from "./templatesGeneration.service";
import {AppService} from "../app.service";
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {HighlightingTemplateService} from "../highlightingTemplate/highlightingTemplate.service";
import {DiagramComponent} from "../ui/diagram.component";
import {TemplatesService} from "../templates/templates.service";
import {Observable} from "rxjs";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-template_generation',
  templateUrl: './templatesGeneration.component.html',
  styleUrls: ['./templatesGeneration.component.css']
})

export class TemplatesGenerationComponent {

  constructor(private  templateService: TemplatesService, private templateGenerationService: TemplatesGenerationService, private http: HttpClient, public dia: DiagramComponent, public highlightingTemplateService: HighlightingTemplateService, private appService: AppService, private  settingsService: SettingsService, private  settingsCsService: SettingsCsSupplementService) {
  }

  // general
  public conceptModelReferencedComponentId = '';
  public conceptModelParentDomain = '';
  public conceptModelProximalPrimitiveConstraint = '';
  public conceptModelProximalPrimitiveRefinement = '';
  public conceptModelDomainConstraint = '';
  public conceptModelURL = '';
  public conceptModelTemplate = '';
  public conceptModelFocusConceptTemplate = '';

  public attributeDefinitionCMCode = new Array();
  public attributeDefinitionCMName = new Array();
  public attributeDefinitionCMDefinition = new Array();
  public attributeDefinitionCMRangeConstraint = new Array();
  public listAttributeRelation = new Array();

  // without role group
  public listWithoutMinCar = new Array();
  public listWithoutMaxCar = new Array();
  public listWithoutConstraint = new Array();
  public listWithoutAttributeCode = new Array();
  public listWithoutAttributeName = new Array();

  // with role group
  public listInitWithMinCar = new Array();
  public listInitWithMaxCar = new Array();
  public listInitWithRoleGroupMinCar = new Array();
  public listInitWithRoleGroupMaxCar = new Array();
  public listInitWithConstraint = new Array();
  public listInitWithAttributeCode = new Array();
  public listInitWithAttributeName = new Array();

  // left table without role group
  public listNumberAttributesWithout = new Array();
  public showGenerationTemplate = false;
  public actualAttributeNameWithout = new Array();
  public actualMinCarWithout = new Array();
  public actualMaxCarWithout = new Array();
  public actualValueRangeWithout = new Array();
  public listNameRangeWithout = new Array();
  public indexWithout = -1;
  public actualCheckCarWithout = true;
  public idRowLeftTableWithout = -1;
  public listLoadDataAttributeWithout = new Array();

  // Focusconcept
  public focusConceptName = '';
  public focusConceptCode = '';
  public listAllNamesFocusConcept = new Array();
  public listAllCodesFocusConcept = new Array();
  public booleanBtnFcDisabled = false;


  // range table without role group
  public listOperatorRangeWithout = new Array();
  public listConstraintRangeWithout = new Array();
  public listSummaryNameRangeWithout = new Array();
  public listSummaryCodeRangeWithout = new Array();
  public listSummaryOperatorRangeWithout = new Array();
  public listSummaryConstraintRangeWithout = new Array();
  public listSummaryAttributeCodesWithout = new Array();
  public listSummaryAttributeNamesWithout = new Array();
  public listSummaryMinCardinalityWithout = new Array();
  public listSummaryMaxCardinalityWithout = new Array();
  public listAllCodesRangeWithout = new Array();
  public listAllNamesRangeWithout = new Array();
  public listIndexWithout = new Array();
  public booleanIntWithout = false;
  public booleanDecWithout = false;
  public booleanNormalWithout = false;
  public intDefMinWithout = -1;
  public intDefMaxWithout = -1;
  public decDefMinWithout = -1;
  public decDefMaxWithout = -1;
  public listIntValueWithout = new Array();
  public listIntMaxWithout = new Array();
  public listIntMinWithout = new Array();
  public listDecValueWithout = new Array();
  public listDecMaxWithout = new Array();
  public listDecMinWithout = new Array();
  public listIntFixedValueWithout = new Array();
  public listIntFixedValueWith = new Array();
  public listIntFixedWithout = new Array();
  public inputIdDecWithout = '';
  public decMinWithout = '';
  public decMaxWithout = '';
  public decFixedWithout = '';
  public listChangedConstraintFlagWithout = new Array();
  public listChangedConstraintWithout = new Array();
  public listDecFixedValueWithout = new Array();
  public listDecFixedWithout = new Array();
  public inputIdIntWithout = '';
  public intMinWithout = '';
  public intMaxWithout = '';
  public intFixedWithout = '';
  public idRowTableRightWithout = -1;
  public correctInputWithout = false;
  public invalidInputCarWithout = false;
  public listChangeCarWithout = new Array();
  public showInformationWindowWithout = false;
  public isWithout = false;

  public booleanClickKeypressWithout = true;
  public oldInputWithout = '';
  public listSummaryValueRangeWithout = new Array();
  public invalidInputMinCarWithout = false;
  public invalidInputMaxCarWithout = false;

  // left table with role group
  public listNumberAttributesWith = new Array();
  public idRowLeftTableWith = -1;
  public listIndexSubRg = new Array();
  public idRg = -1;
  public idSubRg = -1;
  public listOperatorRangeWith = new Array();
  public listConstraintRangeWith = new Array();
  public listNameRangeWith = new Array();
  public actualAttributeNameWith = new Array();
  public showInformationWindowWith = false;
  public indexWith = -1;
  public listLoadDataAttributeWith = new Array();
  public listIndexWith = new Array();
  public actualMinCarWith = new Array();
  public actualMaxCarWith = new Array();
  public actualValueRangeWith = new Array();
  public invalidInputMinCarWith = false;
  public invalidInputMaxCarWith = false;
  public actualCheckCarWith = true;
  public listSummaryValueRangeWith = new Array();
  public listAllCodesRangeWith = new Array();
  public listAllNamesRangeWith = new Array();
  public listSummaryNameRangeWith = new Array();
  public listSummaryCodeRangeWith = new Array();
  public listSummaryMinCardinalityWith = new Array();
  public listSummaryMaxCardinalityWith = new Array();
  public listSummaryAttributeCodesWith = new Array();
  public listSummaryAttributeNamesWith = new Array();
  public listSummaryOperatorRangeWith = new Array();
  public listSummaryConstraintRangeWith = new Array();
  public selfGroupedAttributeCode = new Array();
  public selfGroupedAttributeName = new Array();
  public listChangedConstraintWith = new Array();
  public listChangedAttributeCodeWith = new Array();
  public listChangedAttributeNameWith = new Array();
  public listFlagAttributeRelation = new Array();
  public listChangedWith = new Array();
  public listDecFixedWith = new Array();
  public listDecFixedValueWith = new Array();
  public booleanIntWith = false;
  public booleanDecWith = false;
  public booleanNormalWith = false;
  public intDefMinWith = -1;
  public intDefMaxWith = -1;
  public decDefMinWith = -1;
  public decDefMaxWith = -1;
  public intMaxWith = '';
  public intMinWith = '';
  public intFixedWith = '';
  public decMaxWith = '';
  public decMinWith = '';
  public decFixedWith = '';
  public listIntFixedWith = new Array();
  public isWith = false;
  public listCodeRangeWith = new Array();
  public invalidInputCarWith = false;
  public listInfoDefinitionWith = new Array();
  public listInfoConstraintWith = new Array();
  public correctInputWith = false;
  public idRowTableRightWith = -1;
  public inputIdIntWith = '';
  public inputIdDecWith = '';
  public listChangedFlagWith = new Array();
  public minCarRg = new Array();
  public maxCarRg = new Array();
  public invalidRgCar = false;
  public isUpdate = false;
  public oldInputWith = [];
  public listChangeCarWith = new Array();

  public termTemplate = '';
  public listCodeWordsWith = new Array();
  public listCodeWordsPositionWith = new Array();
  public listCodeWordsWithout = new Array();
  public listCodeWordsPositionWithout = new Array();
  public resultTemplate = '';
  public nameTemplate = '';
  public isTemplatedCreated = false;
  public errorMessageSummary = '';
  public listSelfGroupedAttributesPceCode = new Array();
  public listSelfGroupedAttributesPceName = new Array();
  public listSelfGroupedBoolean = new Array();

  public idLastTab = '';
  public idActualTab = '';


  ngOnInit(): void {
  }


  keypressFocusConcept() {
    if (this.focusConceptName.length > 2) {
      // if (!Number(this.focusConceptName)) {

      let elementInput = document.getElementById('spinnerFc');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }

      this.templateGenerationService.getFocusConceptAttributeValueRequestFilterLimitFSN(this.focusConceptName, '1000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          this.listAllNamesFocusConcept = new Array();
          this.listAllCodesFocusConcept = new Array();
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesFocusConcept.includes(value[i].code)) {
              this.listAllCodesFocusConcept.push(value[i].code);
              this.listAllNamesFocusConcept.push(value[i].fsn);
              if(Number.parseInt(this.focusConceptName)) {
                this.focusConceptName = value[i].fsn;
              }
            }
          }
        }
        setTimeout(() => {
          let elementInput = document.getElementById('spinnerFc');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }, 1000);
      })
    } else {
      this.listAllCodesFocusConcept = new Array();
      this.listAllNamesFocusConcept = new Array();
    }
    // }
  }

  checkBtnFcDisabled() {
    return !this.listAllNamesFocusConcept.includes(this.focusConceptName);
  }

  start() {
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = 'inline';
    }

    let elementDummy = document.getElementById('input-focusConcept-dummy');
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = 'none';
    }
    this.focusConceptName = '';
  }

  getX() {
    return this.listAllNamesFocusConcept;
  }

  changeInputField() {
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
    }

    let elementDummy = document.getElementById('input-focusConcept-dummy');
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';
    }

    setTimeout(() => {
      let elementInput = document.getElementById('spinnerFc');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'none';
      }
    }, 10);
  }


  getConceptModel() {
    this.booleanBtnFcDisabled = true;
    let elementInput = document.getElementById('input-focusConcept-dummy');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }

    let elementOk = document.getElementById('btn-ok-focusConcept');
    if (elementOk != null) {
      // @ts-ignore
      elementOk.disabled = true;
    }

    let pos = -1;
    for (let i = 0; i < this.listAllNamesFocusConcept.length; i++) {
      if (this.focusConceptName === this.listAllNamesFocusConcept[i]) {
        pos = i;
        break;
      }
    }

    this.focusConceptCode = this.listAllCodesFocusConcept[pos];
    this.focusConceptSemanticTag = '(' + this.focusConceptName.split("(")[1].split("/").join("-");
    this.preprocessingConceptModel();


  }

  public focusConceptSemanticTag = '';

  preprocessingConceptModel() {
    this.templateGenerationService.getPreprocessingData(this.focusConceptCode).then((response: any) => {
      let value = response.values;
      this.conceptModelReferencedComponentId = value[0].conceptModelReferencedComponentId;
      this.conceptModelDomainConstraint = value[1].conceptModelDomainConstraint;
      this.conceptModelParentDomain = value[2].conceptModelParentDomain;
      this.conceptModelProximalPrimitiveConstraint = value[3].conceptModelProximalPrimitiveConstraint;
      this.conceptModelProximalPrimitiveRefinement = value[4].conceptModelProximalPrimitiveRefinement;
      this.conceptModelURL = value[5].conceptModelURL;
      this.conceptModelTemplate = value[6].conceptModelTemplate;
      this.conceptModelFocusConceptTemplate = value[7].conceptModelFocusConceptTemplate;

      if(value[35].listInitWithRoleGroupMinCar.length > 0) {
        this.preprocessingWith(value);
        this.isWith = true;
      }

      if(value[10].listWithoutAttributeCode.length > 0) {
        this.preprocessingWithout(value);
        this.isWithout = true;
      }

      this.showGenerationTemplate = true;
    })


  }

  preprocessingWithout(value: any) {
    this.listWithoutMinCar = value[8].listWithoutMinCar;
    for(let i = 0; i < this.listWithoutMinCar.length; i++) {
      this.listWithoutMinCar[i] = Number.parseInt(this.listWithoutMinCar[i]);
    }
    this.listWithoutMaxCar = value[9].listWithoutMaxCar;

    for(let i = 0; i < this.listWithoutMaxCar.length; i++) {
      if(this.listWithoutMaxCar[i] === 'Infinity') {
        this.listWithoutMaxCar[i] = Number.POSITIVE_INFINITY;
      } else {
        this.listWithoutMaxCar[i] = Number.parseInt(this.listWithoutMaxCar[i]);
      }

    }
    this.listWithoutAttributeCode = value[10].listWithoutAttributeCode;
    this.listWithoutAttributeName = value[11].listWithoutAttributeName;
    this.listWithoutConstraint = value[12].listWithoutConstraint;
    this.listChangedConstraintWithout = value[13].listChangedConstraintWithout;
    this.listChangedConstraintFlagWithout = value[14].listChangedConstraintFlagWithout;
    this.listAllNamesRangeWithout = value[15].listAllNamesRangeWithout;
    this.listAllCodesRangeWithout = value[16].listAllCodesRangeWithout;
    this.listIntValueWithout = value[26].listIntValueWithout;
    this.listIntFixedWithout = value[27].listIntFixedWithout;
    this.listDecValueWithout = value[28].listDecValueWithout;
    this.listDecFixedWithout = value[29].listDecFixedWithout;
    this.listOperatorRangeWithout = value[30].listOperatorRangeWithout;
    this.listConstraintRangeWithout = value[31].listConstraintRangeWithout;
    this.listNameRangeWithout = value[32].listNameRangeWithout;
    this.listNumberAttributesWithout = value[33].listNumberAttributesWithout;
    this.listChangeCarWithout = value[34].listChangeCarWithout;

    this.loadAttributeDefinitionConceptModel();
    this.loadWithout();
    this.checkMandatoryElementWithout();
    setTimeout(() => {
      this.setAttributeRelationsWithout();
    }, 1000);


    this.templateGenerationService.testSubsumption(this.focusConceptCode).then((response: any) => {
      this.isProduct = response.result;
    })

    this.showInformationWindowWithout = true;

    setTimeout(() => {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWithout0') {
              element.style.background = 'lightblue';
            }
          }
        }
      }
    }, 10);


  }

  public isProduct = '';

  preprocessingWith(value: any) {
    this.selfGroupedAttributeCode.push('288556008');    // before
    this.selfGroupedAttributeCode.push('371881003');    // during
    this.selfGroupedAttributeCode.push('255234002');    // after
    this.selfGroupedAttributeCode.push('42752001');     // due to
    this.selfGroupedAttributeCode.push('263502005');    // clinical course
    this.selfGroupedAttributeCode.push('726633004');    // temporally related
    this.selfGroupedAttributeCode.push('47429007');     // associated with

    this.selfGroupedAttributeName.push('Before')
    this.selfGroupedAttributeName.push('During')
    this.selfGroupedAttributeName.push('After')
    this.selfGroupedAttributeName.push('Due to')
    this.selfGroupedAttributeName.push('Clinical course')
    this.selfGroupedAttributeName.push('Temporally related to')
    this.selfGroupedAttributeName.push('Associated with')

    this.listInitWithRoleGroupMinCar = value[35].listInitWithRoleGroupMinCar;
    this.listInitWithRoleGroupMaxCar = value[36].listInitWithRoleGroupMaxCar;
    this.listInitWithMinCar = value[37].listInitWithMinCar;
    this.listInitWithMaxCar = value[38].listInitWithMaxCar;
    this.listInitWithAttributeCode = value[39].listInitWithAttributeCode;
    this.listInitWithAttributeName = value[40].listInitWithAttributeName;
    this.listInitWithConstraint = value[41].listInitWithConstraint;
    this.listChangedConstraintWith = value[42].listChangedConstraintWith;
    this.listChangedAttributeCodeWith = value[43].listChangedAttributeCodeWith;
    this.listChangedAttributeNameWith = value[44].listChangedAttributeNameWith;
    this.listChangedFlagWith = value[45].listChangedFlagWith;

    for(let i = 0; i < this.listInitWithRoleGroupMaxCar.length; i++) {
      this.listNumberAttributesWith.push(new Array());
      this.listNumberAttributesWith[i].push(new Array());
      this.listIndexWith.push(new Array());
      this.listIndexWith[i].push(new Array());
      this.listIndexSubRg.push(new Array());
      this.listIndexSubRg[i].push(0);
      this.actualAttributeNameWith.push(new Array());
      this.actualAttributeNameWith[i].push(new Array());
      this.actualAttributeNameWith[i][0].push('');
      this.listSummaryAttributeCodesWith.push(new Array());
      this.listSummaryAttributeCodesWith[i].push(new Array());
      this.listSummaryAttributeNamesWith.push(new Array());
      this.listSummaryAttributeNamesWith[i].push(new Array());
      this.listSummaryOperatorRangeWith.push(new Array());
      this.listSummaryOperatorRangeWith[i].push(new Array());
      this.listSummaryConstraintRangeWith.push(new Array());
      this.listSummaryConstraintRangeWith[i].push(new Array());
      this.listSummaryCodeRangeWith.push(new Array());
      this.listSummaryCodeRangeWith[i].push(new Array());
      this.listSummaryNameRangeWith.push(new Array());
      this.listSummaryNameRangeWith[i].push(new Array());
      this.listSummaryValueRangeWith.push(new Array());
      this.listSummaryValueRangeWith[i].push(new Array());
      this.listOperatorRangeWith.push(new Array());
      this.listOperatorRangeWith[i].push(new Array(new(Array)));
      this.listOperatorRangeWith[i][0][0].push('');
      this.listConstraintRangeWith.push(new Array());
      this.listConstraintRangeWith[i].push(new Array(new(Array)));
      this.listConstraintRangeWith[i][0][0].push('');
      this.listNameRangeWith.push(new Array());
      this.listNameRangeWith[i].push(new Array(new(Array)));
      this.listNameRangeWith[i][0][0].push('');
      this.listCodeRangeWith.push(new Array());
      this.listCodeRangeWith[i].push(new Array(new(Array)));
      this.listCodeRangeWith[i][0][0].push('');
      this.listChangedWith.push(new Array());
      this.listChangedWith[i].push(new Array());

      this.listAllNamesRangeWith.push(new Array());
      this.listAllNamesRangeWith[i].push(new Array());
      this.listAllNamesRangeWith[i][0].push(new Array());

      this.listAllCodesRangeWith.push(new Array());
      this.listAllCodesRangeWith[i].push(new Array());
      this.listAllCodesRangeWith[i][0].push(new Array());

      this.listChangeCarWith.push(new Array());
      this.listChangeCarWith[i].push(new Array());
      this.listChangeCarWith[i][0].push(false);
      this.actualMinCarWith.push(new Array());
      this.actualMinCarWith[i].push(new Array());
      this.actualMinCarWith[i][0].push('');
      this.actualMaxCarWith.push(new Array());
      this.actualMaxCarWith[i].push(new Array());
      this.actualMaxCarWith[i][0].push('');

      this.actualValueRangeWith.push(new Array());
      this.actualValueRangeWith[i].push(new Array());
      this.actualValueRangeWith[i][0].push('');
      this.listSummaryMaxCardinalityWith.push(new Array());
      this.listSummaryMaxCardinalityWith[i].push(new Array());
      this.listSummaryMinCardinalityWith.push(new Array());
      this.listSummaryMinCardinalityWith[i].push(new Array());

      this.minCarRg.push(new Array());
      this.maxCarRg.push(new Array());
      this.minCarRg[i].push(this.listInitWithRoleGroupMinCar[i]);
      let max = this.listInitWithRoleGroupMaxCar[i];
      if(this.listInitWithRoleGroupMaxCar[i].toString() === 'Infinity') {
        max = '*'
      }
      this.maxCarRg[i].push(max);


      this.listSelfGroupedAttributesPceCode.push(new Array());
      this.listSelfGroupedAttributesPceCode[i].push(new Array());
      this.listSelfGroupedAttributesPceName.push(new Array());
      this.listSelfGroupedAttributesPceName[i].push(new Array());
      this.listSelfGroupedBoolean.push(new Array());
      this.listSelfGroupedBoolean[i].push(new Array());


    }
    this.idSubRg = 0;

    this.loadAttributeDefinitionConceptModel();
    this.loadWith(0);


    setTimeout(() => {
      this.setAttributeRelationsWith();
    }, 1000);

    setTimeout(() => {
      this.checkMandatoryElementWith(0);
      setTimeout(() => {
        this.idSubRg = this.actualAttributeNameWith[0].length - 1;
        this.setSubRg(0);
      }, 10);
    }, 1000);



    this.showInformationWindowWith = true;

    setTimeout(() => {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
              element.style.background = 'lightblue';
            }
          }
        }
      }
    }, 10);


  }




  checkMandatoryElementWithout() {
    let idx = 0;
    for(let i = 0; i < this.listWithoutMinCar.length; i++) {
      if (this.listWithoutMinCar[i].toString() === '1' && this.listChangedConstraintFlagWithout[i].toString() === '0') {
        this.actualAttributeNameWithout[idx] = this.listWithoutAttributeName[i];
        this.idRowLeftTableWithout = idx;
        this.loadAttributeDataWithout(idx);
        setTimeout(() => {
          let element = document.getElementById('attributeNameWithoutDataList' + this.idRowLeftTableWithout);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }, 10);
        idx = idx + 1;
      }
    }

  }

  checkMandatoryElementWith(subRg : number) {
    for(let i = 0; i < this.listInitWithMinCar.length; i++) {
      let idx = 0;
      if(this.actualAttributeNameWith[i][subRg] !== undefined) {
        idx = this.actualAttributeNameWith[i][subRg].length - 1;
      }
      for(let j = 0; j < this.listInitWithMinCar[i].length; j++) {
        if (this.listInitWithMinCar[i][j].toString() === '1' && this.listChangedFlagWith[i][j] === '0' && !this.listSummaryAttributeCodesWith[i][subRg].toString().includes(this.listInitWithAttributeCode[i][j])) {        // todo -- hier das eingefügt, dass nur bei 1. subRg die AR angezeigt werden
          this.idRowLeftTableWith = idx;
          this.idRg = i;
          this.listNumberAttributesWith[i][subRg].push(idx);
          this.listCodeRangeWith[i][subRg][idx] = new Array(this.listInitWithAttributeCode[i][j]);
          this.loadAttributeDataWith(idx);
          this.actualAttributeNameWith[i][subRg][idx] = this.listInitWithAttributeName[i][j];
          this.getRowTableLeftWith(idx);
          if(this.listChangedWith[i][subRg] !== undefined) {
            this.listChangedWith[i][subRg].push(false);
          }

          idx = idx + 1;
        }
      }
    }


  }

  loadAttributeDefinitionConceptModel() {
    const file = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "conceptModelObjectAttributes.csv");
    const fileContent = file[0].fileContent;
    const arr = fileContent.split("\n");
    for (let i = 0; i < arr.length; i++) {
      let value = arr[i].split(";")
      this.attributeDefinitionCMCode.push(value[0]);
      this.attributeDefinitionCMName.push(value[1]);
      this.attributeDefinitionCMDefinition.push(value[2]);
      this.attributeDefinitionCMRangeConstraint.push(value[3]);
    }
  }



  preprocessingWithRoleGroup(withRoleGroup: any) {
    this.selfGroupedAttributeCode.push('288556008');    // before
    this.selfGroupedAttributeCode.push('371881003');    // during
    this.selfGroupedAttributeCode.push('255234002');    // after
    this.selfGroupedAttributeCode.push('42752001');     // due to
    this.selfGroupedAttributeCode.push('263502005');    // clinical course
    this.selfGroupedAttributeCode.push('726633004');    // temporally related
    this.selfGroupedAttributeCode.push('47429007');     // associated with

    this.selfGroupedAttributeName.push('Before')
    this.selfGroupedAttributeName.push('During')
    this.selfGroupedAttributeName.push('After')
    this.selfGroupedAttributeName.push('Due to')
    this.selfGroupedAttributeName.push('Clinical course')
    this.selfGroupedAttributeName.push('Temporally related to')
    this.selfGroupedAttributeName.push('Associated with')

    for (let i = 0; i < withRoleGroup.length; i++) {
      this.listInitWithRoleGroupMinCar.push(Number.parseInt(withRoleGroup[i].roleGroupCardinalityMin));
      if (withRoleGroup[i].roleGroupCardinalityMax === '*') {
        this.listInitWithRoleGroupMaxCar.push(Number.POSITIVE_INFINITY);
      } else {
        this.listInitWithRoleGroupMaxCar.push(withRoleGroup[i].roleGroupCardinalityMax);
      }
      let listConstraint = new Array();
      let listMinCar = new Array();
      let listMaxCar = new Array();
      let listCode = new Array();
      let listName = new Array();
      let listFlag = new Array();
      let listChanged = new Array();

      this.listSummaryNameRangeWith.push(new Array());
      this.listSummaryCodeRangeWith.push(new Array());
      this.listSummaryAttributeCodesWith.push(new Array());
      this.listSummaryAttributeNamesWith.push(new Array());
      this.listSummaryOperatorRangeWith.push(new Array());
      this.listSummaryConstraintRangeWith.push(new Array());
      this.listSummaryMinCardinalityWith.push(new Array());
      this.listSummaryMaxCardinalityWith.push(new Array());
      this.minCarRg.push('');
      this.maxCarRg.push('');
      this.listIndexSubRg.push(new Array());
      this.listChangeCarWith.push(new Array())

      let idx = 0;
      for (let j = 0; j < withRoleGroup[i].attribute.length; j++) {
        let code = '';
        this.templateGenerationService.getAttributeValueRequest( this.focusConceptCode + "." + withRoleGroup[i].attribute[j].attributeNameCode).then((response: any) => {
          const expansion = response.expansion;
          if(typeof expansion !== 'undefined') {
            const contains = expansion.contains;
            if(typeof contains !== 'undefined') {
              for (let i = 0; i < contains.length; i++) {
                code = contains[i].code;
                this.listAttributeRelation.push(withRoleGroup[i].attribute[j].attributeNameCode);
                break;
              }

            }
          }

          this.templateGenerationService.lookUpFsn(code).then((response: any) => {
            if (code !== '') {
              listConstraint.push("<<" + code);
              listChanged.push("<<" + code + " |" + response.name + "|");
              listFlag.push(1);
            } else {
              listConstraint.push(withRoleGroup[i].attribute[j].constraint);
              listChanged.push("");
              listFlag.push(0);
            }
            listMinCar.push(Number.parseInt(withRoleGroup[i].attribute[j].attributeCardinalityMin));
            if (withRoleGroup[i].attribute[j].attributeCardinalityMax === '*') {
              listMaxCar.push(Number.POSITIVE_INFINITY);
            } else {
              listMaxCar.push(withRoleGroup[i].attribute[j].attributeCardinalityMax);
            }
            listCode.push(withRoleGroup[i].attribute[j].attributeNameCode);
            listName.push(withRoleGroup[i].attribute[j].attributeNameDisplay);

            this.listSummaryNameRangeWith[i].push(new Array());
            this.listSummaryCodeRangeWith[i].push(new Array());
            this.listSummaryAttributeCodesWith[i].push(new Array());
            this.listSummaryAttributeNamesWith[i].push(new Array());
            this.listSummaryOperatorRangeWith[i].push(new Array());
            this.listSummaryConstraintRangeWith[i].push(new Array());
            this.listSummaryMinCardinalityWith[i].push(new Array());
            this.listSummaryMaxCardinalityWith[i].push(new Array());
          })


        })
      }

      this.listInitWithConstraint.push(listConstraint);
      this.listInitWithMinCar.push(listMinCar);
      this.listInitWithMaxCar.push(listMaxCar);
      this.listInitWithAttributeCode.push(listCode);
      this.listInitWithAttributeName.push(listName);
      this.listChangedFlagWith.push(listFlag);
      this.listChangedConstraintWith.push(listChanged);
      this.listNumberAttributesWith.push(new Array());
      this.actualAttributeNameWith.push(new Array());
      this.listIndexWith.push(new Array());
      this.actualMinCarWith.push(new Array());
      this.actualMaxCarWith.push(new Array());
      this.actualValueRangeWith.push(new Array());
      this.listSummaryValueRangeWith.push(new Array());
      this.listAllCodesRangeWith.push(new Array());
      this.listAllNamesRangeWith.push(new Array());
      this.listIntFixedWith.push(new Array());
      this.listIntFixedValueWith.push(new Array());
      this.listDecFixedWith.push(new Array());
      this.listDecFixedValueWith.push(new Array());
      this.listInfoDefinitionWith.push(new Array());
      this.listInfoConstraintWith.push(new Array());
    }

    for(let i = 0; i < this.listNumberAttributesWith.length; i++) {
      this.listSummaryValueRangeWith[i].push("");
      this.listIntFixedWith.push("");
      this.listIntFixedValueWith.push("");
      this.listDecFixedWith.push("");
      this.listDecFixedValueWith.push("");
      this.listChangeCarWith.push(false)
    }


  }

  setAttributeRelationsWith() {
    let listFlagRg = new Array();

    this.listNumberAttributesWith[0].push(new Array());
    let max = Math.max(...this.listChangedFlagWith);
    for(let i  = 1; i < max; i++) {
      this.loadNewRoleGroup();
      listFlagRg.push(new Array());
    }

    let idx = 0;
    let value = 0;
    let list = new Array();
    for(let i = 0; i < this.listChangedFlagWith.length; i++) {
      for(let j = 0; j < this.listChangedFlagWith[i].length; j++) {
        // alle flags angucken --- erster, der nicht 0 ist wird auf 1 gesetzt, wenn wieder und sich gemerkt
        if(this.listChangedFlagWith[i][j].toString() !== '0') {
          if(this.listChangedFlagWith[i][j].toString() !== idx.toString() && this.listChangedFlagWith[i][j].toString() !== value) {
            idx = idx + 1;
            value = this.listChangedFlagWith[i][j];
            this.listChangedFlagWith[i][j] = idx.toString();
          } else {
            this.listChangedFlagWith[i][j] = idx.toString();
          }
        }
      }
    }

    for(let i = 0 ; i < this.listChangedFlagWith.length; i++) {
      for(let j = 0 ; j < this.listChangedFlagWith[i].length; j++) {
        if (this.listChangedFlagWith[i][j].toString().split('\"').join('') !== '0') {
          this.idRg = i;
          if (this.idSubRg !== this.listChangedFlagWith[i][j] - 1) {
            this.idSubRg = this.listChangedFlagWith[i][j] - 1;
            this.listIndexWith[this.idRg].push(new Array());
            this.listIndexWith[this.idRg][this.idSubRg].push(j);
            this.idRowLeftTableWith = 0;
            this.loadNewRoleGroup();
            this.getRowTableLeftWith(0);
            this.setSubRg(this.listChangedFlagWith[i][j] - 1);
          } else {
            this.idRowLeftTableWith = this.actualAttributeNameWith[this.idRg][this.idSubRg].length - 1;
          }
          this.addRowTableLeftWith();

          if(this.listInitWithAttributeCode[i][j].length > 0) {
            this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listInitWithAttributeName[i][j];
            this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = j;
            this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].push(this.listInitWithAttributeName[i][j]);
            this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].push(this.listInitWithAttributeCode[i][j]);
            this.listChangedWith[this.idRg][this.idSubRg].push(false);
            this.listInitWithMinCar[this.idRg][j] = 1;
            this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg] = this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].filter((x: any) => x.length !== 0);
          }

          if (this.listInitWithConstraint[i][j].includes("<")) {
            this.booleanDecWith = false;
            this.booleanIntWith = false;
            this.booleanNormalWith = true;

            let concept = this.listChangedConstraintWith[i][j].split("<").join("");
            if((this.isProduct == 'equivalent' || this.isProduct == 'subsumes') && concept.includes("|")) {
              concept = concept.replace("<<", "");
            }
            if(concept.substring(0,1) === '') {
              this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept.substring(1);
            } else {
              this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept;
            }
            this.getRowTableLeftWith(this.idRowLeftTableWith)
          } else if (!this.listInitWithConstraint[i][j].includes("<") && this.listInitWithConstraint[i][j].includes("+dec")) {
            this.decFixedWith = this.listInitWithConstraint[i][j].split("+dec(#").join("").split(")").join("");
            this.updateDecFixedValueWith();
            this.getRowTableLeftWith(this.idRowLeftTableWith);
          } else {
            this.intFixedWith = this.listInitWithConstraint[i][j].split("+int(#").join("").split(")").join("");
            this.updateIntFixedValueWith();
            this.getRowTableLeftWith(this.idRowLeftTableWith);
          }
        }
      }
      this.isWith = true;
      this.listLoadDataAttributeWith[this.idRowLeftTableWith - 1] = true;

      if(this.listWithoutMinCar.length > 0) {
        setTimeout(() => {
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWithout0') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 1000);
      } else {
        setTimeout(() => {
          this.idRg = 0;
          this.idSubRg = 0;
          this.idRowLeftTableWith = 0;
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWith000') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 10);
      }
    }
    for(let i = 0; i < this.listSummaryAttributeCodesWith[this.idRg].length; i++) {
      this.listSummaryAttributeCodesWith[this.idRg][i] = this.listSummaryAttributeCodesWith[this.idRg][i].filter((x: any) => x.length !== 0);
      this.listSummaryAttributeNamesWith[this.idRg][i] = this.listSummaryAttributeNamesWith[this.idRg][i].filter((x: any) => x.length !== 0);
    }
  }

  setAttributeRelationsWithout() {
    let idx = 0;

    for(let i = 0 ; i < this.listChangedConstraintFlagWithout.length; i++) {
      if(this.listChangedConstraintFlagWithout[i] != null) {
        if (this.listChangedConstraintFlagWithout[i].toString() === '1') {
          this.addRowTableLeftWithout();
          this.actualAttributeNameWithout.push(this.listWithoutAttributeName[i]);
          this.idRowLeftTableWithout = idx;
          this.listIndexWithout[this.idRowLeftTableWithout] = i
          this.listSummaryAttributeNamesWithout.push(this.listWithoutAttributeName[i]);
          this.listSummaryAttributeCodesWithout.push(this.listWithoutAttributeCode[i]);
          this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;
          this.actualAttributeNameWithout = this.actualAttributeNameWithout.filter((x: any) => x.length !== 0);
          if(this.listWithoutConstraint[i].includes("<")) {
            this.booleanDecWithout = false;
            this.booleanIntWithout = false;
            this.booleanNormalWithout = true;
            let code = this.listChangedConstraintWithout[i].replace("<<", "").split("|")[0].split(" ").join("");
            let name = this.listChangedConstraintWithout[i].replace("<<", "").split("|")[1];
            this.listSummaryValueRangeWithout.push(this.listChangedConstraintWithout[i]);
            this.listSummaryNameRangeWithout.push(new Array());
            this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].push(name);
            this.listSummaryCodeRangeWithout.push(new Array());
            this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout].push(code);
            this.listSummaryOperatorRangeWithout.push(new Array());
            this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout].push("");
            this.listSummaryConstraintRangeWithout.push(new Array());
            this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout].push("");
            this.fillTableRightWithout('start', 'attribute_relation')
            setTimeout(() => {
              this.clickRowTableWithout(i);
            }, 1000);
          } else if (!this.listWithoutConstraint[i].includes("<") && this.listWithoutConstraint[i].includes(".")){
            this.decFixedWithout = this.listWithoutConstraint[i].split("+dec(#").join("").split("dec(#").join("").split(")").join("");
            this.listSummaryValueRangeWithout.push(this.listWithoutConstraint[i].split("+dec(#").join("").split("dec(#").join("").split(")").join(""));
            this.listSummaryNameRangeWithout.push(new Array());
            this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].push("");
            this.listSummaryCodeRangeWithout.push(new Array());
            this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout].push("");
            this.updateDecFixedValueWithout();
          } else if(this.listWithoutConstraint[i].includes("int(")) {
            this.intFixedWithout = this.listWithoutConstraint[i].split("+int(#").join("").split("int(#").join("").split(")").join("");
            this.listSummaryValueRangeWithout.push(this.listWithoutConstraint[i].split("+int(#").join("").split("int(#").join("").split(")").join(""));
            this.listSummaryNameRangeWithout.push(new Array());
            this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].push("");
            this.listSummaryCodeRangeWithout.push(new Array());
            this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout].push("");
            this.updateIntFixedValueWithout();
          }
          idx = idx + 1;
          this.listFlagAttributeRelation.push(true);
        }
      }
    }
    setTimeout(() => {
      for (let i = this.listSummaryCodeRangeWithout.length; i > -1; i--) {
        this.clickRowTableWithout(i);
      }
    }, 10);
    setTimeout(() => {
      this.clickRowTableWithout(0);
    }, 1000);
  }

  popupInformationDefinitionWithout() {
    let info = '';
    for (let i = 0; i < this.attributeDefinitionCMCode.length; i++) {
      if (this.attributeDefinitionCMCode[i].toString() === this.listSummaryAttributeCodesWithout[this.idRowLeftTableWithout]) {
        info = this.attributeDefinitionCMDefinition[i];
      }
    }
    if(info === '' || info === '-') {
      info = '----';
    }
    return info;
  }

  popupInformationConstraintWithout() {
    let ref = this.conceptModelTemplate.substring(this.conceptModelTemplate.lastIndexOf(":") + 1);
    let attr = ref.split(",");
    for(let i = 0; i < attr.length; i++) {
      if(attr[i].includes(this.listSummaryAttributeCodesWithout[this.idRowLeftTableWithout])) {
        if(this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]] === "1") {
          let value= this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          if((this.isProduct === 'equivalent' || this.isProduct === 'subsumes') && this.isAttributeRelationWith() && value.includes("|")) {
            value = value.replace("<<", "");
          }
          if (value[value.length - 1] !== ")") {
            value = value.substring(0, value.length-1) + ")|";
          }
          if (value.includes('+int')) {
            value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split(".|").join("").split(".").join("");
          } else if (value.includes('+dec')) {
            value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split("|").join("");
          }
          return value;
        } else {
          let value = attr[i].split("=")[1].split("[[+scg(").join("").split("[[+id(").join("").split(")]]").join("").split(")").join("");
          if (value[value.length - 1] !== ")") {
            value = value.substring(0, value.length-1) + ")|";
          }
          if (value.includes('+int')) {
            value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split(".|").join("").split(".").join("");
          } else if (value.includes('+dec')) {
            value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split("|").join("");
          }
          return value;
        }

      }
    }
    return "----";
  }

  isAttributeRelationWithout() {
    if(typeof this.listFlagAttributeRelation[this.idRowLeftTableWithout] === 'undefined' && this.listFlagAttributeRelation[this.idRowLeftTableWithout] === null) {
      return false;
    } else if (this.listFlagAttributeRelation[this.idRowLeftTableWithout] != null) {
      return this.listFlagAttributeRelation[this.idRowLeftTableWithout];
    } else {
      return false;
    }
  }

  isMandatoryWithout() {
    if(this.isAttributeRelationWithout()) {
      return false;
    }
    if(this.actualAttributeNameWithout[this.idRowLeftTableWithout] !== undefined) {
      for(let i = 0; i < this.listWithoutAttributeName.length; i++) {
        if(this.actualAttributeNameWithout[this.idRowLeftTableWithout].split(" ").join("") === this.listWithoutAttributeName[i].split(" ").join("")) {
          if(this.listWithoutMinCar[i].toString() === '1') {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;
  }



  loadWithout() {
    setTimeout(() => {
      let elementTab = document.getElementById('tab-without');
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'lightblue';
      }
      if(this.idLastTab !== 'tab-without') {
        elementTab = document.getElementById(this.idLastTab);
        if (elementTab != null) {
          // @ts-ignore
          elementTab.style.backgroundColor = 'transparent';
        }
      }
      this.idLastTab = 'tab-without';
      this.idActualTab = 'tab-without';
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWithout.length; j++) {
        if(typeof this.actualAttributeNameWithout[j] === 'undefined') {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWithout[j].length === 0) {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);

    setTimeout(() => {
      this.getRowTableLeftWithout(0);
    }, 10);
  }


  getRowTableLeftWithout(idx: number, dummy?: string) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWithout' + this.idRowLeftTableWithout) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    this.listOperatorRangeWithout = new Array();
    this.listConstraintRangeWithout = new Array();
    this.listNameRangeWithout = new Array();
    this.booleanIntWithout = false;
    this.booleanDecWithout = false;
    this.booleanNormalWithout = false;

    if(typeof this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if ((this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(") || this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec(")) && this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]].toString() !== '0') {
        let elementInput = document.getElementById('label-range');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'none';
        }
      }
      if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(")) {
        this.booleanIntWithout = true;
        this.booleanDecWithout = false;
        this.booleanNormalWithout = false;
        let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+int').join('').split(')').join("");
        v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.intDefMinWithout = s[0];
        } else {
          this.intDefMinWithout = 0;
        }
        if(v.includes("..")) {
          if (s[1].length > 0) {
            this.intDefMaxWithout = s[1];
          } else {
            this.intDefMaxWithout = Number.POSITIVE_INFINITY;
          }
        }

        if(typeof this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
         if(this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0 && this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '') {
            setTimeout(() => {
              let elementRange = document.getElementById('inlineRadio1Int');
              if (elementRange != null) {
                //@ts-ignore
                elementRange.checked = true;
                this.getInputIntWithout('range');
              }
            }, 10);
          } else if(typeof this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
            if(this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0 && this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '') {
              setTimeout(() => {
                let elementFixed = document.getElementById('inlineRadio2Int');
                if (elementFixed != null) {
                  //@ts-ignore
                  elementFixed.checked = true;
                  this.getInputIntWithout('fixed');
                }
              }, 10);
            }
          }
        } else if(typeof this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
          if(this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0 && this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '') {
            setTimeout(() => {
              let elementFixed = document.getElementById('inlineRadio2Int');
              if (elementFixed != null) {
                //@ts-ignore
                elementFixed.checked = true;
                this.getInputIntWithout('fixed');
              }
            }, 10);
          }
        } else {
          setTimeout(() => {
            let elementRange = document.getElementById('inlineRadio1Int');
            if (elementRange != null) {
              //@ts-ignore
              elementRange.checked = false;
            }
            let elementFixed = document.getElementById('inlineRadio2Int');
            if (elementFixed != null) {
              //@ts-ignore
              elementFixed.checked = false;
            }
            this.getInputIntWithout('');
          }, 10);
        }
      } else if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec(")) {
        this.booleanIntWithout = false;
        this.booleanDecWithout = true;
        this.booleanNormalWithout = false;
        let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+dec').join('').split(')').join("");
        v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.decDefMinWithout = s[0];
        } else {
          this.decDefMinWithout = 0;
        }

        if(v.includes("..")) {
          if (s[1].length > 0) {
            this.decDefMaxWithout = s[1];
          } else {
            this.decDefMaxWithout = Number.POSITIVE_INFINITY;
          }
        }

        if(typeof this.listDecMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
          if(this.listDecMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]].toString().length > 0) {
            setTimeout(() => {
              let elementRange = document.getElementById('inlineRadio1Dec');
              if (elementRange != null) {
                //@ts-ignore
                elementRange.checked = true;
                this.getInputDecWithout('range');
              }
            }, 10);
          }
        } else if(typeof this.listDecFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
          if(this.listDecFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0) {
            setTimeout(() => {
              let elementFixed = document.getElementById('inlineRadio2Dec');
              if (elementFixed != null) {
                //@ts-ignore
                elementFixed.checked = true;
                this.getInputDecWithout('fixed');
              }
            }, 10);
          }
        } else {
          setTimeout(() => {
            let elementRange = document.getElementById('inlineRadio1Dec');
            if (elementRange != null) {
              //@ts-ignore
              elementRange.checked = false;
            }
            let elementFixed = document.getElementById('inlineRadio2Dec');
            if (elementFixed != null) {
              //@ts-ignore
              elementFixed.checked = false;
            }
            this.getInputDecWithout('');
          }, 10);
        }
      } else {
        this.booleanIntWithout = false;
        this.booleanDecWithout = false;
        this.booleanNormalWithout = true;
      }
      this.intMaxWithout = '';
      this.intMinWithout = '';
      this.intFixedWithout = '';
      this.fillTableRightWithout(dummy);

      if(!this.listChangeCarWithout[idx]) {
        this.actualMinCarWithout[idx] = this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]];
        this.listSummaryMinCardinalityWithout[idx] = this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]];
        let max = this.listWithoutMaxCar[this.listIndexWithout[this.idRowLeftTableWithout]];
        if(max.toString() === 'Infinity') {
          max = '*';
        }
        this.actualMaxCarWithout[idx] = max;
        this.listSummaryMaxCardinalityWithout[idx] = max;
      }
      this.checkCardinalityWithout('min', idx);
      this.checkCardinalityWithout('max', idx);
    }

    setTimeout(() => {
      let b = false;
      if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout] !== undefined) {
        if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout].toUpperCase() !== 'count' && this.isAttributeRelationWithout()) {
          b = true;
        }
      }
      if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
        this.disableAttributeRelationWithoutProduct(b);
      }

    }, 10);
  }

  disableElement(name: string, b: boolean) {
    setTimeout(() => {
      let element = document.getElementById(name);
      if (element != null) {
        // @ts-ignore
        element.disabled = b;
      }
    }, 10);
  }

  disableAttributeRelationWithoutProduct(b: boolean) {
    this.disableElement('id-value-without', b);
    this.disableElement('addNewValueWithout', b);
    this.disableElement('updateValueWithout', b);
    this.disableElement('id-constraint-without', b);
    this.disableElement('id-value0', b);
    this.disableElement('id-value-dummy', b);
    this.disableElement('inlineRadio1Dec', b);
    this.disableElement('inlineRadio2Dec', b);
    this.disableElement('attributeDecMinWithout', b);
    this.disableElement('attributeDecMaxWithout', b);
    this.disableElement('attributeDecFixedWithout', b);
    this.disableElement('inlineRadio1Int', b);
    this.disableElement('inlineRadio2Int', b);
    this.disableElement('attributeIntMinWithout', b);
    this.disableElement('attributeIntMaxWithout', b);
    this.disableElement('attributeIntFixedWithout', b);
  }


  clickRowTableWithout(row: number) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWithout' + row) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
    this.idRowLeftTableWithout = row;
    for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
      if ((this.actualAttributeNameWithout[this.idRowLeftTableWithout] === this.listWithoutAttributeName[i])) {
        this.indexWithout = i;
        this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;
        this.listIndexWithout[this.idRowLeftTableWithout] = i;
      }
    }
    this.getRowTableLeftWithout(row);
  }

  resetCarWithout(idx: number) {
    this.listChangeCarWithout[idx] = true;
    this.clickRowTableWithout(idx);
  }


  loadAttributeDataWithout(idx: number, dummy?: string) {
    this.clickRowTableWithout(idx)
    // this.idRowLeftTableWithout = idx;
    if(dummy === undefined) {
      dummy = '0';
    }

    this.idRowLeftTableWithout = idx;
    this.clickRowTableWithout(idx)
    this.showInformationWindowWithout = true;

    setTimeout(() => {
      if (!this.listWithoutAttributeName.includes(this.actualAttributeNameWithout[this.idRowLeftTableWithout])) {
        this.actualAttributeNameWithout[this.idRowLeftTableWithout] = '';
        this.showInformationWindowWithout = false;
      } else {

        for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
          if (this.actualAttributeNameWithout[this.idRowLeftTableWithout] === this.listWithoutAttributeName[i] || this.listChangedConstraintFlagWithout[i].toString() === dummy) {
            this.indexWithout = i;
            this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;
            this.listIndexWithout[this.idRowLeftTableWithout] = i;
            this.showInformationWindowWithout = true;

            let id = -1;
            for (let j = 0; j < this.listWithoutAttributeName.length; j++) {
              if (this.listWithoutAttributeName[j] === this.actualAttributeNameWithout[this.idRowLeftTableWithout]) {
                id = j;
                break;
              }
            }

            this.listSummaryAttributeNamesWithout.push(this.actualAttributeNameWithout[this.idRowLeftTableWithout]);
            this.listSummaryAttributeCodesWithout.push(this.listWithoutAttributeCode[id]);

            this.isWithout = true;
            let s = this.popupInformationConstraintWithout().toString();
            let s_prepro = this.popupInformationConstraintWithout().toString().split('}').join('').split(" |").join("|").split(" ").join("");
            if (!s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
              this.preprocessingRangeWithoutOperator(s);
            } else if (s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
              this.preprocessingRangeSingleWithoutOperator(s, 'OR');
            } else if (!s_prepro.includes("|OR") && s_prepro.includes("|MINUS")) {
              this.preprocessingRangeSingleWithoutOperator(s, 'MINUS');
            } else {
              this.preprocessingRangeMultipleWithoutOperator(s);
            }
            this.fillTableRightWithout('');

            setTimeout(() => {
              this.getInputIntWithout('');
              if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] != undefined) {
                if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(")) {
                  this.booleanIntWithout = true;
                  this.booleanDecWithout = false;
                  this.booleanNormalWithout = false;
                  let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+int').join('').split(')').join("");
                  v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
                  let s = v.split("..");
                  if (s[0].length > 0) {
                    this.intDefMinWithout = s[0];
                  } else {
                    this.intDefMinWithout = 0;
                  }
                  if (s[1].length > 0) {
                    this.intDefMaxWithout = s[1];
                  } else {
                    this.intDefMaxWithout = Number.POSITIVE_INFINITY;
                  }
                } else if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec")) {
                  this.booleanIntWithout = false;
                  this.booleanDecWithout = true;
                  this.booleanNormalWithout = false;
                } else {
                  this.booleanIntWithout = false;
                  this.booleanDecWithout = false;
                  this.booleanNormalWithout = true;
                }
              }

              let elementInput = document.getElementById('attributeNameWithoutDataList' + idx);
              if (elementInput != null) {
                // @ts-ignore
                elementInput.readOnly = true;
                elementInput.style.backgroundColor = '#eaecef';
                elementInput.style.outline = 'none';
                elementInput.style.boxShadow = 'none';
              }
              this.clickRowTableWithout(idx);
            }, 10);
            break;
          }
        }
      }
    }, 10);


  }


  fillTableRightWithout(dummy? : string, dummy2? : string) {
    if(typeof this.listIndexWithout[this.idRowLeftTableWithout] !== 'undefined') {
      if(this.booleanNormalWithout) {
        let s = this.popupInformationConstraintWithout().toString();
        let s_prepro = '';
        if(dummy2 === undefined) {
          s_prepro = this.popupInformationConstraintWithout().toString().split('}').join('').split(" |").join("|").split(" ").join("");
        } else {
          return;
        }
        if(dummy === 'start' && dummy2 === undefined) {
          if (!s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
            this.preprocessingRangeWithoutOperator(s);
          } else if (s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
            this.preprocessingRangeSingleWithoutOperator(s, 'OR');
          } else if (!s_prepro.includes("|OR") && s_prepro.includes("|MINUS")) {
            this.preprocessingRangeSingleWithoutOperator(s, 'MINUS');
          } else {
            this.preprocessingRangeMultipleWithoutOperator(s);
          }
        }
        let result = '';
        let lOperator = new Array();
        let lConstraint = new Array();
        let lName = new Array();

        if (this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] !== null  && this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] !== undefined) {
          for (let j = 0; j < this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout].length; j++) {
            let operator = this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout][j];
            let constraint = this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout][j];
            let code = this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout][j];
            let name = this.listSummaryNameRangeWithout[this.idRowLeftTableWithout][j];
            if(code.length > 0) {
              result = result + operator + constraint + code.split(' ').join('') + ' |' + name + '|';
            }

          }
          if(this.listSummaryValueRangeWithout.length > Number.parseInt(String(this.idRowLeftTableWithout))) {
            this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = result;
          } else {
            this.listSummaryValueRangeWithout.push(result);
          }

          this.correctInputWithout = true;
        }

        if (this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] !== null && this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] !== undefined) {
          for (let j = 0; j < this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout].length; j++) {
            let operator = this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout][j];
            let constraint = this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout][j];
            let name = this.listSummaryNameRangeWithout[this.idRowLeftTableWithout][j];

            lOperator.push(operator);
            lConstraint.push(constraint);
            lName.push(name);
          }
          this.listOperatorRangeWithout = lOperator;
          this.listConstraintRangeWithout = lConstraint;
          this.listNameRangeWithout = lName;
          this.correctInputWithout = true;
        }
      } else if (this.booleanIntWithout) {
        if (this.listIntValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '' && (this.inputIdIntWithout === 'range' || this.inputIdIntWithout === '')) {
          this.intMinWithout = this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.intMaxWithout = this.listIntMaxWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.updateIntValueWithout();
        } else if (this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '' &&  this.inputIdIntWithout === 'fixed') {
          this.intFixedWithout = this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.updateIntFixedValueWithout();
        } else {
          if(this.listSummaryValueRangeWithout.length > Number.parseInt(String(this.idRowLeftTableWithout))) {
            this.listSummaryValueRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split('[').join('').split('+').join('').split('int').join('').split('(').join('').split(')').join('');
          } else {
            this.listSummaryValueRangeWithout.push(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split('[').join('').split('+').join('').split('int').join('').split('(').join('').split(')').join(''));
          }
        }
      } else if(this.booleanDecWithout) {
        if (this.listDecValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '') {
          this.decMinWithout = this.listDecMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.decMaxWithout = this.listDecMaxWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.updateDecValueWithout();
        } else if (this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== '') {
          this.decFixedWithout = this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          this.updateDecFixedValueWithout();
        } else {
          if(this.listSummaryValueRangeWithout.length > Number.parseInt(String(this.idRowLeftTableWithout))) {
            this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = this.listWithoutConstraint[this.idRowLeftTableWithout].split('[').join('').split('+').join('').split('dec').join('').split('(').join('').split(')').join('');

          } else {
            this.listSummaryValueRangeWithout.push(this.listWithoutConstraint[this.idRowLeftTableWithout].split('[').join('').split('+').join('').split('dec').join('').split('(').join('').split(')').join(''));
          }
        }
      }
    }


  }

  preprocessingRangeWithoutOperator(input: any) {
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();

    lOperator.push('');
    if(input.includes("<")) {
      lConstraint.push(input.substring(0,input.lastIndexOf("<")+1).split(" ").join(""));
      lCode.push(input.substring(input.lastIndexOf("<")+1, input.indexOf("|")).split(" ").join(""));
      lName.push(input.substring(input.indexOf("|")+1, input.lastIndexOf("|")));
    } else {
      lConstraint.push('');
      lCode.push(input.substring(0, input.indexOf("|")).split(" ").join(""));
      lName.push(input.substring(input.indexOf("|")+1, input.lastIndexOf("|")));
    }
    this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = lOperator;
    this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = lConstraint;
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = lCode;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = lName;
  }

  preprocessingRangeMultipleWithoutOperator(input: any) {
    let or = input.split('OR');
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();

    for(let j = 0; j < or.length; j++) {
      if(!or[j].includes('MINUS')) {
        if(j === 0) {
          lOperator.push("");
        } else {
          lOperator.push('OR');
        }
        if(or[j].includes("<")) {
          lConstraint.push(or[j].substring(0,or[j].lastIndexOf("<")+1));
          lCode.push(or[j].substring(or[j].lastIndexOf("<")+1, or[j].indexOf("|")).split(" ").join(""));
          lName.push(or[j].substring(or[j].indexOf("|")+1, input.lastIndexOf("|")));
        } else {
          lConstraint.push('');
          lCode.push(or[j].substring(0, or[j].indexOf("|")));
          lName.push(or[j].substring(or[j].indexOf("|")+1, input.lastIndexOf("|")));
        }
      } else {
        let minus = or[j].split('MINUS');
        for (let k = 0; k < minus.length; k++) {
          if (j === 0) {
            lOperator.push("");
          } else if(k === 0) {
            lOperator.push('OR');
            lOperator.push('MINUS');
          }

          if (minus[k].includes("<")) {
            lConstraint.push(minus[k].substring(0, minus[k].lastIndexOf("<") + 1));
            lCode.push(minus[k].substring(minus[k].lastIndexOf("<") + 1, minus[k].indexOf("|")).split(" ").join(""));
            lName.push(minus[k].substring(minus[k].indexOf("|") + 1, minus[k].lastIndexOf("|")));
          } else {
            lConstraint.push('');
            lCode.push(minus[k].substring(0, minus[k].indexOf("|")));
            lName.push(minus[k].substring(minus[k].indexOf("|") + 1, minus[k].lastIndexOf("|")));
          }

        }
      }
    }
    this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = lOperator;
    this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = lConstraint;
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = lCode;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = lName;
  }


  preprocessingRangeSingleWithoutOperator(input: any, operator: String) {
    let or = input.split(operator);
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();
    for(let j = 0; j < or.length; j++) {
      if(j === 0) {
        lOperator.push("");
      } else {
        lOperator.push(operator);
      }
      if(or[j].includes("<")) {
        lConstraint.push(or[j].substring(0,or[j].lastIndexOf("<")+1).split(" ").join(""));
        lCode.push(or[j].substring(or[j].lastIndexOf("<")+1, or[j].indexOf("|")).split(" ").join(""));
        lName.push(or[j].substring(or[j].indexOf("|")+1, or[j].lastIndexOf("|")));
      } else {
        lConstraint.push('');
        lCode.push(or[j].substring(0, or[j].indexOf("|")));
        lName.push(or[j].substring(or[j].indexOf("|")+1, or[j].lastIndexOf("|")));
      }

    }
    this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = lOperator;
    this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = lConstraint;
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = lCode;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = lName;
  }





  getAttributesWithout() {
    let list = new Array();
    if(this.listWithoutAttributeName != null && this.actualAttributeNameWithout != null) {
      for(let i = 0; i < this.listWithoutAttributeName.length; i++) {
        let count = this.listSummaryAttributeNamesWithout.filter(x => x===this.listWithoutAttributeName[i]).length;
        if(this.listWithoutMaxCar[i].toString() === 'Infinity' && this.listChangedConstraintFlagWithout[i].toString() === '0') {
          list.push(this.listWithoutAttributeName[i]);
        } else if(Number.parseInt(String(count)) < Number.parseInt(this.listWithoutMaxCar[i]) && this.listChangedConstraintFlagWithout[i].toString() === '0') {
          list.push(this.listWithoutAttributeName[i]);
        }
      }
    }
    for(let i = 0; i < list.length; i++) {
      if(list[i].substring(list[i].length-1) === " ") {
        list[i] = list[i].substring(0, list[i].length-1);
      }
    }
    return [...new Set(list)]; // remove duplicates
  }

  addRowTableLeftWithout() {
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.invalidInputCarWithout = false;
    this.actualCheckCarWithout = true;

    this.listNumberAttributesWithout.push("");
    this.actualAttributeNameWithout.push("");
    this.actualMinCarWithout.push("");
    this.actualMaxCarWithout.push("");
    this.actualValueRangeWithout.push("");
    this.listLoadDataAttributeWithout.push(false);

    this.listOperatorRangeWithout = new Array();
    this.listConstraintRangeWithout = new Array();
    this.listNameRangeWithout = new Array();
    this.listIndexWith.push(new Array());

    this.listOperatorRangeWithout.push("");
    this.listConstraintRangeWithout.push("");
    this.listNameRangeWithout.push("");

    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';

    this.getRowTableLeftWithout(this.idRowLeftTableWithout)

    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWithout.length; j++) {
        if(typeof this.actualAttributeNameWithout[j] === 'undefined') {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWithout[j].length === 0) {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);
  }

  deleteRowTableLeftWithout() {
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.invalidInputCarWithout = false;
    this.actualCheckCarWithout = true;

    this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.listSummaryAttributeCodesWithout.push('');
    this.listSummaryAttributeNamesWithout.push('');
    this.listSummaryMinCardinalityWithout.push('');
    this.listSummaryMaxCardinalityWithout.push('');
    this.listSummaryOperatorRangeWithout.push('');
    this.listSummaryConstraintRangeWithout.push('');
    this.listSummaryValueRangeWithout.push('');
    this.listIntValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listDecValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listChangeCarWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = false;
    this.listNumberAttributesWithout.splice(this.idRowLeftTableWithout, 1);
    this.actualAttributeNameWithout.splice(this.idRowLeftTableWithout, 1);
    this.actualMinCarWithout.splice(this.idRowLeftTableWithout, 1);
    this.actualMaxCarWithout.splice(this.idRowLeftTableWithout, 1);
    this.actualValueRangeWithout.splice(this.idRowLeftTableWithout, 1);
    this.listLoadDataAttributeWithout.splice(this.idRowLeftTableWithout, 1);
    this.listOperatorRangeWithout = new Array();
    this.listConstraintRangeWithout = new Array();
    this.listNameRangeWithout = new Array();

    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';
    this.listIndexWithout.splice(this.idRowLeftTableWithout, 1);

    if(this.idRowLeftTableWithout > 0) {
      this.getRowTableLeftWithout(this.idRowLeftTableWithout - 1);
    }
    else {
      this.getRowTableLeftWithout(this.idRowLeftTableWithout);
    }
  }

  idBtnAddWithoutDisabled() {
    let l = this.getAttributesWithout();
    let s = this.actualAttributeNameWithout[this.actualAttributeNameWithout.length - 1];
    if(this.listNumberAttributesWithout.length < 1) {
      return false;
    } else if(this.listNumberAttributesWithout.length > this.actualAttributeNameWithout.length) {
      return true;
    }
    if (typeof s !== 'undefined' && l.length > 0) {
      for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
        if (this.listWithoutAttributeName[i] === s) {
          return false;
        }
      }
      return true;
    }
    return true;
  }

  idBtnDeleteWithoutDisabled() {
    if(this.listNumberAttributesWithout.length < 1) {
      return true;
    }
    if(typeof this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if(this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]].toString() === '1') {
        return true;
      }
    }
    if(this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]] != null && typeof this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if(this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0) {
        return true;
      }
    }
    return false;
  }

  checkInputCarWithout(idx: number) {
    if (this.actualMaxCarWithout[idx] !== '' && this.actualMinCarWithout[idx] !== '') {
      let maxCar = this.actualMaxCarWithout[idx];
      if(maxCar === '*') {
        maxCar = Number.POSITIVE_INFINITY;
      } else {
        maxCar = Number.parseInt(maxCar)
      }
      if (maxCar >= Number.parseInt(this.actualMinCarWithout[idx])) {
        this.actualCheckCarWithout = true;
      } else {
        if (this.actualMaxCarWithout[idx] === null || this.actualMinCarWithout[idx] === null || typeof this.actualMinCarWithout[idx] === 'undefined' || typeof this.actualMaxCarWithout[idx] === 'undefined') {
          this.actualCheckCarWithout = true;
        } else {
          this.actualCheckCarWithout = false;
        }
      }
    } else {
      this.actualCheckCarWithout = true;
    }
  }


  checkCardinalityWithout(id: string, idx: number, dummy?: string) {        // todo - bei werten ausßerhalb des Wertebeirechs erscheint keine Info
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.invalidInputCarWithout = false;
    this.actualCheckCarWithout = true;

    if (id === 'min' && this.actualMinCarWithout[idx] !== '' && typeof this.actualMinCarWithout[idx] !== 'undefined') {
      if (isNaN(this.actualMinCarWithout[idx]) || Number.parseInt(this.actualMinCarWithout[idx]) > Number.parseInt(this.listWithoutMaxCar[this.listIndexWithout[this.idRowLeftTableWithout]])) {
        setTimeout(() => {
          this.actualMinCarWithout[idx] = '';
        }, 50);
        this.invalidInputMinCarWithout = true;
      } else {
        this.invalidInputMinCarWithout = false;
      }
    } else if (id === 'max' && this.actualMaxCarWithout[idx] !== '' && typeof this.actualMaxCarWithout[idx] !== 'undefined') {
      if ((isNaN(this.actualMaxCarWithout[idx]) && this.actualMaxCarWithout[idx] !== '*') || Number.parseInt(this.actualMaxCarWithout[idx]) > Number.parseInt(this.listWithoutMaxCar[this.listIndexWithout[this.idRowLeftTableWithout]])) {
        this.invalidInputMaxCarWithout = true;
        setTimeout(() => {
          this.actualMaxCarWithout[idx] = '';
        }, 50);
      } else if(this.actualMinCarWithout[idx].toString() === '0' && this.actualMaxCarWithout[idx].toString() === '0') {
        this.invalidInputCarWithout = true;
      } else {
        this.invalidInputCarWithout = false;
        this.invalidInputMaxCarWithout = false;
      }
      this.checkInputCarWithout(idx);
    }


    // if(!this.invalidInputMaxCarWithout && !this.invalidInputMinCarWithout && dummy === 'withoutRG') {
    this.listSummaryMinCardinalityWithout[idx] = this.actualMinCarWithout[idx];
    this.listSummaryMaxCardinalityWithout[idx] = this.actualMaxCarWithout[idx];
    // }
  }

  keypressGetAttributeValueWithout(idx: number) {
    const arr = this.listNameRangeWithout[idx] || [];
    this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.correctInputWithout = false;

    if(Number.parseInt(arr)) {
      this.keypressEnterWithout(idx);
      console.log("HIER!!!")
    }

    if (arr.length > 2) {
      if (this.oldInputWithout !== this.listNameRangeWithout[idx] && !Number(this.listNameRangeWithout[idx])) {

        // spinnerWithout
        let elementInput = document.getElementById('spinnerWithout' + idx);
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'inline';
        }
        if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("(")) {
          this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace(")", "");
        } else if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes(")")) {
          this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace("(", "");
        }
        this.templateGenerationService.getAttributeValueRequestFilterLimitFSN(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]], this.listNameRangeWithout[idx], '1000').then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let i = 0; i < value.length; i++) {
              if(!this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].includes(value[i].code)) {
                this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].push(value[i].code);
                this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].push(value[i].fsn);
              }
            }
          }
          this.oldInputWithout = this.listNameRangeWithout[this.indexWithout];
          setTimeout(() => {
            let elementInput = document.getElementById('spinnerWithout' + idx);
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          }, 500);



          return "";
        })
      } else {
        this.booleanClickKeypressWithout = true;
        this.listAllCodesRangeWithout[idx] = new Array();
        this.listAllNamesRangeWithout[idx] = new Array();
      }
    }
    return "";
  }

  keypressEnterWithout(i: number) {
    let s = '';
    this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();

    if (s != this.listNameRangeWithout[i]) {

      let elementInput = document.getElementById('spinnerWithout' + i);
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }
      if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("(")) {
        this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace(")", "");
      } else if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes(")")) {
        this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace("(", "");
      }
      this.templateGenerationService.getAttributeValueRequestFilterLimitFSN(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]], this.listNameRangeWithout[i], '100000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].includes(value[i].code)) {
              this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].push(value[i].code);
              this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].push(value[i].fsn);
              this.listNameRangeWithout[i] = value[i].fsn;
            }
          }
          let elementInput = document.getElementById('spinnerWithout' + i);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        } else {
          let elementInput = document.getElementById('spinnerWithout' + i);
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }
      })
    }
  }

  clickKeypressWithout() {
    if (this.listAllCodesRangeWithout.length > 0) {
      this.booleanClickKeypressWithout = false;
      this.correctInputWithout = false
    }
  }

  changeFieldWithout(idx: number) {
    let elementInput = document.getElementById('id-value' + idx);
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
    }

    let elementDummy = document.getElementById('id-value-dummy' + idx);
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';
      if(elementDummy.style.display === 'inline') {
        this.correctInputWithout = true;
      }
    }

    elementInput = document.getElementById('addNewValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    elementInput = document.getElementById('updateValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }

  }

  startFieldValueWithout(idx: number) {
    let elementInput = document.getElementById('id-value' + idx);
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = 'inline';
    }

    let elementDummy = document.getElementById('id-value-dummy' + idx);
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = 'none';
      if (elementDummy.style.display === 'none') {
        // this.listNameRangeWithout[idx] = '';
        this.correctInputWithout = false;
      }
    }
  }

  getIdRowTableRightWithout(idx: number) {
    this.idRowTableRightWithout = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableRightWithout' + this.idRowTableRightWithout) {
            element.style.background = 'lightblue';
          }
          if (element.id === 'rowTableLeftWithout' + this.idRowLeftTableWithout) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    if(this.listNameRangeWithout.length > 0) {
      for(let i = 0; i < this.listNameRangeWithout.length; i++) {
        if(i !== this.idRowTableRightWithout && this.listNameRangeWithout[i].length > 0) {
          let elementInput = document.getElementById('deleteValueWithout');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = false;
          }
          return null;
        }
      }

    }
    let elementInput = document.getElementById('deleteValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    return null;
  }

  addNewLineWithout() {
    this.listOperatorRangeWithout.push("");
    this.listNameRangeWithout.push("");
    this.updateValueWithout();
    this.listAllCodesRangeWithout[this.indexWithout] = new Array();
    this.listAllNamesRangeWithout[this.indexWithout]  = new Array();
  }

  idBtnDeleteValueWithoutDisabled() {
    if(this.listNameRangeWithout.length > 0) {
      for(let i = 0; i < this.listNameRangeWithout.length; i++) {
        if(i !== this.idRowTableRightWithout && this.listNameRangeWithout[i].length > 0) {
          return false;
        }
      }
    }
    return true;
  }

  deleteValueWithout() {
    this.listOperatorRangeWithout.splice(this.idRowTableRightWithout, 1);
    this.listNameRangeWithout.splice(this.idRowTableRightWithout, 1);
    this.listConstraintRangeWithout.splice(this.idRowTableRightWithout, 1);

    if (this.idRowTableRightWithout === 0) {
      this.listOperatorRangeWithout[0] = '';
    }

    this.updateValueWithout();

    let elementInput = document.getElementById('deleteValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    elementInput = document.getElementById('addNewValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    elementInput = document.getElementById('updateValueWithout');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }

  }




  updateValueWithout() {
    let value = '';
    for(let i = 0; i < this.listOperatorRangeWithout.length; i++) {
      if(i === 0) {
        value = value + this.listOperatorRangeWithout[i] + "@";
        value = value + this.listConstraintRangeWithout[i] + "@";
        value = value + this.listNameRangeWithout[i] + "$";
        this.actualValueRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = value;
        this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout].push(this.listOperatorRangeWithout[i])
        this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout].push(this.listConstraintRangeWithout[i])
        this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout].push(this.listSummaryCodeRangeWithout[i])
        this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].push(this.listSummaryNameRangeWithout[i])
        this.getAttributeRangeConstraintWithout(value);
      } else if(this.listOperatorRangeWithout[i] !== "") {
        value = value + this.listOperatorRangeWithout[i] + "@";
        value = value + this.listConstraintRangeWithout[i] + "@";
        value = value + this.listNameRangeWithout[i] + "$";
        this.actualValueRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = value;
        this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout].push(this.listOperatorRangeWithout[i])
        this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout].push(this.listConstraintRangeWithout[i])
        this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout].push(this.listSummaryCodeRangeWithout[i])
        this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].push(this.listSummaryNameRangeWithout[i])
        this.getAttributeRangeConstraintWithout(value);
      }
    }
  }



  getAttributeRangeConstraintWithout(value: string) {
    let result = '';
    let operator = '';
    let constraint = '';
    let code = '';
    let name = '';

    if(typeof this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] !== 'undefined') {
      let s = value.split("$");
      let lNewConstraint = new Array();
      let lNewOperator = new Array();
      let lNewName = new Array();
      let lNewCode = new Array();

      for(let i = 0; i < s.length-1; i++) {
        let a = s[i].split("@");
        if(typeof a[0] !== 'undefined') {
          operator = a[0];
        }
        if(typeof a[1] !== 'undefined') {
          constraint = a[1].replace('undefined', '');
        }
        if(typeof a[2] !== 'undefined') {
          name = a[2];
          let id = -1;

          if(typeof this.listIndexWithout[this.idRowLeftTableWithout] === 'undefined') {
            for (let k = 0; k < this.listAllNamesRangeWithout[this.indexWithout].length; k++) {
              if (this.listAllNamesRangeWithout[this.indexWithout][k] === name) {
                id = k;
                break
              }
            }
            code = this.listAllCodesRangeWithout[this.indexWithout][id];
          } else {
            for (let k = 0; k < this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length; k++) {
              if (this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]][k] === name) {
                id = k;
                break
              }
            }
            code = this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]][id];
          }
          lNewCode.push(code);
          lNewName.push(name);
          lNewOperator.push(operator)
          lNewConstraint.push(constraint);
        }
        this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = lNewOperator;
        this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = lNewConstraint;
        this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = lNewName;
        this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = lNewCode;

      }

      let backup = this.listSummaryValueRangeWithout[this.idRowLeftTableWithout];

      for(let i  = 0; i < this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].length; i++) {
        code = this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout][i];
        if(typeof code === 'undefined') {
          this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout][i] = backup.split('|')[0].split('<<').join('');
        }
      }

      this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = '';
      for (let i = 0; i < this.listSummaryNameRangeWithout[this.idRowLeftTableWithout].length; i++) {
        operator = this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout][i];
        constraint = this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout][i];
        code = this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout][i].split(' ').join('');
        name = this.listSummaryNameRangeWithout[this.idRowLeftTableWithout][i];

        result = operator + constraint + code + " |" + name + "|";
        if (!this.listSummaryValueRangeWithout[this.idRowLeftTableWithout].includes(result) && name !== 'undefined') {
          this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] + result;
        }
      }
    }
  }

  getPossibleValuesWithout() {
    return this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
  }



  getRangeWithout(idx: number) {
    if(this.listSummaryValueRangeWithout[idx] !== undefined) {
      if(this.listSummaryValueRangeWithout[idx] !== null) {
        return this.listSummaryValueRangeWithout[idx].split('MINUS').join(' MINUS ').split('OR').join(' OR ');
      } else {
        return  '';
      }
    }
  }

  checkInputRightTableWithout() {

    for(let i = 1; i < this.listOperatorRangeWithout.length; i++) {
      if((this.listOperatorRangeWithout[i] === '' || this.listNameRangeWithout[i] === '')) {
        if (this.listOperatorRangeWithout[i] === '' && this.listNameRangeWithout[i] === '') {
          let elementInput = document.getElementById('addNewValueWithout');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = true;
          }
          elementInput = document.getElementById('updateValueWithout');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = true;
          }
        }
        return true;
      } else {
        for (let i = 0; i < this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length; i++) {
          if(this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]][i] === this.listNameRangeWithout[this.idRowTableRightWithout]) {
            return false;
          }
        }
        return true;
      }
    }
    if(this.listNameRangeWithout.length === 1 && this.listNameRangeWithout[0] === '') {
      return true;
    }
    if(this.idRowTableRightWithout === -1) {
      this.idRowTableRightWithout = 0;
    }
    for (let i = 0; i < this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length; i++) {
      if(this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]][i] === this.listNameRangeWithout[this.idRowTableRightWithout]) {
        return false;
      }
    }
    return null;



  }

  getInputIntWithout(value: string) {
    this.inputIdIntWithout = value;
  }

  checkInputIntegerRightTableWithout() {
    if((this.intMinWithout === '' || this.intMinWithout === null || this.intMinWithout === undefined) && (this.intMaxWithout === '' || this.intMaxWithout === null || this.intMaxWithout === undefined)) {
      return true;
    } else {
      if(this.intMinWithout === '' || this.intMinWithout === null || this.intMinWithout === undefined) {    // min leer = diabled
        return true;
      } else {                                                                                              // min nicht leer
        if(this.intMaxWithout === '' || this.intMaxWithout === null || this.intMaxWithout === undefined) {  // max leer
          return Number.parseFloat(this.intMinWithout) < this.intDefMinWithout;
        } else {                                                                                            // max leer
          return (Number.parseFloat(this.intMinWithout) < this.intDefMinWithout) ||  (Number.parseFloat(this.intMaxWithout) > this.intDefMaxWithout) || Number.parseFloat(this.intMinWithout) > Number.parseFloat(this.intMaxWithout);
        }
      }
    }
  }

  updateIntValueWithout() {
    if(this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]] === '1') {
      this.intFixedWithout = this.listSummaryValueRangeWithout[this.idRowLeftTableWithout].replace("#", "");
      setTimeout(() => {
        let elementRange = document.getElementById('inlineRadio3Int');
        if (elementRange != null) {
          //@ts-ignore
          elementRange.checked = true;
          this.getInputIntWithout('fixed');
        }
      }, 10);
    } else {


      if (this.intMaxWithout === '' || typeof this.intMaxWithout === 'undefined') {
        this.intMaxWithout = '';
      }
      if (this.intMaxWithout.length > 0) {
        this.intMaxWithout = Math.round(Number.parseFloat(this.intMaxWithout)).toString();
      }
      if (this.intMinWithout === '' || typeof this.intMinWithout === 'undefined') {
        this.intMinWithout = '0';
      }
      this.intMinWithout = Math.round(Number.parseFloat(this.intMinWithout)).toString();
      let v = '';
      if (this.intMaxWithout === '') {
        v = ">#" + this.intMinWithout + "..";
      } else {
        v = ">#" + this.intMinWithout + ".." + "<#" + this.intMaxWithout;
      }
      this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = v;
      this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = '';
      this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = '';
      this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = v;
      this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = '';
      this.listIntValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = v;

      this.listIntMaxWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.intMaxWithout;
      this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.intMinWithout;

      setTimeout(() => {
        let elementFixed = document.getElementById('inlineRadio1Int');
        if (elementFixed != null) {
          //@ts-ignore
          elementFixed.checked = true;
          this.getInputIntWithout('range');
        }
      }, 10);
    }
  }

  updateIntFixedValueWithout() {
    this.intFixedWithout = Math.round(Number.parseFloat(this.intFixedWithout)).toString();
    this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = '#' + this.intFixedWithout;
    this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = '';
    this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = '';
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = '#' + this.intFixedWithout;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = '';
    this.listIntFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '#' + this.intFixedWithout;

    this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.intFixedWithout;
    this.listIntMaxWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listIntMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';

    setTimeout(() => {
      let elementFixed = document.getElementById('inlineRadio2Int');
      if (elementFixed != null) {
        //@ts-ignore
        elementFixed.checked = true;
        this.getInputIntWithout('fixed');
      }
    }, 10);
  }

  isDisabledInputFixedWithout() {
    if(this.intFixedWithout === '' || this.intFixedWithout === null || this.intFixedWithout === undefined) {
      return true;
    } else if(this.intFixedWithout !== undefined) {
      return (Number.parseFloat(this.intFixedWithout) < this.intDefMinWithout) ||  (Number.parseFloat(this.intFixedWithout) > this.intDefMaxWithout)
    } else {
      return true;
    }
  }

  getInputDecWithout(value: string) {
    this.inputIdDecWithout = value;
  }


  updateDecValueWithout() {
    if(this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]] === '1') {
      this.decFixedWithout = this.listSummaryValueRangeWithout[this.idRowLeftTableWithout].replace("#", "");
      setTimeout(() => {
        let elementRange = document.getElementById('inlineRadio3Dec');
        if (elementRange != null) {
          //@ts-ignore
          elementRange.checked = true;
          this.getInputDecWithout('fixed');
        }
      }, 10);
    } else {
      let v = '';
      if(this.decMinWithout === '' || typeof this.decMinWithout === 'undefined') {
        this.decMinWithout = '0';
      }
      this.decMinWithout = Math.round(Number.parseFloat(this.decMinWithout)).toString();
      if(this.decMaxWithout === '' || typeof this.decMaxWithout === 'undefined') {
        if(!this.decMinWithout.includes('.')) {
          this.decMinWithout = this.decMinWithout + '.0';
        }
        v = ">#" + this.decMinWithout + "..";
      } else {
        if(!this.decMaxWithout.toString().includes('.')) {
          this.decMaxWithout = this.decMaxWithout + '.0';
        }
        if(!this.decMinWithout.toString().includes('.')) {
          this.decMinWithout = this.decMinWithout + '.0';
        }
        v = ">#" + this.decMinWithout + ".." + "<#" + this.decMaxWithout;
      }
      this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = v;
      this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = '';
      this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = '';
      this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = v;
      this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = '';
      this.listDecValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = v;
      this.listDecMaxWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.decMaxWithout;
      this.listDecMinWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.decMinWithout;


      setTimeout(() => {
        let elementRange = document.getElementById('inlineRadio1Dec');
        if (elementRange != null) {
          //@ts-ignore
          elementRange.checked = true;
          this.getInputDecWithout('range');
        }
      }, 10);
    }


  }

  checkInputDecRightTableWithout() {
    if((this.decMinWithout === '' || this.decMinWithout === null || this.decMinWithout === undefined) && (this.decMaxWithout === '' || this.decMaxWithout === null || this.decMaxWithout === undefined)) {
      return true;
    } else {
      if(this.decMinWithout === '' || this.decMinWithout === null || this.decMinWithout === undefined) {    // min leer = diabled
        return true;
      } else {                                                                                              // min nicht leer
        if(this.decMaxWithout === '' || this.decMaxWithout === null || this.decMaxWithout === undefined) {  // max leer
          return Number.parseFloat(this.decMinWithout) < this.decDefMinWithout;
        } else {                                                                                            // max leer
          return (Number.parseFloat(this.decMinWithout) < this.decDefMinWithout) ||  (Number.parseFloat(this.decMaxWithout) > this.decDefMaxWithout) || Number.parseFloat(this.decMinWithout) > Number.parseFloat(this.decMaxWithout);
        }
      }
    }
  }

  checkInputDecFixedRightTableWithout() {
    if(this.decFixedWithout === '' || this.decFixedWithout === null || this.decFixedWithout === undefined) {
      return true;
    } else if(this.decFixedWithout !== undefined) {
      return (Number.parseFloat(this.decFixedWithout) < this.decDefMinWithout) ||  (Number.parseFloat(this.decFixedWithout) > this.decDefMaxWithout)
    } else {
      return true;
    }
  }

  updateDecFixedValueWithout() {
    this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout;
    this.listSummaryOperatorRangeWithout[this.idRowLeftTableWithout] = '';
    this.listSummaryConstraintRangeWithout[this.idRowLeftTableWithout] = '';
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = '';
    this.listDecFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '#' + this.decFixedWithout;
    this.listDecValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';

    this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.decFixedWithout;
    if(!this.decFixedWithout.toString().includes('.')) {
      this.listSummaryValueRangeWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout + '.0';
      this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout + '.0';
      this.listDecFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '#' + this.decFixedWithout + '.0';
    }

    setTimeout(() => {
      let elementRange = document.getElementById('inlineRadio2Dec');
      if (elementRange != null) {
        //@ts-ignore
        elementRange.checked = true;
        this.getInputDecWithout('fixed');
      }
    }, 10);
  }



  //  WITH ROLE GROUP

  loadWith(i:number) {
    setTimeout(() => {
      let elementTab = document.getElementById('tab-with' + i);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'lightblue';
      }

      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWith' + i + '00') {
              element.style.background = 'lightblue';
            }
          }
        }
      }
    }, 10);
    setTimeout(() => {
      if(this.idLastTab.toString() !== ('tab-with' + i).toString()) {
        let elementTab = document.getElementById(this.idLastTab);
        if (elementTab != null) {
          // @ts-ignore
          elementTab.style.backgroundColor = 'transparent';
        }
        this.idSubRg = 0;
      }
      this.idLastTab = 'tab-with' + i;
      this.idActualTab = 'tab-with' + i;
      this.idRg = i;
    }, 10);

    setTimeout(() => {
      let element = document.getElementById('tab-subRg' + this.idRg + '0');
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
    }, 10);
    setTimeout(() => {
      if(this.idSubRg.toString() !== '0'.toString()) {
        let element = document.getElementById('tab-subRg' + this.idRg + this.idSubRg);
        if (element != null) {
          // @ts-ignore
          element.style.backgroundColor = 'transparent';
        }
      }
      this.idSubRg = 0;
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.actualAttributeNameWith[this.idRg][this.idSubRg].length; j++) {

        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
      this.idRg = i;
      this.idSubRg = 0;
      this.getRowTableLeftWith(0);
      setTimeout(() => {
        let b = false;
        if(this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
          if(this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].toUpperCase() !== 'count' && this.isAttributeRelationWith()) {
            b = true;
          }
        }
        if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
          this.disableElement('id-value-with' + this.idRg + this.idSubRg + this.idRowLeftTableWith, b);
          this.disableElement('operator' + this.idRg + this.idSubRg + this.idRowLeftTableWith, b);
          this.disableElement('constraint' + this.idRg + this.idSubRg + this.idRowLeftTableWith, b);
          this.disableElement('addNewValueWith', b);
          this.disableElement('updateValueWith', b);
          this.disableElement('id-constraint-with', b);
        }
      }, 100);
    }, 10);
  }

  isAddRgPossible() {
    if(this.isProduct === 'equivalent' || this.isProduct === 'subsumes') {
      return !this.isAttributeRelationWithout();  // count of active ingredient
    }
    return true;
  }

  loadNewRoleGroup() {
    this.listIndexSubRg[this.idRg].push(this.listIndexSubRg[this.idRg].length);
    this.actualAttributeNameWith[this.idRg].push(new Array());
    this.actualMinCarWith[this.idRg].push(new Array());
    this.actualMaxCarWith[this.idRg].push(new Array());
    this.actualValueRangeWith[this.idRg].push(new Array());
    this.listIndexWith[this.idRg].push(new Array());
    this.listSummaryAttributeNamesWith[this.idRg].push(new Array());
    this.listSummaryAttributeCodesWith[this.idRg].push(new Array());
    this.listSummaryOperatorRangeWith[this.idRg].push(new Array());
    this.listSummaryConstraintRangeWith[this.idRg].push(new Array());
    this.listSummaryCodeRangeWith[this.idRg].push(new Array());
    this.listSummaryNameRangeWith[this.idRg].push(new Array());
    this.listNumberAttributesWith[this.idRg].push(new Array());
    this.listSummaryMaxCardinalityWith[this.idRg].push(new Array());
    this.listSummaryMinCardinalityWith[this.idRg].push(new Array());
    this.listSummaryValueRangeWith[this.idRg].push(new Array());
    this.listChangedWith[this.idRg].push(new Array());

    this.listChangeCarWith[this.idRg].push(new Array());
    this.listIndexWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push("")


    this.actualAttributeNameWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualMinCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualMaxCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualValueRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listChangeCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(false);

    this.listSummaryAttributeNamesWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryAttributeCodesWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryNameRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryMaxCardinalityWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryMinCardinalityWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryValueRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');

    this.minCarRg[this.idRg].push(this.listInitWithRoleGroupMinCar[this.idRg]);
    let max = this.listInitWithRoleGroupMaxCar[this.idRg];
    if(this.listInitWithRoleGroupMaxCar[this.idRg].toString() === 'Infinity') {
      max = '*'
    }
    this.maxCarRg[this.idRg].push(max);

    this.listOperatorRangeWith[this.idRg].push(new Array());
    this.listOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listConstraintRangeWith[this.idRg].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listNameRangeWith[this.idRg].push(new Array());
    this.listNameRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listNameRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listCodeRangeWith[this.idRg].push(new Array());
    this.listCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listSelfGroupedAttributesPceCode[this.idRg].push(new Array());
    this.listSelfGroupedAttributesPceName[this.idRg].push(new Array());
    this.listSelfGroupedBoolean[this.idRg].push(new Array());

    this.setSubRg(this.listIndexSubRg[this.idRg].length - 1);
    this.checkMandatoryElementWith(this.listIndexSubRg[this.idRg].length - 1);
  }



  setSubRg(idx : number) {

    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listOperatorRangeWith[this.idRg][this.idSubRg].push(new Array())
    this.listConstraintRangeWith[this.idRg][this.idSubRg].push(new Array())
    this.listNameRangeWith[this.idRg][this.idSubRg].push(new Array())
    this.listCodeRangeWith[this.idRg][this.idSubRg].push(new Array())

    this.checkCardinalityWith('min', idx);
    this.checkCardinalityWith('max', idx);

    setTimeout(() => {
      let element = document.getElementById('tab-subRg' + this.idRg + idx);
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
    }, 10);
    setTimeout(() => {
      if(this.idSubRg.toString() !== idx.toString()) {
        let element = document.getElementById('tab-subRg' + this.idRg + this.idSubRg );
        if (element != null) {
          // @ts-ignore
          element.style.backgroundColor = 'transparent';
        }
      }
      this.idSubRg = idx;
    }, 10);

    setTimeout(() => {
      let element = document.getElementById('rowTableLeftWith' + + this.idRg + this.idSubRg + '0');
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
      this.booleanDecWith = false;
      this.booleanIntWith = false;
      this.booleanNormalWith = false;
      this.getRowTableLeftWith(0);
    }, 100);

    setTimeout(() => {
      for(let j = 0; j < this.actualAttributeNameWith[this.idRg][this.idSubRg].length; j++) {

        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
      this.getRowTableLeftWith(0);
    }, 10);

  }


  getRowTableLeftWith(idx: number) {
    this.showInformationWindowWith = true;
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    this.idRowLeftTableWith = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';
    this.booleanIntWith = false;
    this.booleanDecWith = false;
    this.booleanNormalWith = false;

    if(typeof this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== 'undefined') {
      if ((this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("int(") || this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("dec(")) && this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() !== '0') {
        let elementInput = document.getElementById('label-range');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'none';
        }
      }
      if (this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("int(")) {
        this.booleanIntWith = true;
        this.booleanDecWith = false;
        this.booleanNormalWith = false;
        let v = this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].split("#").join("").split('[[+int').join('').split(')').join("");

        v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.intDefMinWith = s[0];
        } else {
          this.intDefMinWith = 0;
        }
        if (s[1].length > 0) {
          this.intDefMaxWith = s[1];
        } else {
          this.intDefMaxWith = Number.POSITIVE_INFINITY;
        }

        if(this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] === '' || this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] === undefined) {
          this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
          this.updateIntValueWith();
        }

        if(!this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes("..")) {
          this.intFixedWith = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("#").join('');
          this.updateIntFixedValueWith();
        } else {
          let v = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("#").join("").split('[[+int').join('').split(')').join("");
          v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("").split("<").join("");
          let s = v.split("..");
          if (s[0].length > 0) {
            this.intMinWith = s[0];
          }
          if (s[1].length > 0) {
            this.intMaxWith = s[1];
          }
          this.updateIntValueWith();
        }
      } else if (this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("dec(")) {
        this.booleanIntWith = false;
        this.booleanDecWith = true;
        this.booleanNormalWith = false;
        let v = this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].split("#").join("").split('[[+dec').join('').split(')').join("");
        v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.decDefMinWith = 0;
        } else {
          this.decDefMinWith = 0;
        }
        if(s[1] !== undefined) {
          if (s[1].length > 0) {
            this.decDefMaxWith = s[1];
          } else {
            this.decDefMaxWith = Number.POSITIVE_INFINITY;
          }
        } else {
          this.decDefMaxWith = Number.POSITIVE_INFINITY;
        }

        if(this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] === '' || this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] === undefined) {
          this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
          this.updateDecValueWith();
        }

        if(!this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes("..")) {
          this.decFixedWith = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("#").join('');
          this.updateDecFixedValueWith();
        } else {
          let v = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("#").join("").split('[[+dec').join('').split(')').join("");
          v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("").split("<").join("");
          let s = v.split("..");
          if (s[0].length > 0) {
            this.decMinWith = s[0];
          }
          if (s[1].length > 0) {
            this.decMaxWith = s[1];
          }
          this.updateDecValueWith();
        }
      } else {
        this.booleanIntWith = false;
        this.booleanDecWith = false;
        this.booleanNormalWith = true;

        let elementInput = document.getElementById('label-range');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'inline';
        }

        setTimeout(() => {
          let b = false;
          if(this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
            if(this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].toUpperCase() !== 'count' && this.isAttributeRelationWith()) {
              b = true;
            }
          }
          if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
            this.disableAttributeRelationWithProduct(b);
          }
        }, 1500);



        this.fillTableRightWith();

      }
      if(!this.listChangeCarWith[this.idRg][this.idSubRg][idx]) {
        this.actualMinCarWith[this.idRg][this.idSubRg][idx] = this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]];
        this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg][idx] =  this.actualMinCarWith[this.idRg][this.idSubRg][idx].toString();
        let max = this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]];
        if(max.toString() === 'Infinity') {
          max = '*';
        }
        this.actualMaxCarWith[this.idRg][this.idSubRg][idx] = max;
        this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg][idx]] = max.toString();
      }

      this.checkCardinalityWith('min', idx);
      this.checkCardinalityWith('max', idx);
    }

    this.listAllCodes = new Array();
    this.listAllNames = new Array();
    this.templateGenerationService.getAttributeValueRequestFilterLimitFSN(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]], this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith], '380000').then((response: any) => {
      const value = response.values;
      if (typeof value !== 'undefined') {
        for (let i = 0; i < value.length; i++) {
          if (!this.listAllCodes.includes(value[i].code)) {
            this.listAllCodes.push(value[i].code);
            this.listAllNames.push(value[i].fsn);
          }
        }
      }
    })

  }


  disableAttributeRelationWithProduct(b: boolean) {
    if(this.booleanNormalWith) {
      this.disableElement('id-value-with000', b);
      this.disableElement('operator000', b);
      this.disableElement('constraint000', b);
      this.disableElement('addNewValueWith', b);
      this.disableElement('updateValueWith', b);
      this.disableElement('id-constraint-with', b);
      this.disableElement('id-value0', b);
      this.disableElement('id-value-dummy', b);
    } else if(this.booleanDecWith) {
      this.disableElement('inlineRadio1DecW', b);
      this.disableElement('inlineRadio2DecW', b);
      this.disableElement('attributeDecMinWith', b);
      this.disableElement('attributeDecMaxWith', b);
      this.disableElement('attributeFixedDecWith', b);
    } else {
      this.disableElement('inlineRadio1IntW', b);
      this.disableElement('inlineRadio2IntW', b);
      this.disableElement('attributeIntMinWith', b);
      this.disableElement('attributeIntMaxWith', b);
      this.disableElement('attributeFixedIntWith', b);
    }
  }

  fillTableRightWith(dummy?: string, dummy2?: string) {
    if(this.booleanNormalWith) {
      let s = this.popupInformationConstraintWith().toString().split('}').join('').split(" |").join("|");
      let s_prepro = ''
      if(dummy2 === undefined) {
        s_prepro = this.popupInformationConstraintWith().toString().split('}').join('').split(" |").join("|").split(" ").join("");
      } else {
        return;
      }

      if(!this.listChangedWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] || (dummy === 'start')) {
        if (!s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
          this.preprocessingRangeWithOperator(s);
        } else if (s_prepro.includes("|OR") && !s_prepro.includes("|MINUS")) {
          this.preprocessingRangeSingleWithOperator(s, 'OR');
        } else if (!s_prepro.includes("|OR") && s_prepro.includes("|MINUS")) {
          this.preprocessingRangeSingleWithOperator(s, 'MINUS');
        } else {
          this.preprocessingRangeMultipleWithOperator(s);

        }
      }
      let result = '';
      let lOperator = new Array();
      let lConstraint = new Array();
      let lName = new Array();

      if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
        if (typeof this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== 'undefined') {
          for (let j = 0; j < this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; j++) {
            let operator = this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];
            let constraint = this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];
            let code = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];
            let name = this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];

            if (typeof operator !== 'undefined' && typeof name !== 'undefined') {
              if (result.includes(' |')) {
                result = result + operator + constraint + code.split(" ").join("") + '|' + name + '|';
              } else {
                result = result + operator + constraint + code.split(" ").join("") + ' |' + name + '|';
              }
            }
          }
          // result = result.split(" |").join("|")
          // result = result.split("|").join(" |")
          this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = result;
          this.correctInputWithout = true;
        }
      }
      for (let j = 0; j < this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; j++) {
        let operator = this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];
        let constraint = this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];
        let name = this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][j];

        lOperator.push(operator);
        lConstraint.push(constraint);
        lName.push(name);
      }
      this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lOperator;
      this.listConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lConstraint;
      this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lName;
      this.correctInputWith= true;
    }
  }


  preprocessingRangeWithOperator(input: any) {
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();

    lOperator.push('');
    if(input.includes("<")) {
      lConstraint.push(input.substring(0,input.lastIndexOf("<")+1).split(" ").join(""));
      lCode.push(input.substring(input.lastIndexOf("<")+1, input.indexOf("|")).split(" ").join(""));
      lName.push(input.substring(input.indexOf("|")+1, input.lastIndexOf("|")));
    } else {
      lConstraint.push('');
      lCode.push(input.substring(0, input.indexOf("|")).split(" ").join(""));
      lName.push(input.substring(input.indexOf("|")+1, input.lastIndexOf("|")));
    }
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lOperator;
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lConstraint;
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lCode;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lName;
  }

  preprocessingRangeMultipleWithOperator(input: any) {
    let or = input.split('OR');
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();

    for(let j = 0; j < or.length; j++) {
      if (!or[j].includes('MINUS')) {
        if (j === 0) {
          lOperator.push("");
        } else {
          lOperator.push('OR');
        }
        if (or[j].includes("<")) {
          lConstraint.push(or[j].substring(0, or[j].lastIndexOf("<") + 1));
          lCode.push(or[j].substring(or[j].lastIndexOf("<") + 1, or[j].indexOf("|")).split(" ").join(""));
          lName.push(or[j].substring(or[j].indexOf("|") + 1, or[j].lastIndexOf("|")));
        } else {
          lConstraint.push('');
          lCode.push(or[j].substring(0, or[j].indexOf("|")));
          lName.push(or[j].substring(or[j].indexOf("|") + 1, or[j].lastIndexOf("|")));
        }
      } else {
        let minus = or[j].split('MINUS');
        for (let k = 0; k < minus.length; k++) {
          if (j === 0) {
            lOperator.push("");
          } else if (k === 0) {
            lOperator.push('OR');
            lOperator.push('MINUS');
          }

          if (minus[k].includes("<")) {
            lConstraint.push(minus[k].substring(0, minus[k].lastIndexOf("<") + 1));
            lCode.push(minus[k].substring(minus[k].lastIndexOf("<") + 1, minus[k].indexOf("|")).split(" ").join(""));
            lName.push(minus[k].substring(minus[k].indexOf("|") + 1, minus[k].lastIndexOf("|")));
          } else {
            lConstraint.push('');
            lCode.push(minus[k].substring(0, minus[k].indexOf("|")));
            lName.push(minus[k].substring(minus[k].indexOf("|") + 1, minus[k].lastIndexOf("|")));
          }

        }
      }
    }
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lOperator;
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lConstraint;
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lCode;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lName;
  }


  preprocessingRangeSingleWithOperator(input: any, operator: String) {
    let or = input.split(operator);
    let lOperator = new Array();
    let lConstraint = new Array();
    let lCode = new Array();
    let lName = new Array();
    for(let j = 0; j < or.length; j++) {
      if(j === 0) {
        lOperator.push("");
      } else {
        lOperator.push(operator);
      }
      if(or[j].includes("<")) {
        lConstraint.push(or[j].substring(0,or[j].lastIndexOf("<")+1).split(" ").join(""));
        lCode.push(or[j].substring(or[j].lastIndexOf("<")+1, or[j].indexOf("|")).split(" ").join(""));
        lName.push(or[j].substring(or[j].indexOf("|")+1, or[j].lastIndexOf("|")));
      } else {
        lConstraint.push('');
        lCode.push(or[j].substring(0, or[j].indexOf("|")));
        lName.push(or[j].substring(or[j].indexOf("|")+1, or[j].lastIndexOf("|")));
      }

    }
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lOperator;
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lConstraint;
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lCode;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = lName;
  }

  getAttributesWith() {
    let list = new Array();
    if(this.listInitWithAttributeName[this.idRg] != null && this.actualAttributeNameWith[this.idRg][this.idSubRg] != null) {
      for(let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
        if(this.listInitWithAttributeName[this.idRg][i] !== undefined) {
          let counter = this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].filter((x: any) => x.split(" ").join("") === this.listInitWithAttributeName[this.idRg][i].split(" ").join("")).length;

          if(this.listInitWithMaxCar[this.idRg][i] !== undefined) {
            if(this.listInitWithMaxCar[this.idRg][i].toString() === 'Infinity') {
              list.push(this.listInitWithAttributeName[this.idRg][i]);
            } else if(Number.parseInt(String(counter)) < Number.parseInt(this.listInitWithMaxCar[this.idRg][i])) {
              list.push(this.listInitWithAttributeName[this.idRg][i]);
            }
          }
        }

      }
    }
    for(let i = 0; i < list.length; i++) {
      if(list[i].substring(list[i].length-1) === " ") {
        list[i] = list[i].substring(0, list[i].length-1);
      }
    }
    return [...new Set(list)]; // remove duplicates
  }

  loadAttributeDataWith(idx: number) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
    if(this.isMandatoryWithout()) {
      this.listChangedWith[this.idRg][this.idSubRg].push(false);
    }
    setTimeout(() => {
      this.idRowLeftTableWith = idx;

      if (!this.listInitWithAttributeName[this.idRg].includes(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith])) {
        this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
        this.showInformationWindowWith = false;
      } else {
        for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
          if (this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] === this.listInitWithAttributeName[this.idRg][i]) {
            this.indexWith = i;
            // this.listLoadDataAttributeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = true;
            this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = i;

            let id = -1;
            for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
              if (this.listInitWithAttributeName[this.idRg][i] === this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]) {
                id = i;
                break;
              }
            }
            this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
            this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listInitWithAttributeCode[this.idRg][id];
            this.isWith = true;

            this.getRowTableLeftWith(this.idRowLeftTableWith)
            this.fillTableRightWith();
            this.showInformationWindowWith = true;

            setTimeout(() => {
              let element = document.getElementById('btn-add-row-with-left');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
            }, 10);

          }
        }
      }
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].length; j++) {
        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);


  }

  addRowTableLeftWith() {
    setTimeout(() => {
      let element = document.getElementById('btn-add-row-with-left');
      if (element != null) {
        // @ts-ignore
        element.disabled = true;
      }
    }, 10);

    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listNumberAttributesWith[this.idRg][this.idSubRg].push(0);
    this.actualAttributeNameWith[this.idRg][this.idSubRg].push('');
    this.actualMinCarWith[this.idRg][this.idSubRg].push("");
    this.actualMaxCarWith[this.idRg][this.idSubRg].push("");
    this.actualValueRangeWith[this.idRg][this.idSubRg].push("");

    this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].push("");
    this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].push("");
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg].push("");
    this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg].push("");
    this.listSummaryValueRangeWith[this.idRg][this.idSubRg].push("");

    this.listOperatorRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
    // this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = new Array();
    this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';

    this.listConstraintRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';

    this.listNameRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.listNameRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
    this.listNameRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';

    this.listCodeRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.listCodeRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
    this.listCodeRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';

    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';
    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWith[this.idRg][this.idSubRg].length; j++) {
        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);
  }

  deleteRowTableLeftWith() {
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listAllNamesRangeWith = new Array();
    this.listAllCodesRangeWith = new Array();
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryValueRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listNumberAttributesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualAttributeNameWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualMinCarWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualMaxCarWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualValueRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    // this.listLoadDataAttributeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listOperatorRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listConstraintRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listNameRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);

    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';
    this.listIndexWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);

    if(this.idRowLeftTableWith > 0) {
      this.getRowTableLeftWith(this.idRowLeftTableWith - 1);
    }
    else {
      setTimeout(() => {
        let tables = document.querySelectorAll("table");
        for (let i = 0; i < tables.length; i++) {
          let trs = document.querySelectorAll("tr");
          for (let j = 0; j < trs.length; j++) {
            let element = trs[j];
            if (element != null) {
              element.style.background = 'transparent';
              if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + '0') {
                element.style.background = 'lightblue';
              }
            }
          }
        }
        this.getRowTableLeftWith(0);
      }, 10);
    }
    setTimeout(() => {
      let elementInput = document.getElementById('btn-add-row-with-left');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = false;
      }
    }, 10);
  }

  idBtnAddWithDisabled() {
    if(this.listNumberAttributesWith[this.idRg][this.idSubRg].length > this.actualAttributeNameWith[this.idRg][this.idSubRg].length) {
      return true;
    }
    let l = this.getAttributesWith()
    if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
      let s = this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
      if (s === undefined) {
        return true;
      }
      if (this.listNumberAttributesWith[this.idRg][this.idSubRg].length < 1) {
        return false;
      }
      if (typeof s !== 'undefined' && l.length > 0) {
        for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
          if (this.listInitWithAttributeName[this.idRg][i] === s) {
            return false;
          }
        }
        return true;
      }
    }
    return true;
  }

  idBtnDeleteWithDisabled() {
    if(this.listNumberAttributesWith[this.idRg].length < 1) {
      return true;
    }
    if(typeof this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== 'undefined') {
      if(this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() === '1') {
        return true;
      }
    }
    return false;
  }

  popupInformationDefinitionWith() {
    let info = '';
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      for (let i = 0; i < this.attributeDefinitionCMCode.length; i++) {
        if (this.attributeDefinitionCMCode[i].toString() === this.listInitWithAttributeCode[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]) {
          info = this.attributeDefinitionCMDefinition[i];
        }
      }
    }
    if(info === '' || info === '-') {
      info = '----';
    }
    return info;
  }

  popupInformationMinCarWith() {
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      return this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]
    } else {
      return "";
    }
  }

  popupInformationMaxCarWith() {
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      return this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]
    } else {
      return "";
    }
  }

  popupInformationConstraintWith() {
    let ref = this.conceptModelTemplate.substring(this.conceptModelTemplate.lastIndexOf(":") + 1);
    let attr = ref.split(",");

    let value = '';
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      for (let i = 0; i < attr.length; i++) {
        if (attr[i].includes(this.listInitWithAttributeCode[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]])) {
          if (this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== '0') {
            value = this.listChangedConstraintWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]];
            if((this.isProduct === 'equivalent' || this.isProduct === 'subsumes') && this.isAttributeRelationWith() && value.includes("|")) {
              value = value.replace("<<", "");
            }
          } else {
            value = attr[i].split("=")[1].split("[[+scg(").join("").split("[[+id(").join("").split(")]]").join("").split("}").join("").split(".|").join("").split("||").join("");
          }
          if(value != null) {
            if (value.includes('+int')) {
              value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split("+int(").join("").split(")").join("").split(".|").join("").split("||").join("").split(".").join("");;
            } else if (value.includes('+dec')) {
              value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split(".|").join("").split("||").join("");
              if(value[value.length - 1] === '.') {
                value = value.substring(0, value.length - 1);
              }
            }
          }
          return value;
        }
      }
    }
    return "----";
  }

  isAttributeRelationWith() {
    if(typeof this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] === 'undefined') {
      return false;
    } else if(this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] === '0') {
      return false;
    } else if(this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] == null) {
      return false;
    } else if(this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== '0') {
      return true;
    } else {
      return false;
    }
  }

  isMandatoryWith() {
    if(this.isAttributeRelationWith()) {
      return false;
    }
    if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
      for(let i = 0; i < this.listInitWithAttributeName[0].length; i++) {
        if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split(" ").join("") === this.listInitWithAttributeName[0][i].split(" ").join("")) {
          if(this.listInitWithMinCar[0][i].toString() === '1') {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;
  }

  checkCardinalityWith(id: string, idx: number) {
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    // invalidInputMaxCarWith

    if (id === 'min' && this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== '' && typeof this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== 'undefined') {
      if (isNaN(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) ||
        (Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) > Number.parseInt(this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]]) ||
          Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) < Number.parseInt(this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]]))) {
        this.invalidInputMinCarWith = true;
        setTimeout(() => {
          this.actualMinCarWith[this.idRg][this.idSubRg][idx] = '';
        }, 50);

      } else {
        this.invalidInputMinCarWith = false;
      }
    } else if (id === 'max' && this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== '' && typeof this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== 'undefined') {
      if(this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== '*') {
        if ((isNaN(this.actualMaxCarWith[this.idRg][this.idSubRg][idx]) || (Number.parseInt(this.actualMaxCarWith[this.idRg][this.idSubRg][idx]) > Number.parseInt(this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]])))) {
          this.invalidInputMaxCarWith = true;
          setTimeout(() => {
            this.actualMaxCarWith[this.idRg][this.idSubRg][idx] = '';
          }, 50);
        } else if (this.actualMinCarWith[this.idRg][this.idSubRg][idx].toString() === '0' && this.actualMaxCarWith[this.idRg][this.idSubRg][idx].toString() === '0') {
          this.invalidInputCarWith = true;
        } else {
          this.invalidInputMaxCarWith = false;
          this.invalidInputCarWith = false;
        }
      } else if (this.actualMaxCarWith[this.idRg][this.idSubRg][idx].toString() === '*' && this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() !== 'Infinity') {
        this.invalidInputMaxCarWith = true;
        setTimeout(() => {
          this.actualMaxCarWith[this.idRg][this.idSubRg][idx] = '';
        }, 50);
      } else {
        this.invalidInputMaxCarWith = false;
        this.invalidInputCarWith = false;
      }
      this.checkInputCarWith(idx);
    }

    if(!this.invalidInputMaxCarWith && !this.invalidInputMinCarWith) {
      this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg][idx] = this.actualMinCarWith[this.idRg][this.idSubRg][idx];
      this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg][idx] = this.actualMaxCarWith[this.idRg][this.idSubRg][idx];
    }
  }

  checkInputCarWith(idx: number) {
    let maxCar = this.actualMaxCarWith[this.idRg][this.idSubRg][idx];
    if(maxCar === '*') {
      maxCar = Number.POSITIVE_INFINITY;
    } else {
      maxCar = Number.parseInt(maxCar)
    }

    if (maxCar !== '' && this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== '') {
      if (maxCar >= Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx])) {
        this.actualCheckCarWith = true;
      } else {
        if (this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === '' || this.actualMinCarWith[this.idRg][this.idSubRg][idx] === '' || this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === null || this.actualMinCarWith[this.idRg][this.idSubRg][idx] === null || typeof this.actualMinCarWith[this.idRg][this.idSubRg][idx] === 'undefined' || typeof this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === 'undefined') {
          this.actualCheckCarWith = true;
        } else {
          this.actualCheckCarWith = false;
        }
      }
    } else {
      this.actualCheckCarWith = true;
    }
  }

  resetCarWith(idx: number) {
    this.listChangeCarWith[this.idRg][this.idSubRg][idx] = true;
    this.clickRow(this.idRg, this.idSubRg, idx);
  }

  getRangeWith(idx: number) {
    if(typeof this.listSummaryValueRangeWith[this.idRg][this.idSubRg][idx] !== 'undefined') {
      return this.listSummaryValueRangeWith[this.idRg][this.idSubRg][idx].split('MINUS').join(' MINUS ').split('OR').join(' OR ').split('  |').join(" |");
    } else {
      return ""
    }
  }

  keypressGetAttributeValueWith(idx: number) {
    const arr = this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx] || [];
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();
    this.correctInputWithout = false;

    if(Number.parseInt(arr)) {
      this.keypressEnterWith(idx);
    }

    if (arr.length > 2) {
      if (this.oldInputWith !== this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx] && !Number(this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx])) {

        let elementInput = document.getElementById('spinnerWith' + idx);
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'inline';
        }

        this.templateGenerationService.getAttributeValueRequestFilterLimitFSN(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]], this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx], '1000').then((response: any) => {
          const value = response.values;
          if(typeof value !== 'undefined') {
            for (let i = 0; i < value.length; i++) {
              if(!this.listAllCodesRangeWith.includes(value[i].code)) {
                this.listAllCodesRangeWith.push(value[i].code);
                this.listAllNamesRangeWith.push(value[i].fsn);
              }
            }
          }
          setTimeout(() => {
            let elementInput = document.getElementById('spinnerWith' + idx);
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          }, 500);
        })
      } else {
        this.listAllCodesRangeWith = new Array();
        this.listAllNamesRangeWith = new Array();
      }
    }
    return "";
  }

  keypressEnterWith(idx: number) {
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    let s = '';
    if (s != this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx]) {

      let elementInput = document.getElementById('spinnerWith' + idx);
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }

      this.templateGenerationService.getAttributeValueRequestFilterLimitFSN(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]], this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx], '100000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesRangeWith.includes(value[i].code)) {
              this.listAllCodesRangeWith.push(value[i].code);
              this.listAllNamesRangeWith.push(value[i].fsn);
              this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx] = value[i].fsn;
            }
          }
          setTimeout(() => {
            let elementInput = document.getElementById('spinnerWith' + idx);
            if (elementInput != null) {
              // @ts-ignore
              elementInput.style.display = 'none';
            }
          }, 500);
        }
      })
    }
  }




  getPossibleValuesWith(idx: number) {
    return this.listAllNamesRangeWith;
  }




  changeFieldWith(idx: number) {
    let id = -1;
    for(let i = 0; i < this.listAllNamesRangeWith.length; i++) {
      if(this.listAllNamesRangeWith[i] === this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx]) {
        id = i;
        break;
      }
    }
    this.listCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][idx] = this.listAllCodesRangeWith[id]
    if(id !== -1) {

    }

    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    let elementInput = document.getElementById('id-value-with' + this.idRg + this.idSubRg + idx);
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
    }

    let elementDummy = document.getElementById('id-value-with-dummy' + this.idRg + this.idSubRg + idx);
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';
      if(elementDummy.style.display === 'inline') {
        this.correctInputWith = true;
      }
    }
    elementInput = document.getElementById('addNewValueWith');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    elementInput = document.getElementById('updateValueWith');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
  }

  startFieldValueWith(idx: number) {
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    let elementInput = document.getElementById('id-value-with' + this.idRg  + this.idSubRg + idx);
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = 'inline';
    }

    let elementDummy = document.getElementById('id-value-with-dummy' + this.idRg + this.idSubRg + idx);
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = 'none';
      if (elementDummy.style.display === 'none') {
        // this.listNameRangeWith[this.idRg][idx] = '';
        this.correctInputWith = false;
      }
    }

  }

  public listAllCodes = new Array();
  public listAllNames = new Array();


  getIdRowTableRightWith(idx: number) {
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    this.idRowTableRightWith = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableRightWith' + this.idRowTableRightWith) {
            element.style.background = 'lightblue';
          }
          if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    if(this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length > 0) {
      for(let i = 0; i < this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
        if(i !== this.idRowTableRightWith && this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i].length > 0) {
          let elementInput = document.getElementById('deleteValueWith');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = false;
          }
          return null;
        }
      }
    }
    let elementInput = document.getElementById('deleteValueWith');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }
    return null;
  }

  addNewLineWith() {
    this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push("");
    this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push("");
    this.listConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push("");
    this.updateValueWith();
    this.listAllCodesRangeWith[this.idRg] = new Array();
    this.listAllNamesRangeWith[this.idRg] = new Array();
  }


  deleteValueWith() {
    this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].splice(this.idRowTableRightWith, 1);
    this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].splice(this.idRowTableRightWith, 1);
    this.listConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].splice(this.idRowTableRightWith, 1);
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].splice(this.idRowTableRightWith, 1);

    if (this.idRowTableRightWith === 0) {
      this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][0] = '';
    }

    this.updateValueWith();

    if(this.idRowTableRightWith > 0) {
      this.getIdRowTableRightWith(this.idRowTableRightWith - 1);
    }
    else {
      setTimeout(() => {
        let tables = document.querySelectorAll("table");
        for (let i = 0; i < tables.length; i++) {
          let trs = document.querySelectorAll("tr");
          for (let j = 0; j < trs.length; j++) {
            let element = trs[j];
            if (element != null) {
              element.style.background = 'transparent';
              if (element.id === 'rowTableRightWith0') {
                element.style.background = 'lightblue';
              }
            }
          }
        }
        this.getIdRowTableRightWith(0);
      }, 10);
    }
  }

  idBtnDeleteValueWithDisabled() {
    if(this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length > 0) {
      for(let i = 0; i < this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
        if(i !== this.idRowTableRightWith && this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length > 0) {
          return false;
        }
      }
    }
    return true;
  }

  updateValueWith() {
    let value = '';
    let id = -1;

    for(let i = 0; i < this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
      if(i === 0) {
        value = value + this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "@";
        value = value + this.listConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "@";
        value = value + this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "$";
        this.actualValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = value;

      } else if(this.listOperatorRangeWithout[i] !== "") {
        value = value + this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "@";
        value = value + this.listConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "@";
        value = value + this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] + "$";
        this.actualValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = value;
      }
    }
    this.getAttributeRangeConstraintWith(value);
    this.listChangedWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = true;
  }



  getAttributeRangeConstraintWith(value: string) {
    if(typeof this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== 'undefined') {
      let s = value.split("$");
      for(let i = 0; i < s.length-1; i++) {
        let result = '';
        let operator = '';
        let constraint = '';
        let code = '';
        let name = '';
        let id = -1;

        let a = s[i].split("@");
        if (typeof a[0] !== 'undefined') {
          operator = a[0];
        }
        if (typeof a[1] !== 'undefined') {
          constraint = a[1].replace('undefined', '');
        }
        if (typeof a[2] !== 'undefined') {
          name = a[2];

          if(name.length > 0) {
            for (let k = 0; k < this.listAllNamesRangeWith.length; k++) {
              if (this.listAllNamesRangeWith[k] === name) {
                id = k;
                break
              }
            }
            if(this.listAllCodesRangeWith[id] !== undefined) {
              code = this.listAllCodesRangeWith[id].split(" ").join("");
            }

            if(code === '') {
              code = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            }
            this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] = name;
            this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] = code;
            this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] = operator;
            this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] = constraint;
          }

          let backup = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
          this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
          for (let i = 0; i < this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
            operator = this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            constraint = this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            code = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            name = this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            if (typeof code !== 'undefined') {
              result = operator + constraint + code + " |" + name + "|";
              result = result.split("undefined").join("");
              if (!this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes(result) && name !== 'undefined') {
                this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + result;
              }
            } else {
              this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + backup;
            }

          }

          for(let i  = 0; i < this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
            code = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            if(typeof code === 'undefined') {
              this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] = backup.split('|')[0].split('<<').join('');
            }
          }

          this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
          // this.isUpdate = true;
          for (let i = 0; i < this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
            operator = this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            constraint = this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            code = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];
            name = this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i];

            result = operator + constraint + code + " |" + name + "|";
            if (!this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes(result) && name !== 'undefined') {
              this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + result;
            }
          }
        }
      }
    }
  }

  checkInputRightTableWith() {
    if(typeof this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== 'undefined') {
      for(let i = 1; i < this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
        if (this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] !== undefined && this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i]) {
          if ((this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i].length === 0 || this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i].length === 0)) {
            if (this.listOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] === '' && this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] === '') {
              let elementInput = document.getElementById('addNewValueWith');
              if (elementInput != null) {
                // @ts-ignore
                elementInput.disabled = true;
              }
              elementInput = document.getElementById('updateValueWith');
              if (elementInput != null) {
                // @ts-ignore
                elementInput.disabled = true;
              }
            }
            return true;
          } else {
            for (let i = 0; i < this.listAllNamesRangeWith.length; i++) {
              if(this.listAllNamesRangeWith[i] === this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][this.idRowTableRightWith]) {
                return false;
              }
            }
          }
        }
      }
      if(this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length === 1 && this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][0] === '') {
        return true;
      }
      if(this.idRowTableRightWith === -1) {
        this.idRowTableRightWith = 0;
      }
      for (let i = 0; i < this.listAllNamesRangeWith.length; i++) {
        if(this.listAllNamesRangeWith[i] === this.listNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][this.idRowTableRightWith]) {
          return false;
        }
      }
      // return true;
    }
    return null;
  }

  getInputIntWith(value: string) {
    // if(value === 'fixed' && this.inputIdIntWith === 'range') {
    //   this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.intMaxWithout = '';
    //   this.intMinWithout = '';
    // }
    // if(value === 'range' && this.inputIdIntWith === 'fixed') {
    //   this.intFixedWith = Math.round(Number.parseFloat(this.intFixedWith)).toString();
    //   this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '' ;
    //   this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.intFixedWith = '';
    // }

    this.inputIdIntWith = value;
    this.intMaxWithout = '';
    this.intMinWithout = '';
  }

  updateIntValueWith() {
    if(this.intMaxWith === null || this.intMaxWith === undefined) {
      this.intMaxWith = '';
    }
    if(this.intMinWith === '' || this.intMinWith === null) {
      this.intMinWith = '0';
    }
    if(this.intMaxWith.length > 0) {
      this.intMaxWith = Math.round(Number.parseFloat(this.intMaxWith)).toString();
    }
    this.intMinWith = Math.round(Number.parseFloat(this.intMinWith)).toString();

    let v = '';
    if(this.intMaxWith === '') {
      v = ">#" + this.intMinWith + "..";
    } else {
      v = ">#" + this.intMinWith + ".." + "<#" + this.intMaxWith;
    }

    this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';

    setTimeout(() => {
      let elementRange = document.getElementById('inlineRadio1IntW');
      if (elementRange != null) {
        //@ts-ignore
        elementRange.checked = true;
        this.getInputIntWith('range');
      }
    }, 10);
  }

  checkInputIntegerRightTableWith() {
    if((this.intMinWith === '' || this.intMinWith === null || this.intMinWith === undefined) && (this.intMaxWith === '' || this.intMaxWith === null || this.intMaxWith === undefined)) {
      return true;
    } else {
      if(this.intMinWith === '' || this.intMinWith === null || this.intMinWith === undefined) {    // min leer = diabled
        return true;
      } else {                                                                                              // min nicht leer
        if(this.intMaxWith === '' || this.intMaxWith === null || this.intMaxWith === undefined) {  // max leer
          return Number.parseFloat(this.intMinWith) < this.intDefMinWith;
        } else {                                                                                            // max leer
          return (Number.parseFloat(this.intMinWith) <= this.intDefMinWith) ||  (Number.parseFloat(this.intMaxWith) >= this.intDefMaxWith) || Number.parseFloat(this.intMinWith) > Number.parseFloat(this.intMaxWith);
        }
      }
    }
  }

  updateIntFixedValueWith() {
    this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.intFixedWith;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.intFixedWith;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';

    setTimeout(() => {
      let elementRange = document.getElementById('inlineRadio2IntW');
      if (elementRange != null) {
        //@ts-ignore
        elementRange.checked = true;
        this.getInputIntWith('fixed');
      }
    }, 10);
  }


  isDisabledInputFixedWith() {
    if(this.intFixedWith === '' || this.intFixedWith === null || this.intFixedWith === undefined) {
      return true;
    } else if(this.intFixedWith !== undefined) {
      return (Number.parseFloat(this.intFixedWith) < this.intDefMinWith) ||  (Number.parseFloat(this.intFixedWith) > this.intDefMaxWith)
    } else {
      return true;
    }
  }

  getInputDecWith(value: string) {
    // if(value === 'fixed' && this.inputIdDecWith === 'range') {
    //   this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.decMaxWithout = '';
    //   this.decMinWithout = '';
    // }
    // if(value === 'range' && this.inputIdDecWith === 'fixed') {
    //   this.decFixedWith = Math.round(Number.parseFloat(this.decFixedWith)).toString();
    //   this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    //   this.decFixedWith = '';
    // }

    this.inputIdDecWith = value;
    this.decMaxWithout = '';
    this.decMinWithout = '';
  }

  updateDecValueWith() {
    if(this.decMinWith === '' || this.decMinWith === null) {
      this.decMinWith = '0';
    }
    let v = '';
    if(this.decMaxWith === '') {
      if(!this.decMinWith.toString().includes('.')) {
        this.decMinWith = this.decMinWith + '.0';
      }
      v = ">#" + this.decMinWith + "..";
    } else {
      if(!this.decMaxWith.toString().includes('.')) {
        this.decMaxWith = this.decMaxWith + '.0';
      }
      if(!this.decMinWith.toString().includes('.')) {
        this.decMinWith = this.decMinWith + '.0';
      }
      v = ">#" + this.decMinWith + ".." + "<#" + this.decMaxWith;
    }
    this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = v;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';

    setTimeout(() => {
      let elementRange = document.getElementById('inlineRadio1DecW');
      if (elementRange != null) {
        //@ts-ignore
        elementRange.checked = true;
        this.getInputDecWith('range');
      }
    }, 10);

  }

  checkInputDecRightTableWith() {
    if((this.decMinWith === '' || this.decMinWith === null || this.decMinWith === undefined) && (this.decMaxWith === '' || this.decMaxWith === null || this.decMaxWith === undefined)) {
      return true;
    } else {
      if(this.decMinWith === '' || this.decMinWith === null || this.decMinWith === undefined) {    // min leer = diabled
        return true;
      } else {                                                                                              // min nicht leer
        if(this.decMaxWith === '' || this.decMaxWith === null || this.decMaxWith === undefined) {  // max leer
          return Number.parseFloat(this.decMinWith) < this.decDefMinWith;
        } else {                                                                                            // max leer
          return (Number.parseFloat(this.decMinWith) < this.decDefMinWith) ||  (Number.parseFloat(this.decMaxWith) > this.decDefMaxWith) || Number.parseFloat(this.decMinWith) > Number.parseFloat(this.decMaxWith);
        }
      }
    }
  }

  checkInputDecFixedRightTableWith() {
    if(this.decFixedWith === '' || this.decFixedWith === null || this.decFixedWith === undefined) {
      return true;
    } else if(this.decFixedWith !== undefined) {
      return (Number.parseFloat(this.decFixedWith) < this.decDefMinWith) ||  (Number.parseFloat(this.decFixedWith) > this.decDefMaxWith)
    } else {
      return true;
    }
  }

  updateDecFixedValueWith() {
    this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';

    if(!this.decFixedWith.toString().includes('.')) {
      this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith + '.0';
      this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith + '.0';
    }


    setTimeout(() => {
      let elementRange = document.getElementById('inlineRadio2DecW');
      if (elementRange != null) {
        //@ts-ignore
        elementRange.checked = true;
        this.getInputDecWith('fixed');
      }
    }, 10);
  }


  // --------------- SUMMARY -------------------

  loadSummary() {
    let elementTab = document.getElementById('tab-summary');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }
    if(this.idLastTab !== 'tab-summary') {
      elementTab = document.getElementById(this.idLastTab);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'transparent';
      }
    }
    if(this.idLastTab !== 'tab-summary') {
      this.idLastTab = 'tab-summary';
      this.idActualTab = 'tab-summary';
    }
  }

  checkMinCarRg(row: number, subrg: number) {
    let min = Math.round(this.minCarRg[row][subrg]);
    if(this.minCarRg[row][subrg] !== '' && min.toString() !== 'NaN') {
      this.minCarRg[row][subrg] = Math.round(this.minCarRg[row][subrg]);
    } else if (this.minCarRg[row][subrg] !== '*') {
      this.minCarRg[row][subrg] = '';
    }
  }

  checkMaxCarRg(row: number, subrg: number) {
    let max = Math.round(this.maxCarRg[row][subrg]);
    if(this.maxCarRg[row][subrg] !== '' &&  max.toString() !== 'NaN') {
      this.maxCarRg[row][subrg] = Math.round(this.maxCarRg[row][subrg]);
    } else if (this.maxCarRg[row][subrg] !== '*') {
      this.maxCarRg[row][subrg] = '';
    }
  }

  checkCardinalityRg(id: string, idx: number, subrg: number) {
    if (id === 'min' && this.minCarRg[idx][subrg] !== '' && typeof this.minCarRg[idx][subrg] !== 'undefined') {
      if (isNaN(this.minCarRg[idx][subrg]) || Number.parseInt(this.minCarRg[idx][subrg]) > Number.parseInt(this.listInitWithMaxCar[idx][subrg])) {
        this.invalidRgCar = true;
      }
    } else if (id === 'max' && this.maxCarRg[idx][subrg] !== '' && typeof this.maxCarRg[idx][subrg] !== 'undefined') {
      if ((isNaN(this.maxCarRg[idx][subrg]) && this.maxCarRg[idx][subrg] !== '*') || Number.parseInt(this.maxCarRg[idx][subrg]) > Number.parseInt(this.listInitWithMaxCar[idx][subrg])) {
        this.maxCarRg[idx][subrg] = '';
        this.invalidRgCar = true;
      }
    }
    this.checkInputCarRg(idx, subrg);
  }

  checkInputCarRg(idx: number, subrg: number) {
    let maxCar = this.maxCarRg[idx][subrg];
    if(maxCar === '*') {
      maxCar = Number.POSITIVE_INFINITY;
    } else {
      maxCar = Number.parseInt(maxCar)
    }

    let minCar = this.minCarRg[idx][subrg];
    if(minCar === '*') {
      minCar = Number.POSITIVE_INFINITY;
    } else {
      minCar = Number.parseInt(minCar)
    }

    if (maxCar !== '' && minCar !== '') {
      if (maxCar >= Number.parseInt(minCar)) {
        this.invalidRgCar = false;

      } else {
        if (this.maxCarRg[idx][subrg] === '' || this.minCarRg[idx][subrg] === '' || this.maxCarRg[idx][subrg] === null || this.minCarRg[idx][subrg] === null || typeof this.minCarRg[idx][subrg] === 'undefined' || typeof this.maxCarRg[idx][subrg] === 'undefined') {
          this.invalidRgCar = false;
        } else {
          this.invalidRgCar = true;
        }
      }
    } else {
      this.invalidRgCar = false;
    }
  }

  checkerSelfGrouped(id: number, subrg: number) {
    for(let i = 0; i < this.listSummaryAttributeCodesWith[id][subrg].length; i++) {
      for(let j = 0; j < this.selfGroupedAttributeCode.length; j++) {
        if(this.listSummaryAttributeCodesWith[id][subrg][i] === this.selfGroupedAttributeCode[j] && !this.listSelfGroupedAttributesPceCode[id][subrg].includes(this.selfGroupedAttributeCode[j])) {
          this.listSelfGroupedAttributesPceCode[id][subrg].push(this.selfGroupedAttributeCode[j]);
          this.listSelfGroupedAttributesPceName[id][subrg].push(this.selfGroupedAttributeName[j]);
          this.listSelfGroupedBoolean[id][subrg].push(true);
        }
      }
    }
    if(this.listSelfGroupedAttributesPceName[id][subrg].length > 0) {
      return true;
    }
    return false;
  }

  isSelfGrouped(row: number, subrg: number, id: number, value: boolean) {
    this.listSelfGroupedBoolean[row][subrg][id] = value;
  }

  idBtnContinueDisabled() {
    for(let i = 0; i < this.listSummaryAttributeNamesWithout.length; i++) {
      if(this.listSummaryAttributeNamesWithout[i] !== '') {
        if(this.listSummaryMinCardinalityWithout[i] === ''  || typeof this.listSummaryMinCardinalityWithout[i] === 'undefined') {
          this.errorMessageSummary = 'Minimal cardinality of each attribute is a mandatory field!';
          return true;
        }
        if(this.listSummaryMaxCardinalityWithout[i] === '' || typeof this.listSummaryMaxCardinalityWithout[i] === 'undefined') {
          this.errorMessageSummary = 'Maximal cardinality of each attribute is a mandatory field!';
          return true;
        }
        if(this.listSummaryMinCardinalityWithout[i] === '0' && this.listSummaryMaxCardinalityWithout[i] === '0') {
          this.errorMessageSummary = 'Minimal cardinality and maximal cardinality with a value of 0 are not allowed!';
          return true;
        }
        if(Number.parseInt(this.listSummaryMinCardinalityWithout[i]) > Number.parseInt(this.listSummaryMaxCardinalityWithout[i])) {
          this.errorMessageSummary = 'Minimal cardinality is higher than the maximal cardinality of an attribute!';
          return true;
        }
      }
    }

    for(let i = 0; i < this.listSummaryAttributeNamesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeNamesWith[i].length; j++) {
        for(let k = 0; k < this.listSummaryAttributeNamesWith[i][j].length; k++) {
          if(this.listSummaryAttributeNamesWith[i][j][k].length > 0 && this.listSummaryAttributeNamesWith[i][j][k].toString() !== '[]') {
            if(typeof this.listSummaryMinCardinalityWith[i][j][k] === 'undefined') {
              this.errorMessageSummary = 'Minimal cardinality of each attribute is a mandatory field!';
              return true;
            }
            if (this.listSummaryMinCardinalityWith[i][j][k].length === 0  || typeof this.listSummaryMinCardinalityWith[i][j][k] === 'undefined') {
              this.errorMessageSummary = 'Minimal cardinality of each attribute is a mandatory field!';
              return true;
            }
            if(typeof this.listSummaryMaxCardinalityWith[i][j][k] === 'undefined') {
              this.errorMessageSummary = 'Minimal cardinality of each attribute is a mandatory field!';
              return true;
            }
            if(typeof this.listSummaryMaxCardinalityWith[i][j][k] !== 'undefined') {
              if (this.listSummaryMaxCardinalityWith[i][j][k].length === 0 || typeof this.listSummaryMaxCardinalityWith[i][j][k] === 'undefined') {
                this.errorMessageSummary = 'Maximal cardinality of each attribute is a mandatory field!';
                return true;
              }
            } else {
              this.errorMessageSummary = 'Maximal cardinality of each attribute is a mandatory field!';
              return true;
            }
            if(this.listSummaryMinCardinalityWith[i][j][k] === '0' && this.listSummaryMaxCardinalityWith[i][j][k] === '0') {
              this.errorMessageSummary = 'Minimal cardinality and maximal cardinality with a value of 0 are not allowed!';
              return true;
            }
            if(Number.parseInt(this.listSummaryMinCardinalityWith[i][j][k]) > Number.parseInt(this.listSummaryMaxCardinalityWith[i][j][k])) {
              this.errorMessageSummary = 'Minimal cardinality is higher than the maximal cardinality of an attribute!';
              return true;
            }
          }
        }
        if(this.minCarRg[i][j] === '') {
          this.errorMessageSummary = 'Minimal cardinality of each RoleGroup is a mandatory field!';
          return true;
        }
        if(this.maxCarRg[i][j] === '') {
          this.errorMessageSummary = 'Maximal cardinality of each RoleGroup is a mandatory field!';
          return true;
        }
        if(this.maxCarRg[i][j].toString() === '0') {
          this.errorMessageSummary = 'Maximal cardinality of each RoleGroup must be greater than 0!';
          return true;
        }
        if(Number.parseInt(this.minCarRg[i][j]) > Number.parseInt(this.maxCarRg[i][j])) {
          this.errorMessageSummary = 'Minimal cardinality of a RoleGroup is greater than maximal cardinality of a RoleGroup!';
          return true;
        }
      }
    }
    this.errorMessageSummary = ''
    return false;
  }


  continue() {
    let elementTab = document.getElementById('tab-namePce');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }

    if(this.idLastTab !== 'tab-namePce') {
      elementTab = document.getElementById(this.idLastTab);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'transparent';
      }
    }

    this.idLastTab = 'tab-namePce'
    this.idActualTab = 'tab-namePce'

    this.listCodeWordsWith = new Array();
    this.listCodeWordsPositionWith = new Array();
    this.listCodeWordsWithout = new Array();
    this.listCodeWordsPositionWithout = new Array();

    for(let i = 0; i < this.listSummaryAttributeNamesWithout.length; i++) {
      // Pathological process
      let s = this.listSummaryAttributeNamesWithout[i].toLowerCase()
        .split(' ')
        .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
        .join('');

      s = s.substring(0,1).toLowerCase() + s.substring(1);
      if(s.length > 0) {
        if(!this.listCodeWordsWithout.includes(s)) {
          this.listCodeWordsWithout.push(s);
          this.listCodeWordsPositionWithout.push(i);
        } else {
          let listString = this.listCodeWordsWithout.toString();
          let matches = listString.match(s);
          // @ts-ignore
          this.listCodeWordsWithout.push(s + matches.length);
          this.listCodeWordsPositionWithout.push(i);
        }
      }
    }

    for(let i = 0; i < this.listSummaryAttributeNamesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeNamesWith[i].length; j++) {
        for(let k = 0; k < this.listSummaryAttributeNamesWith[i][j].length; k++) {
          if(this.listSummaryAttributeNamesWith[i][j][k].length > 0) {
            let s = this.listSummaryAttributeNamesWith[i][j][k].toLowerCase()
              .split(' ')
              .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
              .join('');

            s = s.substring(0, 1).toLowerCase() + s.substring(1);
            if (s.length > 0) {
              if (!this.listCodeWordsWithout.includes(s) && !this.listCodeWordsWith.includes(s)) {
                this.listCodeWordsWith.push(s);
                this.listCodeWordsPositionWith.push(i + '$' + j + '$' + k);
              } else {
                let listStringWithout = this.listCodeWordsWithout.toString();
                let listStringWith = this.listCodeWordsWith.toString();
                let matchesWithout =  (listStringWithout.replace(new RegExp("[0-9]", "g"), "").match(new RegExp(s, "g")) || []).length
                let matchesWith = (listStringWith.replace(new RegExp("[0-9]", "g"), "").match(new RegExp(s, "g")) || []).length
                let res = matchesWith + matchesWithout;
                this.listCodeWordsWith.push(s + res);
                this.listCodeWordsPositionWith.push(i + '$' + j + '$' + k);
              }
            }
          }
        }
      }
    }
  }

  clickTableTermTemplate(pos: String, idx: number, value: String) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableTermTemplate' + pos) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
  }

  dbClickTableTermTemplate(pos: String, idx: number, value: String) {
    if(value === 'without') {
      this.termTemplate = this.termTemplate + "$" + this.listCodeWordsWithout[idx] + '$';
    } else if(value === 'with') {
      this.termTemplate = this.termTemplate + "$" + this.listCodeWordsWith[idx] + '$';
    }
  }

  checkRg(id: number, subRg: number) {
    let l = new Array();
    if(this.listSummaryAttributeCodesWith[id][subRg] !== undefined) {
      for (let i = 0; i < this.listSummaryAttributeCodesWith[id][subRg].length; i++) {
        if (this.listSummaryAttributeCodesWith[id][subRg][i].length > 0) {
          l.push('yes');
        }
      }
    }

    if(l.length > 0) {
      return true;
    } else{
      return false;
    }
  }

  public isDownload = false;

  downloadTemplate() {
    this.isDownload = true;
    setTimeout(() => {
      this.isDownload = false;
    }, 2000);
  }

  getContent() {
    return JSON.stringify(this.templateFinal);
  }

  createNewTemplate() {
    this.highlightingTemplateService.reset();
    let elementTab = document.getElementById('tab-finish');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }

    if(this.idLastTab !== 'tab-finish') {
      elementTab = document.getElementById(this.idLastTab);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'transparent';
      }
    }
    this.idActualTab = 'tab-finish';

    let template = '';
    let focusConcept = this.focusConceptCode + " |" + this.focusConceptName + "|";
    this.highlightingTemplateService.focusConceptCode = this.focusConceptCode;
    this.highlightingTemplateService.focusConceptName = this.focusConceptName;

    template = template + focusConcept + ":";

    let term = this.termTemplate;
    let list = term.split('$');
    let listNew = new Array();

    for(let i = 0; i < list.length; i++) {
      if(this.listCodeWordsWithout.includes(list[i]) || this.listCodeWordsWith.includes(list[i])) {
        listNew.push(list[i]);
      }
    }

    let lPosWithout = new Array();
    let lPosWith = new Array();
    let lWithout = new Array();
    let lWith = new Array();

    for(let i = 0; i < this.listCodeWordsWithout.length; i++) {
      for(let j = 0; j < listNew.length; j++) {
        if(this.listCodeWordsWithout[i] === listNew[j]) {
          lWithout.push(listNew[j]);
          lPosWithout.push(this.listCodeWordsPositionWithout[i]);
        }
      }
    }

    for(let i = 0; i < this.listCodeWordsWith.length; i++) {
      for(let j = 0; j < listNew.length; j++) {
        if(this.listCodeWordsWith[i] === listNew[j]) {
          lWith.push(listNew[j]);
          lPosWith.push(this.listCodeWordsPositionWith[i]);
        }
      }
    }

    // ungruppierte attribute

    this.highlightingTemplateService.listAttributeWithoutMinCar = this.listSummaryMinCardinalityWithout;
    this.highlightingTemplateService.listAttributeWithoutMaxCar = this.listSummaryMaxCardinalityWithout;
    this.highlightingTemplateService.listAttributeWithoutCode = this.listSummaryAttributeCodesWithout;

    for(let i = 0; i < this.listSummaryAttributeCodesWithout.length; i++) {
      if(this.listSummaryAttributeCodesWithout[i] !== '') {
        let id = '';
        for(let j = 0; j < lPosWithout.length; j++) {
          if(Number.parseInt(lPosWithout[j]) === i) {
            id = ' @' + lWithout[j];
          }
        }

        let attributeName = this.listSummaryAttributeCodesWithout[i] +  " |" + this.listSummaryAttributeNamesWithout[i] + " (attribute)|";
        let attributeValue = this.listSummaryValueRangeWithout[i].split("MINUS").join(" MINUS ").split("OR").join(" OR ");

        this.highlightingTemplateService.listAttributeWithoutName.push(this.listSummaryAttributeNamesWithout[i] + " (attribute)");

        let attribute = '';
        if(attributeValue.includes('|')) {
          attribute = '\n\t[[~' + this.listSummaryMinCardinalityWithout[i] + '..' + this.listSummaryMaxCardinalityWithout[i] + ']] ' + attributeName +  ' = [[+id(' + attributeValue + ') ' + id + ']],'
          this.highlightingTemplateService.listAttributeWithoutConstraintComplete.push('' + attributeValue + ') ' + id);
          this.highlightingTemplateService.listAttributeWithoutConstraint.push(attributeValue);
        } else if(this.listWithoutConstraint[i].includes('int(')) {
          attribute = '\n\t[[~' + this.listSummaryMinCardinalityWithout[i] + '..' + this.listSummaryMaxCardinalityWithout[i] + ']] ' + attributeName +  ' = [[+int(' + attributeValue + ') ' + id + ']],'
          this.highlightingTemplateService.listAttributeWithoutConstraintComplete.push('[[+int(' + attributeValue + ') ' + id);
          this.highlightingTemplateService.listAttributeWithoutConstraint.push(attributeValue);
        } else if(this.listWithoutConstraint[i].includes('dec(')) {
          attribute = '\n\t[[~' + this.listSummaryMinCardinalityWithout[i] + '..' + this.listSummaryMaxCardinalityWithout[i] + ']] ' + attributeName +  ' = [[+dec(' + attributeValue + ') ' + id + ']],'
          this.highlightingTemplateService.listAttributeWithoutConstraintComplete.push('[[+dec(' + attributeValue + ') ' + id);
          this.highlightingTemplateService.listAttributeWithoutConstraint.push('[[+dec(' + attributeValue);
        }
        template = template + attribute;
      }
    }
    if(this.listWithoutMinCar.length > 0) {
      template = template.substring(0, template.lastIndexOf(','));
    }

    let listUsedRg = new Array();
    let listUsedSubRg = new Array();

    // check welche der RG benutzt wurde
    for(let i = 0; i < this.listSummaryAttributeCodesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeCodesWith[i].length; j++) {
        if(this.listSummaryAttributeCodesWith[i][j] !== undefined) {
          for (let k = 0; k < this.listSummaryAttributeCodesWith[i][j].length; k++) {
            if (this.listSummaryAttributeCodesWith[i][j][k] !== undefined) {
              if (this.listSummaryAttributeCodesWith[i][j][k].length > 0) {
                listUsedRg.push(i);
                listUsedSubRg.push(j);
                break;
              }
            }
          }
        }
      }
    }

    let rg = '';
    let attribute = '';
    for(let i = 0; i < listUsedRg.length; i++) {

      rg = rg + '\n\t[[~' + this.minCarRg[listUsedRg[i]][listUsedSubRg[i]] + '..' + this.maxCarRg[listUsedRg[i]][listUsedSubRg[i]] + ']] {';

      this.highlightingTemplateService.listRoleGroupsMinCar.push(this.minCarRg[listUsedRg[i]][listUsedSubRg[i]]);
      this.highlightingTemplateService.listRoleGroupsMaxCar.push(this.maxCarRg[listUsedRg[i]][listUsedSubRg[i]]);
      this.highlightingTemplateService.listAttributeWithMinCar.push(new Array());
      this.highlightingTemplateService.listAttributeWithMaxCar.push(new Array());
      this.highlightingTemplateService.listAttributeWithCode.push(new Array());
      this.highlightingTemplateService.listAttributeWithName.push(new Array());
      this.highlightingTemplateService.listAttributeWithConstraintComplete.push(new Array());

      this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]] = this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]].filter((x: any) => x.length !== 0);
      for(let j = 0; j < this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]].length; j++) {

        let idx = 0;
        if(this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]].length > 0) {
          let id = '';
          for(let k = 0; k < lPosWith.length; k++) {

            if(Number.parseInt(lPosWith[k].split('$')[0]) === listUsedRg[i] && Number.parseInt(lPosWith[k].split('$')[1]) === listUsedSubRg[i] && Number.parseInt(lPosWith[k].split('$')[2]) === j) {
              id = ' @' + lWith[k];
            }
          }

          let attributeName = this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]][j] +  " |" + this.listSummaryAttributeNamesWith[listUsedRg[i]][listUsedSubRg[i]][j] + " (attribute)|";
          let attributeValue = this.listSummaryValueRangeWith[listUsedRg[i]][listUsedSubRg[i]][j].split("MINUS").join(" MINUS ").split("OR").join(" OR ");

          this.highlightingTemplateService.listAttributeWithMinCar[i].push(this.listSummaryMinCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j]);
          this.highlightingTemplateService.listAttributeWithMaxCar[i].push(this.listSummaryMaxCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j]);
          this.highlightingTemplateService.listAttributeWithCode[i].push(this.listSummaryAttributeCodesWith[listUsedRg[i]][listUsedSubRg[i]][j]);
          this.highlightingTemplateService.listAttributeWithName[i].push( this.listSummaryAttributeNamesWith[listUsedRg[i]][listUsedSubRg[i]][j] + " (attribute)");

          idx = idx  + 1;
          attribute = '';
          if(attributeValue.includes('|')) {
            attribute = '\n\t\t[[~' + this.listSummaryMinCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + '..' + this.listSummaryMaxCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + ']] ' + attributeName +  ' = [[+id(' + attributeValue + ')' +  id + ']],'
            this.highlightingTemplateService.listAttributeWithConstraintComplete[i].push('' + attributeValue + ') ' + id);
          } else if(this.listInitWithConstraint[listUsedRg[i]][this.listIndexWith[listUsedRg[i]][listUsedSubRg[i]][j]].includes('int(')) {
            attribute = '\n\t\t[[~' + this.listSummaryMinCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + '..' + this.listSummaryMaxCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + ']] ' + attributeName +  ' = [[+int(' + attributeValue + ')' + id + ']],'
            this.highlightingTemplateService.listAttributeWithConstraintComplete[i].push('[[+int(' + attributeValue + ') ' + id);
          } else if(this.listInitWithConstraint[listUsedRg[i]][this.listIndexWith[listUsedRg[i]][listUsedSubRg[i]][j]].includes('dec(')) {
            attribute = '\n\t\t[[~' + this.listSummaryMinCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + '..' + this.listSummaryMaxCardinalityWith[listUsedRg[i]][listUsedSubRg[i]][j] + ']] ' + attributeName +  ' = [[+dec(' + attributeValue + ')' + id + ']],'
            this.highlightingTemplateService.listAttributeWithConstraintComplete[i].push('[[+dec(' + attributeValue + ') ' + id);
          }
          rg = rg + attribute;
        }
      }
      if(attribute.length > 0) {
        rg = rg.substring(0, rg.lastIndexOf(','));
      }
      rg = rg + '\n\t},';
    }
    if(rg.length > 0) {
      rg = rg.substring(0, rg.lastIndexOf(','));
    }
    template = template + rg;
    this.resultTemplate = template;

    this.nameTemplate = this.nameTemplate.split("(").join("[").split(")").join("]") + " " + this.focusConceptSemanticTag;
    this.nameTemplate = this.nameTemplate[0].toUpperCase() + this.nameTemplate.slice(1);
    this.templateGenerationService.createNewTemplate(this.nameTemplate, template, this.termTemplate, this.listAttributeRelation.toString()).then((response: any) => {
      this.templateFinal = JSON.stringify(response);
      this.downloadFile(this.templateFinal);

      let j = { fileName: this.nameTemplate + ".json", fileContent: this.templateFinal}
      this.appService.allTemplates.templates.push(j);
      return response;

    })
  }

  downloadFile(data: string) {
    // @ts-ignore
    const blob = new Blob([data], { type: 'text/json' });
    const url= window.URL.createObjectURL(blob);
    const anchorElement = document.createElement('a');
    document.body.appendChild(anchorElement);
    anchorElement.style.display = 'none';
    anchorElement.href = url;
    anchorElement.download = this.nameTemplate + ".json";
    anchorElement.click();
    window.URL.revokeObjectURL(url);
    this.isTemplatedCreated = true;
  }

  clickRow(idRg: number, idSubRg: number, row:number) {
    if(this.actualAttributeNameWith[idRg][idSubRg][row] !== '') {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWith' + idRg + idSubRg + row) {
              element.style.background = 'lightblue';
            }
          }
        }
      }
      this.idRg = idRg;
      this.idSubRg = idSubRg;
      this.idRowLeftTableWith = row;
      this.getRowTableLeftWith(row);
    }
  }

  isCopyToClickbloard = false;

  copyPceToClipboard(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.resultTemplate;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.isCopyToClickbloard = true;
    setTimeout(() => {
      this.isCopyToClickbloard = false;
    }, 2000);
  }


  public fileUrl = "";
  public templateFinal = "";

  resetVariables() {
    this.templateGenerationService.clickTemplateGeneration('')
    this.start()
    this.booleanBtnFcDisabled = true;
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = false;
    }

    this.conceptModelReferencedComponentId = '';
    this.conceptModelParentDomain = '';
    this.conceptModelProximalPrimitiveConstraint = '';
    this.conceptModelProximalPrimitiveRefinement = '';
    this.conceptModelDomainConstraint = '';
    this.conceptModelURL = '';
    this.conceptModelTemplate = '';
    this.conceptModelFocusConceptTemplate = '';
    this.attributeDefinitionCMCode = new Array();
    this.attributeDefinitionCMName = new Array();
    this.attributeDefinitionCMDefinition = new Array();
    this.attributeDefinitionCMRangeConstraint = new Array();
    this.listWithoutMinCar = new Array();
    this.listWithoutMaxCar = new Array();
    this.listWithoutConstraint = new Array();
    this.listWithoutAttributeCode = new Array();
    this.listWithoutAttributeName = new Array();
    this.listInitWithMinCar = new Array();
    this.listInitWithMaxCar = new Array();
    this.listInitWithRoleGroupMinCar = new Array();
    this.listInitWithRoleGroupMaxCar = new Array();
    this.listInitWithConstraint = new Array();
    this.listInitWithAttributeCode = new Array();
    this.listInitWithAttributeName = new Array();
    this.listNumberAttributesWithout = new Array();
    this.showGenerationTemplate = false;
    this.actualAttributeNameWithout = new Array();
    this.actualMinCarWithout = new Array();
    this.actualMaxCarWithout = new Array();
    this.actualValueRangeWithout = new Array();
    this.listNameRangeWithout = new Array();
    this.indexWithout = -1;
    this.actualCheckCarWithout = true;
    this.idRowLeftTableWithout = -1;
    this.listLoadDataAttributeWithout = new Array();
    this.focusConceptName = '';
    this.focusConceptCode = '';
    this.listAllNamesFocusConcept = new Array();
    this.listAllCodesFocusConcept = new Array();
    this.booleanBtnFcDisabled = false;
    this.listOperatorRangeWithout = new Array();
    this.listConstraintRangeWithout = new Array();
    this.listSummaryNameRangeWithout = new Array();
    this.listSummaryCodeRangeWithout = new Array();
    this.listSummaryOperatorRangeWithout = new Array();
    this.listSummaryConstraintRangeWithout = new Array();
    this.listSummaryAttributeCodesWithout = new Array();
    this.listSummaryAttributeNamesWithout = new Array();
    this.listSummaryMinCardinalityWithout = new Array();
    this.listSummaryMaxCardinalityWithout = new Array();
    this.listAllCodesRangeWithout = new Array();
    this.listAllNamesRangeWithout = new Array();
    this.listIndexWithout = new Array();
    this.booleanIntWithout = false;
    this.booleanDecWithout = false;
    this.booleanNormalWithout = false;
    this.intDefMinWithout = -1;
    this.intDefMaxWithout = -1;
    this.decDefMinWithout = -1;
    this.decDefMaxWithout = -1;
    this.listIntValueWithout = new Array();
    this.listIntMaxWithout = new Array();
    this.listIntMinWithout = new Array();
    this.listDecValueWithout = new Array();
    this.listDecMaxWithout = new Array();
    this.listDecMinWithout = new Array();
    this.listIntFixedValueWithout = new Array();
    this.listIntFixedValueWith = new Array();
    this.listIntFixedWithout = new Array();
    this.inputIdDecWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';
    this.booleanClickKeypressWithout = true;
    this.oldInputWithout = '';
    this.listSummaryValueRangeWithout = new Array();
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.listNumberAttributesWith = new Array();
    this.idRowLeftTableWith = -1;
    this.listIndexSubRg = new Array();
    this.idRg = -1;
    this.idSubRg = -1;
    this.listOperatorRangeWith = new Array();
    this.listConstraintRangeWith = new Array();
    this.listNameRangeWith = new Array();
    this.actualAttributeNameWith = new Array();
    this.showInformationWindowWith = false;
    this.indexWith = -1;
    this.listLoadDataAttributeWith = new Array();
    this.listIndexWith = new Array();
    this.actualMinCarWith = new Array();
    this.actualMaxCarWith = new Array();
    this.actualValueRangeWith = new Array();
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.actualCheckCarWith = true;
    this.listSummaryValueRangeWith = new Array();
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();
    this.listSummaryNameRangeWith = new Array();
    this.listSummaryCodeRangeWith = new Array();
    this.listSummaryMinCardinalityWith = new Array();
    this.listSummaryMaxCardinalityWith = new Array();
    this.listSummaryAttributeCodesWith = new Array();
    this.listSummaryAttributeNamesWith = new Array();
    this.listSummaryOperatorRangeWith = new Array();
    this.listSummaryConstraintRangeWith = new Array();
    this.selfGroupedAttributeCode = new Array();
    this.selfGroupedAttributeName = new Array();
    this.listChangedConstraintWith = new Array();
    this.listChangedAttributeCodeWith = new Array();
    this.listChangedAttributeNameWith = new Array();
    this.listFlagAttributeRelation = new Array();
    this.listDecFixedWith = new Array();
    this.listDecFixedValueWith = new Array();
    this.booleanIntWith = false;
    this.booleanDecWith = false;
    this.booleanNormalWith = false;
    this.intDefMinWith = -1;
    this.intDefMaxWith = -1;
    this.decDefMinWith = -1;
    this.decDefMaxWith = -1;
    this.intMaxWith = '';
    this.intMinWith = '';
    this.intFixedWith = '';
    this.decMaxWith = '';
    this.decMinWith = '';
    this.decFixedWith = '';
    this.listIntFixedWith = new Array();
    this.isWith = false;
    this.listCodeRangeWith = new Array();
    this.invalidInputCarWith = false;
    this.listInfoDefinitionWith = new Array();
    this.listInfoConstraintWith = new Array();
    this.correctInputWith = false;
    this.idRowTableRightWith = -1;
    this.inputIdIntWith = '';
    this.inputIdDecWith = '';
    this.termTemplate = '';
    this.listCodeWordsWith = new Array();
    this.listCodeWordsPositionWith = new Array();
    this.listCodeWordsWithout = new Array();
    this.listCodeWordsPositionWithout = new Array();
    this.resultTemplate = '';
    this.nameTemplate = '';
    this.isTemplatedCreated = false;
    this.idLastTab = '';
    this.idActualTab = '';
    this.listChangedConstraintFlagWithout = new Array();
    this.listChangedConstraintWithout = new Array();
    this.listAttributeRelation = new Array();
    this.listChangedFlagWith = new Array();
    this.minCarRg = new Array();
    this.maxCarRg = new Array();
    this.errorMessageSummary = '';
    this.listSelfGroupedAttributesPceCode = new Array();
    this.listSelfGroupedAttributesPceName = new Array();
    this.listSelfGroupedBoolean = new Array();
    this.invalidRgCar = false;
    this.isUpdate = false;
    this.oldInputWith = [];
    this.listDecFixedValueWithout = new Array();
    this.listDecFixedWithout = new Array();
    this.inputIdIntWithout = '';
    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.idRowTableRightWithout = -1;
    this.correctInputWithout = false;
    this.invalidInputCarWithout = false;
    this.listChangeCarWithout = new Array();
    this.listChangeCarWith = new Array();
    this.showInformationWindowWithout = false;
    this.isWithout = false;
    this.listChangedWith = new Array();
  }

}
