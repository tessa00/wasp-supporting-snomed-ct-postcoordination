import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {SettingsService} from "../settings/settings.service";
import {AppService} from "../app.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class TemplatesGenerationService {

  constructor(public backendService: BackendService, private settingsService: SettingsService, private appService: AppService) {
  }


  public start = '';
  public initJson = '';

  clickTemplateGeneration(id: string) {
    this.start = id;
  }


  // Spring boot request

  getConceptModalConstraint()  {
    const url = this.backendService.getBackendUrl() + 'initconceptmodalconstraint';
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }


  getAttributeValueRequestFilterLimitFSN(expression: string, filter: string, count: string)  {
    let ex = this.encodeURIComponent(expression);
    let fi = this.encodeURIComponent(filter);
    const url = this.backendService.getBackendUrl() + 'attributevaluesfilterlimitfsn/?url=' + this.settingsService.serverInit + '&expression=' + ex + '&filter=' + fi + '&count=' + count + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

  getFocusConceptAttributeValueRequestFilterLimitFSN(filter: string, count: string){
    const url = this.backendService.getBackendUrl() + 'focusconceptattributevaluesfilterlimitfsn';
    let body = {url: this.settingsService.serverInit, filter: filter, count: count, conceptmodel: this.appService.selectedConceptModel, version: this.settingsService.sctVersion}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  lookUpFsn(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'lookupnamefsn/?url=' + this.settingsService.serverInit + '&code=' + c + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  lookUpName(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'lookupname/?url=' + this.settingsService.serverInit + 'code=' + c + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  createNewTemplate(name: string, pce: string, term: string, attributeRelation: string) {
    let n = this.encodeURIComponent(name);
    let p = this.encodeURIComponent(pce);
    let t = this.encodeURIComponent(term);
    let ar = this.encodeURIComponent(attributeRelation);

    const url = this.backendService.getBackendUrl() + 'createnewtemplate/?name=' + n + '&pce=' + p + '&term=' + t;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }


  downloadTemplate(name: string, pce: string, term: string, attributeRelation: string) {
    let n = this.encodeURIComponent(name);
    let p = this.encodeURIComponent(pce);
    let t = this.encodeURIComponent(term);
    let ar = this.encodeURIComponent(attributeRelation);

    const url = this.backendService.getBackendUrl() + 'downloadtemplate/?name=' + n + '&pce=' + p + '&term=' + t;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  getPreprocessingData(code: string){
    const url = this.backendService.getBackendUrl() + 'preprocessingtemplategeneration';
    let body = {url: this.settingsService.serverInit, version: this.settingsService.sctVersion, code: code, conceptmodel: this.appService.selectedConceptModel}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  lookupFsn(code: string){
    const url = this.backendService.getBackendUrl() + 'lookupnamefsn/?url=' + this.settingsService.serverInit + '&code=' + code + '&version=' + this.settingsService.sctVersion;
    let body = {url: this.settingsService.serverInit, version: this.settingsService.sctVersion, code: code, conceptmodel: this.appService.selectedConceptModel}
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }













  // ################################################################################

  getAttributeValueRequest(expression: string)  {
    let ex = this.encodeURIComponent(expression);
    const url = this.backendService.getBackendUrl() + 'attributevalueswithoutfilter/?url=' + this.settingsService.serverInit + '&expression=' + ex + '&version' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

  testSubsumption(code: string)  {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'subsumptionproduct/?url=' + this.settingsService.serverInit + '&version=' + this.settingsService.sctVersion + '&code=' + c;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

}
