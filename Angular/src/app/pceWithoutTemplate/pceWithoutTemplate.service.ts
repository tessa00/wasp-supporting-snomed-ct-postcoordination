import {Injectable} from '@angular/core';
import {lastValueFrom} from 'rxjs';
import {SettingsService} from "../settings/settings.service";
import {AppService} from "../app.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class PceWithoutTemplateService {

  constructor(public backendService: BackendService,
              private settingsService: SettingsService,
              private appService: AppService) {
  }


  public start = '';
  public initJson = '';

  clickTemplateGeneration(id: string) {
    this.start = id;
  }


  // Spring boot request

  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }

  getAttributeValueRequestFilterLimitFSN(expression: string, filter: string, count: string) {
    let ex = this.encodeURIComponent(expression);
    let fi = this.encodeURIComponent(filter);
    const url = this.backendService.getBackendUrl() + 'attributevaluesfilterlimitfsn/?url=' + this.settingsService.serverInit + '&expression=' + ex + '&filter=' + fi + '&count=' + count + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

  getFocusConceptAttributeValueRequestFilterLimitFSN(filter: string, count: string){
    const url = this.backendService.getBackendUrl() + 'focusconceptattributevaluesfilterlimitfsn';
    let body = {url: this.settingsService.serverInit, filter: filter, count: count, conceptmodel: this.appService.selectedConceptModel, version: this.settingsService.sctVersion}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  lookUpFsn(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'lookupnamefsn/?url=' + this.settingsService.serverInit + '&code=' + c + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  lookUpName(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'lookupname/?url=' + this.settingsService.serverInit + 'code=' + c + '&version=' + this.settingsService.sctVersion;
    const response = this.backendService.httpClient.get(url);

    return lastValueFrom(response);
  }

  getPreprocessingData(code: string){
    const url = this.backendService.getBackendUrl() + 'preprocessingtemplategeneration';
    let body = {url: this.settingsService.serverInit, version: this.settingsService.sctVersion, code: code, conceptmodel: this.appService.selectedConceptModel}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }

  getPreprocessingDataUpdate(code: string) {
    const url = this.backendService.getBackendUrl() + 'preprocessingtemplategenerationupdate';
    let body = {url: this.settingsService.serverInit, version: this.settingsService.sctVersion, code: code, listnumber: this.listNumberElementSubRg, conceptmodel: this.appService.selectedConceptModel}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }


  // ################################################################################
  // Update existing pce --- split in elements, fill interface

  getDataPceUpdate(pce: string, idx: number) {
    this.reset()

    this.rowUpdate = idx;
    let focusconcept = pce.split(":")[0];
    this.focusConceptCode = focusconcept.split('|')[0].split(" ").join("");
    this.focusConceptName = focusconcept.split('|')[1];

    let refinement = pce.split(":")[1];

    let posRGStart = new Array();
    let posRGEnd = new Array();

    for (let i = 0; i < refinement.length; i++) {
      if (refinement[i] === "{") {
        posRGStart.push(i + 1);
      } else if (refinement[i] === "}") {
        posRGEnd.push(i + 1);
      }
    }

    if (posRGStart.length > 0 && posRGEnd.length > 0) {
      if (posRGStart[0] !== 1) {
        this.updateWithoutRoleGroup(posRGStart, posRGEnd, refinement)
      }
      this.updateWithRoleGroup(posRGStart, posRGEnd, refinement);
    } else {
      this.updateWithoutRoleGroup(posRGStart, posRGEnd, refinement);
    }
  }


  public listWithoutAttributeNameCode = new Array();
  public listWithoutAttributeNameName = new Array();
  public listWithoutAttributeValue = new Array();
  public listNameWithout = new Array();
  public listLoadDataAttributeWithout = new Array()
  public listIsUpdateAttributeWithout = new Array()
  public isWithout = false;
  public isWith = false;
  public isUpdate = false;
  public showGenerationTemplate = false;
  public focusConceptCode = '';
  public focusConceptName = '';
  public listWithAttributeNameCode = new Array();
  public listWithAttributeNameName = new Array();
  public listWithAttributeValue = new Array();
  public listNameWith = new Array();
  public listLoadDataAttributeWith = new Array()
  public listIsUpdateAttributeWith = new Array()
  public listSummaryAttributeNamesWith = new Array()
  public rowUpdate = -1;
  public listNumberElementSubRg = new Array();
  public listInitWithAttributeCode = new Array();
  public listInitWithCardinalityMin = new Array();

  updateWithoutRoleGroup(posRGStart: any, posRGEnd: any, refinement: string) {
    let withoutRGSummary = '';
    if (posRGStart.length > 0 && posRGEnd.length > 0) {
      if (refinement.substring(0, posRGStart[0] - 1) === ',') {
        withoutRGSummary = refinement.substring(0, posRGStart[0] - 2);
      } else {
        withoutRGSummary = refinement.substring(0, posRGStart[0] - 1);
      }
    } else {
      withoutRGSummary = refinement;
    }

    let attribute = new Array();

    if (withoutRGSummary.includes(",")) {
      attribute = withoutRGSummary.split(",")
    } else {
      attribute = new Array(withoutRGSummary)
    }


    for (let j = 0; j < attribute.length; j++) {
      if (attribute[j].length > 1) {
        let attributeNameCode = attribute[j].split("=")[0].split("|")[0].split(" ").join("");
        let attributeNameName = attribute[j].split("=")[0].split("|")[1].split(" (attribute)").join("");
        if (attributeNameName.startsWith(' ')) {
          attributeNameName = attributeNameName.replace(/^\s+/, '')
        }
        let attributeValue = attribute[j].split("=")[1]
        let attributeValueCode = attribute[j].split("=")[1].split("|")[0];
        let attributeValueName = attribute[j].split("=")[1].split("|")[1];

        this.listWithoutAttributeNameCode.push(attributeNameCode);
        this.listWithoutAttributeNameName.push(attributeNameName);
        if (attributeValueName !== undefined) {
          this.listWithoutAttributeValue.push(attributeValueCode.split(" ").join("") + " |" + attributeValueName + "|");
        } else {
          this.listWithoutAttributeValue.push(attributeValueCode.split(" ").join(""));
        }

        this.listIsUpdateAttributeWithout.push(true);
        this.listLoadDataAttributeWithout.push(true);
        this.listNameWithout.push(attributeValueName);

      }
    }
    this.isWithout = true;
  }


  updateWithRoleGroup(posRGStart: any, posRGEnd: any, refinement: string) {
    this.listWithAttributeNameCode.push(new Array());
    this.listWithAttributeNameName.push(new Array());
    this.listWithAttributeValue.push(new Array());
    this.listIsUpdateAttributeWith.push(new Array());
    this.listLoadDataAttributeWith.push(new Array());
    this.listNameWith.push(new Array());
    this.listSummaryAttributeNamesWith.push(new Array());


    for (let i = 0; i < posRGStart.length; i++) {
      let listAttributeNameCode = new Array();
      let listAttributeNameName = new Array();
      let listAttributeValue = new Array();
      let listAttributeValueName = new Array();
      let listIsUpdateAttributeWith = new Array();
      let listLoadDataAttributeWith = new Array();
      let listSummaryAttributeNamesWith = new Array();

      let rg = refinement.substring(posRGStart[i], posRGEnd[i]);
      let attribute = rg.split(",")
      this.listNumberElementSubRg.push(attribute.length);

      // für jede rg die einzelnen attribut namen (codes)
      for (let j = 0; j < attribute.length; j++) {
        let attributeNameCode = attribute[j].split("=")[0].split("|")[0].split(" ").join("");
        let attributeNameName = attribute[j].split("=")[0].split("|")[1].split(" (attribute)").join("");
        if (attributeNameName.startsWith(' ')) {
          attributeNameName = attributeNameName.replace(/^\s+/, '')
        }
        let attributeValue = attribute[j].split("=")[1].split("}").join("");
        let attributeValueCode = attribute[j].split("=")[1].split("|")[0];
        let attributeValueName = attribute[j].split("=")[1].split("|")[1];
        let summaryAttributeNamesWith = attribute[j].split("=")[0];


        if (attributeValueName !== undefined) {
          listAttributeValue.push(attributeValueCode.split(" ").join("") + " |" + attributeValueName.split("}").join("") + "|");
        } else {
          listAttributeValue.push(attributeValueCode.split(" ").join("").split("}").join(""));
        }

        listAttributeNameCode.push(attributeNameCode);
        if (attributeNameName !== '') {
          listAttributeNameName.push(attributeNameName);
        }
        // listAttributeValue.push(attributeValue);
        listAttributeValueName.push(attributeValueName);
        listIsUpdateAttributeWith.push(true);
        listLoadDataAttributeWith.push(true);
        listSummaryAttributeNamesWith.push(summaryAttributeNamesWith);
      }
      listAttributeNameName = listAttributeNameName.filter((x: any) => x.length !== 0);

      this.listWithAttributeNameCode[0].push(listAttributeNameCode);
      this.listWithAttributeNameName[0].push(listAttributeNameName);
      this.listWithAttributeValue[0].push(listAttributeValue);
      this.listIsUpdateAttributeWith[0].push(listIsUpdateAttributeWith);
      this.listLoadDataAttributeWith[0].push(listLoadDataAttributeWith);
      this.listNameWith[0].push(new Array(listAttributeValueName));
      this.listSummaryAttributeNamesWith[0].push(listSummaryAttributeNamesWith);
    }

    this.isWith = true;

  }


  reset() {
    this.listWithoutAttributeNameCode = new Array();
    this.listWithoutAttributeNameName = new Array();
    this.listWithoutAttributeValue = new Array();
    this.listNameWithout = new Array();
    this.listLoadDataAttributeWithout = new Array()
    this.listIsUpdateAttributeWithout = new Array()
    this.isWithout = false;
    this.isWith = false;
    this.isUpdate = false;
    this.showGenerationTemplate = false;
    this.focusConceptCode = '';
    this.focusConceptName = '';
    this.listWithAttributeNameCode = new Array();
    this.listWithAttributeNameName = new Array();
    this.listWithAttributeValue = new Array();
    this.listNameWith = new Array();
    this.listLoadDataAttributeWith = new Array()
    this.listIsUpdateAttributeWith = new Array()
    this.listSummaryAttributeNamesWith = new Array()
  }


  // ################################################################################
  getConceptModels() {
    const url = this.backendService.getBackendUrl() + 'getconceptmodels';
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }


  testSubsumption(code: string) {
    let c = this.encodeURIComponent(code);
    const url = this.backendService.getBackendUrl() + 'subsumptionproduct/?url=' + this.settingsService.serverInit + '&version=' + this.settingsService.sctVersion + '&code=' + c;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }

}
