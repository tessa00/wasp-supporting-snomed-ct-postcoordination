import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {PceWithoutTemplateService} from "./pceWithoutTemplate.service";
import {AppService} from "../app.service";
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {TemplatesService} from "../templates/templates.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {HighlightingPceService} from "../highlightingPce/highlightingPce.service";
import {DiagramComponent} from "../ui/diagram.component";

@Component({
  selector: 'app-pce_without_template',
  templateUrl: './pceWithoutTemplate.component.html',
  styleUrls: ['./pceWithoutTemplate.css']
})

export class PceWithoutTemplateComponent {

  constructor(public pceWithoutTemplateService: PceWithoutTemplateService, private http: HttpClient,  public dia: DiagramComponent, private templateService: TemplatesService, public highlightingPceService: HighlightingPceService, public settingsTemplateService: SettingsTemplatesService, private  settingsService: SettingsService, private  settingsCsService: SettingsCsSupplementService, public appService: AppService) {
  }

  // general
  public conceptModelReferencedComponentId = '';
  public conceptModelParentDomain = '';
  public conceptModelProximalPrimitiveConstraint = '';
  public conceptModelProximalPrimitiveRefinement = '';
  public conceptModelDomainConstraint = '';
  public conceptModelURL = '';
  public conceptModelTemplate = '';
  public conceptModelFocusConceptTemplate = '';

  public attributeDefinitionCMCode = new Array();
  public attributeDefinitionCMName = new Array();
  public attributeDefinitionCMDefinition = new Array();
  public attributeDefinitionCMRangeConstraint = new Array();
  public listAttributeRelation = new Array();

  // without role group
  public listWithoutMinCar = new Array();
  public listWithoutMaxCar = new Array();
  public listWithoutConstraint = new Array();
  public listWithoutAttributeCode = new Array();
  public listWithoutAttributeName = new Array();

  // with role group
  public listInitWithMinCar = new Array();
  public listInitWithMaxCar = new Array();
  public listInitWithRoleGroupMinCar = new Array();
  public listInitWithRoleGroupMaxCar = new Array();
  public listInitWithConstraint = new Array();
  public listInitWithAttributeCode = new Array();
  public listInitWithAttributeName = new Array();

  // left table without role group
  public listNumberAttributesWithout = new Array();
  public showGenerationTemplate = false;
  public actualAttributeNameWithout = new Array();
  public listNameWithout = new Array();
  public indexWithout = -1;
  public idRowLeftTableWithout = -1;
  public listLoadDataAttributeWithout = new Array();

  // Focusconcept
  public focusConceptName = '';
  public focusConceptCode = '';
  public listAllNamesFocusConcept = new Array();
  public listAllCodesFocusConcept = new Array();
  public booleanBtnFcDisabled = false;


  // range table without role group
  public listConstraintRangeWithout = new Array();
  public listSummaryNameRangeWithout = new Array();
  public listSummaryCodeRangeWithout = new Array();
  public listSummaryConstraintRangeWithout = new Array();
  public listSummaryAttributeCodesWithout = new Array();
  public listSummaryAttributeNamesWithout = new Array();
  public listSummaryMinCardinalityWithout = new Array();
  public listSummaryMaxCardinalityWithout = new Array();
  public listAllCodesRangeWithout = new Array();
  public listAllNamesRangeWithout = new Array();
  public listIndexWithout = new Array();
  public booleanIntWithout = false;
  public booleanDecWithout = false;
  public booleanNormalWithout = false;
  public intDefMinWithout = -1;
  public intDefMaxWithout = -1;
  public decDefMinWithout = -1;
  public decDefMaxWithout = -1;
  public listIntValueWithout = new Array();
  public listIntMaxWithout = new Array();
  public listIntMinWithout = new Array();
  public listDecValueWithout = new Array();
  public listDecMaxWithout = new Array();
  public listDecMinWithout = new Array();
  public listIntFixedValueWithout = new Array();
  public listIntFixedValueWith = new Array();
  public listIntFixedWithout = new Array();
  public inputIdDecWithout = '';
  public decMinWithout = '';
  public decMaxWithout = '';
  public decFixedWithout = '';
  public listChangedConstraintFlagWithout = new Array();
  public listChangedConstraintWithout = new Array();
  public listDecFixedValueWithout = new Array();
  public listDecFixedWithout = new Array();
  public inputIdIntWithout = '';
  public intMinWithout = '';
  public intMaxWithout = '';
  public intFixedWithout = '';
  public idRowTableRightWithout = -1;
  public correctInputWithout = false;
  public invalidInputCarWithout = false;
  public listChangeCarWithout = new Array();
  public showInformationWindowWithout = false;
  public isWithout = false;

  public booleanClickKeypressWithout = true;
  public oldInputWithout = '';
  public listSummaryValueWithout = new Array();
  public invalidInputMinCarWithout = false;
  public invalidInputMaxCarWithout = false;

  // left table with role group
  public listNumberAttributesWith = new Array();
  public idRowLeftTableWith = -1;
  public listIndexSubRg = new Array();
  public idRg = -1;
  public idSubRg = -1;
  public listOperatorRangeWith = new Array();
  public listConstraintRangeWith = new Array();
  public listNameWith = new Array();
  public actualAttributeNameWith = new Array();
  public showInformationWindowWith = false;
  public indexWith = -1;
  public listLoadDataAttributeWith = new Array();
  public listIndexWith = new Array();
  public actualMinCarWith = new Array();
  public actualMaxCarWith = new Array();
  public actualValueRangeWith = new Array();
  public invalidInputMinCarWith = false;
  public invalidInputMaxCarWith = false;
  public actualCheckCarWith = true;
  public listSummaryValueWith = new Array();
  public listAllCodesRangeWith = new Array();
  public listAllNamesRangeWith = new Array();
  public listSummaryNameRangeWith = new Array();
  public listSummaryCodeRangeWith = new Array();
  public listSummaryMinCardinalityWith = new Array();
  public listSummaryMaxCardinalityWith = new Array();
  public listSummaryAttributeCodesWith = new Array();
  public listSummaryAttributeNamesWith = new Array();
  public listSummaryOperatorRangeWith = new Array();
  public listSummaryConstraintRangeWith = new Array();
  public selfGroupedAttributeCode = new Array();
  public selfGroupedAttributeName = new Array();
  public listChangedConstraintWith = new Array();
  public listChangedAttributeCodeWith = new Array();
  public listChangedAttributeNameWith = new Array();
  public listFlagAttributeRelation = new Array();
  public listDecFixedWith = new Array();
  public listDecFixedValueWith = new Array();
  public booleanIntWith = false;
  public booleanDecWith = false;
  public booleanNormalWith = false;
  public intDefMinWith = -1;
  public intDefMaxWith = -1;
  public decDefMinWith = -1;
  public decDefMaxWith = -1;
  public intMaxWith = '';
  public intMinWith = '';
  public intFixedWith = '';
  public decMaxWith = '';
  public decMinWith = '';
  public decFixedWith = '';
  public listIntFixedWith = new Array();
  public isWith = false;
  public listCodeRangeWith = new Array();
  public invalidInputCarWith = false;
  public listInfoDefinitionWith = new Array();
  public listInfoConstraintWith = new Array();
  public correctInputWith = false;
  public idRowTableRightWith = -1;
  public inputIdIntWith = '';
  public inputIdDecWith = '';
  public listChangedFlagWith = new Array();
  public minCarRg = new Array();
  public maxCarRg = new Array();
  public invalidRgCar = false;
  public isUpdate = false;
  public oldInputWith = [];
  public listChangeCarWith = new Array();

  public termTemplate = '';
  public listCodeWordsWith = new Array();
  public listCodeWordsPositionWith = new Array();
  public listCodeWordsWithout = new Array();
  public listCodeWordsPositionWithout = new Array();
  public resultTemplate = '';
  public nameTemplate = '';
  public isTemplatedCreated = false;
  public errorMessageSummary = '';
  public listSelfGroupedAttributesPceCode = new Array();
  public listSelfGroupedAttributesPceName = new Array();
  public listSelfGroupedBoolean = new Array();
  public jsonDiagramPce = '';

  // Modal
  public listAttributeValueCodesFilter = new Array();
  public listAttributeValueNameFilter = new Array();
  public attributesFilterBoolean = false;
  public idTableRow = '';

  public idLastTab = '';
  public idActualTab = '';
  public expressionPCE = '';
  public namePCE = '';
  public savePCETextarea = '';

  ngOnInit() {
    // Update
    if(this.pceWithoutTemplateService.isUpdate) {
      this.appService.selectedApp = '';
      this.preprocessingUpdate();
    } else {
    }
  }

  preprocessingUpdate() {
    setTimeout(() => {
      let elementInput = document.getElementById('input-focusConcept-dummy');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = true;
      }
      elementInput = document.getElementById('input-focusConcept');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = true;
      }
    }, 10);

    this.booleanBtnFcDisabled  = true;
    this.focusConceptCode = this.pceWithoutTemplateService.focusConceptCode;
    this.focusConceptName = this.pceWithoutTemplateService.focusConceptName;
    this.changeInputField();
    this.preprocessingConceptModel();
    this.pceWithoutTemplateService.showGenerationTemplate = false;

    setTimeout(() => {
      if(this.pceWithoutTemplateService.isWithout && this.showGenerationTemplate) {
        //attribute without Rg
        this.preprocessingUpdateWithout();
      }
      if(this.pceWithoutTemplateService.isWith && this.showGenerationTemplate) {
        //attribute with Rg
        this.preprocessingUpdateWith();
        this.showInformationWindowWith = true;
      }

      if(this.pceWithoutTemplateService.isWithout) {
        setTimeout(() => {
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWithout0') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 1000);
      } else if(this.pceWithoutTemplateService.isWith)  {
        setTimeout(() => {
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWith00') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 5000);
      }
      this.pceWithoutTemplateService.showGenerationTemplate = true;
    }, 15000);
  }


  preprocessingUpdateWithout() {
    this.listIndexWithout = new Array();
    // count, wie oft ein attribut vorkommt

    this.setAttributeRelationsWithout();

    for(let j = 0; j < this.pceWithoutTemplateService.listWithoutAttributeNameName.length; j++) {
      for(let i = 0; i < this.listWithoutAttributeName.length; i++) {
        if(this.listWithoutAttributeName[i] === this.pceWithoutTemplateService.listWithoutAttributeNameName[j]) {



          if (this.listChangedConstraintFlagWithout[i].toString() === '0') {
            let count = this.listWithoutAttributeName.filter(x => x.split(" ").join("") === this.listWithoutAttributeName[i].split(" ").join("")).length;
            if(count === 1) {
              this.listIndexWithout.push(i);
            } else {
              let x = this.listWithoutAttributeName.lastIndexOf(this.listWithoutAttributeName[i]);
              if(this.listChangedConstraintFlagWithout[x].toString() === '1') {
                this.listIndexWithout.push(x);
              } else {
                this.listIndexWithout.push(i);
              }
            }


            this.listSummaryValueWithout.push(this.pceWithoutTemplateService.listWithoutAttributeValue[j]);
            this.actualAttributeNameWithout.push(this.pceWithoutTemplateService.listWithoutAttributeNameName[j]);
            this.listLoadDataAttributeWithout.push(this.pceWithoutTemplateService.listLoadDataAttributeWithout[j]);
            this.listNameWithout.push(this.pceWithoutTemplateService.listNameWithout[j]);
          }
        }
      }
    }

    for(let i = 0; i < this.listSummaryValueWithout.length; i++) {
      if(this.listSummaryValueWithout[i].includes("#")) {
        if(this.listSummaryValueWithout[i].includes(".")) {
          this.decFixedWithout = this.listSummaryValueWithout[i].split(" ").join("").split("#").join("");
        } else {
          this.intFixedWithout = this.listSummaryValueWithout[i].split(" ").join("").split("#").join("");
        }
      }
      this.idRowLeftTableWithout = i;
      this.addRowTableLeftWithout();
      this.loadAttributeDataWithout(i);
      this.getRowTableLeftWithout(i);
      this.pceWithoutTemplateService.showGenerationTemplate = false;
    }

    for(let i  = 0; i < this.actualAttributeNameWithout.length; i++) {
      if (this.actualAttributeNameWithout[i] !== undefined) {
        if (this.actualAttributeNameWithout[i] === '' || this.actualAttributeNameWithout[i].length === 0) {
          this.idRowLeftTableWithout = i;
          this.deleteRowTableLeftWithout();
        }
      }
    }
    this.listSummaryAttributeNamesWithout = this.listSummaryAttributeNamesWithout.filter((x: any) => x.length !== 0);
    this.listSummaryAttributeCodesWithout = this.listSummaryAttributeCodesWithout.filter((x: any) => x.length !== 0);
    this.actualAttributeNameWithout = this.actualAttributeNameWithout.filter((x: any) => x.length !== 0);


    this.idRowLeftTableWithout = this.actualAttributeNameWith.length - 1;
    this.addRowTableLeftWithout();
    this.idRowLeftTableWithout = 0;
    this.getRowTableLeftWithout(0);
    this.loadWithout();
  }

  preprocessingUpdateWith() {
    this.listSummaryValueWith = new Array();
    this.listSummaryAttributeNamesWith = new Array();
    this.listSummaryAttributeCodesWith = new Array();
    this.actualAttributeNameWith = new Array();
    this.listLoadDataAttributeWith = new Array();
    for(let i = 0; i < this.listInitWithMinCar.length; i++) {
      this.listSummaryValueWith.push(new Array());
      this.listSummaryAttributeNamesWith.push(new Array());
      this.listSummaryAttributeCodesWith.push(new Array());
      this.actualAttributeNameWith.push(new Array());
      this.listLoadDataAttributeWith.push(new Array());
    }

    let idx = 0;

    for(let i = 0; i < this.pceWithoutTemplateService.listWithAttributeValue[0].length; i++) {
      for(let k = 0; k < this.listInitWithAttributeCode[0].length; k++) {
        if(this.listInitWithMinCar[0][k] !== undefined) {
          if (this.listInitWithMinCar[0][k].toString() === '1') {
            if (!this.pceWithoutTemplateService.listWithAttributeNameCode[0][i].includes(this.listInitWithAttributeCode[0][k])) {
              idx = 1;
              break;
            } else {
              idx = 0;
            }
          }
        }
      }
      this.listSummaryValueWith[idx].push(this.pceWithoutTemplateService.listWithAttributeValue[0][i]);
      this.listSummaryAttributeNamesWith[idx].push(this.pceWithoutTemplateService.listWithAttributeNameName[0][i]);
      this.listSummaryAttributeCodesWith[idx].push(this.pceWithoutTemplateService.listWithAttributeNameCode[0][i]);
      this.actualAttributeNameWith[idx].push(this.pceWithoutTemplateService.listWithAttributeNameName[0][i]);
      this.listLoadDataAttributeWith[idx].push(this.pceWithoutTemplateService.listLoadDataAttributeWith[0]);
    }

    if(this.listInitWithAttributeCode.length > 1) {
      for(let i = 0; i < this.listInitWithAttributeCode.length; i++) {
        this.pceWithoutTemplateService.listIsUpdateAttributeWith.push(new Array())
        this.pceWithoutTemplateService.listLoadDataAttributeWith.push(new Array())
      }
    }

    this.actualAttributeNameWith[0] = this.actualAttributeNameWith[0].filter((x: any) => x.length !== 0);
    let v = this.actualAttributeNameWith[0].length;

    for (let j = 0; j < v-1; j++) {
      this.actualAttributeNameWith[0][j] = this.actualAttributeNameWith[0][j].filter((x: any) => x.length !== 0);
      this.loadNewRoleGroup();
    }

    this.listIndexWith = new Array();
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();

    for(let i = 0; i < this.actualAttributeNameWith.length; i++) {
      this.listIndexWith.push(new Array());
      this.listAllCodesRangeWith.push(new Array());
      this.listAllNamesRangeWith.push(new Array());
      for (let j = 0; j < this.actualAttributeNameWith[i].length; j++) {
        if(this.actualAttributeNameWith[i][j].length > 0) {
          this.listIndexWith[i].push(new Array());
          this.listAllCodesRangeWith[i].push(new Array());
          this.listAllCodesRangeWith[i].push(new Array());
          this.listAllNamesRangeWith[i].push(new Array());
          for (let k = 0; k < this.actualAttributeNameWith[i][j].length; k++) {
            for (let l = 0; l < this.listInitWithAttributeName[this.idRg].length; l++) {
              if (this.actualAttributeNameWith[i][j][k] === this.listInitWithAttributeName[this.idRg][l]) {
                this.indexWith = l;
                this.listAllCodesRangeWith[i][j].push(new Array());
                this.listAllNamesRangeWith[i][j].push(new Array());
                this.listIndexWith[i][j].push(l);
              }
            }
          }
        }
      }
    }

    if(this.listChangedConstraintWith[0].includes('1')) {
      this.listIndexWith = this.listIndexWithNew;

      // für alle @ die richtigen Numbers finden -- sind keine AR und somit mit 0 vorne
      for(let i = 0; i < this.listIndexWith.length; i++) {
        for(let j = 0; j < this.listIndexWith[i].length; j++) {
          for(let k = 0; k < this.listIndexWith[i][j].length; k++) {
            if(this.listIndexWith[i][j][k] === '@') {
              for(let l = 0; l < this.listInitWithAttributeCode[0].length; l++) {
                if(this.listInitWithAttributeCode[0][l] === this.listSummaryAttributeCodesWith[i][j][k]) {
                  this.listIndexWith[i][j][k] = l.toString();
                }
              }
            }
          }
        }
      }
    }

    for(let i  = 0; i < this.actualAttributeNameWith[0].length; i++) {
      for(let j  = 0; j < this.actualAttributeNameWith[0].length; j++) {
        if (this.actualAttributeNameWith[0][i][j] !== undefined) {
          if (this.actualAttributeNameWith[0][i][j] === '') {
            this.idRg = 0;
            this.idSubRg = i;
            this.idRowLeftTableWith = j;
            this.deleteRowTableLeftWith();
          }
        }
      }
    }
    this.idRowLeftTableWith = 0;
    this.loadWith(0);
    this.showInformationWindowWith = true;
    this.isWith = true;
  }



  keypressFocusConcept() {
    this.listAllNamesFocusConcept = new Array();
    this.listAllCodesFocusConcept = new Array();

    if (this.focusConceptName.length > 2) {
      let elementInput = document.getElementById('spinnerFc');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }


      this.pceWithoutTemplateService.getFocusConceptAttributeValueRequestFilterLimitFSN(this.focusConceptName, '1000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesFocusConcept.includes(value[i].code)) {
              this.listAllCodesFocusConcept.push(value[i].code);
              this.listAllNamesFocusConcept.push(value[i].fsn);
              if(Number.parseInt(this.focusConceptName)) {
                this.focusConceptName = value[i].fsn;
              }
            }
          }
        }

        setTimeout(() => {
          let elementInput = document.getElementById('spinnerFc');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }, 1000);
      })
    } else {
      this.listAllCodesFocusConcept = new Array();
      this.listAllNamesFocusConcept = new Array();
    }
    // }
  }

  checkBtnFcDisabled() {
    return !this.listAllNamesFocusConcept.includes(this.focusConceptName);
  }

  start() {
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = 'inline';
    }

    let elementDummy = document.getElementById('input-focusConcept-dummy');
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = 'none';
    }
    this.focusConceptName = '';
  }

  changeInputField() {
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = elementInput.style.display === 'none' ? 'inline' : 'none';
    }

    let elementDummy = document.getElementById('input-focusConcept-dummy');
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = elementDummy.style.display === 'inline' ? 'none' : 'inline';

    }
  }


  getConceptModel() {
    this.booleanBtnFcDisabled = true;
    let elementInput = document.getElementById('input-focusConcept-dummy');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = true;
    }

    let elementOk = document.getElementById('btn-ok-focusConcept');
    if (elementOk != null) {
      // @ts-ignore
      elementOk.disabled = true;
    }

    let pos = -1;
    for (let i = 0; i < this.listAllNamesFocusConcept.length; i++) {
      if (this.focusConceptName === this.listAllNamesFocusConcept[i]) {
        pos = i;
        break;
      }
    }
    this.focusConceptCode = this.listAllCodesFocusConcept[pos];
    this.preprocessingConceptModel();
  }


  preprocessingConceptModel() {
    if(!this.pceWithoutTemplateService.isUpdate) {
      this.pceWithoutTemplateService.getPreprocessingData(this.focusConceptCode).then((response: any) => {
        let value = response.values;
        this.conceptModelReferencedComponentId = value[0].conceptModelReferencedComponentId;
        this.conceptModelDomainConstraint = value[1].conceptModelDomainConstraint;
        this.conceptModelParentDomain = value[2].conceptModelParentDomain;
        this.conceptModelProximalPrimitiveConstraint = value[3].conceptModelProximalPrimitiveConstraint;
        this.conceptModelProximalPrimitiveRefinement = value[4].conceptModelProximalPrimitiveRefinement;
        this.conceptModelURL = value[5].conceptModelURL;
        this.conceptModelTemplate = value[6].conceptModelTemplate;
        this.conceptModelFocusConceptTemplate = value[7].conceptModelFocusConceptTemplate;

        if(value[35].listInitWithRoleGroupMinCar !== undefined) {
          if(value[35].listInitWithRoleGroupMinCar.length > 0) {
            this.preprocessingWith(value);
          }
        }
        if(value[10].listWithoutAttributeCode.length > 0) {
          this.preprocessingWithout(value);
        }

        this.showGenerationTemplate = true;
      })
    } else if(this.pceWithoutTemplateService.isUpdate) {
      this.pceWithoutTemplateService.getPreprocessingDataUpdate(this.focusConceptCode).then((response: any) => {
        let value = response.values;
        this.conceptModelReferencedComponentId = value[0].conceptModelReferencedComponentId;
        this.conceptModelDomainConstraint = value[1].conceptModelDomainConstraint;
        this.conceptModelParentDomain = value[2].conceptModelParentDomain;
        this.conceptModelProximalPrimitiveConstraint = value[3].conceptModelProximalPrimitiveConstraint;
        this.conceptModelProximalPrimitiveRefinement = value[4].conceptModelProximalPrimitiveRefinement;
        this.conceptModelURL = value[5].conceptModelURL;
        this.conceptModelTemplate = value[6].conceptModelTemplate;
        this.conceptModelFocusConceptTemplate = value[7].conceptModelFocusConceptTemplate;

        if(value[35].listInitWithRoleGroupMinCar.length > 0) {
          this.preprocessingWith(value);
        }
        if(value[10].listWithoutAttributeCode.length > 0) {
          this.preprocessingWithout(value);
        }
        this.showGenerationTemplate = true;
      })
    }



  }

  preprocessingWithout(value: any) {
    this.listWithoutMinCar = value[8].listWithoutMinCar;
    for(let i = 0; i < this.listWithoutMinCar.length; i++) {
      this.listWithoutMinCar[i] = Number.parseInt(this.listWithoutMinCar[i]);
    }
    this.listWithoutMaxCar = value[9].listWithoutMaxCar;

    for(let i = 0; i < this.listWithoutMaxCar.length; i++) {
      if(this.listWithoutMaxCar[i] === 'Infinity') {
        this.listWithoutMaxCar[i] = Number.POSITIVE_INFINITY;
      } else {
        this.listWithoutMaxCar[i] = Number.parseInt(this.listWithoutMaxCar[i]);
      }

    }
    this.listWithoutAttributeCode = value[10].listWithoutAttributeCode;
    this.listWithoutAttributeName = value[11].listWithoutAttributeName;
    this.listWithoutConstraint = value[12].listWithoutConstraint;
    this.listChangedConstraintWithout = value[13].listChangedConstraintWithout;
    this.listChangedConstraintFlagWithout = value[14].listChangedConstraintFlagWithout;
    this.listAllNamesRangeWithout = value[15].listAllNamesRangeWithout;
    this.listAllCodesRangeWithout = value[16].listAllCodesRangeWithout;
    this.listIntValueWithout = value[26].listIntValueWithout;
    this.listIntFixedWithout = value[27].listIntFixedWithout;
    this.listDecValueWithout = value[28].listDecValueWithout;
    this.listDecFixedWithout = value[29].listDecFixedWithout;
    this.listConstraintRangeWithout = value[31].listConstraintRangeWithout;
    this.listNumberAttributesWithout = value[33].listNumberAttributesWithout;
    this.listChangeCarWithout = value[34].listChangeCarWithout;

    this.loadAttributeDefinitionConceptModel();
    this.loadWithout();
    this.checkMandatoryElementWithout();
    if(!this.pceWithoutTemplateService.isUpdate) {
      this.setAttributeRelationsWithout();
    }

    this.pceWithoutTemplateService.testSubsumption(this.focusConceptCode).then((response: any) => {
      this.isProduct = response.result;
    })
    this.showInformationWindowWithout = true;


    setTimeout(() => {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWithout0') {
              element.style.background = 'lightblue';
            }
          }
        }
      }
    }, 10);

    setTimeout(() => {
      let b = false;
      this.idRowLeftTableWithout = 0;
      if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout] !== undefined) {
        if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout].toUpperCase() !== 'count' && this.isAttributeRelationWithout()) {
          b = true;
        }
      }
      if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
        let element = document.getElementById('id-value-without');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
        element = document.getElementById('updateValueWithout');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
      }

    }, 100);
  }

  public isProduct = '';
  public listIndexWithNew = new Array();

  preprocessingWith(value: any) {
    this.selfGroupedAttributeCode.push('288556008');    // before
    this.selfGroupedAttributeCode.push('371881003');    // during
    this.selfGroupedAttributeCode.push('255234002');    // after
    this.selfGroupedAttributeCode.push('42752001');     // due to
    this.selfGroupedAttributeCode.push('263502005');    // clinical course
    this.selfGroupedAttributeCode.push('726633004');    // temporally related
    this.selfGroupedAttributeCode.push('47429007');     // associated with

    this.selfGroupedAttributeName.push('Before')
    this.selfGroupedAttributeName.push('During')
    this.selfGroupedAttributeName.push('After')
    this.selfGroupedAttributeName.push('Due to')
    this.selfGroupedAttributeName.push('Clinical course')
    this.selfGroupedAttributeName.push('Temporally related to')
    this.selfGroupedAttributeName.push('Associated with')

    this.listInitWithRoleGroupMinCar = value[35].listInitWithRoleGroupMinCar;
    this.listInitWithRoleGroupMaxCar = value[36].listInitWithRoleGroupMaxCar;
    this.listInitWithMinCar = value[37].listInitWithMinCar;
    this.listInitWithMaxCar = value[38].listInitWithMaxCar;
    this.listInitWithAttributeCode = value[39].listInitWithAttributeCode;
    this.listInitWithAttributeName = value[40].listInitWithAttributeName;
    this.listInitWithConstraint = value[41].listInitWithConstraint;
    this.listChangedConstraintWith = value[42].listChangedConstraintWith;
    this.listChangedAttributeCodeWith = value[43].listChangedAttributeCodeWith;
    this.listChangedAttributeNameWith = value[44].listChangedAttributeNameWith;
    this.listChangedFlagWith = value[45].listChangedFlagWith;
    this.listIndexWithNew = value[46].listIndexWith;


    for(let i = 0; i < this.listInitWithRoleGroupMaxCar.length; i++) {
      this.listNumberAttributesWith.push(new Array());
      this.listNumberAttributesWith[i].push(new Array());
      this.listIndexWith.push(new Array());
      this.listIndexWith[i].push(new Array());
      this.listIndexSubRg.push(new Array());
      this.listIndexSubRg[i].push(0);
      this.actualAttributeNameWith.push(new Array());
      this.actualAttributeNameWith[i].push(new Array());
      this.actualAttributeNameWith[i][0].push('');
      this.listSummaryAttributeCodesWith.push(new Array());
      this.listSummaryAttributeCodesWith[i].push(new Array());
      this.listSummaryAttributeNamesWith.push(new Array());
      this.listSummaryAttributeNamesWith[i].push(new Array());
      this.listSummaryOperatorRangeWith.push(new Array());
      this.listSummaryOperatorRangeWith[i].push(new Array());
      this.listSummaryConstraintRangeWith.push(new Array());
      this.listSummaryConstraintRangeWith[i].push(new Array());
      this.listSummaryCodeRangeWith.push(new Array());
      this.listSummaryCodeRangeWith[i].push(new Array());
      this.listSummaryNameRangeWith.push(new Array());
      this.listSummaryNameRangeWith[i].push(new Array());
      this.listSummaryValueWith.push(new Array());
      this.listSummaryValueWith[i].push(new Array());
      this.listOperatorRangeWith.push(new Array());
      this.listOperatorRangeWith[i].push(new Array(new(Array)));
      this.listOperatorRangeWith[i][0][0].push('');
      this.listConstraintRangeWith.push(new Array());
      this.listConstraintRangeWith[i].push(new Array(new(Array)));
      this.listConstraintRangeWith[i][0][0].push('');
      this.listNameWith.push(new Array());
      this.listNameWith[i].push(new Array(new(Array)));
      this.listNameWith[i][0][0].push('');
      this.listCodeRangeWith.push(new Array());
      this.listCodeRangeWith[i].push(new Array(new(Array)));
      this.listCodeRangeWith[i][0][0].push('');

      this.listAllNamesRangeWith.push(new Array());
      this.listAllNamesRangeWith[i].push(new Array());
      this.listAllNamesRangeWith[i][0].push(new Array());

      this.listAllCodesRangeWith.push(new Array());
      this.listAllCodesRangeWith[i].push(new Array());
      this.listAllCodesRangeWith[i][0].push(new Array());

      this.listChangeCarWith.push(new Array());
      this.listChangeCarWith[i].push(new Array());
      this.listChangeCarWith[i][0].push(false);
      this.actualMinCarWith.push(new Array());
      this.actualMinCarWith[i].push(new Array());
      this.actualMinCarWith[i][0].push('');
      this.actualMaxCarWith.push(new Array());
      this.actualMaxCarWith[i].push(new Array());
      this.actualMaxCarWith[i][0].push('');

      this.actualValueRangeWith.push(new Array());
      this.actualValueRangeWith[i].push(new Array());
      this.actualValueRangeWith[i][0].push('');
      this.listSummaryMaxCardinalityWith.push(new Array());
      this.listSummaryMaxCardinalityWith[i].push(new Array());
      this.listSummaryMinCardinalityWith.push(new Array());
      this.listSummaryMinCardinalityWith[i].push(new Array());

      this.minCarRg.push(new Array());
      this.maxCarRg.push(new Array());
      this.minCarRg[i].push(this.listInitWithRoleGroupMinCar[i]);
      let max = this.listInitWithRoleGroupMaxCar[i];
      if(this.listInitWithRoleGroupMaxCar[i].toString() === 'Infinity') {
        max = '*'
      }
      this.maxCarRg[i].push(max);


      this.listSelfGroupedAttributesPceCode.push(new Array());
      this.listSelfGroupedAttributesPceCode[i].push(new Array());
      this.listSelfGroupedAttributesPceName.push(new Array());
      this.listSelfGroupedAttributesPceName[i].push(new Array());
      this.listSelfGroupedBoolean.push(new Array());
      this.listSelfGroupedBoolean[i].push(new Array());


    }
    this.idSubRg = 0;

    this.loadAttributeDefinitionConceptModel();
    this.loadWith(0);

    setTimeout(() => {
      if(!this.pceWithoutTemplateService.isUpdate) {
        this.setAttributeRelationsWith();
        this.checkMandatoryElementWith(0);
        setTimeout(() => {
          this.idSubRg = this.actualAttributeNameWith[0].length - 1;
          this.setSubRg(0);
        }, 10);

      }
    }, 1000);

    this.showInformationWindowWith = true;

    setTimeout(() => {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
              element.style.background = 'lightblue';
            }
          }
        }
      }
    }, 10);
  }

  checkMandatoryElementWithout() {
    let idx = 0;
    for(let i = 0; i < this.listWithoutMinCar.length; i++) {
      if (this.listWithoutMinCar[i].toString() === '1' && this.listChangedConstraintFlagWithout[i].toString() === '0') {
        this.actualAttributeNameWithout[idx] = this.listWithoutAttributeName[i];
        this.idRowLeftTableWithout = idx;
        this.listNumberAttributesWithout.push(idx.toString())
        this.loadAttributeDataWithout(idx);
        setTimeout(() => {
          let element = document.getElementById('attributeNameWithoutDataList' + this.idRowLeftTableWithout);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }, 10);
        idx = idx + 1;
      }
    }

  }

  checkMandatoryElementWith(subRg : number) {
    for(let i = 0; i < this.listInitWithMinCar.length; i++) {
      let idx = 0;
      if(this.actualAttributeNameWith[i][subRg] !== undefined) {
        idx = this.actualAttributeNameWith[i][subRg].length - 1;
      }
      for(let j = 0; j < this.listInitWithMinCar[i].length; j++) {
        if (this.listInitWithMinCar[i][j].toString() === '1' && this.listChangedFlagWith[i][j] === '0' && !this.listSummaryAttributeCodesWith[i][subRg].toString().includes(this.listInitWithAttributeCode[i][j])) {        // todo -- hier das eingefügt, dass nur bei 1. subRg die AR angezeigt werden
          this.idRowLeftTableWith = idx;
          this.idRg = i;
          this.listNumberAttributesWith[i][subRg].push(idx);
          this.listCodeRangeWith[i][subRg][idx] = new Array(this.listInitWithAttributeCode[i][j]);
          this.loadAttributeDataWith(idx);
          this.actualAttributeNameWith[i][subRg][idx] = this.listInitWithAttributeName[i][j];
          this.getRowTableLeftWith(idx);
          idx = idx + 1;
        }
      }
    }
  }

  loadAttributeDefinitionConceptModel() {
    const file = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "conceptModelObjectAttributes.csv");
    const fileContent = file[0].fileContent;
    const arr = fileContent.split("\n");
    for (let i = 0; i < arr.length; i++) {
      let value = arr[i].split(";")
      this.attributeDefinitionCMCode.push(value[0]);
      this.attributeDefinitionCMName.push(value[1]);
      this.attributeDefinitionCMDefinition.push(value[2]);
      this.attributeDefinitionCMRangeConstraint.push(value[3]);
    }
  }



  setAttributeRelationsWith() {
    let listFlagRg = new Array();

    this.listNumberAttributesWith[0].push(new Array());
    let max = Math.max(...this.listChangedFlagWith);
    for(let i  = 1; i < max; i++) {
      this.loadNewRoleGroup();
      listFlagRg.push(new Array());
    }

    let idx = 0;
    let value = 0;
    let list = new Array();
    for(let i = 0; i < this.listChangedFlagWith.length; i++) {
      for(let j = 0; j < this.listChangedFlagWith[i].length; j++) {
        // alle flags angucken --- erster, der nicht 0 ist wird auf 1 gesetzt, wenn wieder und sich gemerkt
        if(this.listChangedFlagWith[i][j].toString() !== '0') {
          if(this.listChangedFlagWith[i][j].toString() !== idx.toString() && this.listChangedFlagWith[i][j].toString() !== value) {
            idx = idx + 1;
            value = this.listChangedFlagWith[i][j];
            this.listChangedFlagWith[i][j] = idx.toString();
          } else {
            this.listChangedFlagWith[i][j] = idx.toString();
          }
        }
      }
    }

    for(let i = 0 ; i < this.listChangedFlagWith.length; i++) {
      for(let j = 0 ; j < this.listChangedFlagWith[i].length; j++) {
        if (this.listChangedFlagWith[i][j].toString().split('\"').join('') !== '0') {
          this.idRg = i;
          if (this.idSubRg !== this.listChangedFlagWith[i][j] - 1) {
            this.idSubRg = this.listChangedFlagWith[i][j] - 1;
            this.listIndexWith[this.idRg].push(new Array());
            this.listIndexWith[this.idRg][this.idSubRg].push(j);
            this.idRowLeftTableWith = 0;
            this.loadNewRoleGroup();
            this.getRowTableLeftWith(0);
            this.setSubRg(this.listChangedFlagWith[i][j] - 1);
          } else {
            this.idRowLeftTableWith = this.actualAttributeNameWith[this.idRg][this.idSubRg].length - 1;
          }

          this.addRowTableLeftWith();

          if(this.listInitWithAttributeCode[i][j].length > 0) {
            this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listInitWithAttributeName[i][j];
            this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = j;
            this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].push(this.listInitWithAttributeName[i][j]);
            this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].push(this.listInitWithAttributeCode[i][j]);
            this.listInitWithMinCar[this.idRg][j] = 1;
            this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg] = this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].filter((x: any) => x.length !== 0);
          }

          if (this.listInitWithConstraint[i][j].includes("<")) {

            this.booleanDecWith = false;
            this.booleanIntWith = false;
            this.booleanNormalWith = true;
            // this.listSummaryValueRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith - 1] = this.listInitWithConstraint[i][j] ;

            let concept = this.listChangedConstraintWith[i][j].split("<").join("");
            if(concept.substring(0,1) === '') {
              this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept.substring(1);
            } else {
              this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept;
            }


            this.getRowTableLeftWith(this.idRowLeftTableWith)
          } else if (!this.listInitWithConstraint[i][j].includes("<") && this.listInitWithConstraint[i][j].includes("+dec")) {
            this.decFixedWith = this.listInitWithConstraint[i][j].split("+dec(#").join("").split(")").join("");
            this.updateDecFixedValueWith();
            this.getRowTableLeftWith(this.idRowLeftTableWith);
          } else {
            this.intFixedWith = this.listInitWithConstraint[i][j].split("+int(#").join("").split(")").join("");
            this.updateIntFixedValueWith();
            this.getRowTableLeftWith(this.idRowLeftTableWith);
          }
        }
      }


      this.isWith = true;
      this.listLoadDataAttributeWith[this.idRowLeftTableWith - 1] = true;

      if(this.listWithoutMinCar.length > 0) {
        setTimeout(() => {
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWithout0') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 1000);
      } else {
        setTimeout(() => {
          this.idRg = 0;
          this.idSubRg = 0;
          this.idRowLeftTableWith = 0;
          let tables = document.querySelectorAll("table");
          for (let i = 0; i < tables.length; i++) {
            let trs = document.querySelectorAll("tr");
            for (let j = 0; j < trs.length; j++) {
              let element = trs[j];
              if (element != null) {
                element.style.background = 'transparent';
                if (element.id === 'rowTableLeftWith000') {
                  element.style.background = 'lightblue';
                }
              }
            }
          }
        }, 10);
      }
    }

    for(let i = 0; i < this.listSummaryAttributeCodesWith[this.idRg].length; i++) {
      this.listSummaryAttributeCodesWith[this.idRg][i] = this.listSummaryAttributeCodesWith[this.idRg][i].filter((x: any) => x.length !== 0);
      this.listSummaryAttributeNamesWith[this.idRg][i] = this.listSummaryAttributeNamesWith[this.idRg][i].filter((x: any) => x.length !== 0);
    }
  }

  setAttributeRelationsWithout() {
    let idx = 0;
    let dummy = 'start';

    for(let i = 0 ; i < this.listChangedConstraintFlagWithout.length; i++) {
      if(this.listChangedConstraintFlagWithout[i] != null) {
        if (this.listChangedConstraintFlagWithout[i].toString() === '1') {

          if(!this.pceWithoutTemplateService.isUpdate) {
            this.addRowTableLeftWithout();
          }
          this.actualAttributeNameWithout.push(this.listWithoutAttributeName[i]);
          this.idRowLeftTableWithout = idx;
          this.listIndexWithout[this.idRowLeftTableWithout] = i
          this.listSummaryAttributeNamesWithout.push(this.listWithoutAttributeName[i]);
          this.listSummaryAttributeCodesWithout.push(this.listWithoutAttributeCode[i]);
          this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;

          this.actualAttributeNameWithout = this.actualAttributeNameWithout.filter((x: any) => x.length !== 0);

          if(this.listWithoutConstraint[i].includes("<")) {
            this.booleanDecWithout = false;
            this.booleanIntWithout = false;
            this.booleanNormalWithout = true;
            this.getRowTableLeftWithout(idx, dummy);

            let concept = this.listChangedConstraintWithout[i].split("<").join("");
            if(concept.substring(0,1) === '') {
              this.listSummaryValueWithout[idx] = concept.substring(1);
            } else {
              this.listSummaryValueWithout[idx] = concept;
            }
          }
          else if (!this.listWithoutConstraint[i].includes("<") && this.listWithoutConstraint[i].includes("+dec")){
            this.decFixedWithout = this.listWithoutConstraint[i].split("+dec(#").join("").split(")").join("");
            this.listSummaryValueWithout[idx] = '#' + this.listWithoutConstraint[i].split("+dec(#").join("").split(")").join("");
            this.updateDecFixedValueWithout();
            this.getRowTableLeftWithout(idx);
          } else {
            this.intFixedWithout = this.listWithoutConstraint[i].split("+int(#").join("").split(")").join("");
            this.listSummaryValueWithout[idx] = '#' + this.listWithoutConstraint[i].split("+int(#").join("").split(")").join("");
            this.updateIntFixedValueWithout();
            this.getRowTableLeftWithout(idx);
          }
          idx = idx + 1;
          this.listFlagAttributeRelation.push(true);
        }
      }
    }
  }





  popupInformationDefinitionWithout() {
    let info = '';
    for (let i = 0; i < this.attributeDefinitionCMCode.length; i++) {
      if (this.attributeDefinitionCMCode[i].toString() === this.listWithoutAttributeCode[this.listIndexWithout[this.idRowLeftTableWithout]]) {
        info = this.attributeDefinitionCMDefinition[i];
      }
    }
    if(info === '' || info === '-') {
      info = '----';
    }
    return info;
  }

  popupInformationConstraintWithout() {
    let ref = this.conceptModelTemplate.substring(this.conceptModelTemplate.lastIndexOf(":") + 1);
    let attr = ref.split(",");
    for(let i = 0; i < attr.length; i++) {
      if(attr[i].includes(this.listWithoutAttributeCode[this.listIndexWithout[this.idRowLeftTableWithout]])) {
        if(this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]] === "1") {
          let value = this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]];
          if((this.isProduct === 'equivalent' || this.isProduct === 'subsumes') && this.isAttributeRelationWith() && value.includes("|")) {
            value = value.replace("<<", "");
          }
          if (value.includes('+int')) {
            value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split("+dec(").join("").split(')').join('').split('|').join('');
          } else if (value.includes('+dec')) {
            value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split("+dec(").join("").split(')').join('').split('|').join('');
          }
          return value;
        } else {
          let value = attr[i].split("=")[1].split("[[+scg(").join("").split("[[+id(").join("").split(")]]").join("");
          if (value.includes('+int')) {
            value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split('|').join('');
          } else if (value.includes('+dec')) {
            value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split('|').join('');
          }
          return value;
        }

      }
    }
    return "----";
  }


  checkAdditionalHelp(id:string) {
    if(id === 'without') {
      let productAttributeRelation = (this.isProduct === 'subsumes' || this.isProduct === 'equivalent') && this.isAttributeRelationWithout();
      if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] === undefined || productAttributeRelation) {
        return false;
      } else if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('int(') && !this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('dec(')) {
        return true;
      }
    } else if(id === 'with') {
      let productAttributeRelation = (this.isProduct === 'subsumes' || this.isProduct === 'equivalent') && this.isAttributeRelationWith();
      if(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] === undefined || productAttributeRelation) {
        return false;
      } else if(!this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('int(') && !this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('dec(')) {
        return true;
      }
    }
    return false;
  }

  getAllPossibleConceptsWithout() {
    let element = document.getElementById('linkButton');
    if (element != null) {
      // @ts-ignore
      element.blur();
    }

    this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]], '', '100000').then((response: any) => {
      const value = response.values;
      if(typeof value !== 'undefined') {
        for (let i = 0; i < value.length; i++) {
          if(!this.listAttributeValueCodesFilter.includes(value[i].code)) {
            this.listAttributeValueCodesFilter.push(value[i].code);
            this.listAttributeValueNameFilter.push(value[i].fsn);
          }
        }
      }
      this.attributesFilterBoolean = true;
    })
  }

  getAllPossibleConceptsWith() {
    let element = document.getElementById('linkButton');
    if (element != null) {
      // @ts-ignore
      element.blur();
    }

    // linkButton
    this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]], '', '100000').then((response: any) => {
      const value = response.values;
      if(typeof value !== 'undefined') {
        for (let i = 0; i < value.length; i++) {
          if(!this.listAttributeValueCodesFilter.includes(value[i].code)) {
            this.listAttributeValueCodesFilter.push(value[i].code);
            this.listAttributeValueNameFilter.push(value[i].fsn);
          }
        }
      }
      this.attributesFilterBoolean = true;
      this.getRowTableLeftWith(this.idRowLeftTableWith);
    })
  }

  isAttributeRelationWithout() {
    if(typeof this.listFlagAttributeRelation[this.idRowLeftTableWithout] === 'undefined' && this.listFlagAttributeRelation[this.idRowLeftTableWithout] === null) {
      return false;
    } else if (this.listFlagAttributeRelation[this.idRowLeftTableWithout] != null) {
      return this.listFlagAttributeRelation[this.idRowLeftTableWithout];
    } else {
      return false;
    }
  }

  isMandatoryWithout() {
    if(this.isAttributeRelationWithout()) {
      return false;
    }
    if(this.actualAttributeNameWithout[this.idRowLeftTableWithout] !== undefined) {
      for(let i = 0; i < this.listWithoutAttributeName.length; i++) {
        if(this.actualAttributeNameWithout[this.idRowLeftTableWithout].split(" ").join("") === this.listWithoutAttributeName[i].split(" ").join("")) {
          if(this.listWithoutMinCar[i].toString() === '1') {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;
  }


  loadWithout() {
    setTimeout(() => {
      let elementTab = document.getElementById('tab-without');
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'lightblue';
      }
      if(this.idLastTab !== 'tab-without') {
        elementTab = document.getElementById(this.idLastTab);
        if (elementTab != null) {
          // @ts-ignore
          elementTab.style.backgroundColor = 'transparent';
        }
      }
      this.idLastTab = 'tab-without';
      this.idActualTab = 'tab-without';
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWithout.length; j++) {
        if(typeof this.actualAttributeNameWithout[j] === 'undefined') {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWithout[j].length === 0) {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);

    setTimeout(() => {
      this.getRowTableLeftWithout(0);
    }, 10);
  }


  getRowTableLeftWithout(idx: number, dummy?: string) {
    this.idRowLeftTableWithout = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWithout' + this.idRowLeftTableWithout) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    if(this.getRangeWithout(this.idRowLeftTableWithout) === "") {
      setTimeout(() => {
        let elementInput = document.getElementById('updateValueWithout');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.disabled = true;
        }
      }, 50);
    }

    if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
      this.listNameWithout[this.idRowLeftTableWithout] = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('|')[1];
    } else {
      this.listNameWithout[this.idRowLeftTableWithout] = '';
    }

    this.booleanIntWithout = false;
    this.booleanDecWithout = false;
    this.booleanNormalWithout = false;
    this.intFixedWithout = '';
    this.decFixedWithout = '';

    if(typeof this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if ((this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(") || this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec(")) && this.listChangedConstraintFlagWithout[this.listIndexWithout[this.idRowLeftTableWithout]].toString() !== '0') {
        let elementInput = document.getElementById('label-range');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.style.display = 'none';
        }
      }
      if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(")) {
        this.booleanIntWithout = true;
        this.booleanDecWithout = false;
        this.booleanNormalWithout = false;
        let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+int').join('').split(')').join("");
        v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.intDefMinWithout = s[0];
        } else {
          this.intDefMinWithout = 0;
        }

        if(v.includes("..")) {
          if (s[1].length > 0) {
            this.intDefMaxWithout = s[1];
          } else {
            this.intDefMaxWithout = Number.POSITIVE_INFINITY;
          }
        }

        if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
          this.intFixedWithout = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('#').join("").split(' ').join("");
        } else {
          this.intFixedWithout = '';
        }
      } else if (this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec(")) {
        this.booleanIntWithout = false;
        this.booleanDecWithout = true;
        this.booleanNormalWithout = false;
        let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+dec').join('').split(')').join("");
        v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("");
        let s = v.split("..");
        if (s[0].length > 0) {
          this.decDefMinWithout = s[0];
        } else {
          this.decDefMinWithout = 0;
        }

        if(v.includes("..")) {
          if (s[1].length > 0) {
            this.decDefMaxWithout = s[1];
          } else {
            this.decDefMaxWithout = Number.POSITIVE_INFINITY;
          }
        }

        if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
          this.decFixedWithout = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('#').join("").split(' ').join("");
        } else {
          this.decFixedWithout = '';
        }

      } else {
        this.booleanIntWithout = false;
        this.booleanDecWithout = false;
        this.booleanNormalWithout = true;
      }
      this.intMaxWithout = '';
      this.intMinWithout = '';



      setTimeout(() => {
        let element = document.getElementById('label-range');
        if (element != null) {
          // @ts-ignore
          element.style.display = 'inline';
        }
      }, 10);

      if(this.isAttributeRelationWithout()) {
        this.enableDisableInputRangeField(true);


      } else {
        this.enableDisableInputRangeField(false);
      }

    }


  }

  enableDisableInputRangeField(b: boolean) {
    if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('+dec') || this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('dec(')) {
      this.booleanIntWithout = false;
      this.booleanDecWithout = true;
      this.booleanNormalWithout = false;
      setTimeout(() => {
        let element = document.getElementById('attributeDecFixedWithout');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
        element = document.getElementById('updateValueWithout');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
      }, 10);
      if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
        this.decFixedWithout = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('#').join('');
      }
    } else if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('+int') || this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes('int(')) {
      this.booleanIntWithout = true;
      this.booleanDecWithout = false;
      this.booleanNormalWithout = false;
      setTimeout(() => {
        let element = document.getElementById('attributeIntFixedWithout');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
        element = document.getElementById('updateValueWithout');
        if (element != null) {
          // @ts-ignore
          element.disabled = b;
        }
      }, 10);
      if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
        this.intFixedWithout = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('#').join('');
      }
    } else {
      this.booleanIntWithout = false;
      this.booleanDecWithout = false;
      this.booleanNormalWithout = true;
      setTimeout(() => {
        if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout] !== undefined) {
          if(this.listSummaryAttributeNamesWithout[this.idRowLeftTableWithout].toUpperCase() !== 'count' && this.isAttributeRelationWithout()) {
            b = true;
          }
        }
        if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
          let element = document.getElementById('id-value-without');
          if (element != null) {
            // @ts-ignore
            element.disabled = b;
          }
          element = document.getElementById('updateValueWithout');
          if (element != null) {
            // @ts-ignore
            element.disabled = b;
          }
        }
      }, 10);
    }
  }




  loadAttributeDataWithout(idx: number, dummy?: string) {
    if(dummy === undefined) {
      dummy = '0';
    }
    this.idRowLeftTableWithout = idx;
    this.clickRowTableWithout(idx)
    this.showInformationWindowWithout = true;

    setTimeout(() => {
      if (!this.listWithoutAttributeName.includes(this.actualAttributeNameWithout[this.idRowLeftTableWithout])) {
        this.actualAttributeNameWithout[this.idRowLeftTableWithout] = '';
        this.showInformationWindowWithout = false;
      } else {
        for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
          if ((this.actualAttributeNameWithout[this.idRowLeftTableWithout] === this.listWithoutAttributeName[i] && this.listChangedConstraintFlagWithout[i].toString() === dummy)) {
            this.indexWithout = i;
            this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;
            this.listIndexWithout[this.idRowLeftTableWithout] = i;
            this.showInformationWindowWithout = true;
            let id = -1;
            for(let j = 0; j < this.listWithoutAttributeName.length; j++) {
              if(this.listWithoutAttributeName[j] === this.actualAttributeNameWithout[this.idRowLeftTableWithout]) {
                id = j;
                break;
              }
            }
            this.listSummaryAttributeNamesWithout.push(this.actualAttributeNameWithout[this.idRowLeftTableWithout]);
            this.listSummaryAttributeCodesWithout.push(this.listWithoutAttributeCode[id]);

            this.isWithout = true;

            setTimeout(() => {
              if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] !== undefined) {

                if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("int(")) {
                  this.booleanIntWithout = true;
                  this.booleanDecWithout = false;
                  this.booleanNormalWithout = false;
                  let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+int').join('').split(')').join("");
                  v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
                  let s = v.split("..");
                  if(s[0].length > 0) {
                    this.intDefMinWithout = s[0];
                  } else {
                    this.intDefMinWithout = 0;
                  }
                  if(s[1].length > 0) {
                    this.intDefMaxWithout = s[1];
                  } else {
                    this.intDefMaxWithout = Number.POSITIVE_INFINITY;
                  }
                } else if(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("dec(")) {
                  this.booleanIntWithout = false;
                  this.booleanDecWithout = true;
                  this.booleanNormalWithout = false;

                  let v = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].split("#").join("").split('[[+dec').join('').split(')').join("");
                  v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("");
                  let s = v.split("..");
                  if (s[0].length > 0) {
                    this.decDefMinWithout = s[0];
                  } else {
                    this.decDefMinWithout = 0;
                  }
                  if(v.includes("..")) {
                    if (s[1].length > 0) {
                      this.decDefMaxWithout = s[1];
                    } else {
                      this.decDefMaxWithout = Number.POSITIVE_INFINITY;
                    }
                  }

                  if(this.listSummaryValueWithout[this.idRowLeftTableWithout] !== undefined) {
                    this.decFixedWithout = this.listSummaryValueWithout[this.idRowLeftTableWithout].split('#').join("").split(' ').join("");
                  } else {
                    this.decFixedWithout = '';
                  }


                } else {
                  this.booleanIntWithout = false;
                  this.booleanDecWithout = false;
                  this.booleanNormalWithout = true;

                }
                let elementInput = document.getElementById('attributeNameWithoutDataList' + idx);
                if (elementInput != null) {
                  // @ts-ignore
                  elementInput.readOnly = true;
                  elementInput.style.backgroundColor = '#eaecef';
                  elementInput.style.outline = 'none';
                  elementInput.style.boxShadow = 'none';
                }
              }
            }, 10);
            break;
          }

        }
      }
    }, 10);
  }


  getAttributesWithout() {
    let list = new Array();
    if(this.listWithoutAttributeName != null && this.actualAttributeNameWithout != null) {
      for(let i = 0; i < this.listWithoutAttributeName.length; i++) {
        let count = this.listSummaryAttributeNamesWithout.filter(x => x.split(" ").join("") === this.listWithoutAttributeName[i].split(" ").join("")).length;
        if(this.listWithoutMaxCar[i].toString() === 'Infinity' && this.listChangedConstraintFlagWithout[i].toString() === '0') {
          list.push(this.listWithoutAttributeName[i]);
        } else if(Number.parseInt(String(count)) < Number.parseInt(this.listWithoutMaxCar[i]) && this.listChangedConstraintFlagWithout[i].toString() === '0') {
          list.push(this.listWithoutAttributeName[i]);
        }
      }
    }
    return list;
  }

  addRowTableLeftWithout() {
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.invalidInputCarWithout = false;

    this.listNumberAttributesWithout.push("");
    this.actualAttributeNameWithout.push("");
    this.listLoadDataAttributeWithout.push(false);

    this.listConstraintRangeWithout = new Array();
    // this.listNameRangeWithout = new Array();
    this.listIndexWith.push(new Array());

    this.listConstraintRangeWithout.push("");
    // this.listNameRangeWithout.push("");

    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';

    this.getRowTableLeftWithout(this.idRowLeftTableWithout)

    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWithout.length; j++) {
        if(typeof this.actualAttributeNameWithout[j] === 'undefined') {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWithout[j].length === 0) {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithoutDataList' + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
      // this.getRowTableLeftWith(0);
    }, 10);
  }

  deleteRowTableLeftWithout() {
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.invalidInputCarWithout = false;

    this.listAllNamesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listAllCodesRangeWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = new Array();
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.listSummaryAttributeCodesWithout.push('');
    this.listSummaryMinCardinalityWithout.push('');
    this.listSummaryMaxCardinalityWithout.push('');
    this.listSummaryConstraintRangeWithout.push('');
    this.listIntValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listDecValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '';
    this.listChangeCarWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = false;
    this.listNumberAttributesWithout.splice(this.idRowLeftTableWithout, 1);
    this.actualAttributeNameWithout.splice(this.idRowLeftTableWithout, 1);
    this.listLoadDataAttributeWithout.splice(this.idRowLeftTableWithout, 1);
    this.listSummaryValueWithout.splice(this.idRowLeftTableWithout, 1);
    this.listSummaryAttributeNamesWithout.splice(this.idRowLeftTableWithout, 1);
    this.listConstraintRangeWithout = new Array();
    this.listNameWithout = new Array();

    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';
    this.listIndexWithout.splice(this.idRowLeftTableWithout, 1);

    if(!this.pceWithoutTemplateService.isUpdate) {
      if(this.idRowLeftTableWithout > 0) {
        this.getRowTableLeftWithout(this.idRowLeftTableWithout - 1);
      }
      else {
        this.getRowTableLeftWithout(this.idRowLeftTableWithout);
      }
    }
  }

  idBtnAddWithoutDisabled() {
    let l = this.getAttributesWithout()
    let s = this.actualAttributeNameWithout[this.actualAttributeNameWithout.length - 1];
    if(this.listNumberAttributesWithout.length < 1) {
      return false;
    } else if(this.listNumberAttributesWithout.length > this.actualAttributeNameWithout.length) {
      return true;
    }
    if (typeof s !== 'undefined' && l.length > 0) {
      for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
        if (this.listWithoutAttributeName[i] === s) {
          return false;
        }
      }
      return true;
    }
    return true;
  }

  idBtnDeleteWithoutDisabled() {
    if(this.listNumberAttributesWithout.length < 1) {
      return true;
    }
    if(typeof this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if(this.listWithoutMinCar[this.listIndexWithout[this.idRowLeftTableWithout]].toString() === '1') {
        return true;
      }
    }
    if(this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]] != null && typeof this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]] !== 'undefined') {
      if(this.listChangedConstraintWithout[this.listIndexWithout[this.idRowLeftTableWithout]].length > 0) {
        return true;
      }
    }
    return false;
  }

  enableButtonWithout() {
    for (let i = 0; i < this.listAllNamesRangeWithout[this.idRowLeftTableWithout].length; i++) {
      if(this.listAllNamesRangeWithout[this.idRowLeftTableWithout][i] === this.listNameWithout[this.idRowLeftTableWithout]) {
        setTimeout(() => {
          let elementInput = document.getElementById('updateValueWithout');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = false;
          }
        }, 10);
        return null;
      }
    }
    setTimeout(() => {
      let elementInput = document.getElementById('updateValueWithout');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = true;
      }
    }, 10);
    return null;
  }


  keypressGetAttributeValueWithout(idx: number) {
    this.pceWithoutTemplateService.listIsUpdateAttributeWithout[this.idRowLeftTableWithout] = false;
    const arr = this.listNameWithout[idx] || [];
    this.listAllCodesRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.listAllNamesRangeWithout[this.idRowLeftTableWithout] = new Array();
    this.correctInputWithout = false;

    // if(Number.parseInt(arr)) {
    //   this.keypressEnterWithout(idx);
    //   console.log("HIER!!!")
    // }

    if (arr.length > 2) {
      let elementInput = document.getElementById('spinnerWithout');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }
      if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes("(")) {
        this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace(")", "");
      } else if(!this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].includes(")")) {
        this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]] = this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace("(", "");
      }
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listWithoutConstraint[this.listIndexWithout[this.idRowLeftTableWithout]].replace("id(", ""), this.listNameWithout[idx], '1000').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesRangeWithout[this.idRowLeftTableWithout].includes(value[i].code)) {
              this.listAllCodesRangeWithout[this.idRowLeftTableWithout].push(value[i].code);
              this.listAllNamesRangeWithout[this.idRowLeftTableWithout].push(value[i].fsn);
              if(Number.parseInt(this.listNameWithout[idx])) {
                this.listNameWithout[idx] = value[i].fsn;
              }
            }
          }

          this.enableButtonWithout();

        }
        this.oldInputWithout = this.listNameWithout[this.indexWithout];


        setTimeout(() => {
          let elementInput = document.getElementById('spinnerWithout');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }, 500);

        return "";
      })
    }
    return "";
  }


  getIdRowTableRightWithout(idx: number) {
    this.idRowTableRightWithout = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableRightWithout' + this.idRowTableRightWithout) {
            element.style.background = 'lightblue';
          }
          if (element.id === 'rowTableLeftWithout' + this.idRowLeftTableWithout) {
            element.style.background = 'lightblue';
          }
        }
      }
    }

    if(this.idRowTableRightWithout !== 0) {
      let elementInput = document.getElementById('deleteValueWithout');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = false;
      }
    } else {
      let elementInput = document.getElementById('deleteValueWithout');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = true;
      }
    }
  }



  updateValueWithout() {

    let code = '';
    let id = -1;
    for (let k = 0; k < this.listAllNamesRangeWithout[this.idRowLeftTableWithout].length; k++) {
      if (this.listAllNamesRangeWithout[this.idRowLeftTableWithout][k] === this.listNameWithout[this.idRowLeftTableWithout]) {
        id = k;
        break
      }
    }
    if(this.listAllCodesRangeWithout[this.idRowLeftTableWithout][id] !== undefined) {
      code = this.listAllCodesRangeWithout[this.idRowLeftTableWithout][id];
      this.listSummaryValueWithout[this.idRowLeftTableWithout] = code + ' |' + this.listNameWithout[this.idRowLeftTableWithout] + '|';
    } else {
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.listNameWithout[this.idRowLeftTableWithout].toString(), '1').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            code = value[i].code;
            this.listSummaryValueWithout[this.idRowLeftTableWithout] = code + ' |' + this.listNameWithout[this.idRowLeftTableWithout] + '|';
          }
        }
      })


    }

  }


  getPossibleValuesWithout() {
    return this.listAllNamesRangeWithout[this.idRowLeftTableWithout];
  }



  getRangeWithout(idx: number) {
    if(this.listSummaryValueWithout[idx] != null) {
      return this.listSummaryValueWithout[idx].split('MINUS').join(' MINUS ').split('OR').join(' OR ');
    } else {
      return  '';
    }
  }

  updateIntFixedValueWithout() {
    this.intFixedWithout = Math.round(Number.parseInt(this.intFixedWithout)).toString();
    this.listSummaryValueWithout[this.idRowLeftTableWithout] = '#' + this.intFixedWithout;
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = '#' + this.intFixedWithout;
    this.listIntFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.intFixedWithout;


  }

  isDisabledInputFixedWithout() {
    if(this.intFixedWithout === '' || this.intFixedWithout === null || this.intFixedWithout === undefined) {
      return true;
    } else if(this.intFixedWithout !== undefined) {
      return (Number.parseFloat(this.intFixedWithout) < this.intDefMinWithout) ||  (Number.parseFloat(this.intFixedWithout) > this.intDefMaxWithout)
    } else {
      return true;
    }
  }

  isDisabledDecInputFixedWithout() {
    if(this.decFixedWithout === '' || this.decFixedWithout === null || this.decFixedWithout === undefined) {
      return true;
    }
    return (Number.parseFloat(this.decFixedWithout) < this.decDefMinWithout) ||  (Number.parseFloat(this.decFixedWithout) > this.decDefMaxWithout)
  }


  updateDecFixedValueWithout() {
    this.listSummaryValueWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout;
    this.listSummaryCodeRangeWithout[this.idRowLeftTableWithout] = '#' + this.decFixedWithout;
    this.listSummaryNameRangeWithout[this.idRowLeftTableWithout] = '';
    this.listDecFixedValueWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = '#' + this.decFixedWithout;
    this.listDecFixedWithout[this.listIndexWithout[this.idRowLeftTableWithout]] = this.decFixedWithout;
    if(!this.listSummaryValueWithout[this.idRowLeftTableWithout].includes('.')) {
      this.listSummaryValueWithout[this.idRowLeftTableWithout] = this.listSummaryValueWithout[this.idRowLeftTableWithout] + '.0';
    }
  }



  //  WITH ROLE GROUP

  loadWith(i:number) {
    setTimeout(() => {
      let elementTab = document.getElementById('tab-with' + i);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'lightblue';
      }
    }, 10);
    setTimeout(() => {
      if(this.idLastTab.toString() !== ('tab-with' + i).toString()) {
        let elementTab = document.getElementById(this.idLastTab);
        if (elementTab != null) {
          // @ts-ignore
          elementTab.style.backgroundColor = 'transparent';
        }
        this.idSubRg = 0;
      }
      this.idLastTab = 'tab-with' + i;
      this.idActualTab = 'tab-with' + i;
      this.idRg = i;
    }, 10);

    setTimeout(() => {
      let element = document.getElementById('tab-subRg' + this.idRg + '0');
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
    }, 10);
    setTimeout(() => {
      if(this.idSubRg.toString() !== '0'.toString()) {
        let element = document.getElementById('tab-subRg' + this.idRg + this.idSubRg);
        if (element != null) {
          // @ts-ignore
          element.style.backgroundColor = 'transparent';
        }
      }
      this.idSubRg = 0;
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.actualAttributeNameWith[this.idRg][this.idSubRg].length; j++) {

        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
      this.getRowTableLeftWith(0);
    }, 10);
  }

  isAddRgPossible() {
    if(this.isProduct === 'equivalent' || this.isProduct === 'subsumes') {
      return !this.isAttributeRelationWithout();  // count of active ingredient
    }
    return true;
  }

  loadNewRoleGroup() {
    this.listIndexSubRg[this.idRg].push(this.listIndexSubRg[this.idRg].length);
    this.actualAttributeNameWith[this.idRg].push(new Array());
    this.actualMinCarWith[this.idRg].push(new Array());
    this.actualMaxCarWith[this.idRg].push(new Array());
    this.actualValueRangeWith[this.idRg].push(new Array());
    this.listAllNamesRangeWith[this.idRg].push(new Array());
    this.listAllCodesRangeWith[this.idRg].push(new Array());
    this.listIndexWith[this.idRg].push(new Array());;
    this.listSummaryAttributeNamesWith[this.idRg].push(new Array());
    this.listSummaryAttributeCodesWith[this.idRg].push(new Array());
    this.listSummaryOperatorRangeWith[this.idRg].push(new Array());
    this.listSummaryConstraintRangeWith[this.idRg].push(new Array());
    this.listSummaryCodeRangeWith[this.idRg].push(new Array());
    this.listSummaryNameRangeWith[this.idRg].push(new Array());
    this.listNumberAttributesWith[this.idRg].push(new Array());
    this.listSummaryMaxCardinalityWith[this.idRg].push(new Array());
    this.listSummaryMinCardinalityWith[this.idRg].push(new Array());
    this.listSummaryValueWith[this.idRg].push(new Array());

    this.listChangeCarWith[this.idRg].push(new Array());
    this.listIndexWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push("")



    this.actualAttributeNameWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualMinCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualMaxCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.actualValueRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listAllNamesRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listAllCodesRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listChangeCarWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(false);

    this.listSummaryAttributeNamesWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryAttributeCodesWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryNameRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryMaxCardinalityWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryMinCardinalityWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');
    this.listSummaryValueWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push('');

    this.minCarRg[this.idRg].push(this.listInitWithRoleGroupMinCar[this.idRg]);
    let max = this.listInitWithRoleGroupMaxCar[this.idRg];
    if(this.listInitWithRoleGroupMaxCar[this.idRg].toString() === 'Infinity') {
      max = '*'
    }
    this.maxCarRg[this.idRg].push(max);

    this.listOperatorRangeWith[this.idRg].push(new Array());
    this.listOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listOperatorRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listConstraintRangeWith[this.idRg].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listConstraintRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listNameWith[this.idRg].push(new Array());
    this.listNameWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listNameWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listCodeRangeWith[this.idRg].push(new Array());
    this.listCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1].push(new Array());
    this.listCodeRangeWith[this.idRg][this.listIndexSubRg[this.idRg].length - 1][0].push('');

    this.listSelfGroupedAttributesPceCode[this.idRg].push(new Array());
    this.listSelfGroupedAttributesPceName[this.idRg].push(new Array());
    this.listSelfGroupedBoolean[this.idRg].push(new Array());

    this.setSubRg(this.listIndexSubRg[this.idRg].length - 1);
    this.checkMandatoryElementWith(this.listIndexSubRg[this.idRg].length - 1);

  }



  setSubRg(idx : number) {

    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listOperatorRangeWith[this.idRg][this.idSubRg].push(new Array())
    this.listConstraintRangeWith[this.idRg][this.idSubRg].push(new Array())
    this.listNameWith[this.idRg][this.idSubRg].push(new Array())
    this.listCodeRangeWith[this.idRg][this.idSubRg].push(new Array())

    this.checkCardinalityWith('min', idx);
    this.checkCardinalityWith('max', idx);

    setTimeout(() => {
      let element = document.getElementById('tab-subRg' + this.idRg + idx);
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
    }, 10);
    setTimeout(() => {
      if(this.idSubRg.toString() !== idx.toString()) {
        let element = document.getElementById('tab-subRg' + this.idRg + this.idSubRg);
        if (element != null) {
          // @ts-ignore
          element.style.backgroundColor = 'transparent';
        }
      }
      this.idSubRg = idx;
    }, 10);

    setTimeout(() => {
      let element = document.getElementById('rowTableLeftWith' + + this.idRg + this.idSubRg + '0');
      if (element != null) {
        // @ts-ignore
        element.style.backgroundColor = 'lightblue';
      }
      this.booleanDecWith = false;
      this.booleanIntWith = false;
      this.booleanNormalWith = false;
      this.getRowTableLeftWith(0);
    }, 100);

    setTimeout(() => {
      for(let j = 0; j < this.actualAttributeNameWith[this.idRg][this.idSubRg].length; j++) {

        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
      this.getRowTableLeftWith(0);
    }, 10);

  }


  getRowTableLeftWith(idx: number) {
    this.idRowLeftTableWith = idx;

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + this.idRowLeftTableWith) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';
    this.booleanIntWith = false;
    this.booleanDecWith = false;
    this.booleanNormalWith = false;

    if(this.getRangeWith(this.idRowLeftTableWith) === "") {
      setTimeout(() => {
        let elementInput = document.getElementById('updateValueWith');
        if (elementInput != null) {
          // @ts-ignore
          elementInput.disabled = true;
        }
      }, 50);
    }
    if(this.idRowLeftTableWith > -1) {
      if(typeof this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== 'undefined') {
        if ((this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("int(") || this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("dec(")) && this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() !== '0') {
          let elementInput = document.getElementById('label-range');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }
        if (this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("int(")) {
          this.booleanIntWith = true;
          this.booleanDecWith = false;
          this.booleanNormalWith = false;
          let v = this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].split("#").join("").split('[[+int').join('').split(')').join("");
          v = v.split('int').join('').split(')').join("").split("(").join("").split(">").join("");
          let s = v.split("..");
          if (s[0].length > 0) {
            this.intDefMinWith = s[0];
          } else {
            this.intDefMinWith = 0;
          }

          if(v.includes("..")) {
            if (s[1].length > 0) {
              this.intDefMaxWith = s[1];
            } else {
              this.intDefMaxWith = Number.POSITIVE_INFINITY;
            }
          }

          if(this.listSummaryValueWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== undefined) {
            this.intFixedWith = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split('#').join("").split(" ").join("");

          } else {
            this.intFixedWith = '';
          }

        } else if (this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes("dec(")) {
          this.booleanIntWith = false;
          this.booleanDecWith = true;
          this.booleanNormalWith = false;
          let v = this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].split("#").join("").split('[[+dec').join('').split(')').join("");
          v = v.split('dec').join('').split(')').join("").split("(").join("").split(">").join("");
          let s = v.split("..");
          if (s[0].length > 0) {
            this.decDefMinWith = s[0];
          } else {
            this.decDefMinWith = 0;
          }

          if(v.includes("..")) {
            if (s[1].length > 0) {
              this.decDefMaxWith = s[1];
            } else {
              this.decDefMaxWith = Number.POSITIVE_INFINITY;
            }
          }

          if (this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
            this.decFixedWith = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split('#').join("").split(" ").join("");
          } else {
            this.decFixedWith = '';
          }

        } else {
          this.booleanIntWith = false;
          this.booleanDecWith = false;
          this.booleanNormalWith = true;

          if(this.pceWithoutTemplateService.isUpdate) {
            this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("|")[1];
          }
        }
        this.intMaxWith = '';
        this.intMinWith = '';


        setTimeout(() => {
          let element = document.getElementById('label-range');
          if (element != null) {
            // @ts-ignore
            element.style.display = 'inline';
          }
        }, 10);

        if(this.isAttributeRelationWith()) {
          if(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('dec(')) {
            this.booleanIntWith = false;
            this.booleanDecWith = true;
            this.booleanNormalWith = false;
            setTimeout(() => {
              let element = document.getElementById('attributeDecFixedWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = true;
              }
              element = document.getElementById('updateValueWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = true;
              }
            }, 10);
          } else if(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('+int')) {
            this.booleanIntWith = true;
            this.booleanDecWith = false;
            this.booleanNormalWith = false;
            setTimeout(() => {
              let element = document.getElementById('attributeIntFixedWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = true;
              }
              element = document.getElementById('updateValueWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = true;
              }
            }, 10);
            if(this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
              this.intFixedWith = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split('#').join('');
            }
          } else {
            this.booleanIntWith = false;
            this.booleanDecWith = false;
            this.booleanNormalWith = true;



            setTimeout(() => {
              // bei Produkten sind die Substanzen, wie Aspirin ausgegraut, weil es genau für das Produkt steht und kein Unterkonzept
              if(this.isProduct === 'subsumes' || this.isProduct === 'equivalent') {
                let element = document.getElementById('id-value-with');
                if (element != null) {
                  // @ts-ignore
                  element.disabled = true;
                }
                element = document.getElementById('updateValueWith');
                if (element != null) {
                  // @ts-ignore
                  element.disabled = true;
                }
              }
            }, 10);

            let concept = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("|")[1];
            if(concept !== undefined) {
              if(concept.substring(0,1) === '') {
                this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept.substring(1);
              } else {
                this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept;
              }
            }
          }
        } else {
          if(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('dec(')) {
            this.booleanIntWith = false;
            this.booleanDecWith = true;
            this.booleanNormalWith = false;
            setTimeout(() => {
              let element = document.getElementById('attributeDecFixedWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
              element = document.getElementById('updateValueWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
            }, 10);
            if(this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
              this.decFixedWith = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split('#').join('');
            }
          } else if(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].includes('int(')) {
            this.booleanIntWith = true;
            this.booleanDecWith = false;
            this.booleanNormalWith = false;
            setTimeout(() => {
              let element = document.getElementById('attributeIntFixedWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
              element = document.getElementById('updateValueWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
            }, 10);
            this.intFixedWith = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split('#').join('');
          } else {
            this.booleanIntWith = false;
            this.booleanDecWith = false;
            this.booleanNormalWith = true;
            setTimeout(() => {
              let element = document.getElementById('id-value-with');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
              element = document.getElementById('updateValueWith');
              if (element != null) {
                // @ts-ignore
                element.disabled = false;
              }
            }, 10);

            if(this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
              let concept = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split("|")[1];
              if(concept !== undefined) {
                if(concept.substring(0,1) === '') {
                  this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept.substring(1);
                } else {
                  this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = concept;
                }
              }
            }
          }
        }
      }
    }
  }


  getAttributesWith() {
    let list = new Array();
    if(this.listInitWithAttributeName[this.idRg] != null && this.actualAttributeNameWith[this.idRg][this.idSubRg] != null) {
      for(let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
        if(this.listInitWithAttributeName[this.idRg][i] !== undefined) {
          if(this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg]) {
            let counter = this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].filter((x: any) => x.split(" ").join("") === this.listInitWithAttributeName[this.idRg][i].split(" ").join("")).length;

            if(this.listInitWithMaxCar[this.idRg][i] !== undefined) {
              if(this.listInitWithMaxCar[this.idRg][i].toString() === 'Infinity') {
                if (this.listInitWithAttributeName[this.idRg][i].toString().startsWith(' ')) {
                  list.push(this.listInitWithAttributeName[this.idRg][i].trimLeft());
                } else {
                  list.push(this.listInitWithAttributeName[this.idRg][i]);
                }
              } else if(Number.parseInt(String(counter)) < Number.parseInt(this.listInitWithMaxCar[this.idRg][i])) {
                // list.push(this.listInitWithAttributeName[this.idRg][i]);
                if (this.listInitWithAttributeName[this.idRg][i].toString().startsWith(' ')) {
                  list.push(this.listInitWithAttributeName[this.idRg][i].trimLeft());
                } else {
                  list.push(this.listInitWithAttributeName[this.idRg][i]);
                }
              }
            }
          }
        }

      }
    }
    return list;
  }

  loadAttributeDataWith(idx: number) {
    setTimeout(() => {
      this.idRowLeftTableWith = idx;

      if (!this.listInitWithAttributeName[this.idRg].includes(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]) && this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
        this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
        this.showInformationWindowWith = false;
      } else {
        for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
          if (this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined && this.listInitWithAttributeName[this.idRg][i] !== undefined) {
            if (this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].toString() === this.listInitWithAttributeName[this.idRg][i].toString()) {
              this.indexWith = i;
              // this.listLoadDataAttributeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = true;
              this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = i;

              let id = -1;
              for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
                if (this.listInitWithAttributeName[this.idRg][i].toString() === this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].toString()) {
                  id = i;
                  break;
                }
              }
              this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
              this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listInitWithAttributeCode[this.idRg][id];
              this.isWith = true;

              this.getRowTableLeftWith(this.idRowLeftTableWith)
              this.showInformationWindowWith = true;

              setTimeout(() => {
                let element = document.getElementById('btn-add-row-with-left');
                if (element != null) {
                  // @ts-ignore
                  element.disabled = false;
                }
              }, 10);

            }
          }
        }
      }
    }, 10);

    setTimeout(() => {
      for(let j = 0; j < this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].length; j++) {
        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }
    }, 10);


  }

  addRowTableLeftWith() {


    setTimeout(() => {
      let element = document.getElementById('btn-add-row-with-left');
      if (element != null) {
        // @ts-ignore
        element.disabled = true;
      }
    }, 10);

    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listNumberAttributesWith[this.idRg][this.idSubRg].push(0);
    if(!this.pceWithoutTemplateService.isUpdate) {
      this.actualAttributeNameWith[this.idRg][this.idSubRg].push('');
    }

    this.listAllNamesRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.listAllCodesRangeWith[this.idRg][this.idSubRg].push(new Array());
    this.actualMinCarWith[this.idRg][this.idSubRg].push("");
    this.actualMaxCarWith[this.idRg][this.idSubRg].push("");
    this.actualValueRangeWith[this.idRg][this.idSubRg].push("");

    // this.listLoadDataAttributeWith[this.idRg][this.idSubRg].push(false);

    this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].push("");
    this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].push("");
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg].push("");
    this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg].push("");
    this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg].push("");
    this.listSummaryValueWith[this.idRg][this.idSubRg].push("");

    if(this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length]) {
      this.listOperatorRangeWith[this.idRg][this.idSubRg].push(new Array());
      this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
      this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = new Array();
      this.listOperatorRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0][0] = '';
    }

    if(this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length]) {
      this.listConstraintRangeWith[this.idRg][this.idSubRg].push(new Array());
      this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
      this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';
    }

    this.listNameWith[this.idRg][this.idSubRg].push(new Array());
    this.listNameWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
    this.listNameWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';

    if(this.listConstraintRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length]) {
      this.listCodeRangeWith[this.idRg][this.idSubRg].push(new Array());
      this.listCodeRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length].push(new Array());
      this.listCodeRangeWith[this.idRg][this.idSubRg][this.listIndexWith[this.idRg][this.idSubRg].length][0] = '';
    }

    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';

    setTimeout(() => {
      for(let j = 0; j < this.listNumberAttributesWith[this.idRg][this.idSubRg].length; j++) {
        if(typeof this.actualAttributeNameWith[this.idRg][this.idSubRg][j] === 'undefined') {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else if(this.actualAttributeNameWith[this.idRg][this.idSubRg][j].length === 0) {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = false;
          }
        } else {
          let element = document.getElementById('attributeNameWithDataList' + this.idRg + this.idSubRg + j);
          if (element != null) {
            // @ts-ignore
            element.readOnly = true;
            element.style.backgroundColor = '#eaecef';
            element.style.outline = 'none';
            element.style.boxShadow = 'none';
          }
        }
      }




    }, 10);
  }

  deleteRowTableLeftWith() {
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    this.listSummaryNameRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryAttributeCodesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryAttributeNamesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listSummaryValueWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listNumberAttributesWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualAttributeNameWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualMinCarWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualMaxCarWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.actualValueRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    // this.listLoadDataAttributeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listOperatorRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listConstraintRangeWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    this.listNameWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);

    this.intMinWith = '';
    this.intMaxWith = '';
    this.intFixedWith = '';
    this.decMinWith = '';
    this.decMaxWith = '';
    this.decFixedWith = '';
    if(this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      this.listIndexWith[this.idRg][this.idSubRg].splice(this.idRowLeftTableWith, 1);
    }

    if(this.idRowLeftTableWith > 0) {
      this.getRowTableLeftWith(this.idRowLeftTableWith - 1);
    }
    else {
      setTimeout(() => {
        let tables = document.querySelectorAll("table");
        for (let i = 0; i < tables.length; i++) {
          let trs = document.querySelectorAll("tr");
          for (let j = 0; j < trs.length; j++) {
            let element = trs[j];
            if (element != null) {
              element.style.background = 'transparent';
              if (element.id === 'rowTableLeftWith' + this.idRg + this.idSubRg + '0') {
                element.style.background = 'lightblue';
              }
            }
          }
        }
        this.getRowTableLeftWith(0);
      }, 10);
    }
    setTimeout(() => {
      let elementInput = document.getElementById('btn-add-row-with-left');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = false;
      }
    }, 10);
  }

  idBtnAddWithDisabled() {
    if(this.listNumberAttributesWith[this.idRg][this.idSubRg].length > this.actualAttributeNameWith[this.idRg][this.idSubRg].length) {
      return true;
    }

    if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.actualAttributeNameWith[this.idRg][this.idSubRg].length - 1].length === 0) {
      return true;
    }

    let l = this.getAttributesWith()
    if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
      let s = this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
      if (s === undefined) {
        return true;
      }
      if (this.listNumberAttributesWith[this.idRg][this.idSubRg].length < 1) {
        return false;
      }
      if (typeof s !== 'undefined' && l.length > 0) {
        for (let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
          if (this.listInitWithAttributeName[this.idRg][i] === s) {
            return false;
          }
        }
        return true;
      }
    }
    return true;
  }

  idBtnDeleteWithDisabled() {
    if(this.listNumberAttributesWith[this.idRg].length < 1) {
      return true;
    }
    if(typeof this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== 'undefined') {
      if(this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() === '1') {
        return true;
      }
    }
    return false;
  }

  popupInformationDefinitionWith() {
    let info = '';
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      for (let i = 0; i < this.attributeDefinitionCMCode.length; i++) {
        if (this.attributeDefinitionCMCode[i].toString() === this.listInitWithAttributeCode[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]) {
          info = this.attributeDefinitionCMDefinition[i];
        }
      }
    }
    if(info === '' || info === '-') {
      info = '----';
    }
    return info;
  }

  popupInformationMinCarWith() {
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      return this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]
    } else {
      return "";
    }
  }

  popupInformationMaxCarWith() {
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      return this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]]
    } else {
      return "";
    }
  }

  popupInformationConstraintWith() {
    let ref = this.conceptModelTemplate.substring(this.conceptModelTemplate.lastIndexOf(":") + 1);
    let attr = ref.split(",");

    let value = '';
    if (this.listIndexWith[this.idRg][this.idSubRg] !== undefined) {
      for (let i = 0; i < attr.length; i++) {
        if (attr[i].includes(this.listInitWithAttributeCode[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]])) {
          if (this.listChangedFlagWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]] !== '0') {
            value = this.listChangedConstraintWith[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]];
            if((this.isProduct === 'equivalent' || this.isProduct === 'subsumes') && this.isAttributeRelationWith() && value.includes("|")) {
              value = value.replace("<<", "");
            }
          } else {
            value = attr[i].split("=")[1].split("[[+scg(").join("").split("[[+id(").join("").split(")]]").join("").split("}").join("");
          }
          if(value != null) {
            if (value.includes('+int')) {
              value = "- Integer: " + value.split("[[+int(").join("").split('#').join('').split("+int(").join("").split(")").join("").split("||").join("");
            } else if (value.includes('+dec')) {
              value = "- Decimal: " + value.split("[[+dec(").join("").split('#').join('').split("+dec(").join("").split(")").join("").split("||").join("");
            }
          }
          return value;
        }
      }
    }
    return "----";
  }

  isAttributeRelationWith() {
    for(let i = 0; i < this.listInitWithAttributeName[this.idRg].length; i++) {
      let x = Number.parseInt(this.listChangedFlagWith[this.idRg][i]) - 1;
      if(this.listInitWithAttributeName[this.idRg][i] === this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] && x.toString() === this.idSubRg.toString()) {
        return true;
      }
    }
    return false;
  }

  isMandatoryWith() {
    if(this.isAttributeRelationWith()) {
      return false;
    }

    if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] !== undefined) {
      for(let i = 0; i < this.listInitWithAttributeName[0].length; i++) {
        if(this.actualAttributeNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].split(" ").join("") === this.listInitWithAttributeName[0][i].split(" ").join("")) {
          if(this.listInitWithMinCar[0][i].toString() === '1') {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;
  }

  checkCardinalityWith(id: string, idx: number) {
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.invalidInputCarWith = false;
    this.actualCheckCarWith = true;

    if (id === 'min' && this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== '' && typeof this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== 'undefined') {
      if (isNaN(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) ||
        (Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) > Number.parseInt(this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]]) ||
          Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx]) < Number.parseInt(this.listInitWithMinCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][idx]]))) {
        this.actualMinCarWith[this.idRg][this.idSubRg][idx] = ''; // todo - hier ist das Alert immer gleich wieder weg
        this.invalidInputMinCarWith = true;

      } else {
        this.invalidInputMinCarWith = false;
      }
    } else if (id === 'max' && this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== '' && typeof this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== 'undefined') {
      if(this.actualMaxCarWith[this.idRg][this.idSubRg][idx] !== '*') {
        if ((isNaN(this.actualMaxCarWith[this.idRg][this.idSubRg][idx]) || (Number.parseInt(this.actualMaxCarWith[this.idRg][this.idSubRg][idx]) > Number.parseInt(this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]])))) {
          this.actualMaxCarWith[this.idRg][this.idSubRg][idx] = '';
          this.invalidInputMaxCarWith = true;
        } else if (this.actualMinCarWith[this.idRg][this.idSubRg][idx].toString() === '0' && this.actualMaxCarWith[this.idRg][this.idSubRg][idx].toString() === '0') {
          this.invalidInputCarWith = true;
        } else {
          this.invalidInputMaxCarWith = false;
          this.invalidInputCarWith = false;
        }
      } else if (this.actualMaxCarWith[this.idRg][this.idSubRg][idx].toString() === '*' && this.listInitWithMaxCar[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]].toString() !== 'Infinity') {
        this.actualMaxCarWith[this.idRg][this.idSubRg][idx] = '';       // todo - hier ist das Alert immer gleich wieder weg
        this.invalidInputMaxCarWith = true;
      } else {
        this.invalidInputMaxCarWith = false;
        this.invalidInputCarWith = false;
      }
      this.checkInputCarWith(idx);
    }

    if(!this.invalidInputMaxCarWith && !this.invalidInputMinCarWith) {
      this.listSummaryMinCardinalityWith[this.idRg][this.idSubRg][idx] = this.actualMinCarWith[this.idRg][this.idSubRg][idx];
      this.listSummaryMaxCardinalityWith[this.idRg][this.idSubRg][idx] = this.actualMaxCarWith[this.idRg][this.idSubRg][idx];
    }
  }

  checkInputCarWith(idx: number) {
    let maxCar = this.actualMaxCarWith[this.idRg][this.idSubRg][idx];
    if(maxCar === '*') {
      maxCar = Number.POSITIVE_INFINITY;
    } else {
      maxCar = Number.parseInt(maxCar)
    }

    if (maxCar !== '' && this.actualMinCarWith[this.idRg][this.idSubRg][idx] !== '') {
      if (maxCar >= Number.parseInt(this.actualMinCarWith[this.idRg][this.idSubRg][idx])) {
        this.actualCheckCarWith = true;
      } else {
        if (this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === '' || this.actualMinCarWith[this.idRg][this.idSubRg][idx] === '' || this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === null || this.actualMinCarWith[this.idRg][this.idSubRg][idx] === null || typeof this.actualMinCarWith[this.idRg][this.idSubRg][idx] === 'undefined' || typeof this.actualMaxCarWith[this.idRg][this.idSubRg][idx] === 'undefined') {
          this.actualCheckCarWith = true;
        } else {
          this.actualCheckCarWith = false;
        }
      }
    } else {
      this.actualCheckCarWith = true;
    }
  }

  getRangeWith(idx: number) {
    if(this.listSummaryValueWith[this.idRg][this.idSubRg][idx] !== undefined) {
      return this.listSummaryValueWith[this.idRg][this.idSubRg][idx].split('MINUS').join(' MINUS ').split('OR').join(' OR ').split('  |').join(" |");
    } else {
      return ""
    }
  }

  keypressGetAttributeValueWith(idx: number) {
    const arr = this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] || [];
    this.correctInputWithout = false;

    if (arr.length > 2) {
      if(this.pceWithoutTemplateService.isUpdate && this.pceWithoutTemplateService.listIsUpdateAttributeWith[this.idRg][this.idSubRg] !== undefined) {
        this.pceWithoutTemplateService.listIsUpdateAttributeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = false;
      }
      let elementInput = document.getElementById('spinnerWith');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.style.display = 'inline';
      }
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN(this.listInitWithConstraint[this.idRg][this.listIndexWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]], this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith], '1000').then((response: any) => {
        const value = response.values;
        this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = [];
        this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = [];
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            if(!this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes(value[i].code)) {
              this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push(value[i].code);
              this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push(value[i].fsn);
              if(Number.parseInt(this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith])) {
                this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = value[i].fsn;
              }
            }
          }
          this.enableButton()
        }

        setTimeout(() => {
          let elementInput = document.getElementById('spinnerWith');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.style.display = 'none';
          }
        }, 1000);
      })
    }
    return "";
  }


  getPossibleValuesWith() {
    return this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith];
  }


  enableButton() {
    for (let i = 0; i < this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; i++) {
      if(this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][i] === this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]) {
        setTimeout(() => {
          let elementInput = document.getElementById('updateValueWith');
          if (elementInput != null) {
            // @ts-ignore
            elementInput.disabled = false;
          }
        }, 10);
        return null;
      }
    }
    setTimeout(() => {
      let elementInput = document.getElementById('updateValueWith');
      if (elementInput != null) {
        // @ts-ignore
        elementInput.disabled = true;
      }
    }, 10);
    return null;
  }



  startFieldValueWith(idx: number) {
    let elementInput = document.getElementById('id-value-with' + this.idRg  + this.idSubRg + idx);
    if (elementInput != null) {
      // @ts-ignore
      elementInput.style.display = 'inline';
    }

    let elementDummy = document.getElementById('id-value-with-dummy' + this.idRg + this.idSubRg + idx);
    if (elementDummy != null) {
      // @ts-ignore
      elementDummy.style.display = 'none';
      if (elementDummy.style.display === 'none') {
        // this.listNameRangeWith[this.idRg][idx] = '';
        this.correctInputWith = false;
      }
    }

  }

  updateValueWith() {
    // this.listNameWith[idRg][idSubRg][idRowLeftTableWith]
    let code = '';
    let id = -1;
    for (let k = 0; k < this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].length; k++) {
      if (this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][k] === this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith]) {
        id = k;
        break
      }
    }
    code = this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][id];
    this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = code + ' |' + this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + '|';

    if(this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][id] !== undefined) {
      code = this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith][id];
      this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = code + ' |' + this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + '|';
    } else {
      this.pceWithoutTemplateService.getAttributeValueRequestFilterLimitFSN('<<138875005', this.listNameWithout[this.idRowLeftTableWithout].toString(), '1').then((response: any) => {
        const value = response.values;
        if(typeof value !== 'undefined') {
          for (let i = 0; i < value.length; i++) {
            code = value[i].code;
            this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = code + ' |' + this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + '|';
          }
        }
      })


    }

  }

  updateIntFixedValueWith() {
    this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.intFixedWith;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.intFixedWith;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';

  }


  isDisabledInputFixedWith() {
    if(this.intFixedWith === '' || this.intFixedWith === null || this.intFixedWith === undefined) {
      return true;
    } else if(this.intFixedWith !== undefined) {
      return (Number.parseFloat(this.intFixedWith) < this.intDefMinWith) ||  (Number.parseFloat(this.intFixedWith) > this.intDefMaxWith)
    } else {
      return true;
    }
  }

  isDisabledDecInputFixedWith() {
    if(this.decFixedWith === '' || this.decFixedWith === null || this.decFixedWith === undefined) {
      return true;
    } else if(this.decFixedWith !== undefined) {
      return (Number.parseFloat(this.decFixedWith) < this.decDefMinWith) ||  (Number.parseFloat(this.decFixedWith) > this.decDefMaxWith)
    } else {
      return true;
    }
  }


  updateDecFixedValueWith() {
    this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith;
    this.listSummaryOperatorRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryConstraintRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '#' + this.decFixedWith;
    this.listSummaryNameRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = '';
    if(!this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].includes('.')) {
      this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryCodeRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + '.0';
      this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] + '.0';
    }
  }


  // --------------- SUMMARY -------------------

  loadSummary() {
    let elementTab = document.getElementById('tab-summary');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }
    if(this.idLastTab !== 'tab-summary') {
      elementTab = document.getElementById(this.idLastTab);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'transparent';
      }
    }
    if(this.idLastTab !== 'tab-summary') {
      this.idLastTab = 'tab-summary';
      this.idActualTab = 'tab-summary';
    }

    if(this.pceWithoutTemplateService.isUpdate && this.listSummaryAttributeNamesWithout.length === 0) {
      for(let i = 0; i < this.actualAttributeNameWithout.length; i++) {
        this.listSummaryAttributeNamesWithout.push(this.actualAttributeNameWithout[i]);
        this.listSummaryAttributeCodesWithout.push(this.pceWithoutTemplateService.listWithoutAttributeNameCode[i]);
      }
    }

  }


  continue() {
    let elementTab = document.getElementById('tab-namePce');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }

    if(this.idLastTab !== 'tab-namePce') {
      elementTab = document.getElementById(this.idLastTab);
      if (elementTab != null) {
        // @ts-ignore
        elementTab.style.backgroundColor = 'transparent';
      }
    }

    this.idLastTab = 'tab-namePce'
    this.idActualTab = 'tab-namePce'

    for(let i = 0; i < this.listSummaryAttributeNamesWithout.length; i++) {
      // Pathological process
      let s = this.listSummaryAttributeNamesWithout[i].toLowerCase()
        .split(' ')
        .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
        .join('');

      s = s.substring(0,1).toLowerCase() + s.substring(1);
      if(s.length > 0) {
        if(!this.listCodeWordsWithout.includes(s)) {
          this.listCodeWordsWithout.push(s);
          this.listCodeWordsPositionWithout.push(i);
        } else {
          let listString = this.listCodeWordsWithout.toString();
          let matches = listString.match(s);
          // @ts-ignore
          this.listCodeWordsWithout.push(s + matches.length);
          this.listCodeWordsPositionWithout.push(i);
        }
      }
    }

    for(let i = 0; i < this.listSummaryAttributeNamesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeNamesWith[i].length; j++) {
        for(let k = 0; k < this.listSummaryAttributeNamesWith[i][j].length; k++) {
          if(this.listSummaryAttributeNamesWith[i][j][k].length > 0) {
            let s = this.listSummaryAttributeNamesWith[i][j][k].toLowerCase()
              .split(' ')
              .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1))
              .join('');

            s = s.substring(0, 1).toLowerCase() + s.substring(1);
            if (s.length > 0) {
              if (!this.listCodeWordsWithout.includes(s) && !this.listCodeWordsWith.includes(s)) {
                this.listCodeWordsWith.push(s);
                this.listCodeWordsPositionWith.push(i + '$' + j + '$' + k);
              } else {
                let listStringWithout = this.listCodeWordsWithout.toString();
                let listStringWith = this.listCodeWordsWith.toString();
                let matchesWithout = listStringWithout.match(s);
                let matchesWith = listStringWith.match(s);
                let lenWithout = 0;
                let lenWith = 0;

                if (matchesWithout !== null) {
                  lenWithout = matchesWithout.length;
                }
                if (matchesWith !== null) {
                  lenWith = matchesWith.length;
                }
                let res = lenWith + lenWithout;
                this.listCodeWordsWith.push(s + res);
                this.listCodeWordsPositionWith.push(i + '$' + j + '$' + k);
              }
            }
          }
        }
      }
    }
  }

  clickTableTermTemplate(pos: String, idx: number, value: String) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableTermTemplate' + pos) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
  }


  checkRg(id: number, subRg: number) {
    let l = new Array();
    if(this.listSummaryValueWith[id][subRg] !== undefined) {
      for (let i = 0; i < this.listSummaryValueWith[id][subRg].length; i++) {
        if (this.listSummaryValueWith[id][subRg][i].length > 0) {
          l.push('yes');
        }
      }
    }
    if(l.length > 0) {
      return true;
    } else{
      return false;
    }
  }

  checkValue(row: number, subrg: number, col: number) {
    if(this.listSummaryValueWith[row] !== undefined) {
      if(this.listSummaryValueWith[row][subrg] !== undefined) {
        if(this.listSummaryValueWith[row][subrg][col] !== undefined) {
          if(this.listSummaryValueWith[row][subrg][col].length > 0) {
            return true;
          }
        }
      }
    }
    if(this.listSummaryAttributeNamesWith[row][subrg][col].length > 0) {
      for(let i = 0; i < this.listInitWithAttributeCode[0].length; i++) {
        if(this.listInitWithAttributeName[0][i].split(" ").join("") == this.actualAttributeNameWith[row][subrg][col].split(" ").join("")) {
          if(this.listInitWithMinCar[0][i].toString() === '1') {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;


  }

  checkValueWithout0() {
    if(this.listSummaryValueWithout.length > 0) {
      return true;
    } else {
      for(let i = 0; i < this.actualAttributeNameWithout.length; i++) {

        for(let j = 0; j < this.listWithoutAttributeCode.length; j++) {
          if(this.actualAttributeNameWithout[i] === undefined) {
            return false;
          } else {
            if(this.listWithoutAttributeName[j].split(" ").join("") == this.actualAttributeNameWithout[i].split(" ").join("")) {
              if(this.listWithoutMinCar[j].toString() === '1') {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  checkValueWithout(idx: number) {
    if(this.listSummaryValueWithout[idx] !== undefined) {
      if(this.listSummaryValueWithout[idx].length > 0) {
        return true;
      }
    }
    if(this.listSummaryAttributeNamesWithout[idx].length > 0) {
      for(let i = 0; i < this.listWithoutAttributeCode.length; i++) {
        if(this.listWithoutAttributeName[i] !== undefined && this.actualAttributeNameWithout[idx] !== undefined) {
          if(this.listWithoutAttributeName[i].split(" ").join("") === this.actualAttributeNameWithout[idx].split(" ").join("")) {
            if(this.listWithoutMinCar[i].toString() === '1') {
              return true;
            } else {
              return false;
            }
        }
        }
      }
    }
    return false;

  }


  // Modal
  closeModal() {
    if(this.idRowLeftTableWith !== -1) {
      this.getRowTableLeftWith(this.idRowLeftTableWith);
    } else if(this.idRowLeftTableWithout !== -1) {
      this.getRowTableLeftWithout(this.idRowLeftTableWithout);
    }
    this.listAttributeValueCodesFilter = new Array();
    this.listAttributeValueNameFilter = new Array();
    this.attributesFilterBoolean = false;
  }

  clickRowTable(idRg: number, idSubRg: number, row:number) {
    if(this.actualAttributeNameWith[idRg][idSubRg][row] !== '') {
      let tables = document.querySelectorAll("table");
      for (let i = 0; i < tables.length; i++) {
        let trs = document.querySelectorAll("tr");
        for (let j = 0; j < trs.length; j++) {
          let element = trs[j];
          if (element != null) {
            element.style.background = 'transparent';
            if (element.id === 'rowTableLeftWith' + idRg + idSubRg + row) {
              element.style.background = 'lightblue';
            }
          }
        }
      }
      this.idRg = idRg;
      this.idSubRg = idSubRg;
      this.idRowLeftTableWith = row;
      this.getRowTableLeftWith(row)
    }
  }

  clickRowTableWithout(row: number) {
    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === 'rowTableLeftWithout' + row) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
    this.idRowLeftTableWithout = row;
    for (let i = 0; i < this.listWithoutAttributeName.length; i++) {
      if ((this.actualAttributeNameWithout[this.idRowLeftTableWithout] === this.listWithoutAttributeName[i])) {
        this.indexWithout = i;
        this.listLoadDataAttributeWithout[this.idRowLeftTableWithout] = true;
        this.listIndexWithout[this.idRowLeftTableWithout] = i;
      }
    }
    this.getRowTableLeftWithout(row)
  }

  clickRow(event: any, row: number) {
    this.idTableRow = '';

    if(this.idRowLeftTableWith > -1) {
      this.getRowTableLeftWith(this.idRowLeftTableWith);
    }
    if(this.idRowLeftTableWithout > -1) {
      this.getRowTableLeftWithout(this.idRowLeftTableWithout);
    }

    let tables = document.querySelectorAll("table");
    for (let i = 0; i < tables.length; i++) {
      this.idTableRow = row.toString();
      let trs = document.querySelectorAll("tr");
      for (let j = 0; j < trs.length; j++) {
        let element = trs[j];
        if (element != null) {
          element.style.background = 'transparent';
          if (element.id === this.idTableRow) {
            element.style.background = 'lightblue';
          }
        }
      }
    }
  }

  getIdRow(row: number) {
    return row;
  }

  addCode(id: string) {
    let code = this.listAttributeValueCodesFilter[parseInt(this.idTableRow)];
    this.pceWithoutTemplateService.lookUpFsn(this.listAttributeValueCodesFilter[parseInt(this.idTableRow)]).then((response: any) => {
      let name = response.name;
      if(id === 'without') {
        this.listSummaryValueWithout[this.idRowLeftTableWithout] = code + ' |' + name + '|';
        this.listNameWithout[this.idRowLeftTableWithout] = name;
        this.listAllCodesRangeWithout[this.idRowLeftTableWithout].push(code);
        this.listAllNamesRangeWithout[this.idRowLeftTableWithout].push(name);
        if(this.idRowLeftTableWithout > 0) {
          this.getRowTableLeftWithout(this.idRowLeftTableWithout);
        }
      } else if(id === 'with') {
        this.listSummaryValueWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = code + ' |' + name + '|';
        this.listNameWith[this.idRg][this.idSubRg][this.idRowLeftTableWith] = name;
        this.listAllCodesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push(code);
        this.listAllNamesRangeWith[this.idRg][this.idSubRg][this.idRowLeftTableWith].push(name);

        if(this.idRowLeftTableWith > 0) {
          this.getRowTableLeftWith(this.idRowLeftTableWith);
        }
      }
      let element = document.getElementById('linkButton');
      if (element != null) {
        // @ts-ignore
        element.blur();
      }
    })

  }


  isButtonAddActiveModal() {
    if (this.idTableRow !== '') {
      return false;
    }
    return true;
  }


  // summary

  checkUserInput() {
    if(this.listSummaryValueWithout.length > 0) {
      return false;
    }

    let b = false;

    for(let i = 0; i < this.listSummaryAttributeNamesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeNamesWith[i].length; j++) {
        for(let k = 0; k < this.listSummaryAttributeNamesWith[i][j].length; k++) {
          if(this.listSummaryAttributeNamesWith[i][j][k] !== undefined)  {
            if(this.listSummaryAttributeNamesWith[i][j][k].length > 0)  {
              b = true;
            }
            for(let l = 0; l < this.listInitWithAttributeCode[0].length; l++) {
              if(this.actualAttributeNameWith[i][j][k] !== undefined) {
                if(this.listInitWithAttributeName[0][l].split(" ").join("") == this.actualAttributeNameWith[i][j][k].split(" ").join("")) {
                  if(this.listInitWithMinCar[0][l].toString() === '1' && this.listSummaryValueWith[i][j][k] === undefined) {
                    return true;
                  } else if (this.listInitWithMinCar[0][l].toString() === '1' && this.listSummaryValueWith[i][j][k].length === 0) {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
    }

    if(b) {
      return false;
    }
    return true;
  }



  createNewPce() {

    let elementTab = document.getElementById('tab-finish');
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'lightblue';
    }

    elementTab = document.getElementById(this.idLastTab);
    if (elementTab != null) {
      // @ts-ignore
      elementTab.style.backgroundColor = 'transparent';
    }

    this.idLastTab = 'tab-finish'
    this.idActualTab = 'tab-finish'
    this.jsonDiagramPce = "";
    this.highlightingPceService.focusConceptCode = this.focusConceptCode;
    this.highlightingPceService.focusConceptName = this.focusConceptName;
    this.highlightingPceService.listSummaryAttributeNamesWithout = this.listSummaryAttributeNamesWithout;
    this.highlightingPceService.listSummaryAttributeCodesWithout = this.listSummaryAttributeCodesWithout;
    this.highlightingPceService.listSummaryValueWithout = this.listSummaryValueWithout;
    this.highlightingPceService.listSummaryAttributeNamesWith = this.listSummaryAttributeNamesWith;
    this.highlightingPceService.listSummaryAttributeCodesWith = this.listSummaryAttributeCodesWith;
    this.highlightingPceService.listSummaryValueWith = this.listSummaryValueWith;
    this.highlightingPceService.input = 'ConceptModel';

    for(let i = 0; i < this.highlightingPceService.listSummaryValueWith.length; i++) {
      this.highlightingPceService.listSummaryAttributeCodesWith[i] = this.highlightingPceService.listSummaryAttributeCodesWith[i].filter((x: any) => x.length !== 0)
      this.highlightingPceService.listSummaryAttributeNamesWith[i] = this.highlightingPceService.listSummaryAttributeNamesWith[i].filter((x: any) => x.length !== 0)
      this.highlightingPceService.listSummaryValueWith[i] = this.highlightingPceService.listSummaryValueWith[i].filter((x: any) => x.length !== 0)
    }

    this.jsonDiagramPce = this.jsonDiagramPce + "{\"focusconcept\" : [{\"code\" :\"" + this.focusConceptCode.split("+").join(" + ") + "\",\"name\" :\"" + this.focusConceptName.split("+").join(" + ")  + "\"}]";
    this.expressionPCE = this.focusConceptCode + " |" + this.focusConceptName + "|";

    // check if refinement and set ":"
    if(this.listSummaryValueWithout.length > 0) {
      this.expressionPCE = this.expressionPCE + ':\n';
      this.highlightingPceService.isRefinement = true;
    } else {
      for(let i = 0; i < this.listSummaryValueWith.length; i++) {
        for(let j = 0; j < this.listSummaryValueWith[i].length; j++) {
          if(this.listSummaryValueWith[i][j].length > 0) {
            this.expressionPCE = this.expressionPCE + ':\n';
            this.highlightingPceService.isRefinement = true;
            break;
          }
        }
        if(this.highlightingPceService.isRefinement) {
          break;
        }
      }
    }

    // WITHOUT RG
    this.jsonDiagramPce = this.jsonDiagramPce + ",\"withoutRoleGroup\" : [";
    for(let i = 0; i < this.listSummaryValueWithout.length; i++) {
      if(this.listSummaryAttributeNamesWithout[i].length > 0) {
        let attribute = this.listSummaryAttributeCodesWithout[i] + ' |' + this.listSummaryAttributeNamesWithout[i] + ' (attribute)| =' + this.listSummaryValueWithout[i];
        this.expressionPCE = this.expressionPCE + attribute + ",\n";
        this.jsonDiagramPce = this.jsonDiagramPce + "{\"attributecode\" : \"" + this.listSummaryAttributeCodesWithout[i] + "\",\"attributename\" : \"" + this.listSummaryAttributeNamesWithout[i]  + " (attribute)\",";
        this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + this.getValue(this.listSummaryValueWithout[i], 0) + "\",\"valuename\" : \"" + this.getValue(this.listSummaryValueWithout[i], 1) + "\"},"
      }
    }
    if(this.listSummaryAttributeNamesWithout.length > 0) {
      if(this.listSummaryAttributeNamesWithout[0].length > 0) {
        this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.length-1);
      }
    }
    this.jsonDiagramPce = this.jsonDiagramPce + "]";

    let x = false;
    this.jsonDiagramPce = this.jsonDiagramPce + ",\"withRoleGroup\" : [";

    // WITH RG
    for(let i = 0; i < this.listSummaryAttributeCodesWith.length; i++) {
      for(let j = 0; j < this.listSummaryAttributeCodesWith[i].length; j++) {
        let rg = '{'
        this.listSummaryAttributeCodesWith[i][j] = this.listSummaryAttributeCodesWith[i][j].filter((x: any) => x.length !== 0)
        if(this.listSummaryAttributeCodesWith[i][j].length > 0) {
          this.jsonDiagramPce = this.jsonDiagramPce + "{\"roleGroup\" : ["
        }
        for(let k = 0; k < this.listSummaryValueWith[i][j].length; k++) {
          if(this.listSummaryAttributeCodesWith[i][j][k] !== undefined || this.listSummaryAttributeNamesWith[i][j][k] !== undefined) {
            if(this.listSummaryValueWith[i][j][k].length > 0) {
              let attribute = this.listSummaryAttributeCodesWith[i][j][k] + ' |' + this.listSummaryAttributeNamesWith[i][j][k] + ' (attribute)| =' + this.listSummaryValueWith[i][j][k];
              rg = rg + attribute + ",\n";
              x = true;
              this.highlightingPceService.isRoleGroup = true;
              this.jsonDiagramPce = this.jsonDiagramPce + "{\"attributecode\" : \"" + this.listSummaryAttributeCodesWith[i][j][k] + "\",\"attributename\" : \"" + this.listSummaryAttributeNamesWith[i][j][k]  + " (attribute)\",";
              this.jsonDiagramPce = this.jsonDiagramPce + "\"valuecode\" : \"" + this.getValue(this.listSummaryValueWith[i][j][k], 0) + "\",\"valuename\" : \"" + this.getValue(this.listSummaryValueWith[i][j][k], 1) + "\"},"
            }
          }
        }
        rg = rg.substring(0, rg.lastIndexOf(",\n"));
        if(rg.length > 1) {
          this.expressionPCE = this.expressionPCE + rg + '},\n';
        }
        if(this.listSummaryAttributeCodesWith[i][j].length > 0) {
          this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.length-1);
          this.jsonDiagramPce = this.jsonDiagramPce + "]},";
        }
      }
    }
    if(x) {
      this.jsonDiagramPce = this.jsonDiagramPce.substring(0, this.jsonDiagramPce.lastIndexOf(","));
    }
    if(this.expressionPCE.includes(',')) {
      this.expressionPCE = this.expressionPCE.substring(0, this.expressionPCE.lastIndexOf(','))
    }

    this.expressionPCE = this.expressionPCE.split(" =").join("=");
    this.expressionPCE = this.expressionPCE.split("=").join(" = ");

    this.jsonDiagramPce = this.jsonDiagramPce + "]}";
    setTimeout(() => {
      this.dia.drawDiagram(this.jsonDiagramPce);
    }, 10);
  }

  getValue(value: string, id: number) {
    if(value.includes("#")) {
      if(id === 0) {
        return "";
      } else {
        return value;
      }
    } else {
      return value.split("|")[id];
    }
  }
  isCopyToClickbloard = false;
  isDownload = false;

  copyPceToClipboard(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.expressionPCE;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.isCopyToClickbloard = true;
    setTimeout(() => {
      this.isCopyToClickbloard = false;
    }, 2000);
  }

  downloadPce() {
    this.isDownload = true;
    setTimeout(() => {
      this.isDownload = false;
    }, 2000);
    const universalBOM = "\uFEFF";
    const a = window.document.createElement('a');
    a.setAttribute('href', 'data:text/plain; charset=utf-8,' + encodeURIComponent(universalBOM + this.expressionPCE));
    a.setAttribute('download', 'pce.txt');
    window.document.body.appendChild(a);
    a.click();
  }



  // -------------------------------------------------------------------------------------------------------------------
  // save PCE

  public uploadPCE = '';

  storePCE() {
    this.manageAlert(true, false, false);
    this.uploadPCE = 'start';
    setTimeout(() => {
      let element = document.getElementById('div-settingstemplates');
      if (element != null) {
        // @ts-ignore
        element.style.pointerEvents = 'none';
      }
    }, 10);

    this.templateService.updateNewCodeSystem(this.expressionPCE.split('\n').join(''), this.namePCE.split('\n').join('').split('|').join(''), 'ConceptModel').then((response: any) => {
      const message = response.result;
      this.uploadPCE = '';
      if(!message.includes('error')) {
        this.settingsTemplateService.ready = false;
        this.manageAlert(false, true, false);
      } else {

        this.settingsTemplateService.ready = false;
        if(message.includes('@')) {
          this.errorMessageUrl = response.result.split('@')[1];
        } else {
          this.errorMessageUrl = '';
        }
        this.manageAlert(false, false, true);
      }
      setTimeout(() => {
        let element = document.getElementById('div-settingstemplates');
        if (element != null) {
          // @ts-ignore
          element.style.pointerEvents = 'auto';
        }
      }, 10);


    })
    this.settingsTemplateService.ready = false;
  }

  createNamePce() {
    this.manageAlert(false, false, false);

    this.uploadPCE = '';
    this.namePCE =  this.expressionPCE.split(":")[0].replace(/[0-9]/g, '').split("|").join("");
    if(this.expressionPCE.split(":")[1] !== undefined) {
      this.namePCE = this.namePCE + ":"
      let y = this.expressionPCE.split(":")[1].split(",");
      for(let i = 0; i < y.length; i++) {
        if(!y[i].includes("#")) {
          this.namePCE = this.namePCE + y[i].replace(/[0-9]/g, '').split(' |').join('').split('|').join('') + ",";
        } else {
          let attr = y[i].toString().split("=")[0];
          let value = y[i].toString().split("=")[1];
          this.namePCE = this.namePCE + attr.replace(/[0-9]/g, '').split(' |').join('').split('|').join('') + "= " + value + ",";
        }
      }
    } else {
      this.namePCE.split(":").join("");
    }
    if(this.namePCE.includes(',')) {
      this.namePCE = this.namePCE.substring(0, this.namePCE.lastIndexOf(','))
    }

  }


  isCreateSuccessful() {
    return this.expressionPCE.length === 0;
  }

  savePCE() {
    this.createNamePce();
    this.savePCETextarea = "PCE: \n" + this.expressionPCE + "\n \nName: \n" + this.namePCE;
  }

  manageAlert(x: boolean, y: boolean, z: boolean) {
    if(x) {
      this.setAlert('inline', 'alert-start');
      this.setAlert('none', 'alert-success');
      this.setAlert('none', 'alert-error');
    } else if(y) {
      this.setAlert('none', 'alert-start');
      this.setAlert('inline', 'alert-success');
      this.setAlert('none', 'alert-error');
    } else if(z) {
      this.setAlert('none', 'alert-start');
      this.setAlert('none', 'alert-success');
      this.setAlert('inline', 'alert-error');
    } else {
      this.setAlert('none', 'alert-start');
      this.setAlert('none', 'alert-success');
      this.setAlert('none', 'alert-error');
    }
  }

  setAlert(value: string, name: string) {
    let element = document.getElementById(name);
    if (element != null) {
      // @ts-ignore
      element.style.display = value;
    }
  }


  updateCodeSystem() {
    setTimeout(() => {
      let element = document.getElementById('div-settingstemplates');
      if (element != null) {
        // @ts-ignore
        element.style.pointerEvents = 'none';
      }
    }, 10);

    this.createNamePce();
    let id = this.pceWithoutTemplateService.rowUpdate.toString();
    let templateName = 'ConceptModel';
    this.uploadPCE = 'start'
    this.manageAlert(true, false, false);

    let sameCS = '';
    if(this.settingsCsService.newUrl === this.settingsTemplateService.mainUrl) {
      sameCS = 'true';
    } else {
      sameCS = 'false';
    }
    this.templateService.updateCodeSystemUpdatedPce(id, this.expressionPCE.split("\n").join(""), this.namePCE.split("\n").join(""), templateName, sameCS).then((response: any) => {
      const message = response.result;
      this.uploadPCE = '';
      if(!message.includes('error')) {
        this.settingsTemplateService.ready = false;
        this.manageAlert(false, true, false);
      } else {
        // this.uploadPCE = 'error';
        this.settingsTemplateService.ready = false;
        if(message.includes('@')) {
          this.errorMessageUrl = response.result.split('@')[1];
        } else {
          this.errorMessageUrl = '';
        }
        this.manageAlert(false, false, true);
      }
      setTimeout(() => {
        let element = document.getElementById('div-settingstemplates');
        if (element != null) {
          // @ts-ignore
          element.style.pointerEvents = 'auto';
        }
      }, 10);

    })

  }

  public errorMessageUrl = '';

  isBtnSaveActive() {
    let b = !this.settingsTemplateService.ready;
    let c = this.appService.uploadPCE === 'start';
    let d = this.uploadPCE === 'start';

    if(this.uploadPCE === 'start') {
      return true;
    } else if(this.settingsTemplateService.id === 'new') {
      if(this.settingsTemplateService.newIdCsSupplement !== undefined) {
        return !((this.settingsTemplateService.newIdCsSupplement.length > 0) && (this.settingsTemplateService.newNameCsSupplement.length > 0));
      }
      return false;
    } else if(this.settingsTemplateService.id === 'existing') {
      if(this.settingsTemplateService.newIdCsSupplement !== undefined){
        return !(this.settingsTemplateService.newIdCsSupplement.length > 0);
      }
    }
    return !(b || c || d);
  }




  resetVariables() {
    this.pceWithoutTemplateService.clickTemplateGeneration('')
    this.start()
    this.booleanBtnFcDisabled = true;
    let elementInput = document.getElementById('input-focusConcept');
    if (elementInput != null) {
      // @ts-ignore
      elementInput.disabled = false;
    }
    this.isCopyToClickbloard = false;
    this.pceWithoutTemplateService.isUpdate = false;
    this.booleanBtnFcDisabled = true;
    this.templateService.click('ID_HOME')
    this.start();
    // this.pceWithoutTemplateService.clickTemplateGeneration('');
    // this.settingsCsService.clickSettings('');
    this.focusConceptName = '';
    this.conceptModelReferencedComponentId = '';
    this.conceptModelParentDomain = '';
    this.conceptModelProximalPrimitiveConstraint = '';
    this.conceptModelProximalPrimitiveRefinement = '';
    this.conceptModelDomainConstraint = '';
    this.conceptModelURL = '';
    this.conceptModelTemplate = '';
    this.conceptModelFocusConceptTemplate = '';
    this.attributeDefinitionCMCode = new Array();
    this.attributeDefinitionCMName = new Array();
    this.attributeDefinitionCMDefinition = new Array();
    this.attributeDefinitionCMRangeConstraint = new Array();
    this.listWithoutMinCar = new Array();
    this.listWithoutMaxCar = new Array();
    this.listWithoutConstraint = new Array();
    this.listWithoutAttributeCode = new Array();
    this.listWithoutAttributeName = new Array();
    this.listInitWithMinCar = new Array();
    this.listInitWithMaxCar = new Array();
    this.listInitWithRoleGroupMinCar = new Array();
    this.listInitWithRoleGroupMaxCar = new Array();
    this.listInitWithConstraint = new Array();
    this.listInitWithAttributeCode = new Array();
    this.listInitWithAttributeName = new Array();
    this.listNumberAttributesWithout = new Array();
    this.showGenerationTemplate = false;
    this.actualAttributeNameWithout = new Array();
    this.listNameWithout = new Array();
    this.indexWithout = -1;
    this.idRowLeftTableWithout = -1;
    this.listLoadDataAttributeWithout = new Array();
    this.focusConceptName = '';
    this.focusConceptCode = '';
    this.listAllNamesFocusConcept = new Array();
    this.listAllCodesFocusConcept = new Array();
    this.booleanBtnFcDisabled = false;
    this.listConstraintRangeWithout = new Array();
    this.listSummaryNameRangeWithout = new Array();
    this.listSummaryCodeRangeWithout = new Array();
    this.listSummaryConstraintRangeWithout = new Array();
    this.listSummaryAttributeCodesWithout = new Array();
    this.listSummaryAttributeNamesWithout = new Array();
    this.listSummaryMinCardinalityWithout = new Array();
    this.listSummaryMaxCardinalityWithout = new Array();
    this.listAllCodesRangeWithout = new Array();
    this.listAllNamesRangeWithout = new Array();
    this.listIndexWithout = new Array();
    this.booleanIntWithout = false;
    this.booleanDecWithout = false;
    this.booleanNormalWithout = false;
    this.intDefMinWithout = -1;
    this.intDefMaxWithout = -1;
    this.decDefMinWithout = -1;
    this.decDefMaxWithout = -1;
    this.listIntValueWithout = new Array();
    this.listIntMaxWithout = new Array();
    this.listIntMinWithout = new Array();
    this.listDecValueWithout = new Array();
    this.listDecMaxWithout = new Array();
    this.listDecMinWithout = new Array();
    this.listIntFixedValueWithout = new Array();
    this.listIntFixedValueWith = new Array();
    this.listIntFixedWithout = new Array();
    this.inputIdDecWithout = '';
    this.decMinWithout = '';
    this.decMaxWithout = '';
    this.decFixedWithout = '';
    this.booleanClickKeypressWithout = true;
    this.oldInputWithout = '';
    this.listSummaryValueWithout = new Array();
    this.invalidInputMinCarWithout = false;
    this.invalidInputMaxCarWithout = false;
    this.listNumberAttributesWith = new Array();
    this.idRowLeftTableWith = -1;
    this.listIndexSubRg = new Array();
    this.idRg = -1;
    this.idSubRg = -1;
    this.listOperatorRangeWith = new Array();
    this.listConstraintRangeWith = new Array();
    this.listNameWith = new Array();
    this.actualAttributeNameWith = new Array();
    this.showInformationWindowWith = false;
    this.indexWith = -1;
    this.listLoadDataAttributeWith = new Array();
    this.listIndexWith = new Array();
    this.actualMinCarWith = new Array();
    this.actualMaxCarWith = new Array();
    this.actualValueRangeWith = new Array();
    this.invalidInputMinCarWith = false;
    this.invalidInputMaxCarWith = false;
    this.actualCheckCarWith = true;
    this.listSummaryValueWith = new Array();
    this.listAllCodesRangeWith = new Array();
    this.listAllNamesRangeWith = new Array();
    this.listSummaryNameRangeWith = new Array();
    this.listSummaryCodeRangeWith = new Array();
    this.listSummaryMinCardinalityWith = new Array();
    this.listSummaryMaxCardinalityWith = new Array();
    this.listSummaryAttributeCodesWith = new Array();
    this.listSummaryAttributeNamesWith = new Array();
    this.listSummaryOperatorRangeWith = new Array();
    this.listSummaryConstraintRangeWith = new Array();
    this.selfGroupedAttributeCode = new Array();
    this.selfGroupedAttributeName = new Array();
    this.listChangedConstraintWith = new Array();
    this.listChangedAttributeCodeWith = new Array();
    this.listChangedAttributeNameWith = new Array();
    this.listFlagAttributeRelation = new Array();
    this.listDecFixedWith = new Array();
    this.listDecFixedValueWith = new Array();
    this.booleanIntWith = false;
    this.booleanDecWith = false;
    this.booleanNormalWith = false;
    this.intDefMinWith = -1;
    this.intDefMaxWith = -1;
    this.decDefMinWith = -1;
    this.decDefMaxWith = -1;
    this.intMaxWith = '';
    this.intMinWith = '';
    this.intFixedWith = '';
    this.decMaxWith = '';
    this.decMinWith = '';
    this.decFixedWith = '';
    this.listIntFixedWith = new Array();
    this.isWith = false;
    this.listCodeRangeWith = new Array();
    this.invalidInputCarWith = false;
    this.listInfoDefinitionWith = new Array();
    this.listInfoConstraintWith = new Array();
    this.correctInputWith = false;
    this.idRowTableRightWith = -1;
    this.inputIdIntWith = '';
    this.inputIdDecWith = '';
    this.termTemplate = '';
    this.listCodeWordsWith = new Array();
    this.listCodeWordsPositionWith = new Array();
    this.listCodeWordsWithout = new Array();
    this.listCodeWordsPositionWithout = new Array();
    this.resultTemplate = '';
    this.nameTemplate = '';
    this.isTemplatedCreated = false;
    this.idLastTab = '';
    this.idActualTab = '';
    this.listChangedConstraintFlagWithout = new Array();
    this.listChangedConstraintWithout = new Array();
    this.listAttributeRelation = new Array();
    this.listChangedFlagWith = new Array();
    this.minCarRg = new Array();
    this.maxCarRg = new Array();
    this.errorMessageSummary = '';
    this.listSelfGroupedAttributesPceCode = new Array();
    this.listSelfGroupedAttributesPceName = new Array();
    this.listSelfGroupedBoolean = new Array();
    this.invalidRgCar = false;
    this.isUpdate = false;
    this.oldInputWith = [];
    this.listDecFixedValueWithout = new Array();
    this.listDecFixedWithout = new Array();
    this.inputIdIntWithout = '';
    this.intMinWithout = '';
    this.intMaxWithout = '';
    this.intFixedWithout = '';
    this.idRowTableRightWithout = -1;
    this.correctInputWithout = false;
    this.invalidInputCarWithout = false;
    this.listChangeCarWithout = new Array();
    this.listChangeCarWith = new Array();
    this.showInformationWindowWithout = false;
    this.isWithout = false;
    this.expressionPCE = '';
    this.namePCE = '';
    this.savePCETextarea = '';
    this.listAttributeValueCodesFilter = new Array();
    this.listAttributeValueNameFilter = new Array();
    this.attributesFilterBoolean = false;
    this.idTableRow = '';
    this.jsonDiagramPce = '';
  }

}
