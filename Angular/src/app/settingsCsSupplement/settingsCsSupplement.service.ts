import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {SettingsService} from "../settings/settings.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class SettingsCsSupplementService {

  constructor(public backendService: BackendService, public settings: SettingsService) {
  }

  public start = '';
  public isOtherServer = false;
  public syntaxCheck = '';
  public serverChange = '';
  public url_other_server = '';

  public listFullUrl = new Array();
  public listTitleUrl = new Array();
  public nameCsSupplement = '';

  public validatedUrl = '';
  public errorMessageUrl = '';


  clickSettings(id: string) {

    if(this.serverChange === '') {
      setTimeout(() => {
        let elementRange = document.getElementById('radioCs0');
        if (elementRange !== null) {
          //@ts-ignore
          elementRange.checked = true;
        }
      }, 10);
      this.serverChange = this.settings.listUrlServer[0];
    } else {
      for (let i = 0; i < this.settings.listUrlServer.length; i++) {
        if (this.serverChange === this.settings.listUrlServer[i]) {
          setTimeout(() => {
            let elementRange = document.getElementById('radioCs' + i);
            if (elementRange !== null) {
              //@ts-ignore
              elementRange.checked = true;
            }
          }, 10);
        }
      }
    }
    this.start = id;
  }

  public isLoading = '';

  getServer(value: string, idx?: number) {
    this.newUrl = '';
    this.nameCsSupplement = '';
    if(value !== 'other') {
      this.isOtherServer = false;
      this.syntaxCheck = '';
      // @ts-ignore
      this.serverChange = this.settings.listUrlServer[idx];
      this.listTitleUrl = new Array();
      this.listFullUrl = new Array();
      this.isLoading = 'start';
      this.getAllCsSupplements(this.serverChange).then((response: any) => {
        const value = response.value;
        if(value !== undefined) {
          for (let i = 0; i < value.length; i++) {
            const fullUrl = value[i].url;
            const title = value[i].title;

            this.listTitleUrl.push(title);
            this.listFullUrl.push(fullUrl);
          }
          this.isLoading = '';
        } else {
          this.isLoading = 'error';
        }

      })
    } else if(value === 'other') {
      this.isOtherServer = true;
    }
  }

  keypressUrl() {
    if(this.validatedUrl.length > 0) {
      if(this.validatedUrl !== this.url_other_server) {
        this.syntaxCheck = '';
        setTimeout(() => {
          let elementRange = document.getElementById('btn-load-css');
          if (elementRange != null) {
            //@ts-ignore
            elementRange.disabled = true;
          }
        }, 10);
        // btn-otherUrl
      }
    }
  }

  checkUrl() {
    this.syntaxCheck = 'check';
    this.checkSyntaxUrl(this.url_other_server).then((response: any) => {
      const result = response.result;
      if(result.toString() === 'true') {
        this.errorMessageUrl = '';
        this.syntaxCheck = 'success';
        this.validatedUrl = this.url_other_server;
        this.serverChange = this.url_other_server;
        this.getAllCsSupplements(this.url_other_server).then((response: any) => {
          const value = response.value;
          if(value !== undefined) {
            for (let i = 0; i < value.length; i++) {
              const fullUrl = value[i].url;
              const title = value[i].title;

              this.listTitleUrl.push(title);
              this.listFullUrl.push(fullUrl);
            }
            this.isLoading = '';
          } else {
            this.isLoading = 'error';
          }
        })
      } else {
        this.syntaxCheck = 'error';
        this.newUrl = '';
        if(result.includes('@')) {
          this.errorMessageUrl = result.split('@')[1];
        } else {
          this.errorMessageUrl = '';
        }
      }
    })
  }

  getNames() {
    let list = new Array();
    for(let i = 0; i < this.listTitleUrl.length; i++) {
      if(!list.includes(this.listTitleUrl[i])) {
        list.push(this.listTitleUrl[i])
      }
    }
    return list;
  }

  isInputDisabled()  {
    if(this.isLoading === 'start' || this.isLoading === 'error') {
      return true;
    }

    if(this.isOtherServer && this.syntaxCheck !== 'success') {
      return true;
    }
    return false;
  }

  isSaveDisabled() {
    if(this.isLoading === 'start') {
      return true;
    }

    if(this.isOtherServer && this.syntaxCheck !== 'success' && !this.listTitleUrl.includes(this.nameCsSupplement)) {
      return true;
    } else if((this.syntaxCheck === '' || this.syntaxCheck === 'success') && this.listTitleUrl.includes(this.nameCsSupplement)) {
      return false;
    } else {
      return true;
    }
  }


  public newUrl = '';

  loadCsSupplement() {
    let idx = 0;
    for (let i = 0; i < this.listTitleUrl.length; i++) {
      if(this.nameCsSupplement === this.listTitleUrl[i]) {
        idx = i;
        break;
      }
    }
    this.newUrl = this.listFullUrl[idx];
  }

  reset() {
    this.start = '';
    this.newUrl
    this.isOtherServer = false;
    this.syntaxCheck = '';
    this.serverChange = '';
    // this.url_server_luebeck = 'https://ontoserver.imi.uni-luebeck.de/fhir';
    this.url_other_server = '';
    this.listFullUrl = new Array();
    this.listTitleUrl = new Array();
    this.nameCsSupplement = '';
  }


  // Spring boot
  checkSyntaxUrl(url: string) {
    let u = this.encodeURIComponent(url);
    const urlRequest = this.backendService.getBackendUrl() + 'settingsurl/?url=' + u;
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

  getAllCsSupplements(urlServer: string) {
    let u = this.encodeURIComponent(urlServer);
    const url = this.backendService.getBackendUrl() + 'getallcodesystemsupplement/?url=' + u;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }


  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }
}
