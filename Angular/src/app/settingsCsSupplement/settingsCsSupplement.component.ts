import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {SettingsCsSupplementService} from "./settingsCsSupplement.service";
import {SettingsService} from "../settings/settings.service";

@Component({
  selector: 'app-settingscs',
  templateUrl: './settingsCsSupplement.component.html',
  styleUrls: ['./settingsCsSupplement.component.css']
})

export class SettingsCsSupplementComponent {

  constructor(private http: HttpClient, public appService: AppService, public templateGenerationService: TemplatesGenerationService, public settingsCsService: SettingsCsSupplementService, public settingsService: SettingsService) {
  }

  ngOnInit() {
    if(this.settingsCsService.listTitleUrl.length === 0) {
      this.settingsCsService.getServer('existing',0);
    }
  }





}

