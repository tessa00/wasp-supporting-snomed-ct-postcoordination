import {Component} from '@angular/core';
import {ProcessingMrcmService} from "./processingMrcm.service";
import {SettingsService} from "../settings/settings.service";

@Component({
  selector: 'app-processingMrcm',
  templateUrl: './processingMrcm.component.html',
  styleUrls: ['./processingMrcm.css']
})

export class ProcessingMrcmComponent {

  public messageMrcmProcessing = '';
  public errorMessage = ''

  constructor(private processingMrcmService: ProcessingMrcmService, private settingService: SettingsService) {
  }

  filePicked(files: any) {
    for (let i = 0; i < files.length; i++) {
      const reader = new FileReader();
      reader.readAsText(files[i]);
      reader.onload = () => {
        this.processingMrcmService.mrcm_content = reader.result == null ? '' : reader.result.toString();
        this.generateJsonMrcm();
      };
    }
  }

  generateJsonMrcm() {
    this.messageMrcmProcessing = 'START_PROCESSING';
    this.processingMrcmService.processingMrcm(this.settingService.serverChange, this.settingService.sctVersion, this.processingMrcmService.mrcm_content).then((response: any) => {
      if(response.conceptmodel !== undefined) {
        this.downloadFile(JSON.stringify(response))
      } else {
        this.errorMessage = response.result;
        this.messageMrcmProcessing = 'ERROR_PROCESSING';
      }
    })
  }

  downloadFile(data: string) {
    // @ts-ignore
    const blob = new Blob([data], { type: 'text/json' });
    const url= window.URL.createObjectURL(blob);
    const anchorElement = document.createElement('a');
    document.body.appendChild(anchorElement);
    anchorElement.style.display = 'none';
    anchorElement.href = url;
    anchorElement.download = "mrcm.json";
    anchorElement.click();
    window.URL.revokeObjectURL(url);
    this.messageMrcmProcessing = 'PROCESSING_CORRECT';
  }
}

