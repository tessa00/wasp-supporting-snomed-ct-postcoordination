import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {AppService} from "../app.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class ProcessingMrcmService {

  public start = '';
  public mrcm_content = '';

  constructor(public backendService: BackendService, public appService: AppService) {
  }

  clickProcessingMrcm(id: string) {
    this.start = id;
  }

  processingMrcm(urlServer: String, version: String, mrcm: String) {
    const url = this.backendService.getBackendUrl() + 'processingmrcm';
    let body = {url: urlServer, version: version, mrcm: mrcm}
    const response = this.backendService.httpClient.post(url, body);
    return lastValueFrom(response);
  }




}
