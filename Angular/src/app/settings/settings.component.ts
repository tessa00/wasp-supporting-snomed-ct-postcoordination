import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {SettingsService} from "./settings.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})

export class SettingsComponent {
  private uploadPaths: Array<File> = [];
  public showInfoAlert = false;

  constructor(private settingService: SettingsService, private http: HttpClient, public appService: AppService, public templateGenerationService: TemplatesGenerationService, public settingsService: SettingsService) {
  }

  ngOnInit(): void {

  }

  folderPicked(files: any): void {
    const jsonArray: any = [];
    const jsonTemplatesArray: any = [];

    for (let i = 0; i < files.length; i++) {
      const path = files[i].webkitRelativePath.split('/');
      const reader = new FileReader();
      reader.readAsText(files[i]);
      reader.onload = () => {
        const json = {fileName: files[i].name, fileContent: reader.result == null ? '' : reader.result.toString()};
        if (path.length < 3) {
          jsonArray.push(json)
        } else {
          jsonTemplatesArray.push(json)
        }
      };
    }

    setTimeout(() => {
      this.appService.selectedFiles = {root: jsonArray, templates: jsonTemplatesArray};
        this.settingsService.preprocessingWaspData();

      // let template = this.appService.selectedFiles.templates.filter((x: { fileName: string; }) => x.fileName === "Allergic disease (disorder).json");

    }, 1000);
  }

  changeAlert(identifier?: String) {
    if(identifier) {
      this.showInfoAlert = !this.showInfoAlert;
    } else {
      this.showInfoAlert = false
    }
  }


  goToLink(url: string){
    window.open(url, "_blank");
  }

}

