import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {AppService} from "../app.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {


  constructor(public backendService: BackendService, public appService: AppService) {
  }

  public start = '';

  public serverInit = '';
  public url_other_server = '';

  public listNameServer = new Array();
  public listUrlServer = new Array();

  public serverChange = '';
  public serverName = '';
  public isOtherServer = false;
  public syntaxCheck = '';

  public listSnomedVersions = new Array();
  public listSnomedVersionsDisplay = new Array();
  public versionChange = '';
  public versionHome = '';
  public sctVersion = ''

  public isLoadingReady = true;
  public id = '';
  public uploadSuccessful = false;
  public snomedBrowserVersion = '';

  public sctEdition : Record<string, string> = {
    '900000000000207008': 'International edition',
    '11000221109': 'Argentinian Edition',
    '32506021000036107': 'Australian edition',
    '11000234105': 'Austrian edition',
    '11000172109': 'Belgian edition',
    '20611000087101': 'Canadian French edition',
    '20621000087109' : 'Canadian English edition',
    '554471000005108': 'Danish edition',
    '11000181102': 'Estonian edition',
    '11000229106': 'Finnish edition',
    '11000274103': 'German edition',
    '1121000189102': 'Indian edition',
    '11000220105' : 'Irish edition',
    '11000146104': 'Netherlands edition',
    '21000210109': 'New Zealand edition',
    '51000202101' : 'Norwegian edition',
    '449081005': 'Spanish edition',
    '45991000052106': 'Swedish edition',
    '2011000195101': 'Swiss edition',
    '83821000000107': 'UK edition',
    '5631000179106': 'Uruguay edition',
    '731000124108': 'US edition',
    '5991000124107': 'US edition (with ICD-10-CM maps)',
  }

  clickSettings(id: string) {
    if(this.id !== 'SETTINGS_FIRST_READY') {
      this.listNameServer = new Array();
      this.listUrlServer = new Array();
    }
  }

  public idxRadioButton: number | undefined | any = -1;

  getServer(value: string, idx?: number) {
    this.isLoadingReady = true;
    if(value !== 'other') {
      this.isOtherServer = false;
      this.syntaxCheck = '';
      // @ts-ignore
      this.serverChange = this.listUrlServer[idx];
      // @ts-ignore
      this.serverName = this.listNameServer[idx];
      this.idxRadioButton = idx;
      this.listSnomedVersions = new Array();
      this.listSnomedVersionsDisplay = new Array();
      if(this.serverChange === undefined) {
        this.serverChange = this.serverInit;
        this.serverName = '';
      }
      this.requestSnomedVersions()
    } else {
      this.isOtherServer = true;
      this.syntaxCheck = '';
    }
  }

    requestSnomedVersions() {
      this.getSnomedVersions(this.serverChange).then((response: any) => {
        const value = response.value;
        if(value !== undefined) {
          for(let i = 0; i < value.length; i++) {
            this.listSnomedVersions.push(value[i].result);
            const s = value[i].result.split("http://snomed.info/sct/").join("");
            const code = s.split("/version/")[0];
            const version = s.split("/version/")[1];
            if(this.sctEdition[code.toString()] !== undefined) {
              let sctDisplay = this.sctEdition[code.toString()] + " -- " + version
              if(!this.listSnomedVersionsDisplay.includes(sctDisplay)) {
                this.listSnomedVersionsDisplay.push(sctDisplay);
              }
            } else {
              this.listSnomedVersionsDisplay.push(value[i].result);
            }
          }
          this.listSnomedVersionsDisplay.sort();
          this.isLoadingReady = false;

          if(value.length === 0) {
            this.errorMessageUrl = response.message;
          }
        } else {
          this.errorMessageUrl = response.message;
          this.syntaxCheck = 'error';
        }
      })
  }

  public errorMessageUrl = '';

  checkUrl() {
    this.syntaxCheck = 'check';
    this.isLoadingReady = true;
    this.versionChange = '';
    this.listSnomedVersionsDisplay = new Array();
    this.listSnomedVersions = new Array();
    this.checkSyntaxUrl(this.url_other_server).then((response: any) => {
      let result = '';
      if(response.result.includes("@")) {
        result = response.result.split("@")[0];
        this.errorMessageUrl = response.result.split("@")[1];
      } else {
        result = response.result;
        this.errorMessageUrl = '';
      }
      if(result.toString() === 'true') {
        this.syntaxCheck = 'success';
        this.serverChange = this.url_other_server;
        this.serverName = 'Stored Server'
        this.listSnomedVersions = new Array();
        this.listSnomedVersionsDisplay = new Array();
        this.getSnomedVersions(this.url_other_server).then((response: any) => {
          const value = response.value;
          if(value !== undefined) {
            for(let i = 0; i < value.length; i++) {
              this.listSnomedVersions.push(value[i].result);
              const s = value[i].result.split("http://snomed.info/sct/").join("");
              const code = s.split("/version/")[0];
              const version = s.split("/version/")[1];
              if(this.sctEdition[code.toString()] !== undefined) {
                this.listSnomedVersionsDisplay.push(this.sctEdition[code.toString()] + " -- " + version);
              } else {
                this.listSnomedVersionsDisplay.push(value[i].result);
              }
            }
            this.listSnomedVersionsDisplay.sort();
            this.isLoadingReady = false;

            if(value.length === 0) {
              this.errorMessageUrl = response.message;
            }
          } else {
            this.errorMessageUrl = response.message;
            this.syntaxCheck = 'error';
          }
        });
      } else {
        this.syntaxCheck = 'error';
        this.listSnomedVersions = new Array();
        this.listSnomedVersionsDisplay = new Array();
      }
    })
  }

  checkCacheCheckbox(b: boolean) {
    let element = document.getElementById('checkedCache');
    if (element != null) {
      //@ts-ignore
      element.disabled = b;
    }
  }

  isSaveDisabled() {
    if(this.isOtherServer && this.syntaxCheck !== 'success' && !this.listSnomedVersionsDisplay.includes(this.versionChange)) {
      this.checkCacheCheckbox(true)
      return true
    } else if((this.syntaxCheck === '' || this.syntaxCheck === 'success') && this.listSnomedVersionsDisplay.includes(this.versionChange)) {
      this.checkCacheCheckbox(false)
      return false;
    } else {
      this.checkCacheCheckbox(true)
      return true;
    }
  }


  isCloseDisabled() {
    return !(this.serverInit.length > 0 && this.versionHome.length > 0);
  }

  saveData() {
    if(this.idxRadioButton === -1) {
      this.idxRadioButton = 'radioOther';
    }
    let wasp_data = {data: JSON.stringify(this.appService.selectedFiles),
      sct_version: this.sctVersion, server_url: this.serverChange, server_name: this.serverName, server_idx: this.idxRadioButton}
    localStorage.setItem("wasp_data", JSON.stringify(wasp_data));

  }

  saveModal() {
    this.serverInit = this.serverChange;
    this.versionHome = this.versionChange;
    let version = '';
    for (const v in this.sctEdition) {
      version = this.versionChange.split("--")[1].toString().split(" ").join("")
      if(this.sctEdition[v].toString().split(" ").join("") === this.versionChange.split("--")[0].toString().split(" ").join("")) {
        this.sctVersion = "http://snomed.info/sct/" + v + "/version/" + version;
      }
    }
    this.snomedBrowserVersion = version.substring(0, 4) + "-" + version.substring(4, 6) + "-" + version.substring(6, 8)
    let element = document.getElementById('checkedCache');
    if (element != null) {
      //@ts-ignore
      let checked = !element.disabled;
      if(checked) {
        this.saveData()
      }
    }
    this.id = 'SETTINGS_FIRST_READY';
    this.appService.selectedItem = 'ID_HOME';
  }

  cancelModal() {
    for(let i = 0; i < this.listUrlServer.length; i++) {
      if(this.serverInit === this.listUrlServer[i]) {
        setTimeout(() => {
          let elementRange = document.getElementById('radio' + i);
          if (elementRange != null) {
            //@ts-ignore
            elementRange.checked = true;
            this.getServer(this.listNameServer[i]);
          }
        }, 10);
      }
    }
  }

  preprocessingWaspData() {
    const server = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "server.json");
    if(server[0] !== undefined) {
      this.listNameServer = new Array();
      this.listUrlServer = new Array();

      this.appService.selectedServer = server[0].fileContent;

      let json = JSON.parse('{\"j\":' + this.appService.selectedServer + "}");

      let serverArray = json.j;
      for (let i = 0; i < serverArray.length; i++) {
        this.listNameServer.push(serverArray[i].name);
        this.listUrlServer.push(serverArray[i].url.split(" ").join(""));
      }
    }
    let json = '{\"templates\":' + JSON.stringify(this.appService.selectedFiles.templates) + '}';
    this.appService.allTemplates = JSON.parse(json)

    // this.appService.selectedConceptModel = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "mrcm.json");
    const mrcm = this.appService.selectedFiles.root.filter((x: { fileName: string; }) => x.fileName === "mrcm.json");
    if(mrcm[0] !== undefined) {
      this.appService.selectedConceptModel = mrcm[0].fileContent;
    }
    this.uploadSuccessful = true;
    return [this.listUrlServer, this.listNameServer]
  }

  retrieveData(event: any) {
    const waspData = localStorage.getItem("wasp_data");
    if (waspData) {
      let element = document.getElementById("useCacheData")
      // @ts-ignore
      let isChecked = element.checked;
      if(isChecked) {
        this.setCacheData(waspData)

      } else {
        this.removeCacheData()
      }
    }
  }

  setCacheData(waspData: any) {
    this.appService.selectedFiles = JSON.parse(JSON.parse(waspData).data);
    let server = this.preprocessingWaspData()
    this.listUrlServer = server[0]
    this.listNameServer = server[1]
    this.sctVersion = JSON.parse(waspData).sct_version;
    this.serverChange = JSON.parse(waspData).server_url;
    this.serverInit = JSON.parse(waspData).server_url;
    this.serverName = JSON.parse(waspData).server_name;
    this.idxRadioButton = JSON.parse(waspData).server_idx;

    let editionCode = this.sctVersion.split("http://snomed.info/sct/").join("").split("/version/")[0];
    let edition = this.sctEdition[editionCode];
    let version = this.sctVersion.split("/version/")[1];
    this.versionChange = edition + " -- " + version;
    this.versionHome = this.versionChange;

    this.listSnomedVersionsDisplay.push(this.versionHome)
    this.requestSnomedVersions()

    this.syntaxCheck = 'success'
    this.isLoadingReady = false;

    setTimeout(() => {
      let radioButton;
      if(this.idxRadioButton !== 'radioOther') {
          radioButton = document.getElementById('radio' + this.idxRadioButton);
      } else {
          radioButton = document.getElementById(this.idxRadioButton);
          this.isOtherServer = true;
          this.url_other_server = this.serverChange;
      }
      // @ts-ignore
      radioButton.checked = true;
    }, 10);
  }

  removeCacheData() {
    this.appService.selectedFiles = null;
    this.listUrlServer = Array();
    this.listNameServer = Array()
    this.sctVersion = '';
    this.serverChange = '';
    this.serverInit = '';
    this.serverName = '';
    this.versionChange = '';
    this.versionHome = '';
    this.listSnomedVersionsDisplay = Array();
    this.syntaxCheck = '';
    this.isOtherServer = false;
    this.uploadSuccessful = false;
    this.snomedBrowserVersion = '';


    let element = document.getElementById("useCacheData")
    // @ts-ignore
    element.checked = false;
  }

  getSnomedVersionList() {
    this.listSnomedVersionsDisplay.sort((a, b) => b.localeCompare(a))
    return this.listSnomedVersionsDisplay;
  }

  // Spring boot

  getStoredServer() {
    const urlRequest = this.backendService.getBackendUrl() + 'settingsserver';
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

  checkSyntaxUrl(url: string) {
    let u = this.encodeURIComponent(url);
    const urlRequest = this.backendService.getBackendUrl() + 'settingsurl/?url=' + u;
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

  // Todo - expample GET request in SpringBoot 3.2
  // let u = this.encodeURIComponent(url);
  //     const urlRequest = this.backendService.getBackendUrl() + 'settingssnomedversions/?url=' + u;
  //     return this.http.get(urlRequest, { params: { url } });

  getSnomedVersions(url: string) {
    let u = this.encodeURIComponent(url);
    const urlRequest = this.backendService.getBackendUrl() + 'settingssnomedversions/?url=' + u;
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }





}
