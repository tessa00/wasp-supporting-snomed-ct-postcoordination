import {Component} from '@angular/core';


import {AppService} from "./app.service";
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "./settings/settings.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../styles.css']
})


export class AppComponent {

  constructor(private app: AppService, private http: HttpClient, private settings: SettingsService) {
  }

  public xx = "Test";


  openFile(){
    console.log('hell')
    // @ts-ignore
    document.querySelector('input').click()
  }
  handle(){
    console.log('Change input file')
  }

  ngOnInit(): void {
  }

  displayStyle = 'none';
  openPopup(id? : string) {
    if (id == null) {
      this.displayStyle = 'block';
    } else {
      let e = document.getElementById(id);
      if (e != null) {
        e.style.display = 'block';
      }
    }
  }
  closePopup(id? : string) {
    if (id == null) {
      this.displayStyle = 'none';
    } else {
      let e = document.getElementById(id);
      if (e != null) {
        e.style.display = 'none';
      }
    }
  }

  test() {
    console.log("CLICK")
  }
}

