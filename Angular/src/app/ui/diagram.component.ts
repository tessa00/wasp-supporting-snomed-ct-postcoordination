import {Component, Input} from "@angular/core";

@Component({
  selector: 'pce-diagram',
  template: '<div id="dia-container" style="width: 1000px;"></div>'
})

export class DiagramComponent {
  @Input() values: any;
  public lineVerticalFactor = 50;

  getList(values: any): any {
    let json = JSON.parse(values);
    let list = new Array<any>();

    let x = json.focusconcept;
    list.push(x[0])

    x = json.withoutRoleGroup;
    for (let i = 0; x != null && i < x.length; i++) {
      let data = x[i];
      data.node = 0;
      data.nodeCount = 0;
      list.push(data)
    }
    x = json.withRoleGroup;
    for (let i = 0; x != null && i < x.length; i++) {
      let roleGroups = x[i].roleGroup;
      for (let j = 0; roleGroups != null && j < roleGroups.length; j++) {
        let data = roleGroups[j];
        data.node = -1;
        data.nodeCount = 0;
        if (j === 0) {
          data.node = 1;
          data.nodeCount = roleGroups.length - 1;
        }
        list.push(data)
      }
    }

    return list;
  }

  drawDiagram(values: any): void {
    let lineVerticalLeftHeight = 0;
    let list = this.getList(values);
    let htmlContainer = '<div id="dia-focusconcept" class="box box-focusconcept"></div>';

    let onlyWithoutGroup = 0;
    for (let i = 0; list != null && i < list.length; i++) {
      const data = list[i];
      if (data.node > 0) {
        data.node = i;
      }
      if (i > 0) {
        lineVerticalLeftHeight = data.node > 0 ? data.node : lineVerticalLeftHeight;
        onlyWithoutGroup++;
        if (lineVerticalLeftHeight > 0) {
          onlyWithoutGroup = 0;
        }
        htmlContainer += `
            <div id="wrapper[` + i + `]" class="wrapper">
              <div id="line-horizontal-attribute[` + i + `]" class="line-horizontal">
                <div id="arrow-attribute[` + i + `]" class="arrow"></div>
              </div>
              <div id="box-attribute[` + i + `]" class="box box-attribute"></div>
              <div id="line-horizontal-value[` + i + `]" class="line-horizontal">
                <div class="arrow"></div>
              </div>
              <div id="box-value[` + i + `]" class="box box-value"></div>
              <div id="circle[` + i + `]" class="circle"></div>
            </div>
      `;
      }
    }
    lineVerticalLeftHeight += onlyWithoutGroup;

    let element = document.getElementById('dia-container');
    if (element != null) {
      element.innerHTML = htmlContainer;
    }

    let height = (lineVerticalLeftHeight * this.lineVerticalFactor) - 10;
    let lineVerticalLeft = document.createElement('div');
    lineVerticalLeft.style.cssText = 'position:absolute; top:38px; left: 39px; height: ' + height + 'px; border-left: 1px solid black';

    for (let i = 0; list != null && i < list.length; i++) {
      const data = list[i];

      element = document.getElementById('dia-focusconcept');
      if (i === 0 && element != null) {
        element.setAttribute('title', data.code + '\n' + data.name);
        element.appendChild(lineVerticalLeft);
      }

      if (data.node > 0) {
        element = document.getElementById('line-horizontal-attribute[' + i + ']');
        if (element != null) {
          element.style.minWidth = '110px';
          element.style.maxWidth = '110px';
          if (data.node === i) {
            let height = (this.lineVerticalFactor * data.nodeCount);
            let lineVertical = document.createElement('div');
            lineVertical.style.cssText = 'position:absolute; top:1px; left: 49px; height:' + height + 'px; border-left: 1px solid black';
            lineVertical.style.height = (this.lineVerticalFactor * data.nodeCount) + 'px';
            element.appendChild(lineVertical);
          }
        }
        element = document.getElementById('arrow-attribute[' + i + ']');
        if (element != null) {
          element.style.left = '100px';
        }
        element = document.getElementById('circle[' + i + ']');
        if (element != null) {
          element.style.top = '25px';
          element.style.left = '50px';
          element.style.display = 'inline';
          element.style.zIndex = '99';
        }
      }
      if (data.node === -1) {
        element = document.getElementById('wrapper[' + i + ']');
        if (element != null) {
          element.style.left = '50px';
        }
      }
      element = document.getElementById('box-attribute[' + i + ']');
      if (element != null) {
        element.setAttribute('title', data.attributecode + '\n' + data.attributename);
      }
      if (data.valuecode != null) {
        element = document.getElementById('box-value[' + i + ']');
        if (element != null) {
          element.setAttribute('title', data.valuecode + '\n' + data.valuename);
        }
      }
    }
  }
}
