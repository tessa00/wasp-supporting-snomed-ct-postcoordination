import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable({
  "providedIn": "root"
})
export class BackendService {

  backendUri = "";

  constructor(public httpClient: HttpClient) {
    let settingsUri = "/assets/json/backend.json";
    this.httpClient.get<BackendSettings>(settingsUri).subscribe(s => {
      this.backendUri = s.backendUri;
      console.log(`Configured backend URI: ${this.backendUri}`);
      //todo maybe generate a better warning message in case connection fails...
      this.httpClient.get(this.backendUri + "api/version").subscribe(v => {
        console.log("Connected to", v);
      });
    });
  }

  getBackendUrl(){
    return this.backendUri;
  }

}

export interface BackendSettings {
  backendUri: string;
}
