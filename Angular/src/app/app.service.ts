import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {BackendService} from "./backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public uploadPCE = '';
  public selectedApp = '';
  public selectedItem = '';
  public selectedFiles: any;
  public selectedServer = [];
  public selectedConceptModel = '';
  public selectedTemplate = '';
  public allTemplates: any;
  private modalDisplayStyle: Array<any> = [];
  public errorMessage = '';

  constructor(public backendService: BackendService,) {
  }

  modalStyleDisplay(id : string): string {
    let display = 'none';
    let json = this.modalDisplayStyle.filter(x => x.id === id);
    if (json[0] != null) {
      display = json[0].display;
    }
    return display;
  }

  openPopup(id: string): void {
    let json = {id: id, display: 'block'};
    this.modalDisplayStyle = this.modalDisplayStyle.filter(x => x.id !== id);
    this.modalDisplayStyle.push(json);
  }

  closePopup(id : string): void {
    let json = this.modalDisplayStyle.filter(x => x.id === id);
    if (json[0] !== null) {
      json[0].display = 'none';
    }
  }

  getStoredServer() {
    const urlRequest = this.backendService.getBackendUrl() + 'settingsserver';
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

}
