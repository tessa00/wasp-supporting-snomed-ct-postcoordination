import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {TemplatesComponent} from "./templates/templates.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {TemplatesGenerationComponent} from "./templateGeneration/templatesGeneration.component";
import {SettingsComponent} from "./settings/settings.component";
import {SettingsCsSupplementComponent} from "./settingsCsSupplement/settingsCsSupplement.component";
import {SettingsTemplatesComponent} from "./settingsTemplates/settingsTemplates.component";
import {PceWithoutTemplateComponent} from "./pceWithoutTemplate/pceWithoutTemplate.component";
import {HighlightingPceComponent} from "./highlightingPce/highlightingPce.component";
import {HighlightingTemplateComponent} from "./highlightingTemplate/highlightingTemplate.component";
import {DiagramComponent} from "./ui/diagram.component";

import { RouterModule } from '@angular/router';
import {ProcessingMrcmComponent} from "./processingMrcm/processingMrcm.component";
import {ValidationToolComponent} from "./validationTool/validationTool.component";

@NgModule({
  declarations: [
    AppComponent,
    TemplatesGenerationComponent,
    TemplatesComponent,
    SettingsComponent,
    SettingsCsSupplementComponent,
    SettingsTemplatesComponent,
    PceWithoutTemplateComponent,
    HighlightingPceComponent,
    HighlightingTemplateComponent,
    DiagramComponent,
    ProcessingMrcmComponent,
    ValidationToolComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [DiagramComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
