import {Injectable} from '@angular/core';
import {SettingsService} from "../settings/settings.service";
import {SettingsCsSupplementService} from "../settingsCsSupplement/settingsCsSupplement.service";
import {SettingsTemplatesService} from "../settingsTemplates/settingsTemplates.service";
import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {PceWithoutTemplateService} from "../pceWithoutTemplate/pceWithoutTemplate.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class HighlightingPceService {

  constructor() {
  }

  focusConceptCode = '';
  focusConceptName = '';
  listSummaryAttributeNamesWithout = new Array();
  listSummaryAttributeCodesWithout = new Array();
  listSummaryValueWithout = new Array();
  listSummaryAttributeNamesWith = new Array();
  listSummaryAttributeCodesWith = new Array();
  listSummaryValueWith = new Array();
  isRefinement = false;
  isRoleGroup = false;
  input = '';


  getFocusconcept() {
    let s = "";
    let l1 = this.focusConceptCode.split("+");
    let l2 = this.focusConceptName.split("+");
    for (let i = 0; i < l2.length; i++) {
      s = s + l1[i] + " |" + l2[i] + "|" + " + ";
    }
    s = s.substring(0, s.lastIndexOf(" + "));
    return s;
  }


  checkRG0(rg: number, subrg: number) {
    for (let i = 0; i < this.listSummaryAttributeCodesWith[rg][subrg].length; i++) {
      if (this.listSummaryAttributeCodesWith[rg][subrg] !== undefined) {
        if (this.listSummaryAttributeCodesWith[rg][subrg].length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  checkRG1(rg: number, subrg: number, col: number) {
    if (this.listSummaryAttributeCodesWith[rg][subrg][col] !== undefined && this.listSummaryAttributeNamesWith[rg][subrg][col] !== undefined) {
      if (this.listSummaryValueWith[rg][subrg][col].length > 0) {
        return true;
      }
    }
    return false;
  }

  checkRG2(rg: number, subrg: number, col: number) {
    return (this.listSummaryAttributeNamesWith[rg][subrg].length - 1 > col && this.listSummaryValueWith[rg][subrg][col+1] !== undefined);
  }


  checkRG1Template(rg: number, subrg: number, col: number) {
    if (this.listSummaryAttributeCodesWith[rg][subrg] !== undefined && this.listSummaryAttributeNamesWith[rg][subrg] !== undefined) {
      if (this.listSummaryValueWith[rg][subrg][col].length > 0) {
        return true;
      }
    }
    return false;
  }

  checkRG2Template(rg: number, subrg: number, col: number) {
    return (this.listSummaryAttributeNamesWith[rg].length - 1 > subrg) || (this.listSummaryAttributeNamesWith[rg][subrg].length - 1 > col);
  }

}
