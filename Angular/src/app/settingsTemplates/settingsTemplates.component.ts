import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";


import {AppService} from "../app.service";
import {TemplatesGenerationService} from "../templateGeneration/templatesGeneration.service";
import {SettingsTemplatesService} from "./settingsTemplates.service";
import {SettingsService} from "../settings/settings.service";
import {TemplatesService} from "../templates/templates.service";

@Component({
  selector: 'app-settingstemplates',
  templateUrl: './settingsTemplates.component.html',
  styleUrls: ['./settingsTemplates.css']
})

export class SettingsTemplatesComponent {

  constructor(private http: HttpClient, public appService: AppService, public templateGenerationService: TemplatesGenerationService, public settingsTemplateService: SettingsTemplatesService, public  settingsService: SettingsService, public templateService: TemplatesService) {
  }

  ngOnInit(): void {
    if(this.settingsTemplateService.listUrlServer.length === 0) {
      this.settingsTemplateService.startSettings();
    }
  }



}

