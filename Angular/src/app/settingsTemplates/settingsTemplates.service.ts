import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {lastValueFrom} from 'rxjs';
import {SettingsService} from "../settings/settings.service";
import {TemplatesService} from "../templates/templates.service";
import {AppService} from "../app.service";
import {BackendService} from "../backendService/backend.service";

@Injectable({
  providedIn: 'root'
})
export class SettingsTemplatesService {


  constructor(public backendService: BackendService, public settingsService: SettingsService, private appService: AppService) {
  }

  // private templateService: TemplatesService

  public start = '';
  public isOtherServer = false;
  public syntaxCheck = '';
  // public serverChange = 'https://ontoserver.imi.uni-luebeck.de/fhir';
  public serverChange = '';
  public serverName = '';
  // public url_server_luebeck = 'https://ontoserver.imi.uni-luebeck.de/fhir';
  public url_other_server = '';

  public listFullUrl = new Array();
  public listTitleUrl = new Array();
  public nameCsSupplement = '';
  public newNameCsSupplement = '';
  public newIdCsSupplement = '';

  public listNameServer = new Array();
  public listUrlServer = new Array();


  startSettings() {
    this.appService.uploadPCE = '';
    this.listNameServer = this.settingsService.listNameServer;
    this.listUrlServer = this.settingsService.listUrlServer;
    if(this.serverChange.length === 0) {
      this.serverChange = this.listUrlServer[0];
      this.serverName = this.listNameServer[0];
    }
    this.getAllCsSupplements(this.serverChange).then((response: any) => {
      const value = response.value;
      if(value !== undefined) {
        for (let i = 0; i < value.length; i++) {
          const fullUrl = value[i].url;
          const title = value[i].title;

          this.listTitleUrl.push(title);
          this.listFullUrl.push(fullUrl);
          this.manageChoiceCsS("CsS");
        }
      } else {
        this.manageChoiceCsS("none");
      }
    })
  }

  changeOption() {
    this.newNameCsSupplement = this.nameCsSupplement;
    this.appService.uploadPCE = '';
    this.listTitleUrl = this.listTitleUrl.filter((x: any) => x.length !== 0);
    for (let i = 0; i < this.listTitleUrl.length; i++) {
      if(this.nameCsSupplement === this.listTitleUrl[i]) {
        this.mainUrl = this.listFullUrl[i];
        break;
      }
    }
    this.mainName = this.nameCsSupplement;
    this.newIdCsSupplement = this.mainUrl.split("CodeSystem/")[1];
    this.ready = true;
  }

  resetTitleId() {
    for (let i = 0; i < this.listTitleUrl.length; i++) {
      if(this.nameCsSupplement === this.listTitleUrl[i]) {
        this.mainUrl = this.listFullUrl[i];
        return;
      }
    }
    this.appService.uploadPCE = '';
    this.mainName = '';
    this.newIdCsSupplement = '';
    this.newNameCsSupplement = '';
    this.nameCsSupplement = '';
  }



  getServer(value: string, s?: any, idx?: any) {
    this.appService.uploadPCE = '';
    this.newIdCsSupplement = '';
    this.newNameCsSupplement = '';
    if(value !== 'other') {
      this.isOtherServer = false;
      this.syntaxCheck = '';
      // @ts-ignore
      this.serverChange = this.listUrlServer[idx];
      this.serverName = this.listNameServer[idx];
      this.newNameCsSupplement = this.listNameServer[idx]
      setTimeout(() => {
        let elementRange = document.getElementById('radioOther');
        if (elementRange != null) {
          //@ts-ignore
          elementRange.checked = false;
        }
      }, 10);

    } else {
      this.isOtherServer = true;
      this.serverChange = '';
      this.serverName = '';
      this.listTitleUrl = new Array();
      this.listFullUrl = new Array();
      this.nameCsSupplement = '';
      this.manageChoiceCsS("other");
    }
    this.listTitleUrl = new Array();
    this.listFullUrl = new Array();
    this.getAllCsSupplements(this.serverChange).then((response: any) => {
      const value = response.value;
      if(value !== undefined) {
        for (let i = 0; i < value.length; i++) {
          const fullUrl = value[i].url;
          const title = value[i].title;
          this.listTitleUrl.push(title);
          this.listFullUrl.push(fullUrl);
        }
        this.manageChoiceCsS("CsS");
      } else {
        this.manageChoiceCsS("none");
      }
    })
  }

  manageChoiceCsS(id: String) {
    if(id === 'CsS') {
      setTimeout(() => {
        this.getExistingCs('existing');
        let element = document.getElementById('radioExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = false;
          //@ts-ignore
          element.checked = true;
        }
        element = document.getElementById('inputExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = false;
        }
        element = document.getElementById('radioNew');
        if (element != null) {
          //@ts-ignore
          element.checked = false;
          //@ts-ignore
          element.disabled = false;
        }
      }, 10);
    } else if(id === 'none'){
      setTimeout(() => {
        let element = document.getElementById('radioExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = true;
          //@ts-ignore
          element.checked = false;
        }
        element = document.getElementById('inputExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = true;
          //@ts-ignore
          element.checked = false;
        }
        element = document.getElementById('radioNew');
        if (element != null) {
          //@ts-ignore
          element.checked = true;
          //@ts-ignore
          element.disabled = false;
          this.getExistingCs('new');
        }
      }, 10);
    } else if(id === 'other') {
      setTimeout(() => {
        this.getExistingCs('existing');
        this.listTitleUrl.push('');
        let element = document.getElementById('radioExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = true;
          //@ts-ignore
          element.checked = true;
        }
        element = document.getElementById('inputExisting');
        if (element != null) {
          //@ts-ignore
          element.disabled = true;
          //@ts-ignore
          element.checked = false;
        }
        element = document.getElementById('radioNew');
        if (element != null) {
          //@ts-ignore
          element.disabled = true;
          //@ts-ignore
          element.checked = false;
        }
      }, 100);
    }

  }

  getNames() {
    let list = new Array();
    for(let i = 0; i < this.listTitleUrl.length; i++) {
      if(!list.includes(this.listTitleUrl[i])) {
        list.push(this.listTitleUrl[i])
      }
    }

    return list;
  }

  isInputDisabled(id?: String)  {
    if(id === "existing" && (this.isOtherServer && this.syntaxCheck !== 'success')) {
      return true;
    }
    return false;
  }

  isSaveDisabled() {
    if(this.isOtherServer && this.syntaxCheck !== 'success' && (!this.listTitleUrl.includes(this.nameCsSupplement) || this.nameCsSupplement === '')) {
      return true;
    } else if((this.syntaxCheck === '' || this.syntaxCheck === 'success') && this.listTitleUrl.includes(this.nameCsSupplement)) {
      return false;
    } else {
      return true;
    }
  }


  public mainUrl = '';
  public mainName = '';

  save() {
    this.appService.uploadPCE = '';
    this.listTitleUrl = this.listTitleUrl.filter((x: any) => x.length !== 0);
    for (let i = 0; i < this.listTitleUrl.length; i++) {
      if(this.nameCsSupplement === this.listTitleUrl[i]) {
        this.mainUrl = this.listFullUrl[i];
        break;
      }
    }
    this.mainName = this.nameCsSupplement;
    this.newIdCsSupplement = this.mainUrl.split("CodeSystem/")[1];
    this.ready = true;
  }

  saveNew() {
    this.checkUrl()
    this.mainUrl = this.serverChange + "/CodeSystem/" + this.newIdCsSupplement;
    this.mainName = this.newNameCsSupplement;
  }

  public validatedUrl = '';

  keypressUrl() {
    if(this.validatedUrl.length > 0) {
      if(this.validatedUrl !== this.serverChange) {
        this.syntaxCheck = '';
        this.manageChoiceCsS("other");
      }
    }
  }

  public errorMessageUrl = '';

  checkUrl() {
    this.syntaxCheck = 'check';
    this.checkSyntaxUrl(this.serverChange).then((response: any) => {
      const result = response.result;
      if(result.toString() === 'true') {
        this.errorMessageUrl = '';
        this.ready = true;
        this.syntaxCheck = 'success';
        this.validatedUrl = this.serverChange;
        this.getAllCsSupplements(this.serverChange).then((response: any) => {
          const value = response.value;
          if(value !== undefined) {
            for (let i = 0; i < value.length; i++) {
              const fullUrl = value[i].url;
              const title = value[i].title;

              this.listTitleUrl.push(title);
              this.listFullUrl.push(fullUrl);
            }
            this.manageChoiceCsS("CsS");
          } else {
            this.manageChoiceCsS("none");
          }
        })
      } else {
        this.syntaxCheck = 'error';
        if(result.includes('@')) {
          this.errorMessageUrl = result.split('@')[1];
        } else {
          this.errorMessageUrl = '';
        }
      }
    })
  }

  public id = 'existing';
  public ready = false;


  getExistingCs(id: string) {
    this.newIdCsSupplement = '';
    this.newNameCsSupplement = '';
    this.nameCsSupplement = '';
    this.id = id;
  }


  // Spring boot
  checkSyntaxUrl(url: string) {
    let u = this.encodeURIComponent(url);
    const urlRequest = this.backendService.getBackendUrl() + 'settingsurl/?url=' + u;
    const response = this.backendService.httpClient.get(urlRequest);
    return lastValueFrom(response);
  }

  getAllCsSupplements(urlServer: string) {
    let u = this.encodeURIComponent(urlServer);
    const url = this.backendService.getBackendUrl() + 'getallcodesystemsupplement/?url=' + u;
    const response = this.backendService.httpClient.get(url);
    return lastValueFrom(response);
  }


  encodeURIComponent(value: any): string {
    value += '';
    value = value.trim();
    value = encodeURIComponent(value);
    return value;
  }
}
